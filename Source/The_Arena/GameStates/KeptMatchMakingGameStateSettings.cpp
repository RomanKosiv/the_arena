// Fill out your copyright notice in the Description page of Project Settings.


#include "KeptMatchMakingGameStateSettings.h"
#include "The_Arena/GameStates/The_Arena_MatchMakingGameState.h"

// Sets default values
AKeptMatchMakingGameStateSettings::AKeptMatchMakingGameStateSettings()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AKeptMatchMakingGameStateSettings::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKeptMatchMakingGameStateSettings::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKeptMatchMakingGameStateSettings::CopyParametersFrom(AThe_Arena_MatchMakingGameState* InGameState)
{
	if (!InGameState)
		return;

	WonRounds_Team_1 = InGameState->GetWonRounds_Team_1();
	WonRounds_Team_2 = InGameState->GetWonRounds_Team_2();
	LastWonRoundTeam = InGameState->GetWonRoundTeam();
	
	EndMatchWithoutPlayersTimePassed = InGameState->GetEndMatchWithoutPlayersTimePassed();
}

void AKeptMatchMakingGameStateSettings::CopyParametersTo(AThe_Arena_MatchMakingGameState* InGameState)
{
	if (!InGameState)
		return;

	InGameState->SetWonRounds_Team_1(WonRounds_Team_1);
	InGameState->SetWonRounds_Team_2(WonRounds_Team_2);
	InGameState->SetLastWonRoundTeam(LastWonRoundTeam);
	
	InGameState->SetEndMatchWithoutPlayersTimePassed(EndMatchWithoutPlayersTimePassed);
}

