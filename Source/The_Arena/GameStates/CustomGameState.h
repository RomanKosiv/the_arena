// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"

#include "The_Arena/Actors/InventoryItemObject.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemObjectAssetType.h"
#include "The_Arena/UObjects/Factories/CharacteristicEffectsManager.h"
#include "The_Arena/UObjects/Factories/InventoryItemsManager.h"
#include "The_Arena/UObjects/Factories/DroppedItemsManager.h"
#include "The_Arena/UObjects/Factories/InventoryItemObjectsManager.h"
#include "The_Arena/UObjects/DefaultDelegates.h"

#include "CustomGameState.generated.h"


UCLASS()
class THE_ARENA_API ACustomGameState : public AGameState
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable)
		UInventoryItemObjectsManager* GetInventoryItemObjectsManager() const;
	UFUNCTION(BlueprintCallable)
		UInventoryItemsManager* GetInventoryItemsManager() const;
	UFUNCTION(BlueprintCallable)
		UDroppedItemsManager* GetDroppedItemsManager() const;
	UFUNCTION(BlueprintCallable)
		UCharacteristicEffectsManager* GetCharacteristicEffectsManager() const;

	FDMD& GetInitializationDispatcher();
	
protected:
	void BeginPlay() override;

	void Initialization();
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UInventoryItemObjectsManager> InventoryItemObjectsManagerClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UInventoryItemsManager> InventoryItemsManagerClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UDroppedItemsManager> DroppedItemsManagerClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UCharacteristicEffectsManager> CharacteristicEffectManagerClass;

	UPROPERTY()
	UInventoryItemObjectsManager* InventoryItemObjectsManager;
	UPROPERTY()
	UInventoryItemsManager* InventoryItemsManager;
	UPROPERTY()
	UDroppedItemsManager* DroppedItemsManager;
	UPROPERTY()
	UCharacteristicEffectsManager* CharacteristicEffectsManager;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD InitializationDispatcher;
};
