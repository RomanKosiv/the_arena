// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomGameState.h"

#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"
#include "The_Arena/UObjects/DataAssets/FiringWeaponCommonParams.h"
#include "The_Arena/Actors/PersonalEquipment/AdditionalEquipment.h"
#include "The_Arena/Actors/PersonalEquipment/Outfit.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"


UInventoryItemObjectsManager* ACustomGameState::GetInventoryItemObjectsManager() const
{
	return InventoryItemObjectsManager;
}

UCharacteristicEffectsManager* ACustomGameState::GetCharacteristicEffectsManager() const
{
	return CharacteristicEffectsManager;
}

FDMD& ACustomGameState::GetInitializationDispatcher()
{
	return InitializationDispatcher;
}

UInventoryItemsManager* ACustomGameState::GetInventoryItemsManager() const
{
	return InventoryItemsManager;
}

UDroppedItemsManager* ACustomGameState::GetDroppedItemsManager() const
{
	return DroppedItemsManager;
}

void ACustomGameState::BeginPlay()
{
	Super::BeginPlay();

	Initialization();
}

void ACustomGameState::Initialization()
{
	InventoryItemObjectsManager = NewObject<UInventoryItemObjectsManager>(this, InventoryItemObjectsManagerClass);
	InventoryItemsManager = NewObject<UInventoryItemsManager>(this, InventoryItemsManagerClass);
	DroppedItemsManager = NewObject<UDroppedItemsManager>(this, DroppedItemsManagerClass);
	CharacteristicEffectsManager = NewObject<UCharacteristicEffectsManager>(this, CharacteristicEffectManagerClass);

	InitializationDispatcher.Broadcast();
}
