// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"
#include "The_Arena/UObjects/DefaultDelegates.h"
#include "The_Arena/GameStates/CustomGameState.h"

#include "The_Arena_MatchMakingGameState.generated.h"

/**
 * Please don't try to refactor this class. This class was written by several people in Epic, whose logic is different.
 * Just solve your problems is this class and don't spend time to imagine how to improve this code.
 */
UCLASS()
class THE_ARENA_API AThe_Arena_MatchMakingGameState : public ACustomGameState
{
	GENERATED_BODY()

		//Methods
public:	
	UFUNCTION()
	void AddPlayerStateInTeam(EGameTeam Team, APlayerState* PlayerState);
	UFUNCTION()
	void RemovePlayerStateFromTeam(EGameTeam Team, APlayerState* PlayerState);

	UFUNCTION()
	void AddPlayerStateInAlivePlayersTeam(APlayerState* PlayerState);
	UFUNCTION()
	void RemovePlayerStateFromAlivePlayersTeam(APlayerState* PlayerState);
	UFUNCTION()
	void RemovePlayerStateFromAlivePlayersTeam_Cover(AController* Controller);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		const TArray<APlayerState*> GetTeam_1();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		const TArray<APlayerState*> GetTeam_2();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		const TArray<APlayerState*> GetSpectators();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		const TArray<APlayerState*> GetAlivePlayersInTeam_1();
	UFUNCTION(BlueprintCallable, BlueprintPure)
		const TArray<APlayerState*> GetAlivePlayersInTeam_2();
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetBuyingStateDuration() const;
	UFUNCTION()
		void SetBuyingStateDuration(int Duration);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetBuyingStateTimePassed() const;	

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetRoundDuration() const;
	UFUNCTION()
		void SetRoundDuration(int Duration);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetEndMatchWithoutPlayersDuration() const;
	UFUNCTION()
		void SetEndMatchWithoutPlayersDuration(int Duration);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetEndMatchWithoutPlayersTimePassed() const;
	UFUNCTION()
		void SetEndMatchWithoutPlayersTimePassed(int TimePassed);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetRestartServerDuration() const;
	UFUNCTION()
		void SetRestartServerDuration(int Duration);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetRestartServerTimePassed() const;		

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetWonRounds_Team_1() const;
	UFUNCTION()
		void SetWonRounds_Team_1(int WonRounds);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetWonRounds_Team_2() const;
	UFUNCTION()
		void SetWonRounds_Team_2(int WonRounds);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsGameStarted() const;
	UFUNCTION()
		void SetGameStarted(bool bInGameStarted);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsGameOver() const;
	UFUNCTION()
		void SetGameOver(bool bInGameOver);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		EGameTeam GetWonMatchTeam() const;
	UFUNCTION()
		void SetWonMatchTeam(EGameTeam InWonTeam);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		EGameTeam GetWonRoundTeam() const;
	UFUNCTION()
		void SetWonRoundTeam(EGameTeam InWonTeam);

	UFUNCTION(BlueprintCallable, BlueprintPure)
		EGameTeam GetLastWonRoundTeam() const;
	UFUNCTION()
		void SetLastWonRoundTeam(EGameTeam InWonTeam);

	FDMD& GetWonRounds_Team_1_Dispatcher();
	FDMD& GetWonRounds_Team_2_Dispatcher();
	FDMD& GetBuyingStateTimePassedDispatcher();
	FDMD& GetEndMatchWithoutPlayersTimePassedDispatcher();
	FDMD& GetElapsedTimeDispatcher();
	FDMD& GetRestartServerTimePassedDispatcher();
	FDMD& GetMatchStateDispatcher();
	FDMD& GetAlivePlayersInTeam_1_Dispatcher();
	FDMD& GetAlivePlayersInTeam_2_Dispatcher();
protected:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void DefaultTimer() override;

	void SetBuyingStateTimePassed(int TimePassed);
	void SetRestartServerTimePassed(int TimePassed);
	
	UFUNCTION()
	void OnRep_WonRounds_Team_1();
	UFUNCTION()
	void OnRep_WonRounds_Team_2();
	UFUNCTION()
	void OnRep_BuyingStateTimePassed();
	UFUNCTION()
	void OnRep_EndMatchWithoutPlayersTimePassed();
	UFUNCTION()
	void OnRep_RestartServerTimePassed();
	UFUNCTION()
	void OnRep_PlayerAliveInTeam_1();
	UFUNCTION()
	void OnRep_PlayerAliveInTeam_2();
	void OnRep_ElapsedTime() override;
	void OnRep_MatchState() override;
	
	//Fields
protected:
	UPROPERTY(Replicated)
		TArray<APlayerState*> Team_1;
	UPROPERTY(Replicated)
		TArray<APlayerState*> Team_2;
	UPROPERTY(Replicated)
		TArray<APlayerState*> Spectators;

	UPROPERTY(ReplicatedUsing = OnRep_PlayerAliveInTeam_1)
		TArray<APlayerState*> AlivePlayersInTeam_1;
	UPROPERTY(ReplicatedUsing = OnRep_PlayerAliveInTeam_2)
		TArray<APlayerState*> AlivePlayersInTeam_2;

	UPROPERTY(Replicated)
		int BuyingStateDuration;
	UPROPERTY(ReplicatedUsing = OnRep_BuyingStateTimePassed)
		int BuyingStateTimePassed;
	UPROPERTY(Replicated)
		int RoundDuration;
	UPROPERTY(Replicated)
		int EndMatchWithoutPlayersDuration;
	UPROPERTY(ReplicatedUsing = OnRep_EndMatchWithoutPlayersTimePassed)
		int EndMatchWithoutPlayersTimePassed = 0;
	UPROPERTY(Replicated)
		int RestartServerDuration;
	UPROPERTY(ReplicatedUsing = OnRep_RestartServerTimePassed)
		int RestartServerTimePassed;
	
	UPROPERTY(Replicated)
		bool bGameStarted = false;
	UPROPERTY(Replicated)
		bool bGameOver = false;
	
	UPROPERTY(ReplicatedUsing = OnRep_WonRounds_Team_1)
		int WonRounds_Team_1;
	UPROPERTY(ReplicatedUsing = OnRep_WonRounds_Team_2)
		int WonRounds_Team_2;
	UPROPERTY(Replicated)
	EGameTeam WonMatchTeam;
	UPROPERTY(Replicated)
	EGameTeam WonRoundTeam;
	UPROPERTY(Replicated)
	EGameTeam LastWonRoundTeam;
	
	//Events
	UPROPERTY(BlueprintAssignable)
		FDMD WonRounds_Team_1_Dispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD WonRounds_Team_2_Dispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD BuyingStateTimePassedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD EndMatchWithoutPlayersTimePassedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD ElapsedTimeDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD RestartServerTimePassedDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD MatchStateDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD AlivePlayersInTeam_1_Dispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD AlivePlayersInTeam_2_Dispatcher;
};
