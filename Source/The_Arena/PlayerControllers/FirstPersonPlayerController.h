// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/UObjects/DefaultDelegates.h"
#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"

#include "FirstPersonPlayerController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogFirstPersonPlayerController, Log, All);

UCLASS()
class THE_ARENA_API AFirstPersonPlayerController : public APlayerController
{
	GENERATED_BODY()
	//Methods
public:
	virtual void PostSeamlessTravel() override;
	virtual void NotifyLoadedWorld(FName WorldPackageName, bool bFinalDest) override;
	virtual void SetPawn(APawn* InPawn) override;
	virtual void StartSpectatingOnly() override;
	virtual void ClientSetHUD_Implementation(TSubclassOf<AHUD> NewHUDClass) override;
	virtual void ChangeState(FName NewState) override;
	virtual void PawnLeavingGame() override;
	
	FDMD_Bool& GetHoldingToCrouchDispatcher();
	FDMD_Bool& GetHoldingToAimDispatcher();
	FDMD_Bool& GetHoldingToSlowWalkDispatcher();
	FDMD_Bool& GetHoldingToRunDispatcher();

	FDMD& GetRemoveAllWidgetsDispatcher();
	FDMD& GetHUDChangedDispatcher();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsSelectTeamWidgetInitialized();
	UFUNCTION(BlueprintCallable)
	void SetSelectTeamWidgetInitialized(bool Value);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastSetSelectTeamWidgetInitialized(bool Value);

	UFUNCTION()
	void StartDeathSpectating();
	UFUNCTION(Client, Reliable)
	void ClientStartDeathSpectating();
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerSelectTeam(EGameTeam Team);
	
	UFUNCTION(Client, Reliable, BlueprintCallable)
	void ClientRemoveAllWidgets();
	
	UFUNCTION(Client, Reliable)
	void ClientSpectatorPawnLocationAndRotationInitialization();
	UFUNCTION(Client, Reliable)
	void ClientSetSpectatorLocation(FVector Location);
	UFUNCTION(Client, Reliable)
	void ClientSetSpectatorRotation(FRotator Rotation);

	UFUNCTION(BlueprintCallable)
	bool IsHoldingToCrouch() const;
	UFUNCTION(BlueprintCallable)
	void SetHoldingToCrouch(bool bInHoldingToCrouch);
	
	UFUNCTION(BlueprintCallable)
	bool IsHoldingToAim() const;
	UFUNCTION(BlueprintCallable)
	void SetHoldingToAim(bool bInHoldingToAim);

	UFUNCTION(BlueprintCallable)
	bool IsHoldingToSlowWalk() const;
	UFUNCTION(BlueprintCallable)
	void SetHoldingToSlowWalk(bool bInHoldingToSlowWalk);

	UFUNCTION(BlueprintCallable)
	bool IsHoldingToRun() const;
	UFUNCTION(BlueprintCallable)
	void SetHoldingToRun(bool bInHoldingToRun);
	
protected:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginSpectatingState() override;
	ASpectatorPawn* SpawnDeathSpectatorPawn();
	UFUNCTION(Client, Reliable)
	void ClientSetDeathSpectatingActivated(bool bActivated);

	UFUNCTION(Server, Reliable)
	void ServerSetHoldingToCrouch(bool bInHoldingToCrouch);
	UFUNCTION(Server, Reliable)
	void ServerSetHoldingToAim(bool bInHoldingToAim);
	UFUNCTION(Server, Reliable)
	void ServerSetHoldingToSlowWalk(bool bInHoldingToSlowWalk);
	UFUNCTION(Server, Reliable)
	void ServerSetHoldingToRun(bool bInHoldingToRun);

	UFUNCTION(BlueprintNativeEvent)
	void OnRemoveAllWidgets();
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	bool bDestroyPawnAfterLeavingGame = false;
	
	UPROPERTY(Replicated)
	bool bHoldingToCrouch = false;
	UPROPERTY(Replicated)
	bool bHoldingToAim = true;
	UPROPERTY(Replicated)
	bool bHoldingToSlowWalk = true;
	UPROPERTY(Replicated)
	bool bHoldingToRun = true;

	bool bDeathSpectatingActivated = false;
	bool bSelectTeamWidgetInitialized = false;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASpectatorPawn> DeathSpectatorPawnClass;
	
	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool HoldingToCrouchDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool HoldingToAimDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool HoldingToSlowWalkDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD_Bool HoldingToRunDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD RemoveAllWidgetsDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD HUDChangedDispatcher;
};