// Fill out your copyright notice in the Description page of Project Settings.


#include "KeptFPControllerSettings.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/EquipmentComponent.h"
#include "The_Arena/ActorComponents/MoneyComponent.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectsManager.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"
#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"


void AKeptFPControllerSettings::LoadPlayerData()
{
	if (!Controller || !bDataSaved)
		return;

	auto World = GetWorld();
	ACustomGameState* ArenaGameState = nullptr;
	UInventoryItemObjectsManager* InventoryItemObjectsManager = nullptr;
	UDroppedItemsManager* DroppedItemsManager = nullptr;

	if(World)
	{
		ArenaGameState = Cast<ACustomGameState>(World->GetGameState());
	}

	if(ArenaGameState)
	{
		InventoryItemObjectsManager = ArenaGameState->GetInventoryItemObjectsManager();
		DroppedItemsManager = ArenaGameState->GetDroppedItemsManager();
	}

	if(!InventoryItemObjectsManager || !DroppedItemsManager)
	{
		return;
	}
	
	if (auto Pawn = Controller->GetPawn())
	{		
		if(!bInventoryDataLoaded)
		{
			if (auto InventoryComponent = Pawn->FindComponentByClass<UCustomInventoryComponent>())
			{
				if(InventoryComponent->HasBegunPlay())
				{
					for (int i = 0; i < KeptInventoryParams.Num(); ++i)
					{
						auto KeptInventoryParam = KeptInventoryParams[i];

						if (auto InventoryItemParam = NewObject<UInventoryItemParam>())
						{
							InventoryItemParam->ItemAmount = KeptInventoryParam.Amount;

							for (int j = 0; j < KeptInventoryParam.ModuleItemObjectNames.Num(); ++j)
							{
								auto ModuleItemObjectName = KeptInventoryParam.ModuleItemObjectNames[j];
								
								FActorSpawnParameters SpawnParameters;
								SpawnParameters.Owner = Pawn;

								if (auto ModuleItemObject = InventoryItemObjectsManager->CreateInventoryItemObject(ModuleItemObjectName, SpawnParameters))
								{
									InventoryItemParam->ModuleItemObjects.Add(ModuleItemObject);
								}		
							}

							FActorSpawnParameters SpawnParameters;
							SpawnParameters.Owner = Pawn;

							if (auto ItemObject = InventoryItemObjectsManager->CreateInventoryItemObject(KeptInventoryParam.ItemObjectName, SpawnParameters, InventoryItemParam))
							{
								if (!InventoryComponent->TryAddItem(ItemObject, true))
								{
									if (DroppedItemsManager)
									{
										DroppedItemsManager->SpawnInventoryItemInFrontOfTargetActor(ItemObject, Pawn);
										ItemObject->Destroy();
									}
								}
							}
						}
					}

					bInventoryDataLoaded = true;
				}	
			}
		}

		if(!bEquipmentDataLoaded)
		{
			if (auto EquipmentComponent = Pawn->FindComponentByClass<UEquipmentComponent>())
			{
				if(EquipmentComponent->HasBegunPlay())
				{
					for (int i = 0; i < KeptEquipmentParams.Num(); ++i)
					{
						auto KeptEquipmentParam = KeptEquipmentParams[i];

						if (auto InventoryItemParam = NewObject<UInventoryItemParam>())
						{
							InventoryItemParam->ItemAmount = KeptEquipmentParam.Amount;

							for (int j = 0; j < KeptEquipmentParam.ModuleItemObjectNames.Num(); ++j)
							{
								auto ModuleItemObjectName = KeptEquipmentParam.ModuleItemObjectNames[j];

								FActorSpawnParameters SpawnParameters;
								SpawnParameters.Owner = Pawn;

								if (auto ModuleItemObject = InventoryItemObjectsManager->CreateInventoryItemObject(ModuleItemObjectName, SpawnParameters))
								{
									InventoryItemParam->ModuleItemObjects.Add(ModuleItemObject);
								}
							}

							FActorSpawnParameters SpawnParameters;
							SpawnParameters.Owner = Pawn;

							if (auto ItemObject = InventoryItemObjectsManager->CreateInventoryItemObject(KeptEquipmentParam.ItemObjectName, SpawnParameters, InventoryItemParam))
							{
								EquipmentComponent->CreateEquipment(ItemObject, KeptEquipmentParam.EquipmentType, true);
							}
						}
					}

					bEquipmentDataLoaded = true;
				}
			}
		}

		if(!bMoneyDataLoaded)
		{
			if (auto MoneyComponent = Pawn->FindComponentByClass<UMoneyComponent>())
			{
				if (MoneyComponent->HasBegunPlay())
				{
					MoneyComponent->SetMoney(MoneyComponent->GetMoney() + Money);
					bMoneyDataLoaded = true;
				}
			}
		}
		
		if(bInventoryDataLoaded && bEquipmentDataLoaded && bMoneyDataLoaded)
		{
			bDataLoaded = true;
		}
	}	
}

void AKeptFPControllerSettings::SetPlayerController(APlayerController* PlayerController)
{
	Controller = PlayerController;
}

APlayerController* AKeptFPControllerSettings::GetPlayerController() const
{
	return Controller;
}

bool AKeptFPControllerSettings::IsDataLoaded() const
{
	return bDataLoaded;
}

// Sets default values
AKeptFPControllerSettings::AKeptFPControllerSettings()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AKeptFPControllerSettings::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKeptFPControllerSettings::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKeptFPControllerSettings::SavePlayerData()
{
	auto SaveEquipmentLambda = [&](FEquipment Equipment, EEquipmentType EquipmentType)
	{
		if (auto ItemObject = Equipment.InventoryItemObject)
		{
			if(auto InventoryCommonParams = ItemObject->GetInventoryItemCommonParams())
			{
				FKeptEquipmentParams ItemParams;
				
				ItemParams.ItemObjectName = *InventoryCommonParams->GetItemName();
				ItemParams.Amount = ItemObject->GetCurrentItemAmount();
				ItemParams.EquipmentType = EquipmentType;

				if (auto FiringWeapon = Cast<ARifleBase>(Equipment.InventoryItem))
				{
					auto BulletsInMagazine = FiringWeapon->GetCurrentBulletsInMagazine();

					if (BulletsInMagazine > 0)
					{
						if(auto AmmoTypeClass = FiringWeapon->GetAmmoTypeClass())
						{
							if(auto AmmoDefaultObject = AmmoTypeClass.GetDefaultObject())
							{
								if(auto AmmoItemCommonParams = AmmoDefaultObject->GetInventoryItemCommonParams())
								{
									FKeptInventoryParams AmmoParams;
									AmmoParams.ItemObjectName = *AmmoItemCommonParams->GetItemName();
									AmmoParams.Amount = BulletsInMagazine;

									KeptInventoryParams.Add(AmmoParams);
								}
							}
						}
					}
				}

				if (auto ModuleItemObjectsManager = ItemObject->FindComponentByClass<UModuleItemObjectsManager>())
				{
					auto AttachedModules = ModuleItemObjectsManager->GetModuleItemObjects();

					for (int i = 0; i < AttachedModules.Num(); ++i)
					{
						if (auto AttachedModule = AttachedModules[i])
						{
							if (auto ModuleInventoryCommonParams = AttachedModule->GetInventoryItemCommonParams())
							{
								ItemParams.ModuleItemObjectNames.Add(*ModuleInventoryCommonParams->GetItemName());
							}
						}
					}
				}

				KeptEquipmentParams.Add(ItemParams);
			}	
		}	
	};

	if (!Controller || bDataSaved)
		return;

	if (auto FPCharacter = Cast<AFirstPersonCharacter>(Controller->GetPawn()))
	{
		if(FPCharacter->IsAlive())
		{
			if (auto InventoryComponent = FPCharacter->FindComponentByClass<UCustomInventoryComponent>())
			{
				auto AllItems = InventoryComponent->GetAllItems();

				for (auto Iter = AllItems.begin(); Iter != AllItems.end(); ++Iter)
				{
					if (auto ItemObject = Iter->Key)
					{
						if (auto InventoryCommonParams = ItemObject->GetInventoryItemCommonParams())
						{
							FKeptInventoryParams ItemParams;

							ItemParams.ItemObjectName = *InventoryCommonParams->GetItemName();
							ItemParams.Amount = ItemObject->GetCurrentItemAmount();

							if (auto ModuleItemObjectsManager = ItemObject->FindComponentByClass<UModuleItemObjectsManager>())
							{
								auto AttachedModules = ModuleItemObjectsManager->GetModuleItemObjects();

								for (int i = 0; i < AttachedModules.Num(); ++i)
								{
									if (auto AttachedModule = AttachedModules[i])
									{
										if (auto ModuleInventoryCommonParams = AttachedModule->GetInventoryItemCommonParams())
										{
											ItemParams.ModuleItemObjectNames.Add(*ModuleInventoryCommonParams->GetItemName());
										}
									}
								}
							}

							KeptInventoryParams.Add(ItemParams);
						}
					}
				}
			}

			if (auto EquipmentComponent = FPCharacter->FindComponentByClass<UEquipmentComponent>())
			{
				auto Outfit = EquipmentComponent->GetEquipment(EEquipmentType::Outfit);
				SaveEquipmentLambda(Outfit, EEquipmentType::Outfit);

				auto AdditionalEquipment = EquipmentComponent->GetEquipment(EEquipmentType::AdditionalEquipment);
				SaveEquipmentLambda(AdditionalEquipment, EEquipmentType::AdditionalEquipment);

				auto FirstFiringWeapon = EquipmentComponent->GetEquipment(EEquipmentType::FirstFiringWeapon);
				SaveEquipmentLambda(FirstFiringWeapon, EEquipmentType::FirstFiringWeapon);

				auto SecondFiringWeapon = EquipmentComponent->GetEquipment(EEquipmentType::SecondFiringWeapon);
				SaveEquipmentLambda(SecondFiringWeapon, EEquipmentType::SecondFiringWeapon);

				auto Pistol = EquipmentComponent->GetEquipment(EEquipmentType::Pistol);
				SaveEquipmentLambda(Pistol, EEquipmentType::Pistol);

				auto Melee = EquipmentComponent->GetEquipment(EEquipmentType::Melee);
				SaveEquipmentLambda(Melee, EEquipmentType::Melee);
			}
		}

		if (auto MoneyComponent = FPCharacter->FindComponentByClass<UMoneyComponent>())
		{
			Money = MoneyComponent->GetMoney();
		}

		bDataSaved = true;
	}
}