// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "FirstPersonHUD.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AFirstPersonHUD : public AHUD
{
	GENERATED_BODY()

	//Methods
protected:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	//Fields
protected:
	UPROPERTY(BlueprintReadWrite)
		UUserWidget* SelectTeamWidget;
};
