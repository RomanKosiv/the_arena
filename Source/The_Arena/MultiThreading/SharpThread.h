// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <cstdarg>
#include <tuple>
#include "Runtime/Core/Public/HAL/Runnable.h"

/**
 * 
 */
template<class CurrentClass, class... Args>
class SharpThread : public FRunnable
{

	///////////////Methods/////////////
public:
	SharpThread(CurrentClass* currentObject, void (CurrentClass::*function)(Args... arg))
	{
		p_Function = function;
		pCurrentObject = currentObject;
	}
	~SharpThread()
	{
		delete pThread;
		pThread = nullptr;
	}

	virtual bool Init() override
	{
		return true;
	}

	template <size_t... Is>
	void TrueRun(std::index_sequence<Is...>)
	{
		(pCurrentObject->*(p_Function))(std::get<Is>(m_arguments)...);
	}

	virtual uint32 Run() override
	{
		TrueRun(std::make_index_sequence<sizeof...(Args)>());
		return 0;
	}


	virtual void Stop() override
	{
	}

	void Start(Args... args)
	{
		m_arguments = std::make_tuple(args...);
		pThread = FRunnableThread::Create(this, TEXT("FThreadWorker"), 0, TPri_BelowNormal);
	}

private:
	void (CurrentClass::*p_Function)(Args... arg);

	///////////////Fields/////////////
private:
	FRunnableThread* pThread = nullptr;
	CurrentClass* pCurrentObject;

	using TupleType = std::tuple<Args...>;
	TupleType m_arguments;
};