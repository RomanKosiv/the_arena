// Fill out your copyright notice in the Description page of Project Settings.


#include "FiringWeaponCommonParams.h"

TEnumAsByte<EFiringWeaponType> UFiringWeaponCommonParams::GetFiringWeaponType() const
{
	return FiringWeaponType;
}
