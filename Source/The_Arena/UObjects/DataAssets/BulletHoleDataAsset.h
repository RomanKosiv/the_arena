// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Materials/Material.h"
#include "Components/DecalComponent.h"

#include "BulletHoleDataAsset.generated.h"


USTRUCT(BlueprintType)
struct FDecalParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
		UMaterialInterface* Material;

	UPROPERTY(EditDefaultsOnly)
		bool UseDefaultSize = true;

	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "!UseDefaultSize"))
		FVector DecalSize;
};

USTRUCT(BlueprintType)
struct FBulletHoleParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
		TEnumAsByte<EPhysicalSurface> Surface;
	UPROPERTY(EditDefaultsOnly)
		TArray<FDecalParam> Decals;
	UPROPERTY(EditDefaultsOnly)
		TArray<USoundBase*> Sounds;
	UPROPERTY(EditDefaultsOnly)
		TArray<UParticleSystem*> Particles;
};

UCLASS()
class THE_ARENA_API UBulletHoleDataAsset : public UDataAsset
{
	GENERATED_BODY()

	//Fields	
public:
	UPROPERTY(EditDefaultsOnly)
		FVector DefaultDecalSize;

	UPROPERTY(EditDefaultsOnly)
		int MaxBulletHolesAmmount;

	UPROPERTY(EditDefaultsOnly)
		float BulletHoleScale_X;

	UPROPERTY(EditDefaultsOnly)
		TArray<FBulletHoleParam> BulletHoles;
};
