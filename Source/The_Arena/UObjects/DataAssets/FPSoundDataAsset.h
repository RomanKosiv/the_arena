// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"

#include "FPSoundDataAsset.generated.h"


USTRUCT(BlueprintType)
struct FFootstepSoundParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
	TEnumAsByte<EPhysicalSurface> Surface;
	UPROPERTY(EditDefaultsOnly)
	TArray<USoundBase*> Sounds;
};

UCLASS()
class THE_ARENA_API UFPSoundDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		TArray<FFootstepSoundParam> StepSounds;
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		TArray<FFootstepSoundParam> JumpSounds;
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		float CrouchVolumeMultiplier = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		float RemoteVolumeMultiplier = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		float WalkSoundFrequency = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		float RunSoundFrequency = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		float SlowWalkSoundFrequency = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "SoundParametrs")
		float CrouchSoundFrequency = 0.0f;
};