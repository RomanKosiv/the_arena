// Fill out your copyright notice in the Description page of Project Settings.


#include "PriorityWithOneEqualParamAsset.h"


int UPriorityWithOneEqualParamAsset::GetPriority(FString Name) const
{
	auto Priority = Priorities.FindByPredicate([&](const FPriorityParam& Param)
	{
		return Param.Name == Name;
	});

	if(Priority)
		return Priority->Priority;

	UE_LOG(LogTemp, Warning, TEXT("Priority \"%s\" not found."), *Name);
	return -1;
}