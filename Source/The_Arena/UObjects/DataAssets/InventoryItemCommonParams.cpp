// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemCommonParams.h"

FString UInventoryItemCommonParams::GetItemName() const
{
	return ItemName;
}

TSubclassOf<ADroppedItem> UInventoryItemCommonParams::GetClassOfDroppedItem() const
{
	return ClassOfDroppedItem;
}

TSubclassOf<AInventoryItem> UInventoryItemCommonParams::GetClassOfInventoryItem() const
{
	return ClassOfInventoryItem;
}

TSubclassOf<AInventoryItemObject> UInventoryItemCommonParams::GetClassOfInventoryItemObject() const
{
	return ClassOfInventoryItemObject;
}
