// Fill out your copyright notice in the Description page of Project Settings.


#include "JumpPriorityDataAsset.h"

bool operator==(const FJumpPriorityParam& Pr1, const FJumpPriorityParam& Pr2)
{
	if (Pr1.Ability == Pr2.Ability && Pr1.Priority == Pr2.Priority && Pr1.Name == Pr2.Name)
		return true;

	return false;
}


bool operator<(const FJumpPriorityParam& Pr1, const FJumpPriorityParam& Pr2)
{
	if (Pr1.Priority > Pr2.Priority)
		return true;

	return false;
}

FJumpPriorityParam UJumpPriorityDataAsset::GetPriority(FString Name) const
{
	auto FoundPriority = Priorities.FindByPredicate([Name](const FJumpPriorityParam& Param)
		{
			return Param.Name == Name;
		});

	FJumpPriorityParam FinalPriority;
	FinalPriority.Ability = EAbility::NONE;
	FinalPriority.Name = "None";
	FinalPriority.Priority = 0;

	if(FoundPriority)
	{
		FinalPriority = *FoundPriority;
	}
	
	return FinalPriority;
}
