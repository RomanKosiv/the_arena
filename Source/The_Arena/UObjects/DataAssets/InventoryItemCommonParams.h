// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"

#include "The_Arena/Actors/DroppedItem.h"
#include "The_Arena/Actors/InventoryItem.h"
#include "The_Arena/Actors/InventoryItemObject.h"

#include "InventoryItemCommonParams.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class THE_ARENA_API UInventoryItemCommonParams : public UDataAsset
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FString GetItemName() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TSubclassOf<ADroppedItem> GetClassOfDroppedItem() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TSubclassOf<AInventoryItem> GetClassOfInventoryItem() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TSubclassOf<AInventoryItemObject> GetClassOfInventoryItemObject() const;
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Category = "ItemCommonParametrs")
	FString ItemName;
	UPROPERTY(EditDefaultsOnly, Category = "ItemCommonParametrs")
	TSubclassOf<ADroppedItem> ClassOfDroppedItem;
	UPROPERTY(EditDefaultsOnly, Category = "ItemCommonParametrs")
	TSubclassOf<AInventoryItem> ClassOfInventoryItem;
	UPROPERTY(EditDefaultsOnly, Category = "ItemCommonParametrs")
	TSubclassOf<AInventoryItemObject> ClassOfInventoryItemObject;
};
