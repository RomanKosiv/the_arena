// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "InventoryItemParam.generated.h"

class AInventoryItemObject;

UCLASS()
class THE_ARENA_API UInventoryItemParam : public UObject
{
	GENERATED_BODY()

	//Methods
public:
	virtual void CopyTo(UInventoryItemParam* InventoryItemParam) const;
	
	//Fields
public:
	int ItemAmount;
	TArray<AInventoryItemObject*> ModuleItemObjects;
};

