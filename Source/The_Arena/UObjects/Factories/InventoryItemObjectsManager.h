// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "The_Arena/Actors/InventoryItemObject.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemObjectAssetType.h"

#include "InventoryItemObjectsManager.generated.h"



UCLASS(Blueprintable)
class THE_ARENA_API UInventoryItemObjectsManager : public UObject
{
	GENERATED_BODY()

	//Methods
public:
	AInventoryItemObject* CreateInventoryItemObject(FName ItemObjectName, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters(), UInventoryItemParam* ItemParam = nullptr);
	AInventoryItemObject* CreateInventoryItemObject(UClass* ItemObjectClass, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters(), UInventoryItemParam* ItemParam = nullptr);

	UFUNCTION(BlueprintCallable)
		UInventoryItemObjectAssetType* GetInventoryItemObjectAsset() const;

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	UInventoryItemObjectAssetType* InventoryItemObjectAsset;
};
