// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"

#include "The_Arena/ActorComponents/Effects/CharacteristicEffectBase.h"
#include "The_Arena/ActorComponents/Effects/SmoothStopStatEffect.h"

#include "CharacteristicEffectsManager.generated.h"


class UStatBaseComponent;
class UStandartEffectOnStat;
class UStopCharacteristicEffect;
class UAccuringEffectComponent;

USTRUCT(BlueprintType)
struct FEffectDataStruct : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UCharacteristicEffectBase> EffectClass;
};
	
UCLASS(Blueprintable)
class THE_ARENA_API UCharacteristicEffectsManager : public UObject
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable)
	TSubclassOf<UCharacteristicEffectBase> FindCharacteristicEffectClass(FName EffectName);

	UFUNCTION(BlueprintCallable)
		UStandartEffectOnStat* CreateStandartEffectOnStat(AActor* Owner, float AmountEffectPerSec, TSubclassOf<UStatBaseComponent> StatComponentClass, FCharacteristicEffectParams EffectParams);
	UFUNCTION(BlueprintCallable)
		UStopCharacteristicEffect* CreateStopCharacteristicEffect(AActor* Owner, TSubclassOf<UCharacteristicEffectBase> CharacteristicEffectClass, FCharacteristicEffectParams EffectParams);
	UFUNCTION(BlueprintCallable)
		UAccuringEffectComponent* CreateAccuringEffect(AActor* Owner, float MaxAmountEffectPerSec, float AmountEffectPerSec, TSubclassOf<UStatBaseComponent> StatComponentClass, FCharacteristicEffectParams EffectParams);
	UFUNCTION(BlueprintCallable)
		USmoothStopStatEffect* CreateSmoothStopStatEffect(AActor* Owner, TSubclassOf<UStatEffectBaseComponent> StatEffectComponentClass, float AmountEffectPerSec, FCharacteristicEffectParams EffectParams);
	
protected:
	FEffectDataStruct* FindEffectTableRow(FName EffectName);

	template<class T>
	T* CreateEffect(UObject* Outer, const UClass* Class);
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	UDataTable* EffectsDataTable;
};

template <class T>
T* UCharacteristicEffectsManager::CreateEffect(UObject* Outer, const UClass* Class)
{
	T* EffectComponent = NewObject<T>(Outer, Class);

	if(EffectComponent)
	{
		EffectComponent->RegisterComponent();
		EffectComponent->SetIsReplicated(true);	
	}

	return EffectComponent;
}

