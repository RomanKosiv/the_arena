// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacteristicEffectsManager.h"

#include "The_Arena/ActorComponents/Effects/StandartEffectOnStat.h"
#include "The_Arena/ActorComponents/Effects/StopCharacteristicEffect.h"
#include "The_Arena/ActorComponents/Effects/AccuringEffectComponent.h"


TSubclassOf<UCharacteristicEffectBase> UCharacteristicEffectsManager::FindCharacteristicEffectClass(FName EffectName)
{
	if(!EffectsDataTable)
		return nullptr;

	auto EffectTableRow = FindEffectTableRow(EffectName);

	if(EffectTableRow)
	{
		return EffectTableRow->EffectClass;
	}
	
	return nullptr;
}

UStandartEffectOnStat* UCharacteristicEffectsManager::CreateStandartEffectOnStat(AActor* Owner, float AmountEffectPerSec, TSubclassOf<UStatBaseComponent> StatComponentClass, FCharacteristicEffectParams EffectParams)
{
	if(!Owner)
		return nullptr;
	
	auto EffectTableRow = FindEffectTableRow(EffectParams.EffectName);

	if(EffectTableRow)
	{
		auto StandartEffectOnStat = CreateEffect<UStandartEffectOnStat>(Owner, EffectTableRow->EffectClass);

		if(StandartEffectOnStat)
		{
			StandartEffectOnStat->StandartEffectBaseOnStatInitialization(AmountEffectPerSec, StatComponentClass, EffectParams);
			StandartEffectOnStat->CreateEffect();

			return StandartEffectOnStat;
		}
	}

	return nullptr;
}

UStopCharacteristicEffect* UCharacteristicEffectsManager::CreateStopCharacteristicEffect(AActor* Owner, TSubclassOf<UCharacteristicEffectBase> CharacteristicEffectClass, FCharacteristicEffectParams EffectParams)
{
	if (!Owner)
		return nullptr;
	
	auto EffectTableRow = FindEffectTableRow(EffectParams.EffectName);

	if (EffectTableRow)
	{
		auto StopCharacteristicEffect = CreateEffect<UStopCharacteristicEffect>(Owner, EffectTableRow->EffectClass);

		if (StopCharacteristicEffect)
		{		
			StopCharacteristicEffect->StopCharacteristicEffectInitialization(CharacteristicEffectClass, EffectParams);
			StopCharacteristicEffect->CreateEffect();

			return StopCharacteristicEffect;	
		}
	}

	return nullptr;
}

UAccuringEffectComponent* UCharacteristicEffectsManager::CreateAccuringEffect(AActor* Owner,
	float MaxAmountEffectPerSec, float AmountEffectPerSec, TSubclassOf<UStatBaseComponent> StatComponentClass,
	FCharacteristicEffectParams EffectParams)
{
	if (!Owner)
		return nullptr;

	auto EffectTableRow = FindEffectTableRow(EffectParams.EffectName);

	if (EffectTableRow)
	{
		auto AccuringEffect = CreateEffect<UAccuringEffectComponent>(Owner, EffectTableRow->EffectClass);

		if (AccuringEffect)
		{
			AccuringEffect->AccuringEffectInitialization(MaxAmountEffectPerSec, AmountEffectPerSec, StatComponentClass, EffectParams);
			AccuringEffect->CreateEffect();

			return AccuringEffect;
		}
	}

	return nullptr;
}

USmoothStopStatEffect* UCharacteristicEffectsManager::CreateSmoothStopStatEffect(AActor* Owner,
	TSubclassOf<UStatEffectBaseComponent> StatEffectComponentClass, float AmountEffectPerSec, FCharacteristicEffectParams EffectParams)
{
	if (!Owner)
		return nullptr;

	auto EffectTableRow = FindEffectTableRow(EffectParams.EffectName);

	if (EffectTableRow)
	{
		auto SmoothStopStatEffect = CreateEffect<USmoothStopStatEffect>(Owner, EffectTableRow->EffectClass);

		if (SmoothStopStatEffect)
		{
			SmoothStopStatEffect->SmoothStopStatEffectInitialization(StatEffectComponentClass, AmountEffectPerSec, EffectParams);
			SmoothStopStatEffect->CreateEffect();

			return SmoothStopStatEffect;
		}
	}

	return nullptr;
}

FEffectDataStruct* UCharacteristicEffectsManager::FindEffectTableRow(FName EffectName)
{
	if(!EffectsDataTable)
		return nullptr;
	
	static const FString ContextString(TEXT("EffectsFactory"));
	auto EffectTableRow = EffectsDataTable->FindRow<FEffectDataStruct>(EffectName, ContextString);

	return EffectTableRow;
}

