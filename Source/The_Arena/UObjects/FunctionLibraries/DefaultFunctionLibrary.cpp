// Fill out your copyright notice in the Description page of Project Settings.


#include "DefaultFunctionLibrary.h"
#include "Misc/App.h"

#include "The_Arena/ActorComponents/HealthComponent.h"
#include "The_Arena/Actors/InventoryItemObject.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/TraderInventoryComponent.h"
#include "The_Arena/Actors/NPC.h"
#include "The_Arena/Actors/TraderBasketHolder.h"


EBuildConfigurationBP UDefaultFunctionLibrary::GetConfigurationBuild()
{
	EBuildConfiguration Configuration = FApp::GetBuildConfiguration();

	switch (Configuration)
	{
		case EBuildConfiguration::Debug:
			return EBuildConfigurationBP::Debug;
		case EBuildConfiguration::DebugGame:
			return EBuildConfigurationBP::DebugGame;
		case EBuildConfiguration::Development:
			return EBuildConfigurationBP::Development;
		case EBuildConfiguration::Shipping:
			return EBuildConfigurationBP::Shipping;
		case EBuildConfiguration::Test:
			return EBuildConfigurationBP::Test;
		default:
			return EBuildConfigurationBP::Unknown;
	}
}

FString UDefaultFunctionLibrary::GetProjectVersion()
{
	FString ProjectVersion;

	if(GConfig)
	{
		GConfig->GetString(TEXT("/Script/EngineSettings.GeneralProjectSettings"), TEXT("ProjectVersion"), ProjectVersion, GGameIni);
	}

	return ProjectVersion;
}

bool UDefaultFunctionLibrary::CanItemObjectUsed(AActor* UsedActor, AInventoryItemObject* ItemObject)
{
	if (!UsedActor || !ItemObject)
		return false;
	
	if(Cast<ANPC>(ItemObject->GetOwner()) || Cast<ATraderBasketHolder>(ItemObject->GetOwner()))
	{
		return false;
	}

	return CanItemObjectUsedDefault(UsedActor, ItemObject);
}

bool UDefaultFunctionLibrary::CanModuleAttached(AActor* UsedActor, AInventoryItemObject* ModulableItemObject, UInventoryBaseComponent* ModulableItemObjectInventory,
	AInventoryItemObject* ModuleItemObject, UInventoryBaseComponent* ModuleItemObjectInventory)
{
	if (!UsedActor || !ModulableItemObject || !ModulableItemObjectInventory || !ModuleItemObject || !ModuleItemObjectInventory)
		return false;

	if(Cast<ANPC>(ModulableItemObject->GetOwner()) || Cast<ATraderBasketHolder>(ModulableItemObject->GetOwner()))
	{
		if(Cast<UTraderBasketComponent>(ModulableItemObjectInventory) && 
			(Cast<UTraderBasketComponent>(ModuleItemObjectInventory) || Cast<UTraderInventoryComponent>(ModuleItemObjectInventory)))
		{
			if (CanItemObjectUsedDefault(UsedActor, ModulableItemObject))
			{
				return CanItemObjectUsedDefault(UsedActor, ModuleItemObject);
			}
		}
	}
	else
	{
		if(!Cast<UTraderBasketComponent>(ModuleItemObjectInventory) && !Cast<UTraderInventoryComponent>(ModuleItemObjectInventory))
		{
			if (CanItemObjectUsedDefault(UsedActor, ModulableItemObject))
			{
				return CanItemObjectUsedDefault(UsedActor, ModuleItemObject);
			}
		}	
	}

	return false;
}

bool UDefaultFunctionLibrary::CanModuleDetached(AActor* UsedActor, AInventoryItemObject* ModulableItemObject)
{
	return CanItemObjectUsedDefault(UsedActor, ModulableItemObject);
}

void UDefaultFunctionLibrary::DecrementItemAmount(AActor* UsedActor, AInventoryItemObject* ItemObject)
{
	if (!UsedActor || !ItemObject)
		return;

	if(ItemObject->GetCurrentItemAmount() > 1)
	{
		ItemObject->SetCurrentItemAmount(ItemObject->GetCurrentItemAmount() - 1);
	}
	else
	{
		if(auto ItemObjectOwner = ItemObject->GetOwner())
		{
			if(auto CustomInventoryComponent = ItemObjectOwner->FindComponentByClass<UCustomInventoryComponent>())
			{
				CustomInventoryComponent->RemoveItem(ItemObject, ERemoveItemOption::WithoutSave);
			}
		}
	}
}

bool UDefaultFunctionLibrary::CanItemObjectUsedDefault(AActor* UsedActor, AInventoryItemObject* ItemObject)
{
	if (auto ItemObjectOwner = ItemObject->GetOwner())
	{
		auto HealthComponent = ItemObjectOwner->FindComponentByClass<UHealthComponent>();

		if (HealthComponent)
		{
			if (ItemObjectOwner == UsedActor)
			{
				if (!HealthComponent->IsAlive())
				{
					return false;
				}
			}
			else
			{
				if (HealthComponent->IsAlive())
				{
					return false;
				}
			}
		}

		return true;
	}

	return false;
}

