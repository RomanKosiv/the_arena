// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/Actors/InventoryItemObject.h"

#include "OutfitItemObject.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AOutfitItemObject : public AInventoryItemObject
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetPhysicalProtection() const;
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Category = "Attributes", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
	float PhysicalProtection;
};
