// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "The_Arena/ActorComponents/TransformController.h"
#include "The_Arena/ActorComponents/WeaponClippingComponent.h"
#include "The_Arena/ActorComponents/WeaponRecoilComponent.h"
#include "The_Arena/ActorComponents/WeaponSpreadComponent.h"
#include "The_Arena/ActorComponents/BulletHoleComponent.h"
#include "The_Arena/Actors/Weapons/Weapon.h"
#include "The_Arena/Actors/Weapons/AmmoItemObject.h"

#include "RifleBase.generated.h"


UENUM(BlueprintType)
enum EReloadingType
{
	Magazine, Cartridge
};

#define AUTOFIRE 0
#define SINGLEFIRE 1
#define RANGEFIRE 2

UCLASS(Abstract)
class THE_ARENA_API ARifleBase : public AWeapon
{
	GENERATED_BODY()

		////////////////////////////////////////Methods/////////////////////////////////////////////
public:
	// Sets default values for this actor's properties
	ARifleBase();

	FDMD_Bool& GetFiringDispatcher();
	FDMD_Bool& GetAimingDispatcher();
	FDMD_Bool& GetReloadingDispatcher();
	FDMD_Bool& GetFullReloadingDispatcher();
	FDMD_Bool& GetChangingFireDispatcher();
	FDMD& GetCurrentBulletsInMagazineDispatcher();

	void MulticastInitialization_Implementation(bool bInTestItem) override;

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetMaxBulletsInMagazine() const;
	UFUNCTION(BlueprintCallable)
		void SetMaxBulletsInMagazine(int Bullets);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetCurrentBulletsInMagazine() const;
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly)
		void SetCurrentBulletsInMagazine(int Bullets);
	UFUNCTION(NetMulticast, Reliable)
		void MulticastSetCurrentBulletsInMagazine(int Bullets);
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetBulletsInBackPack() const;
	UFUNCTION(BlueprintCallable)
		void SetBulletsInBackPack(int Bullets);
	UFUNCTION(BlueprintCallable)
		EReloadingType GetReloadingType() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		TSubclassOf<AAmmoItemObject> GetAmmoTypeClass() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		int GetBulletsAmount() const;
	
	UFUNCTION(BlueprintCallable)
		bool IsFiring() const;
	UFUNCTION(BlueprintCallable)
		void SetFiring(bool bInFiring);
	UFUNCTION(BlueprintCallable)
		bool IsAiming() const;
	UFUNCTION(BlueprintCallable)
		bool IsReloading() const;
	UFUNCTION(BlueprintCallable)
		void SetReloading(bool bInReloading);
	UFUNCTION(BlueprintCallable)
		bool IsFullReloading() const;
	UFUNCTION(BlueprintCallable)
		void SetFullReloading(bool bInFullReloading);
	UFUNCTION(BlueprintCallable)
		bool IsChangingFireMode() const;
	UFUNCTION(BlueprintCallable)
		void SetChangingFireMode(bool bInChangingFireMode);

	UFUNCTION(BlueprintCallable)
		bool IsLoadingCartridge() const;
	UFUNCTION(BlueprintCallable)
		bool IsStoppedLoadingCartridges() const;

	UFUNCTION(BlueprintCallable)
		float GetFireDistance() const;
	UFUNCTION(BlueprintCallable)
		void SetFireDistance(float Distance);
	
	UFUNCTION(BlueprintCallable)
		int GetCurrentFireMode() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsHasAutoFire() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsHasSingleFire() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool IsHasRangedFire() const;
	
	UFUNCTION(BlueprintCallable)
		UWeaponClippingComponent* GetWeaponClippingComponent() const;

	UFUNCTION(BlueprintCallable)
		float GetHorizontalRecoil() const;
	UFUNCTION(BlueprintCallable)
		void SetHorizontalRecoil(float HRecoil);
	UFUNCTION(BlueprintCallable)
		float GetVerticalRecoil() const;
	UFUNCTION(BlueprintCallable)
		void SetVerticalRecoil(float VRecoil);

	UFUNCTION(BlueprintCallable)
		float GetHorizontalSpread() const;
	UFUNCTION(BlueprintCallable)
		void SetHorizontalSpread(float HSpread);
	UFUNCTION(BlueprintCallable)
		float GetVerticalSpread() const;
	UFUNCTION(BlueprintCallable)
		void SetVerticalSpread(float VSpread);

	UTransformController* GetAimingTransformController() const;

	float GetAimingFOV() const;
	void SetAimingFOV(float Value);

	void FullFillMagazine();
	float GetTimeFOVLerp();

	void AddPriority(EWeaponAction Action, FString PriorityName, EAbility Ability) override;
	bool FindPriority(EWeaponAction Action, FString PriorityName, EAbility Ability) override;
	void RemovePriority(EWeaponAction Action, FString PriorityName, EAbility Ability) override;
	FAblePriority GetMinPriority(EWeaponAction Action) const override;
	
protected:
	void BeginPlay() override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void Tick(float DeltaTime) override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void OnBackInitialization(EEquipmentType EquipmentType) override;
	void InHandsInitialization() override;
	void InHandsDeInitialization() override;

	void OnFireButtonPressed_Implementation() override;
	void OnAimingButtonPressed_Implementation() override;
	void OnAimingButtonReleased_Implementation() override;
	void OnReloadButtonPressed_Implementation() override;

	void OnInclineButton_Implementation(float Value) override;           //for now not replicating
	void OnChangeFireModeButtonPressed_Implementation() override;        //for now not replicating
	void OnHorizontalMovingButton_Implementation(float Value) override;  //for now not replicating

	UFUNCTION()
		void ActivateFiring();
	UFUNCTION()
		void AutoFire();
	UFUNCTION()
		void SingleFire();
	UFUNCTION()
		void RangeFire();
	UFUNCTION()
		bool MakeFiring();
	UFUNCTION()
		void Fire();

	UFUNCTION()
		void FireWhenRunning(float DeltaTime);

	UFUNCTION()
		void OnFiring(bool bInFiring);
	UFUNCTION(Server, Reliable)
		void ServerOnFiring(bool bInFiring);
	UFUNCTION(NetMulticast, Reliable)
		void MulticastOnFiring(bool bInFiring);

	void OnAttack() override;
	void ApplyDamage(FHitResult Result) override;
	
	void AfterHolstering() override;

	void AddDefaultPriorities() override;

	UFUNCTION()
		void OnAiming(bool bInAiming);
	UFUNCTION()
		void Aim();
	UFUNCTION()
		void AimWithHoldingButton();
	UFUNCTION()
		void AimWithoutHoldingButton();
	UFUNCTION()
		void UpdateAbilityToAim();

	UFUNCTION()
		void MagazineReloading();
	UFUNCTION()
		void ReloadMagazine();
	UFUNCTION()
		void StopMagazineReloading();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastStopMagazineReloading();
	
	UFUNCTION()
		void CartridgeReloading();
	UFUNCTION()
		void StartCartridgeReloading();
	UFUNCTION()
		void LoadCartridge();
	UFUNCTION()
		void StopCartridgeLoading();
	UFUNCTION(NetMulticast, Reliable)
		void MulticastStopCartridgeLoading();
	UFUNCTION()
		void EndCartridgeReloading();
	UFUNCTION()
		void UpdateCartridgeReloadingInfo();
	
	UFUNCTION()
		void OnReloading(bool bInReloading);
	UFUNCTION()
		void OnFullReloading(bool bInFullReloading);

	UFUNCTION()
	void OnRep_CurrentBulletsInMagazine();

	UFUNCTION()
		void OnChangingFireMode(bool bInChangingFireMode);
	UFUNCTION()
		void IncreaseCurrentFireMode();

	UFUNCTION()
		void UpdateFOV(float DeltaTime);
	UFUNCTION()
		void UpdateRecoil(float DeltaTime);

	UFUNCTION()
		void OnClippingFireInitialization();
	UFUNCTION()
		void OnPutDownWeaponFireInitialization();

	////////////////////////////////////////Fields/////////////////////////////////////////////
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* AimingTransformController;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* AimingRightInclineTransformController;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* AimingLeftInclineTransformController;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* LeftMovingTransformController;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UTransformController* RightMovingTransformController;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UWeaponClippingComponent* WeaponClippingComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UWeaponRecoilComponent* WeaponRecoilComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UWeaponSpreadComponent* WeaponSpreadComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		USceneComponent* CurrentEndOfWeaponInWeaponArea;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UBulletHoleComponent* BulletHoleComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float CurrencyChangeWeaponRotationWhenAimingAndHorizontalMoving = 0.2f;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon parameters", meta = (ClampMin = 0, ClampMax = 1))
		float HorizontalRecoil;
	UPROPERTY(EditDefaultsOnly, Category = "Weapon parameters", meta = (ClampMin = 0, ClampMax = 1))
		float VerticalRecoil;
	UPROPERTY(EditDefaultsOnly, Category = "Weapon parameters", meta = (ClampMin = 0, ClampMax = 1))
		float HorizontalSpread;
	UPROPERTY(EditDefaultsOnly, Category = "Weapon parameters", meta = (ClampMin = 0, ClampMax = 1))
		float VerticalSpread;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Aiming")
		float AimingFOV = 90;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Aiming")
		float TimeFOVLerp = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "ReloadParameters")
	TEnumAsByte<EReloadingType> ReloadingType;
	
	UPROPERTY(EditDefaultsOnly, Category = "Magazine")
		int MaxBulletsInMagazine = 30;
	UPROPERTY(EditDefaultsOnly, Category = "Magazine")
		TSubclassOf<AAmmoItemObject> AmmoTypeClass;
	UPROPERTY(EditDefaultsOnly, Category = "Magazine")
		int CurrentBulletsInMagazine = 30;
	
	UPROPERTY(EditDefaultsOnly, Category = "Fire parameters")
		float FireDistance = 500.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Fire parameters")
		int BulletsAmount = 1;
	UPROPERTY(EditDefaultsOnly, Category = "Fire parameters")
		float MinSpeedForShootingInRun = 200;
	UPROPERTY(EditDefaultsOnly, Category = "Fire parameters")
		int CurrentFireMode = 0;

	UPROPERTY(EditDefaultsOnly, Category = "AutoFire mode")
		bool AutoFireBool = true;
	UPROPERTY(EditDefaultsOnly, Category = "SingleFire mode")
		bool SingleFireBool = true;
	UPROPERTY(EditDefaultsOnly, Category = "RangeFire mode")
		bool RangeFireBool = false;
	UPROPERTY(EditDefaultsOnly, Category = "RangeFire mode")
		int AmountRangeShootingBullets = 3;

	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "ReloadingType == EReloadingType::Cartridge"), Category = "DurationActions")
		float StartReloadingTime = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float ReloadingTime = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float FullReloadingTime = 0.0f;
	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "ReloadingType == EReloadingType::Cartridge"), Category = "DurationActions")
		float EndReloadingTime = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float ChangeFireModeTime = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float TimeToShootInRun = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DurationActions")
		float TimeToShootAfterRun = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "WeaponPriorities")
		UPriorityWithOneEqualParamAsset* AimingPrioritiesAsset;
	UPROPERTY(EditDefaultsOnly, Category = "WeaponPriorities")
		UPriorityWithOneEqualParamAsset* ReloadPrioritiesAsset;
	UPROPERTY(EditDefaultsOnly, Category = "WeaponPriorities")
		UPriorityWithOneEqualParamAsset* ChangeFirePrioritiesAsset;
	
	TArray<FAblePriority> AimingPriorities;
	TArray<FAblePriority> ReloadPriorities;
	TArray<FAblePriority> ChangeFirePriorities;

	int CurrentAmountRangeShootedBullets = 0;

	bool bFiring = false;  //Considering shooting for all the time. Can stopped with able priority, button and etc.
	bool bAiming = false;
	bool bReloading = false;
	bool bFullReloading = false;
	bool bChangingFireMode = false;

	bool bLoadingCartridge = false;
	bool bStopLoadingCartridges = false;

	TArray<int> FireMods;
	
	FTimerHandle FireHandle;
	FTimerHandle ReloadingHandle;
	FTimerHandle ChangeFireModeHandle;

	float CurrentTimeToShootInRun = 0.0f;
	float CurrentTimeToShootAfterRun = 0.0f;
	float StartFOV;
	float CurrentTimeOfFOVLerp = 0.0f;
	float CurDeltaTime = 0.0f;

	bool bStartFOVInitialized = false;
	bool bRangeFired = false;
	bool bRangeFireButtonPressed = false;
	bool bSingleFired = false;
	bool bFiredAfterFirstFrame = false;  // Used for single shots in low framerate, because bFireButtonPressed has time to change value before fire starts. Don't remove from code!

	bool bDoOnceAiming = true;
	bool bDoOnceFirePriorityInitializationInRun = false;

	//Events
	UPROPERTY(BlueprintAssignable)
		FDMD_Bool FiringDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD_Bool AimingDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD_Bool ReloadingDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD_Bool FullReloadingDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD_Bool ChangingFireDispatcher;
	UPROPERTY(BlueprintAssignable)
		FDMD CurrentBulletsInMagazineDispatcher;
};
