// Fill out your copyright notice in the Description page of Project Settings.


#include "TPWeaponMount.h"

void ATPWeaponMount::MountInitialization(UMotherConnectorComponent* InMotherConnector,
	UMeshComponent* InActivatedWeaponModuleMesh, UMeshComponent* InDeactivatedWeaponModuleMesh)
{
	Super::MountInitialization(InMotherConnector, InActivatedWeaponModuleMesh, InDeactivatedWeaponModuleMesh);

	if (bMountActivated)
	{
		UpdateMountActivatedVisibility();
	}
	else
	{
		UpdateMountDeactivatedVisibility();
	}
}

void ATPWeaponMount::OnRep_MountActivated()
{
	Super::OnRep_MountActivated();

	if (bMountActivated)
	{
		UpdateMountActivatedVisibility();
	}
	else
	{
		if (AttachedModules == 0)
		{
			UpdateMountDeactivatedVisibility();
		}
	}
}
