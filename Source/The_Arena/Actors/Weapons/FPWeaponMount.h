// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/Actors/Weapons/WeaponMountBase.h"
#include "The_Arena/ActorComponents/TransformController.h"

#include "FPWeaponMount.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AFPWeaponMount : public AWeaponMountBase
{
	GENERATED_BODY()

	//Methods
public:
	void MountInitialization(UMotherConnectorComponent* InMotherConnector, UMeshComponent* InActivatedWeaponModuleMesh, UMeshComponent* InDeactivatedWeaponModuleMesh) override;
	
protected:
	void OnRep_Owner() override;
	void OnRep_MountActivated() override;

	UFUNCTION()
	void Aim(bool IsAiming);

	void ActivateMountInternal();
	void DeactivateMountInternal();

	UFUNCTION()
		void OnInHandsInitialization();
	UFUNCTION()
		void OnBackInitialization();
	
	//Fields
protected:
	UPROPERTY(BlueprintReadWrite)
		UTransformController* MountTransformController;
};
