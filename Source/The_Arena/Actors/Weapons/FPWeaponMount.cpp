// Fill out your copyright notice in the Description page of Project Settings.


#include "FPWeaponMount.h"

#include "The_Arena/Actors/Weapons/RifleBase.h"

void AFPWeaponMount::MountInitialization(UMotherConnectorComponent* InMotherConnector,
	UMeshComponent* InActivatedWeaponModuleMesh, UMeshComponent* InDeactivatedWeaponModuleMesh)
{
	Super::MountInitialization(InMotherConnector, InActivatedWeaponModuleMesh, InDeactivatedWeaponModuleMesh);

	if (bMountActivated)
	{
		ActivateMountInternal();
	}
	else
	{
		DeactivateMountInternal();
	}
}

void AFPWeaponMount::OnRep_Owner()
{
	Super::OnRep_Owner();

	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		FiringWeapon->GetInHandsInitializationDispatcher().AddDynamic(this, &AFPWeaponMount::OnInHandsInitialization);
		FiringWeapon->GetOnBackInitializationDispatcher().AddDynamic(this, &AFPWeaponMount::OnBackInitialization);

		if (auto FPCharacter = Cast<AFirstPersonCharacter>(FiringWeapon->GetOwner()))
		{
			if (MountTransformController)
			{
				MountTransformController->SetSceneComp(FPCharacter->GetFirstPersonSkeletalMesh());
			}
		}
	}
}

void AFPWeaponMount::OnRep_MountActivated()
{
	Super::OnRep_MountActivated();

	if (bMountActivated)
	{
		ActivateMountInternal();
	}
	else
	{
		if (AttachedModules == 0)
		{
			DeactivateMountInternal();
		}
	}	
}

void AFPWeaponMount::Aim(bool IsAiming)
{
	if (MountTransformController)
	{
		if (IsAiming)
		{
			MountTransformController->SetScaleOfMoving(1.0f);
		}
		else
		{
			MountTransformController->SetScaleOfMoving(0.0f);
		}
	}
}

void AFPWeaponMount::ActivateMountInternal()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		if (!FiringWeapon->GetAimingDispatcher().IsAlreadyBound(this, &AFPWeaponMount::Aim))
		{
			FiringWeapon->GetAimingDispatcher().AddDynamic(this, &AFPWeaponMount::Aim);
		}

		if (FiringWeapon->IsInHandsInitialized())
		{
			UpdateMountActivatedVisibility();
		}
		else
		{
			UpdateMountHideVisibility();
		}
	}
}

void AFPWeaponMount::DeactivateMountInternal()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		if (FiringWeapon->GetAimingDispatcher().IsAlreadyBound(this, &AFPWeaponMount::Aim))
		{
			FiringWeapon->GetAimingDispatcher().RemoveDynamic(this, &AFPWeaponMount::Aim);
		}

		if (FiringWeapon->IsInHandsInitialized())
		{
			UpdateMountDeactivatedVisibility();
		}
		else
		{
			UpdateMountHideVisibility();
		}
	}
}

void AFPWeaponMount::OnInHandsInitialization()
{
	if (bMountActivated)
	{
		UpdateMountActivatedVisibility();
	}
	else
	{
		UpdateMountDeactivatedVisibility();
	}
}

void AFPWeaponMount::OnBackInitialization()
{
	UpdateMountHideVisibility();
}
