// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/Weapons/WeaponMountBase.h"
#include "TPWeaponMount.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API ATPWeaponMount : public AWeaponMountBase
{
	GENERATED_BODY()

	//Methods
public:
	void MountInitialization(UMotherConnectorComponent* InMotherConnector, UMeshComponent* InActivatedWeaponModuleMesh, UMeshComponent* InDeactivatedWeaponModuleMesh) override;

protected:
	void OnRep_MountActivated() override;
};
