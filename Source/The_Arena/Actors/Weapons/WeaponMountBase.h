// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "The_Arena/ActorComponents/WeaponModules/MotherConnectorComponent.h"

#include "WeaponMountBase.generated.h"

UCLASS()
class THE_ARENA_API AWeaponMountBase : public AActor
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable)
	virtual void MountInitialization(UMotherConnectorComponent* InMotherConnector, UMeshComponent* InActivatedWeaponModuleMesh, UMeshComponent* InDeactivatedWeaponModuleMesh);
	
	UFUNCTION(BlueprintCallable)
	void ActivateMount();	
	UFUNCTION(BlueprintCallable)
	void DeactivateMount();

	UFUNCTION(BlueprintCallable)
	bool IsActivated() const;
	UFUNCTION(BlueprintCallable)
	bool IsMountInitialized() const;

protected:
	AWeaponMountBase();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void UpdateMountActivatedVisibility();
	void UpdateMountDeactivatedVisibility();
	void UpdateMountHideVisibility();

	UFUNCTION()
	virtual void OnRep_MountActivated();

	//Fields
protected:
	UPROPERTY(BlueprintReadWrite)
	UStaticMeshComponent* MountStaticMesh;
	UPROPERTY(BlueprintReadWrite)
	UMeshComponent* ActivatedWeaponModuleMesh;
	UPROPERTY(BlueprintReadWrite)
	UMeshComponent* DeactivatedWeaponModuleMesh;
	UPROPERTY()
	int AttachedModules = 0;
	UPROPERTY(ReplicatedUsing= OnRep_MountActivated)
	bool bMountActivated = false;
	
	bool bMountInitialized = false;
};