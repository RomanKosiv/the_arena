// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"

AWeapon::AWeapon()
{
	
}

void AWeapon::MulticastOnBackInitialization_Implementation(EEquipmentType EquipmentType)
{
	OnBackInitialization(EquipmentType);
}

void AWeapon::Use_Implementation()
{
	InHandsInitialization();
}

void AWeapon::Remove_Implementation()
{
	MulticastInHandsDeinitialization();
}

void AWeapon::BindToRemovedDispatcher_Implementation(UObject* InObject, const FName& InFunctionName)
{
	if (!InObject)
		return;

	TScriptDelegate<> ScriptDelegate;
	ScriptDelegate.BindUFunction(InObject, InFunctionName);

	HolsteringDispatcher.Add(ScriptDelegate);
}

void AWeapon::UnbindFromRemovedDispatcher_Implementation(UObject* InObject, const FName& InFunctionName)
{
	if (!InObject)
		return;

	TScriptDelegate<> ScriptDelegate;
	ScriptDelegate.BindUFunction(InObject, InFunctionName);

	HolsteringDispatcher.Remove(ScriptDelegate);
}

float AWeapon::GetDamage() const
{
	return Damage;
}

void AWeapon::SetDamage(float Value)
{
	Damage = Value;
}

float AWeapon::GetAttackTime() const
{
	return AttackTime;
}

FDMD& AWeapon::GetAttackDispatcher()
{
	return AttackDispatcher;
}

FDMD_Bool& AWeapon::GetPureAttackDispatcher()
{
	return PureAttackDispatcher;
}

FDMD& AWeapon::GetInHandsInitializationDispatcher()
{
	return InHandsInitializationDispatcher;
}

FDMD& AWeapon::GetOnBackInitializationDispatcher()
{
	return BackInitializationDispatcher;
}

FDMD_Bool& AWeapon::GetFireButtonPressedDispatcher()
{
	return FireButtonPressedDispatcher;
}

FDMD_Bool& AWeapon::GetDrawingDispatcher()
{
	return DrawingDispatcher;
}

FDMD_Bool& AWeapon::GetHolsteringDispatcher()
{
	return HolsteringDispatcher;
}

bool AWeapon::IsPureAttack()
{
	return bPureAttack;
}

void AWeapon::SetPureAttack(bool bInPureAttack)
{
	bPureAttack = bInPureAttack;

	OnPureAttack(bInPureAttack);
}

bool AWeapon::IsDrawing() const
{
	return bDrawing;
}

void AWeapon::SetDrawing(bool bInDrawing)
{
	bDrawing = bInDrawing;

	OnDrawing(bInDrawing);
}

bool AWeapon::IsHolstering() const
{
	return bHolstering;
}

void AWeapon::SetHolstering(bool bInHolstering)
{
	bHolstering = bInHolstering;

	OnHolstering(bInHolstering);
}

bool AWeapon::IsInHandsInitialized() const
{
	return bInHandsInitialized;
}

bool AWeapon::IsFireButtonPressed() const
{
	return bFireButtonPressed;
}

USkeletalMeshComponent* AWeapon::GetFPWeaponMeshComponent() const
{
	return FPWeaponSkeletalMesh;
}

void AWeapon::AddDefaultPriorities()
{
	AddPriority(EWeaponAction::Fire, "Standart", EAbility::Can);
	AddPriority(EWeaponAction::Holster, "Standart", EAbility::Can);
}

void AWeapon::AddPriorityProtected(FAblePriority Priority, TArray<FAblePriority>& Array)
{
	Array.HeapPush(Priority);
}

bool AWeapon::FindPriorityProtected(FAblePriority Priority, TArray<FAblePriority>& Array)
{
	int PriorityIndex = Array.Find(Priority);

	if (PriorityIndex != INDEX_NONE)
	{
		return true;
	}

	return false;;
}

void AWeapon::RemovePriorityProtected(FAblePriority Priority, TArray<FAblePriority>& Array)
{
	int PriorityIndex = Array.Find(Priority);

	if (PriorityIndex != INDEX_NONE)
		Array.HeapRemoveAt(PriorityIndex);
}

FAblePriority AWeapon::GetMinPriorityProtected(const TArray<FAblePriority>& Array) const
{
	if (Array.Num() > 0)
		return Array.HeapTop();

	FAblePriority NONE = { EAbility::Cant, 0 };
	return NONE;
}

void AWeapon::AddPriority(EWeaponAction Action, FString PriorityName, EAbility Ability)
{
	auto SetPriorityFromAssetLambda = [&](UPriorityWithOneEqualParamAsset* Asset, FAblePriority& AblePriority)->bool
	{
		if (Asset)
		{
			int Priority = Asset->GetPriority(PriorityName);

			if (Priority != -1)
			{
				AblePriority.Priority = Priority;
				AblePriority.Name = PriorityName;
				return true;
			}
			return false;
		}
		return false;
	};

	FAblePriority Priority = { Ability, 0, "NONE" };

	switch (Action)
	{
	case EWeaponAction::Fire:
		if (SetPriorityFromAssetLambda(FirePrioritiesAsset, Priority))
		{
			AddPriorityProtected(Priority, FirePriorities);
		}
		break;
	case EWeaponAction::Holster:
		if (SetPriorityFromAssetLambda(HolsterPrioritiesAsset, Priority))
		{
			AddPriorityProtected(Priority, HolsterPriorities);
		}
		break;
	default:
		break;
	}
}

void AWeapon::MulticastAddPriority_Implementation(EWeaponAction Action, const FString& PriorityName, EAbility Ability)
{
	AddPriority(Action, PriorityName, Ability);
}

bool AWeapon::FindPriority(EWeaponAction Action, FString PriorityName, EAbility Ability)
{
	auto FindPriorityLambda = [&](UPriorityWithOneEqualParamAsset* Asset, FAblePriority& AblePriority, TArray<FAblePriority>& Array)->bool
	{
		if (Asset)
		{
			int Priority = Asset->GetPriority(PriorityName);

			if (Priority != -1)
			{
				AblePriority.Priority = Priority;
				AblePriority.Name = PriorityName;

				return FindPriorityProtected(AblePriority, Array);
			}

			return false;
		}

		return false;
	};

	FAblePriority AblePriority = { Ability, 0, "NONE" };

	switch (Action)
	{
	case EWeaponAction::Fire:
		return FindPriorityLambda(FirePrioritiesAsset, AblePriority, FirePriorities);
	case EWeaponAction::Holster:
		return FindPriorityLambda(HolsterPrioritiesAsset, AblePriority, HolsterPriorities);
	default:
		break;
	}

	return false;
}

void AWeapon::RemovePriority(EWeaponAction Action, FString PriorityName, EAbility Ability)
{
	auto SetPriorityFromAssetLambda = [&](UPriorityWithOneEqualParamAsset* Asset, FAblePriority& AblePriority)->bool
	{
		if (Asset)
		{
			int Priority = Asset->GetPriority(PriorityName);

			if (Priority != -1)
			{
				AblePriority.Priority = Priority;
				AblePriority.Name = PriorityName;
				return true;
			}
			return false;
		}
		return false;
	};

	FAblePriority Priority = { Ability, 0 };

	switch (Action)
	{
	case EWeaponAction::Fire:
		if (SetPriorityFromAssetLambda(FirePrioritiesAsset, Priority))
		{
			RemovePriorityProtected(Priority, FirePriorities);
		}
		break;
	case EWeaponAction::Holster:
		if (SetPriorityFromAssetLambda(HolsterPrioritiesAsset, Priority))
		{
			RemovePriorityProtected(Priority, HolsterPriorities);
		}
		break;
	default:
		break;
	}
}

void AWeapon::MulticastRemovePriority_Implementation(EWeaponAction Action, const FString& PriorityName, EAbility Ability)
{
	RemovePriority(Action, PriorityName, Ability);
}

FAblePriority AWeapon::GetMinPriority(EWeaponAction Action) const
{
	FAblePriority ReturnPriority = {};

	switch (Action)
	{
	case EWeaponAction::Fire:
		ReturnPriority = GetMinPriorityProtected(FirePriorities);
		break;
	case EWeaponAction::Holster:
		ReturnPriority = GetMinPriorityProtected(HolsterPriorities);
		break;
	default:
		break;
	}

	return ReturnPriority;
}

void AWeapon::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (IsHolstering())
	{
		MulticastAfterHolstering();
	}
}

void AWeapon::InHandsInitialization()
{
	AFirstPersonCharacter* FPCharacter = Cast<AFirstPersonCharacter>(GetOwner());
	
	if (FPCharacter && TPInventoryItem)
	{
		auto FPSkeletalMesh = FPCharacter->GetFirstPersonSkeletalMesh();
		auto TPSkeletalMesh = FPCharacter->GetMesh();

		p_FPPlayerControllerRepParams = FPCharacter->GetFPPlayerControllerRepParams();

		if (FPWeaponSkeletalMesh)
		{
			FPWeaponSkeletalMesh->SetVisibility(true);
		}

		if (FPSkeletalMesh)
		{
			if (FPCharacterAnimBP)
			{
				FPSkeletalMesh->SetAnimInstanceClass(FPCharacterAnimBP.Get());
			}

			FPSkeletalMesh->SetVisibility(true);

			FPSkeletalMesh->AddRelativeLocation(RelativeWeaponHandsLocation);
			FPSkeletalMesh->AddRelativeRotation(RelativeWeaponHandsRotation);
		}

		if (TPSkeletalMesh)
		{
			if (TPCharacterAnimBP)
			{
				TPSkeletalMesh->SetAnimInstanceClass(TPCharacterAnimBP);
			}
		}

		SetDrawing(true);
		GetWorldTimerManager().SetTimer(DrawHandle, this, &AWeapon::AfterDrawing, DrawTime, false);

		if(InitializaitonHandle.IsValid())
		{
			GetWorldTimerManager().ClearTimer(InitializaitonHandle);
		}

		bInHandsInitialized = true;
		InHandsInitializationDispatcher.Broadcast();
	}
	else
	{
		if(!InitializaitonHandle.IsValid())
		{
			GetWorldTimerManager().SetTimer(InitializaitonHandle, this, &AWeapon::InHandsInitialization, 0.5f, true);
		}
	}
}

void AWeapon::InHandsDeInitialization()
{
	AddPriority(EWeaponAction::Fire, "Holster", EAbility::Cant);

	SetHolstering(true);

	if (GetLocalRole() == ROLE_Authority)
	{
		GetWorldTimerManager().SetTimer(HolsterHandle, this, &AWeapon::MulticastAfterHolstering, HolsterTime, false);
	}
	
	bInHandsInitialized = false;
}

void AWeapon::OnBackInitialization(EEquipmentType EquipmentType)
{
	if (FPWeaponSkeletalMesh)
	{
		FPWeaponSkeletalMesh->SetVisibility(false);
	}
	
	BackInitializationDispatcher.Broadcast();
}

void AWeapon::OnFireButtonPressed_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (FPCharacter->IsLocallyControlled())
		{
			if (FPCharacter->GetLocalRole() == ROLE_Authority)
			{
				MulticastOnFireButtonPressed();
			}
			else
			{
				ServerOnFireButtonPressed();
			}
		}
	}
	
	bFireButtonPressed = true;
	FireButtonPressedDispatcher.Broadcast(true);
}

void AWeapon::ServerOnFireButtonPressed_Implementation()
{
	MulticastOnFireButtonPressed();
}

void AWeapon::MulticastOnFireButtonPressed_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (!FPCharacter->IsLocallyControlled())
		{
			Execute_OnFireButtonPressed(this);
		}
	}
}

void AWeapon::OnFireButtonReleased_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (FPCharacter->IsLocallyControlled())
		{
			if (FPCharacter->GetLocalRole() == ROLE_Authority)
			{
				MulticastOnFireButtonReleased();
			}
			else
			{
				ServerOnFireButtonReleased();
			}
		}
	}

	bFireButtonPressed = false;
	FireButtonPressedDispatcher.Broadcast(false);
}

void AWeapon::ServerOnFireButtonReleased_Implementation()
{
	MulticastOnFireButtonReleased();
}

void AWeapon::MulticastOnFireButtonReleased_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (!FPCharacter->IsLocallyControlled())
		{
			Execute_OnFireButtonReleased(this);
		}
	}
}

void AWeapon::OnAimingButtonPressed_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (FPCharacter->IsLocallyControlled())
		{
			if (FPCharacter->GetLocalRole() == ROLE_Authority)
			{
				MulticastOnAimingButtonPressed();
			}
			else
			{
				ServerOnAimingButtonPressed();
			}
		}
	}

	bAimingButtonPressed = true;
}

void AWeapon::ServerOnAimingButtonPressed_Implementation()
{
	MulticastOnAimingButtonPressed();
}

void AWeapon::MulticastOnAimingButtonPressed_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (!FPCharacter->IsLocallyControlled())
		{
			Execute_OnAimingButtonPressed(this);
		}
	}
}

void AWeapon::OnAimingButtonReleased_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (FPCharacter->IsLocallyControlled())
		{
			if (FPCharacter->GetLocalRole() == ROLE_Authority)
			{
				MulticastOnAimingButtonReleased();
			}
			else
			{
				ServerOnAimingButtonReleased();
			}
		}
	}

	bAimingButtonPressed = false;
}

void AWeapon::ServerOnAimingButtonReleased_Implementation()
{
	MulticastOnAimingButtonReleased();
}

void AWeapon::MulticastOnAimingButtonReleased_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (!FPCharacter->IsLocallyControlled())
		{
			Execute_OnAimingButtonReleased(this);
		}
	}
}

void AWeapon::OnReloadButtonPressed_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (FPCharacter->IsLocallyControlled())
		{
			if (FPCharacter->GetLocalRole() == ROLE_Authority)
			{
				MulticastOnReloadButtonPressed();
			}
			else
			{
				ServerOnReloadButtonPressed();
			}
		}
	}
}

void AWeapon::ServerOnReloadButtonPressed_Implementation()
{
	MulticastOnReloadButtonPressed();
}

void AWeapon::MulticastOnReloadButtonPressed_Implementation()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (!FPCharacter->IsLocallyControlled())
		{
			Execute_OnReloadButtonPressed(this);
		}
	}
}

void AWeapon::OnDrawing(bool bInDrawing)
{
	DrawingDispatcher.Broadcast(bInDrawing);
}

void AWeapon::OnHolstering(bool bInHolstering)
{
	HolsteringDispatcher.Broadcast(bInHolstering);
}

void AWeapon::OnAttack()
{
	auto TurnOffPureShootingLambda = [&]()
	{
		SetPureAttack(false);
	};

	SetPureAttack(true);

	if (PureAttackTime < AttackTime)
	{
		GetWorldTimerManager().SetTimer(PureAttackHandle, TurnOffPureShootingLambda, PureAttackTime, false);
	}
	
	AttackDispatcher.Broadcast();
}

void AWeapon::OnPureAttack(bool bInPureAttack)
{
	PureAttackDispatcher.Broadcast(bInPureAttack);
}

void AWeapon::ApplyDamage(FHitResult Result)
{
	
}

void AWeapon::AfterDrawing()
{
	SetDrawing(false);
	AddDefaultPriorities();
}

void AWeapon::AfterHolstering()
{
	OnBackInitialization(WeaponEquipmentType);

	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (auto FPMesh = FPCharacter->GetFirstPersonSkeletalMesh())
		{
			FPMesh->SetAnimInstanceClass(nullptr);
			FPMesh->SetVisibility(false);

			FPMesh->AddRelativeLocation((-1) * RelativeWeaponHandsLocation);
			FPMesh->AddRelativeRotation((-1) * RelativeWeaponHandsRotation);
		}

		if (auto TPMesh = FPCharacter->GetMesh())
		{
			TPMesh->SetAnimInstanceClass(FPCharacter->GetThirdPersonAnimBP());
		}
	}

	FirePriorities.Empty();
	HolsterPriorities.Empty();

	SetHolstering(false);
}

void AWeapon::MulticastAfterHolstering_Implementation()
{
	AfterHolstering();
}

void AWeapon::ServerApplyDamage_Implementation(FHitResult Result)
{
	ApplyDamage(Result);
}

void AWeapon::MulticastInHandsDeinitialization_Implementation()
{
	InHandsDeInitialization();
}
