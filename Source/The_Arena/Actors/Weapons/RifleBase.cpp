// Fill out your copyright notice in the Description page of Project Settings.

#include "RifleBase.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/UObjects/Factories/InventoryItemObjectsManager.h"
#include "The_Arena/GameStates/CustomGameState.h"

#include "Runtime/Engine/Public/TimerManager.h"



bool operator<(const FAblePriority& Pr1, const FAblePriority& Pr2)
{
	if (Pr1.Priority > Pr2.Priority)
		return true;

	return false;
}

bool operator==(const FAblePriority& Pr1, const FAblePriority& Pr2)
{
	if (Pr1.Type == Pr2.Type && Pr1.Priority == Pr2.Priority && Pr1.Name == Pr2.Name)
		return true;

	return false;
}


// Sets default values
ARifleBase::ARifleBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ARifleBase::ApplyDamage(FHitResult Result)
{
	if (GetCurrentBulletsInMagazine() > 0 && GetMinPriority(EWeaponAction::Fire).Type == EAbility::Can)
	{
		if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
		{
			if (auto EquipmentComponent = FPCharacter->GetFPEquipmentComponent())
			{
				if (EquipmentComponent->GetUsingItemInHands() == this)
				{
					UGameplayStatics::ApplyPointDamage(Result.GetActor(), GetDamage(), -1 * Result.ImpactNormal, Result, FPCharacter->GetController(), FPCharacter, DamageType);
				}
			}
		}
	}
}

int ARifleBase::GetMaxBulletsInMagazine() const
{
	return MaxBulletsInMagazine;
}

void ARifleBase::SetMaxBulletsInMagazine(int Bullets)
{
	MaxBulletsInMagazine = Bullets;
}

int ARifleBase::GetCurrentBulletsInMagazine() const
{
	return CurrentBulletsInMagazine;
}

void ARifleBase::SetCurrentBulletsInMagazine(int Bullets)
{
	CurrentBulletsInMagazine = Bullets;

	OnRep_CurrentBulletsInMagazine();
}

void ARifleBase::MulticastSetCurrentBulletsInMagazine_Implementation(int Bullets)
{
	SetCurrentBulletsInMagazine(Bullets);
}

int ARifleBase::GetBulletsInBackPack() const
{
	//return BulletsInBackPack;

	int AmmoAmount = 0;
	
	if(auto Character = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if(auto InventoryComponent = Character->FindComponentByClass<UCustomInventoryComponent>())
		{
			auto AllItems = InventoryComponent->GetAllItems();
		
			for(auto AllItemsIter = AllItems.begin(); AllItemsIter != AllItems.end(); ++AllItemsIter)
			{
				if(auto ItemObject = AllItemsIter->Key)
				{
					if(ItemObject->GetClass() == AmmoTypeClass)
					{
						AmmoAmount += ItemObject->GetCurrentItemAmount();
					}
				}
			}
		}
	}

	return AmmoAmount;
}

void ARifleBase::SetBulletsInBackPack(int Bullets)
{
	//BulletsInBackPack = Bullets;

	if (GetLocalRole() == ROLE_Authority)
	{
		int CurentBulletsInBackPack = GetBulletsInBackPack();

		if(Bullets < CurentBulletsInBackPack)
		{
			if (auto Character = Cast<AFirstPersonCharacter>(GetOwner()))
			{
				if (auto InventoryComponent = Character->FindComponentByClass<UCustomInventoryComponent>())
				{
					int BulletDifference = CurentBulletsInBackPack - Bullets;
					auto AllItems = InventoryComponent->GetAllItems();

					for (auto AllItemsIter = AllItems.begin(); AllItemsIter != AllItems.end(); ++AllItemsIter)
					{
						if (auto ItemObject = AllItemsIter->Key)
						{
							if (ItemObject->GetClass() == AmmoTypeClass)
							{
								if(ItemObject->TryRemoveItems(BulletDifference))
								{
									break;
								}
								
								BulletDifference -= ItemObject->GetCurrentItemAmount();
								InventoryComponent->RemoveItem(ItemObject, ERemoveItemOption::WithoutSave);

								if(BulletDifference == 0)
								{
									break;
								}
							}
						}
					}
				}
			}
		}
		else if(Bullets > CurentBulletsInBackPack)
		{
			if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
			{
				if (auto InventoryItemObjectManager = CustomGameState->GetInventoryItemObjectsManager())
				{
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.Owner = GetOwner();
					
					if(auto Ammo = InventoryItemObjectManager->CreateInventoryItemObject(AmmoTypeClass, SpawnParameters))
					{
						int BulletDifference = Bullets - CurentBulletsInBackPack;
						
						Ammo->SetCurrentItemAmount(BulletDifference);
						
						if (auto Character = Cast<AFirstPersonCharacter>(GetOwner()))
						{
							if (auto InventoryComponent = Character->FindComponentByClass<UCustomInventoryComponent>())
							{
								if(!InventoryComponent->TryAddItem(Ammo, true))
								{
									if(auto DroppedItemsManager = CustomGameState->GetDroppedItemsManager())
									{
										DroppedItemsManager->SpawnInventoryItemInFrontOfTargetActor(Ammo, Character);
									}
								}
							}
						}
					}
				}
			}
		}
		
		//OnRep_BulletsInBackPack();
	}
}

EReloadingType ARifleBase::GetReloadingType() const
{
	return ReloadingType;
}

TSubclassOf<AAmmoItemObject> ARifleBase::GetAmmoTypeClass() const
{
	return AmmoTypeClass;
}

int ARifleBase::GetBulletsAmount() const
{
	return BulletsAmount;
}

float ARifleBase::GetFireDistance() const
{
	return FireDistance;
}

void ARifleBase::SetFireDistance(float Distance)
{
	FireDistance = Distance;
}

int ARifleBase::GetCurrentFireMode() const
{
	return CurrentFireMode;
}

bool ARifleBase::IsHasAutoFire() const
{
	return AutoFireBool;
}

bool ARifleBase::IsHasSingleFire() const
{
	return SingleFireBool;
}

bool ARifleBase::IsHasRangedFire() const
{
	return RangeFireBool;
}

void ARifleBase::IncreaseCurrentFireMode()
{
	/*if (FireMods.Num() > 0)
	{
		if (CurrentFireMode >= FireMods.Num() - 1 || CurrentFireMode < 0)
		{
			CurrentFireMode = 0;
		}
		else
		{
			CurrentFireMode++;
		}
	}*/

	if (FireMods.Num() > 0)
	{
		auto Index = FireMods.Find(CurrentFireMode);

		if(Index != INDEX_NONE)
		{
			++Index;

			if(Index < FireMods.Num())
			{
				CurrentFireMode = FireMods[Index];
			}
			else
			{
				CurrentFireMode = FireMods[0];
			}
		}
		else
		{
			CurrentFireMode = FireMods[0];
		}
	}

	RemovePriority(EWeaponAction::Fire, "ChangeFireMode", EAbility::Cant);
	RemovePriority(EWeaponAction::Reload, "ChangeFireMode", EAbility::Cant);
	RemovePriority(EWeaponAction::Holster, "ChangeFireMode", EAbility::Cant);

	if (bFireButtonPressed)
	{
		GetWorldTimerManager().ClearTimer(FireHandle);
		OnFireButtonPressed_Implementation();
	}

	SetChangingFireMode(false);

}

UWeaponClippingComponent* ARifleBase::GetWeaponClippingComponent() const
{
	return WeaponClippingComponent;
}


// Called when the game starts or when spawned
void ARifleBase::BeginPlay()
{
	Super::BeginPlay();

	if (AutoFireBool)
		FireMods.Add(AUTOFIRE);

	if (SingleFireBool)
		FireMods.Add(SINGLEFIRE);

	if (RangeFireBool)
		FireMods.Add(RANGEFIRE);

	auto FireModeIndex = FireMods.Find(CurrentFireMode);
	
	if(FireModeIndex == INDEX_NONE && FireMods.Num() > 0)
	{
		CurrentFireMode = FireMods[0];
	}

	if (WeaponRecoilComponent)
	{
		AttackDispatcher.AddDynamic(WeaponRecoilComponent, &UWeaponRecoilComponent::PlayRecoilShake);

		WeaponRecoilComponent->SetHorizontalRecoil(HorizontalRecoil);
		WeaponRecoilComponent->SetVerticalRecoil(VerticalRecoil);
	}

	if (WeaponClippingComponent)
	{
		WeaponClippingComponent->StartClippingDispatcher.AddDynamic(this, &ARifleBase::OnClippingFireInitialization);
		WeaponClippingComponent->PutDownWeaponDispatcher.AddDynamic(this, &ARifleBase::OnPutDownWeaponFireInitialization);
	}
}

bool ARifleBase::IsAiming() const
{
	return bAiming;
}

// Called every frame
void ARifleBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CurDeltaTime = DeltaTime;
	UpdateFOV(DeltaTime);
	UpdateRecoil(DeltaTime);
	FireWhenRunning(DeltaTime);
	UpdateCartridgeReloadingInfo();
}

void ARifleBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (auto FPMesh = FPCharacter->GetFirstPersonSkeletalMesh())
		{
			if (WeaponClippingComponent)
			{
				if (auto TransformController = WeaponClippingComponent->GetClippingTransformController())
				{
					FPMesh->AddRelativeLocation((-1) * TransformController->GetPassLocation());
					FPMesh->AddRelativeRotation((-1) * TransformController->GetPassRotation());
				}
			}
		}
	}

	Super::EndPlay(EndPlayReason);
}

void ARifleBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void ARifleBase::OnAttack()
{	
	Super::OnAttack();
}

void ARifleBase::OnAiming(bool bInAiming)
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (bInAiming)
		{
			FPCharacter->AddMovementPriority(EMovementType::SlowWalk, "Aiming");
		}
		else
		{
			FPCharacter->RemoveMovementPriority(EMovementType::SlowWalk, "Aiming");
		}
	}

	AimingDispatcher.Broadcast(bInAiming);
}

void ARifleBase::EndCartridgeReloading()
{
	RemovePriority(EWeaponAction::Fire, "Reload", EAbility::Cant);
	RemovePriority(EWeaponAction::ChangeFireMode, "Reload", EAbility::Cant);
	RemovePriority(EWeaponAction::Holster, "Reload", EAbility::Cant);

	if (IsReloading())
	{
		SetReloading(false);
	}
	else if (IsFullReloading())
	{
		SetFullReloading(false);
	}

	bStopLoadingCartridges = false;
}

void ARifleBase::UpdateCartridgeReloadingInfo()
{
	if((bReloading || bFullReloading) && ReloadingType == EReloadingType::Cartridge && bFireButtonPressed)
	{
		if(!bStopLoadingCartridges)
		{
			bStopLoadingCartridges = true;
		}
	}
}

void ARifleBase::OnReloading(bool bInReloading)
{
	ReloadingDispatcher.Broadcast(bInReloading);
}

void ARifleBase::OnFullReloading(bool bInFullReloading)
{
	FullReloadingDispatcher.Broadcast(bInFullReloading);
}

void ARifleBase::OnChangingFireMode(bool bInChangingFireMode)
{
	ChangingFireDispatcher.Broadcast(bInChangingFireMode);
}

UTransformController* ARifleBase::GetAimingTransformController() const
{
	return AimingTransformController;
}

float ARifleBase::GetAimingFOV() const
{
	return AimingFOV;
}

void ARifleBase::SetAimingFOV(float Value)
{
	AimingFOV = Value;
}

void ARifleBase::FullFillMagazine()
{
	if (GetLocalRole() != ROLE_Authority)
		return;
	
	int MissingAmmo = GetMaxBulletsInMagazine() - GetCurrentBulletsInMagazine();

	if (MissingAmmo <= GetBulletsInBackPack())
	{
		SetBulletsInBackPack(GetBulletsInBackPack() - MissingAmmo);
		MulticastSetCurrentBulletsInMagazine(GetMaxBulletsInMagazine());
	}
	else
	{
		MulticastSetCurrentBulletsInMagazine(GetCurrentBulletsInMagazine() + GetBulletsInBackPack());
		SetBulletsInBackPack(0);
	}
}

float ARifleBase::GetTimeFOVLerp()
{
	return TimeFOVLerp;
}

void ARifleBase::AddPriority(EWeaponAction Action, FString PriorityName, EAbility Ability)
{
	auto SetPriorityFromAssetLambda = [&](UPriorityWithOneEqualParamAsset* Asset, FAblePriority& AblePriority)->bool
	{
		if (Asset)
		{
			int Priority = Asset->GetPriority(PriorityName);

			if (Priority != -1)
			{
				AblePriority.Priority = Priority;
				AblePriority.Name = PriorityName;
				return true;
			}
			return false;
		}
		return false;
	};

	FAblePriority Priority = { Ability, 0, "NONE" };

	switch (Action)
	{
	case EWeaponAction::Fire:
		if (SetPriorityFromAssetLambda(FirePrioritiesAsset, Priority))
		{
			AddPriorityProtected(Priority, FirePriorities);
		}
		break;
	case EWeaponAction::Aiming:
		if (SetPriorityFromAssetLambda(AimingPrioritiesAsset, Priority))
		{
			AddPriorityProtected(Priority, AimingPriorities);
			UpdateAbilityToAim();
		}
		break;
	case EWeaponAction::Reload:
		if (SetPriorityFromAssetLambda(ReloadPrioritiesAsset, Priority))
		{
			AddPriorityProtected(Priority, ReloadPriorities);
		}
		break;
	case EWeaponAction::ChangeFireMode:
		if (SetPriorityFromAssetLambda(ChangeFirePrioritiesAsset, Priority))
		{
			AddPriorityProtected(Priority, ChangeFirePriorities);
		}
		break;
	case EWeaponAction::Holster:
		if (SetPriorityFromAssetLambda(HolsterPrioritiesAsset, Priority))
		{
			AddPriorityProtected(Priority, HolsterPriorities);
		}
		break;
	default:
		break;
	}
}

bool ARifleBase::FindPriority(EWeaponAction Action, FString PriorityName, EAbility Ability)
{
	auto FindPriorityLambda = [&](UPriorityWithOneEqualParamAsset* Asset, FAblePriority& AblePriority, TArray<FAblePriority>& Array)->bool
	{
		if (Asset)
		{
			int Priority = Asset->GetPriority(PriorityName);

			if (Priority != -1)
			{
				AblePriority.Priority = Priority;
				AblePriority.Name = PriorityName;

				return FindPriorityProtected(AblePriority, Array);
			}

			return false;
		}

		return false;
	};

	FAblePriority AblePriority = { Ability, 0, "NONE" };

	switch (Action)
	{
	case EWeaponAction::Fire:
		return FindPriorityLambda(FirePrioritiesAsset, AblePriority, FirePriorities);
	case EWeaponAction::Aiming:
		return FindPriorityLambda(AimingPrioritiesAsset, AblePriority, AimingPriorities);
	case EWeaponAction::Reload:
		return FindPriorityLambda(ReloadPrioritiesAsset, AblePriority, ReloadPriorities);
	case EWeaponAction::ChangeFireMode:
		return FindPriorityLambda(ChangeFirePrioritiesAsset, AblePriority, ChangeFirePriorities);
	case EWeaponAction::Holster:
		return FindPriorityLambda(HolsterPrioritiesAsset, AblePriority, HolsterPriorities);
	default:
		break;
	}

	return false;
}

void ARifleBase::RemovePriority(EWeaponAction Action, FString PriorityName, EAbility Ability)
{
	auto SetPriorityFromAssetLambda = [&](UPriorityWithOneEqualParamAsset* Asset, FAblePriority& AblePriority)->bool
	{
		if (Asset)
		{
			int Priority = Asset->GetPriority(PriorityName);

			if (Priority != -1)
			{
				AblePriority.Priority = Priority;
				AblePriority.Name = PriorityName;
				return true;
			}
			return false;
		}
		return false;
	};

	FAblePriority Priority = { Ability, 0 };

	switch (Action)
	{
	case EWeaponAction::Fire:
		if (SetPriorityFromAssetLambda(FirePrioritiesAsset, Priority))
		{
			RemovePriorityProtected(Priority, FirePriorities);
		}
		break;
	case EWeaponAction::Aiming:
		if (SetPriorityFromAssetLambda(AimingPrioritiesAsset, Priority))
		{
			RemovePriorityProtected(Priority, AimingPriorities);
		}
		break;
	case EWeaponAction::Reload:
		if (SetPriorityFromAssetLambda(ReloadPrioritiesAsset, Priority))
		{
			RemovePriorityProtected(Priority, ReloadPriorities);
		}
		break;
	case EWeaponAction::ChangeFireMode:
		if (SetPriorityFromAssetLambda(ChangeFirePrioritiesAsset, Priority))
		{
			RemovePriorityProtected(Priority, ChangeFirePriorities);
		}
		break;
	case EWeaponAction::Holster:
		if (SetPriorityFromAssetLambda(HolsterPrioritiesAsset, Priority))
		{
			RemovePriorityProtected(Priority, HolsterPriorities);
		}
		break;
	default:
		break;
	}
}

FAblePriority ARifleBase::GetMinPriority(EWeaponAction Action) const
{
	FAblePriority ReturnPriority = {};

	switch (Action)
	{
	case EWeaponAction::Fire:
		ReturnPriority = GetMinPriorityProtected(FirePriorities);
		break;
	case EWeaponAction::Aiming:
		ReturnPriority = GetMinPriorityProtected(AimingPriorities);
		break;
	case EWeaponAction::Reload:
		ReturnPriority = GetMinPriorityProtected(ReloadPriorities);
		break;
	case EWeaponAction::ChangeFireMode:
		ReturnPriority = GetMinPriorityProtected(ChangeFirePriorities);
		break;
	case EWeaponAction::Holster:
		ReturnPriority = GetMinPriorityProtected(HolsterPriorities);
		break;
	default:
		break;
	}

	return ReturnPriority;
}

FDMD_Bool& ARifleBase::GetFiringDispatcher()
{
	return FiringDispatcher;
}

FDMD_Bool& ARifleBase::GetAimingDispatcher()
{
	return AimingDispatcher;
}

FDMD_Bool& ARifleBase::GetReloadingDispatcher()
{
	return ReloadingDispatcher;
}

FDMD_Bool& ARifleBase::GetFullReloadingDispatcher()
{
	return FullReloadingDispatcher;
}

FDMD_Bool& ARifleBase::GetChangingFireDispatcher()
{
	return ChangingFireDispatcher;
}

FDMD& ARifleBase::GetCurrentBulletsInMagazineDispatcher()
{
	return CurrentBulletsInMagazineDispatcher;
}

void ARifleBase::MulticastInitialization_Implementation(bool bInTestItem)
{
	Super::MulticastInitialization_Implementation(bInTestItem);

	if(bInTestItem)
	{
		SetMaxBulletsInMagazine(1);
		SetCurrentBulletsInMagazine(1);
	}
}

//This implementation for RayCast. Change this implementation in future.
bool ARifleBase::MakeFiring()
{	
	if (GetCurrentBulletsInMagazine() > 0 && GetMinPriority(EWeaponAction::Fire).Type == EAbility::Can)
	{
		OnAttack();

		if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
		{
			if (FPCharacter->IsLocallyControlled())
			{
				for (int i = 0; i < BulletsAmount; ++i)
				{
					Fire();
				}
			}

			SetCurrentBulletsInMagazine(GetCurrentBulletsInMagazine() - 1);
		}

		//if (GEngine)
		//	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::FromInt(GetCurrentBulletsInMagazine()));

		return true;
	}

	return false;
}

void ARifleBase::Fire()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		auto FPCamera = FPCharacter->GetFirstPersonCamera();

		if (FPCamera)
		{
			FHitResult OutHit;
			FVector Start = FPCamera->GetComponentLocation();
			FVector End = FPCamera->GetForwardVector();

			if (WeaponSpreadComponent)
			{
				FVector2D RandomRotation = WeaponSpreadComponent->GetRandomSpread();
				End = End.RotateAngleAxis(RandomRotation.X, FPCamera->GetRightVector());
				End = End.RotateAngleAxis(RandomRotation.Y, FPCamera->GetUpVector());		
			}
			End = Start + End * GetFireDistance();

			FCollisionQueryParams CollisionParams;

			//Tracer
			//const FName TraceTag("MyTraceTag");
			//GetWorld()->DebugDrawTraceTag = TraceTag;
			//CollisionParams.TraceTag = TraceTag;

			CollisionParams.AddIgnoredActor(FPCharacter);
			CollisionParams.AddIgnoredActor(this);
			CollisionParams.bReturnPhysicalMaterial = true;

			if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams))
			{
				if (OutHit.GetActor() != nullptr)
				{
					if (FPCharacter->GetLocalRole() == ROLE_AutonomousProxy)
					{
						ServerApplyDamage(OutHit);
						ApplyDamage(OutHit);
					}
					else if (FPCharacter->GetLocalRole() == ROLE_Authority)
					{
						ApplyDamage(OutHit);
					}
				}

				if(Cast<AFirstPersonCharacter>(OutHit.GetActor()) && DefaultHitSound)
				{
					UGameplayStatics::PlaySound2D(this, DefaultHitSound);
				}

				if (BulletHoleComponent)
				{
					BulletHoleComponent->CreateBulletHole(OutHit);
				}
			}
		}
	}
}

void ARifleBase::Aim()
{
	if (p_FPPlayerControllerRepParams && p_FPPlayerControllerRepParams->IsHoldingToAim())
	{
		AimWithHoldingButton();
	}
	else
	{
		AimWithoutHoldingButton();
	}
}

void ARifleBase::AimWithHoldingButton()
{
	if (bAimingButtonPressed)
	{
		if (!IsAiming())
		{
			bAiming = true;
			OnAiming(true);

			if (AimingTransformController)
				AimingTransformController->SetScaleOfMoving(1);
		}
	}
	else
	{
		if (IsAiming())
		{
			bAiming = false;
			OnAiming(false);

			if (AimingTransformController)
				AimingTransformController->SetScaleOfMoving(0);
		}
	}
}

void ARifleBase::AimWithoutHoldingButton()
{
	if (bAimingButtonPressed)
	{
		if (!bDoOnceAiming)
		{
			bDoOnceAiming = true;

			if (!IsAiming())
			{
				OnAiming(true);
				bAiming = true;

				if (AimingTransformController)
					AimingTransformController->SetScaleOfMoving(1);
			}
		}
		else
		{
			bDoOnceAiming = false;

			if (IsAiming())
			{
				OnAiming(false);
				bAiming = false;

				if (AimingTransformController)
					AimingTransformController->SetScaleOfMoving(0);
			}
		}
	}
}

void ARifleBase::UpdateAbilityToAim()
{
	if (GetMinPriority(EWeaponAction::Aiming).Type == EAbility::Cant)
	{
		if (IsAiming())
		{
			bAiming = false;
			OnAiming(false);

			if (AimingTransformController)
				AimingTransformController->SetScaleOfMoving(0);
		}
	}
	else
	{
		Aim();
	}
}

void ARifleBase::AutoFire()
{
	if (bFireButtonPressed || !bFiredAfterFirstFrame)
	{
		bFiredAfterFirstFrame = true;

		if (MakeFiring())
		{
			if (!IsFiring())
			{
				SetFiring(true);
			}
		}
		else
		{
			if (IsFiring())
			{
				SetFiring(false);
			}
		}
	}
	else
	{
		SetFiring(false);
		GetWorldTimerManager().ClearTimer(FireHandle);
	}
}

void ARifleBase::SingleFire()
{
	if (bFireButtonPressed || !bFiredAfterFirstFrame)
	{
		bFiredAfterFirstFrame = true;

		if (!bSingleFired)
		{
			if (MakeFiring())
			{
				bSingleFired = true;

				if (!IsFiring())
				{
					SetFiring(true);
				}
			}
			else
			{
				GetWorldTimerManager().ClearTimer(FireHandle);
			}

			return;
		}
	}

	if (bSingleFired)
	{
		bSingleFired = false;

		if (IsFiring())
		{
			SetFiring(false);
			GetWorldTimerManager().ClearTimer(FireHandle);
		}
	}
}

void ARifleBase::RangeFire()
{
	if (bFireButtonPressed || !bFiredAfterFirstFrame)
	{
		bFiredAfterFirstFrame = true;

		if (!bRangeFireButtonPressed)
		{
			if (!bRangeFired)
				bRangeFired = true;

			if (!bRangeFireButtonPressed)
				bRangeFireButtonPressed = true;
		}
	}
	else
	{
		if (bRangeFireButtonPressed)
			bRangeFireButtonPressed = false;
	}

	if (bRangeFired)
	{
		if (CurrentAmountRangeShootedBullets < AmountRangeShootingBullets)
		{
			if (MakeFiring())
			{
				CurrentAmountRangeShootedBullets++;

				if (!IsFiring())
				{
					SetFiring(true);
				}
			}
			else
			{
				if (IsFiring())
				{
					SetFiring(false);
					CurrentAmountRangeShootedBullets = 0;
				}

				if (!bRangeFireButtonPressed)
				{
					if (bRangeFired)
						bRangeFired = false;
				}
			}
		}
		else
		{
			CurrentAmountRangeShootedBullets = 0;

			if (bRangeFired)
				bRangeFired = false;

			if (IsFiring())
			{
				SetFiring(false);

				if (bRangeFireButtonPressed)
					bRangeFireButtonPressed = false;

				GetWorldTimerManager().ClearTimer(FireHandle);
			}
		}
	}
}

void ARifleBase::UpdateFOV(float DeltaTime)
{
	if (!bStartFOVInitialized || (!IsInHandsInitialized() && !IsHolstering()))
		return;
	
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (auto Camera = FPCharacter->GetFirstPersonCamera())
		{
			if (IsAiming())
			{
				if (!FMath::IsNearlyEqual(CurrentTimeOfFOVLerp, 1.0f))
				{
					if (TimeFOVLerp > 0)
						CurrentTimeOfFOVLerp = CurrentTimeOfFOVLerp + DeltaTime / TimeFOVLerp;
					else
						CurrentTimeOfFOVLerp = CurrentTimeOfFOVLerp + DeltaTime;

					if (CurrentTimeOfFOVLerp > 1.0f)
					{
						CurrentTimeOfFOVLerp = 1.0f;
					}
				}
			}
			else
			{
				if (!FMath::IsNearlyZero(CurrentTimeOfFOVLerp))
				{
					if (TimeFOVLerp > 0)
						CurrentTimeOfFOVLerp = CurrentTimeOfFOVLerp - DeltaTime / TimeFOVLerp;
					else
						CurrentTimeOfFOVLerp = CurrentTimeOfFOVLerp - DeltaTime;

					if (CurrentTimeOfFOVLerp < 0)
					{
						CurrentTimeOfFOVLerp = 0;
					}
				}
			}
			Camera->SetFieldOfView(FMath::Lerp(StartFOV, AimingFOV, CurrentTimeOfFOVLerp));
		}
	}
}

void ARifleBase::UpdateRecoil(float DeltaTime)
{
	if (IsPureAttack() && WeaponRecoilComponent)
	{		
		WeaponRecoilComponent->UpdateRecoilParameters(1, bAiming);	
	}
	else
	{
		WeaponRecoilComponent->UpdateRecoilParameters(0, bAiming);
	}
}

void ARifleBase::FireWhenRunning(float DeltaTime)
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (FPCharacter->IsRunning() && FPCharacter->GetCurrentVelocity() >= MinSpeedForShootingInRun)
		{
			if (!bDoOnceFirePriorityInitializationInRun && !bFireButtonPressed && !bRangeFired)
			{
				AddPriority(EWeaponAction::Fire, "Run", EAbility::Cant);
				bDoOnceFirePriorityInitializationInRun = true;
			}

			if (bDoOnceFirePriorityInitializationInRun && bFireButtonPressed)
			{
				CurrentTimeToShootInRun += DeltaTime;

				if (CurrentTimeToShootInRun >= TimeToShootInRun)
				{
					RemovePriority(EWeaponAction::Fire, "Run", EAbility::Cant);
					bDoOnceFirePriorityInitializationInRun = false;

					CurrentTimeToShootInRun = 0;

					if (CurrentFireMode == RANGEFIRE)
						bRangeFireButtonPressed = false;

					GetWorldTimerManager().ClearTimer(FireHandle);
					ActivateFiring();
				}
			}
			else
			{
				if (!FMath::IsNearlyZero(CurrentTimeToShootInRun))
					CurrentTimeToShootInRun = 0;
			}

			return;
		}
	}

	if (bDoOnceFirePriorityInitializationInRun)
	{
		CurrentTimeToShootAfterRun += DeltaTime;

		if (CurrentTimeToShootAfterRun >= TimeToShootAfterRun)
		{
			RemovePriority(EWeaponAction::Fire, "Run", EAbility::Cant);
			bDoOnceFirePriorityInitializationInRun = false;
			CurrentTimeToShootAfterRun = 0;

			if(bFireButtonPressed)
			{
				GetWorldTimerManager().ClearTimer(FireHandle);
				ActivateFiring();
			}	
		}

		if (!FMath::IsNearlyZero(CurrentTimeToShootInRun))
			CurrentTimeToShootInRun = 0;
	}
}

void ARifleBase::OnFiring(bool bInFiring)
{
	if(bInFiring == false)
	{
		SetPureAttack(false);
		GetWorldTimerManager().ClearTimer(PureAttackHandle);
	}
	
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (FPCharacter->IsLocallyControlled())
		{
			if (GetLocalRole() == ROLE_Authority)
			{
				MulticastOnFiring(bInFiring);
			}
			else
			{
				ServerOnFiring(bInFiring);
			}
		}

		if (bInFiring)
		{			
			FPCharacter->AddMovementPriority(EMovementType::FastWalk, "Fire");
		}
		else
		{		
			FPCharacter->RemoveMovementPriority(EMovementType::FastWalk, "Fire");
		}

		FiringDispatcher.Broadcast(bInFiring);
	}
}

bool ARifleBase::IsFullReloading() const
{
	return bFullReloading;
}

void ARifleBase::SetFullReloading(bool bInFullReloading)
{
	bFullReloading = bInFullReloading;

	OnFullReloading(bInFullReloading);
}

bool ARifleBase::IsFiring() const
{
	return bFiring;
}

void ARifleBase::SetFiring(bool bInFiring)
{
	bFiring = bInFiring;

	OnFiring(bInFiring);
}

bool ARifleBase::IsReloading() const
{
	return bReloading;
}

void ARifleBase::SetReloading(bool bInReloading)
{
	bReloading = bInReloading;

	OnReloading(bInReloading);
}

bool ARifleBase::IsChangingFireMode() const
{
	return bChangingFireMode;
}

void ARifleBase::SetChangingFireMode(bool bInChangingFireMode)
{
	bChangingFireMode = bInChangingFireMode;

	OnChangingFireMode(bInChangingFireMode);
}

bool ARifleBase::IsLoadingCartridge() const
{
	return bLoadingCartridge;
}

bool ARifleBase::IsStoppedLoadingCartridges() const
{
	return bStopLoadingCartridges;
}

void ARifleBase::AfterHolstering()
{
	Super::AfterHolstering();
	
	/*if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (auto FPMesh = FPCharacter->GetFirstPersonSkeletalMesh())
		{
			if (WeaponClippingComponent)
			{
				if (auto TransformController = WeaponClippingComponent->GetClippingTransformController())
				{
					FPMesh->AddRelativeLocation((-1) * TransformController->GetPassLocation());
					FPMesh->AddRelativeRotation((-1) * TransformController->GetPassRotation());
				}
			}
		}
	}*/

	AimingPriorities.Empty();
	ReloadPriorities.Empty();
	ChangeFirePriorities.Empty();
}

void ARifleBase::ReloadMagazine()
{
	FullFillMagazine();
	MulticastStopMagazineReloading();
}

void ARifleBase::StopMagazineReloading()
{
	RemovePriority(EWeaponAction::Fire, "Reload", EAbility::Cant);
	RemovePriority(EWeaponAction::ChangeFireMode, "Reload", EAbility::Cant);
	RemovePriority(EWeaponAction::Holster, "Reload", EAbility::Cant);

	if (IsReloading())
	{
		SetReloading(false);
	}
	else if (IsFullReloading())
	{
		SetFullReloading(false);
	}
}

void ARifleBase::MulticastStopMagazineReloading_Implementation()
{
	StopMagazineReloading();
}

void ARifleBase::OnRep_CurrentBulletsInMagazine()
{	
	CurrentBulletsInMagazineDispatcher.Broadcast();
}

void ARifleBase::InHandsInitialization()
{
	AFirstPersonCharacter* FPCharacter = Cast<AFirstPersonCharacter>(GetOwner());

	if (FPCharacter && TPInventoryItem)
	{
		auto FPSkeletalMesh = FPCharacter->GetFirstPersonSkeletalMesh();
		auto Camera = FPCharacter->GetFirstPersonCamera();
		WeaponClippingComponent->InitializationParameters(FPCharacter, CurrentEndOfWeaponInWeaponArea);

		if (FPSkeletalMesh)
		{
			if (AimingTransformController)
				AimingTransformController->SetSceneComp(FPSkeletalMesh);

			if (AimingRightInclineTransformController)
				AimingRightInclineTransformController->SetSceneComp(FPSkeletalMesh);

			if (AimingLeftInclineTransformController)
				AimingLeftInclineTransformController->SetSceneComp(FPSkeletalMesh);

			if (RightMovingTransformController)
				RightMovingTransformController->SetSceneComp(FPSkeletalMesh);

			if (LeftMovingTransformController)
				LeftMovingTransformController->SetSceneComp(FPSkeletalMesh);

			if (WeaponRecoilComponent && FPSkeletalMesh)
				WeaponRecoilComponent->InitializationParameters(FPCharacter, FPSkeletalMesh);
		}

		if (Camera)
		{
			StartFOV = Camera->FieldOfView;
			bStartFOVInitialized = true;
		}

		if (WeaponEquipmentType == EEquipmentType::Pistol)
		{	
			if (auto TPInventoryItemMesh = TPInventoryItem->GetTPItemMeshComponent())
			{
				TPInventoryItemMesh->SetVisibility(true);
			}
		}
	}

	Super::InHandsInitialization();
}

void ARifleBase::InHandsDeInitialization()
{
	GetWorldTimerManager().ClearTimer(ReloadingHandle);
	SetReloading(false);
	SetFullReloading(false);

	GetWorldTimerManager().ClearTimer(ChangeFireModeHandle);
	SetChangingFireMode(false);

	AddPriority(EWeaponAction::Aiming, "Holster", EAbility::Cant);
	AddPriority(EWeaponAction::Reload, "Holster", EAbility::Cant);
	AddPriority(EWeaponAction::ChangeFireMode, "Holster", EAbility::Cant);

	Super::InHandsDeInitialization();
}

void ARifleBase::OnBackInitialization( EEquipmentType EquipmentType)
{
	WeaponEquipmentType = EquipmentType;

	if (GetLocalRole() == ROLE_Authority)
	{
		if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
		{
			if (TPInventoryItem)
			{
				if (auto TPCharacterSkeletalMesh = FPCharacter->GetMesh())
				{
					FName SocketName = "NONE";

					if (WeaponEquipmentType == EEquipmentType::FirstFiringWeapon)
					{
						SocketName = FPCharacter->GetFirstFiringWeaponSocketName();
					}
					else if (WeaponEquipmentType == EEquipmentType::SecondFiringWeapon)
					{
						SocketName = FPCharacter->GetSecondFiringWeaponSocketName();
					}
					else if (WeaponEquipmentType == EEquipmentType::Pistol)
					{
						SocketName = FPCharacter->GetSecondFiringWeaponSocketName();
					}

					FAttachmentTransformRules TransformRules(EAttachmentRule::KeepRelative, false);
					TPInventoryItem->AttachToComponent(TPCharacterSkeletalMesh, TransformRules, SocketName);
				}
			}
		}
	}

	Super::OnBackInitialization(EquipmentType);
}

void ARifleBase::ServerOnFiring_Implementation(bool bInFiring)
{
	MulticastOnFiring(bInFiring);
}

void ARifleBase::MulticastOnFiring_Implementation(bool bInFiring)
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (!FPCharacter->IsLocallyControlled())
		{
			OnFiring(bInFiring);
		}
	}
}

void ARifleBase::OnFireButtonPressed_Implementation()
{
	Super::OnFireButtonPressed_Implementation();
		
	ActivateFiring();
}

void ARifleBase::OnAimingButtonPressed_Implementation()
{
	Super::OnAimingButtonPressed_Implementation();

	if (GetMinPriority(EWeaponAction::Aiming).Type == EAbility::Can)
	{
		Aim();
	}
}

void ARifleBase::OnAimingButtonReleased_Implementation()
{
	Super::OnAimingButtonReleased_Implementation();
	
	Aim();
}

void ARifleBase::MagazineReloading()
{
	if (GetCurrentBulletsInMagazine() == 0)
	{
		if(GetLocalRole() == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(ReloadingHandle, this, &ARifleBase::ReloadMagazine, FullReloadingTime, false);
		}
		
		SetFullReloading(true);
	}
	else
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			GetWorldTimerManager().SetTimer(ReloadingHandle, this, &ARifleBase::ReloadMagazine, ReloadingTime, false);
		}
		
		SetReloading(true);
	}
}

void ARifleBase::CartridgeReloading()
{
	if (GetCurrentBulletsInMagazine() == 0)
	{
		SetFullReloading(true);
	}
	else
	{
		SetReloading(true);
	}

	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ARifleBase::StartCartridgeReloading, StartReloadingTime, false);
}

void ARifleBase::StartCartridgeReloading()
{
	if(GetLocalRole() == ROLE_Authority)
	{
		GetWorldTimerManager().SetTimer(ReloadingHandle, this, &ARifleBase::LoadCartridge, ReloadingTime, true);
	}
	
	bLoadingCartridge = true;
}

void ARifleBase::LoadCartridge()
{
	if (CurrentBulletsInMagazine < MaxBulletsInMagazine && GetBulletsInBackPack() > 0 && GetLocalRole() == ROLE_Authority)
	{		
		SetBulletsInBackPack(GetBulletsInBackPack() - 1);
		MulticastSetCurrentBulletsInMagazine(GetCurrentBulletsInMagazine() + 1);
	}

	if(CurrentBulletsInMagazine == MaxBulletsInMagazine || GetBulletsInBackPack() == 0 || bStopLoadingCartridges)
	{
		MulticastStopCartridgeLoading();
	}
}

void ARifleBase::StopCartridgeLoading()
{
	GetWorldTimerManager().ClearTimer(ReloadingHandle);
	bLoadingCartridge = false;

	FTimerHandle TimerHandle;

	if (IsFullReloading())
	{
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ARifleBase::EndCartridgeReloading, FullReloadingTime + EndReloadingTime, false);
	}
	else
	{
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ARifleBase::EndCartridgeReloading, EndReloadingTime, false);
	}
}

void ARifleBase::MulticastStopCartridgeLoading_Implementation()
{
	StopCartridgeLoading();
}

void ARifleBase::OnInclineButton_Implementation(float Value)
{
	if (!FMath::IsNearlyZero(Value, 0.001f) && bAiming)
	{
		if (Value < 0)
		{
			if (AimingRightInclineTransformController->GetPassLocation().IsNearlyZero())
			{
				if (AimingLeftInclineTransformController)
					AimingLeftInclineTransformController->SetScaleOfMoving(1);
			}
			else
			{
				if (AimingRightInclineTransformController)
					AimingRightInclineTransformController->SetScaleOfMoving(0);
			}
		}
		else
		{
			if (AimingLeftInclineTransformController->GetPassLocation().IsNearlyZero())
			{
				if (AimingRightInclineTransformController)
					AimingRightInclineTransformController->SetScaleOfMoving(1);
			}
			else
			{
				if (AimingLeftInclineTransformController)
					AimingLeftInclineTransformController->SetScaleOfMoving(0);
			}
		}
	}
	else
	{
		if (AimingLeftInclineTransformController)
			AimingLeftInclineTransformController->SetScaleOfMoving(0);

		if (AimingRightInclineTransformController)
			AimingRightInclineTransformController->SetScaleOfMoving(0);
	}
}

void ARifleBase::OnReloadButtonPressed_Implementation()
{
	Super::OnReloadButtonPressed_Implementation();
	
	if (GetCurrentBulletsInMagazine() < GetMaxBulletsInMagazine() && GetBulletsInBackPack() > 0
		&& !IsReloading() && !IsFullReloading() && GetMinPriority(EWeaponAction::Reload).Type == EAbility::Can)
	{
		AddPriority(EWeaponAction::Fire, "Reload", EAbility::Cant);
		AddPriority(EWeaponAction::ChangeFireMode, "Reload", EAbility::Cant);
		AddPriority(EWeaponAction::Holster, "Reload", EAbility::Cant);

		if (ReloadingType == EReloadingType::Magazine)
		{
			MagazineReloading();
		}
		else if (ReloadingType == EReloadingType::Cartridge)
		{
			CartridgeReloading();
		}
	}
}

void ARifleBase::OnChangeFireModeButtonPressed_Implementation()
{
	if (GetMinPriority(EWeaponAction::ChangeFireMode).Type == EAbility::Can && !IsChangingFireMode())
	{
		AddPriority(EWeaponAction::Fire, "ChangeFireMode", EAbility::Cant);
		AddPriority(EWeaponAction::Reload, "ChangeFireMode", EAbility::Cant);
		AddPriority(EWeaponAction::Holster, "ChangeFireMode", EAbility::Cant);

		SetChangingFireMode(true);
		GetWorldTimerManager().SetTimer(ChangeFireModeHandle, this, &ARifleBase::IncreaseCurrentFireMode, ChangeFireModeTime, false);
	}
}

void ARifleBase::OnHorizontalMovingButton_Implementation(float Value)
{
	if (!FMath::IsNearlyZero(Value) && bAiming)
	{
		if (Value < 0)
		{
			if (RightMovingTransformController->GetPassLocation().IsNearlyZero(CurrencyChangeWeaponRotationWhenAimingAndHorizontalMoving) && RightMovingTransformController->GetPassRotation().IsNearlyZero(CurrencyChangeWeaponRotationWhenAimingAndHorizontalMoving))
			{
				if (LeftMovingTransformController)
					LeftMovingTransformController->SetScaleOfMoving(fabsf(Value));
			}
			else
			{
				if (RightMovingTransformController)
					RightMovingTransformController->SetScaleOfMoving(0);
			}
		}
		else
		{
			if (LeftMovingTransformController->GetPassLocation().IsNearlyZero(CurrencyChangeWeaponRotationWhenAimingAndHorizontalMoving) && LeftMovingTransformController->GetPassRotation().IsNearlyZero(CurrencyChangeWeaponRotationWhenAimingAndHorizontalMoving))
			{
				if (RightMovingTransformController)
					RightMovingTransformController->SetScaleOfMoving(fabsf(Value));
			}
			else
			{
				if (LeftMovingTransformController)
					LeftMovingTransformController->SetScaleOfMoving(0);
			}
		}
	}
	else
	{
		if (LeftMovingTransformController)
			LeftMovingTransformController->SetScaleOfMoving(0);

		if (RightMovingTransformController)
			RightMovingTransformController->SetScaleOfMoving(0);
	}
}

void ARifleBase::ActivateFiring()
{
	bFiredAfterFirstFrame = false;

	if (!FireHandle.IsValid())
	{
		if (CurrentFireMode == AUTOFIRE)
		{
			GetWorldTimerManager().SetTimer(FireHandle, this, &ARifleBase::AutoFire, AttackTime, true, 0);
		}
		else if (CurrentFireMode == SINGLEFIRE)
		{
			GetWorldTimerManager().SetTimer(FireHandle, this, &ARifleBase::SingleFire, AttackTime, true, 0);
		}
		else if (CurrentFireMode == RANGEFIRE)
		{
			GetWorldTimerManager().SetTimer(FireHandle, this, &ARifleBase::RangeFire, AttackTime, true, 0);
		}
	}
}

float ARifleBase::GetHorizontalRecoil() const
{
	return HorizontalRecoil;
}

void ARifleBase::SetHorizontalRecoil(float HRecoil)
{
	HorizontalRecoil = HRecoil;

	if (WeaponRecoilComponent)
	{
		WeaponRecoilComponent->SetHorizontalRecoil(HorizontalRecoil);
	}
}

float ARifleBase::GetVerticalRecoil() const
{
	return VerticalRecoil;
}

void ARifleBase::SetVerticalRecoil(float VRecoil)
{
	VerticalRecoil = VRecoil;

	if (WeaponRecoilComponent)
	{
		WeaponRecoilComponent->SetVerticalRecoil(VerticalRecoil);
	}
}

float ARifleBase::GetHorizontalSpread() const
{
	return HorizontalSpread;
}

void ARifleBase::SetHorizontalSpread(float HSpread)
{
	HorizontalSpread = HSpread;
}

float ARifleBase::GetVerticalSpread() const
{
	return VerticalSpread;
}

void ARifleBase::SetVerticalSpread(float VSpread)
{
	VerticalSpread = VSpread;
}

void ARifleBase::AddDefaultPriorities()
{
	Super::AddDefaultPriorities();
	
	AddPriority(EWeaponAction::Aiming, "Standart", EAbility::Can);
	AddPriority(EWeaponAction::Reload, "Standart", EAbility::Can);
	AddPriority(EWeaponAction::ChangeFireMode, "Standart", EAbility::Can);
}

void ARifleBase::OnClippingFireInitialization()
{
	if (WeaponClippingComponent)
	{
		if (!WeaponClippingComponent->GetDoOnceAfterPutDownTheWeapon())
		{
			AddPriority(EWeaponAction::Aiming, "Clipping", EAbility::Cant);
		}
	}
}

void ARifleBase::OnPutDownWeaponFireInitialization()
{
	if (WeaponClippingComponent)
	{
		if (WeaponClippingComponent->GetDoOnceAfterPutDownTheWeapon())
			RemovePriority(EWeaponAction::Aiming, "Clipping", EAbility::Cant);

		/*if (bFireButtonPressed)
		{
			GetWorldTimerManager().ClearTimer(FireHandle);
			ActivateFiring();
		}*/

		if(bAimingButtonPressed && !IsHolstering())
		{
			Aim();
		}
	}
}
