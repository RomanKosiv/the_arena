// Fill out your copyright notice in the Description page of Project Settings.


#include "DroppedItem.h"

#include "The_Arena/ActorComponents/MoneyComponent.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"
#include "The_Arena/Actors/InventoryItemObject.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemsManager.h"
#include "The_Arena/GameStates/CustomGameState.h"


// Sets default values
ADroppedItem::ADroppedItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}

void ADroppedItem::Initialization(const UInventoryItemParam* InInventoryItemParam)
{
	if (InInventoryItemParam)
	{
		ItemAmount = InInventoryItemParam->ItemAmount;
		
		auto ModuleItemObjectsLocal = InInventoryItemParam->ModuleItemObjects;
		CreateModuleItems(ModuleItemObjectsLocal);
		ModuleItemObjects.Append(ModuleItemObjectsLocal);
	}

	MulticastEnableReplicatedPhysicsSimulation();
}

void ADroppedItem::CreateModuleItems(TArray<AInventoryItemObject*> ItemObjects)
{
	if (GetLocalRole() != ROLE_Authority)
		return;
	
	for (auto ModuleItemObject : ItemObjects)
	{
		CreateModuleItem(ModuleItemObject);
	}
}

void ADroppedItem::CreateModuleItem(AInventoryItemObject* ItemObject)
{
	if (!ItemObject || GetLocalRole() != ROLE_Authority)
		return;
	
	if (auto ModuleItemsManager = FindComponentByClass<UModuleItemsManager>())
	{
		if (auto CommonParams = ItemObject->GetInventoryItemCommonParams())
		{
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.Owner = this;

			if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
			{
				if (auto DroppedItemsManager = CustomGameState->GetDroppedItemsManager())
				{
					auto DroppedModuleItem = DroppedItemsManager->CreateDroppedItem(CommonParams->GetClassOfDroppedItem(), SpawnParameters, ItemObject->GetInventoryItemParam());

					if (DroppedModuleItem)
					{
						DroppedModuleItem->ModuleItemInitialization();
						
						if (ModuleItemsManager->TryAttachModule(DroppedModuleItem))
						{
							StartUpdatingTransformOnClients();  //Needs updating owner transform on clients too.
						}
						else
						{
							DroppedModuleItem->Destroy();
						}
					}
				}
			}
		}
	}
}

void ADroppedItem::MulticastDisableCollision_Implementation()
{
	DisableCollision();
}

void ADroppedItem::Interact_Implementation(AActor* InteractionCauser)
{
	if (GetLocalRole() != ROLE_Authority)
		return;
	
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(InteractionCauser))
	{	
		if(FVector::Dist(GetActorLocation(), FPCharacter->GetActorLocation()) < CanAddItemDistance)
		{
			AInventoryItemObject* InventoryItemObject = nullptr;

			if (InventoryItemCommonParams)
			{
				if(auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
				{
					if(auto InventoryItemObjectManager = CustomGameState->GetInventoryItemObjectsManager())
					{
						FActorSpawnParameters ActorSpawnParameters;
						ActorSpawnParameters.Owner = InteractionCauser;

						InventoryItemObject = InventoryItemObjectManager->CreateInventoryItemObject(InventoryItemCommonParams->GetClassOfInventoryItemObject(), ActorSpawnParameters, GetInventoryItemParam());

						if (InventoryItemObject)
						{
							InventoryItemObject->Initialization(GetInventoryItemParam());
						}
						else
						{
							return;
						}
					}
				}
			}
			
			if(auto EquipmentComponent = FPCharacter->FindComponentByClass<UEquipmentComponent>())
			{
				auto FirstFiringWeapon = EquipmentComponent->GetEquipment(EEquipmentType::FirstFiringWeapon);
				auto SecondFiringWeapon = EquipmentComponent->GetEquipment(EEquipmentType::SecondFiringWeapon);
				auto Pistol = EquipmentComponent->GetEquipment(EEquipmentType::Pistol);
					
				if(!FirstFiringWeapon.InventoryItemObject)
				{
					auto Equipment = EquipmentComponent->CreateEquipment(InventoryItemObject, EEquipmentType::FirstFiringWeapon, true);

					if (Equipment.InventoryItemObject)
					{
						Destroy();
						return;
					}
				}
					
				if(!SecondFiringWeapon.InventoryItemObject)
				{
					auto Equipment = EquipmentComponent->CreateEquipment(InventoryItemObject, EEquipmentType::SecondFiringWeapon, true);
					
					if (Equipment.InventoryItemObject)
					{
						Destroy();
						return;
					}
				}

				if(!Pistol.InventoryItemObject)
				{
					auto Equipment = EquipmentComponent->CreateEquipment(InventoryItemObject, EEquipmentType::Pistol, true);

					if (Equipment.InventoryItemObject)
					{
						Destroy();
						return;
					}
				}
			}

			if(auto MoneyComponent = FPCharacter->FindComponentByClass<UMoneyComponent>())
			{
				if (MoneyComponent->TryAddItem(InventoryItemObject, true))
				{
					Destroy();
					return;
				}
			}
			
			if (auto InventoryComponent = FPCharacter->FindComponentByClass<UCustomInventoryComponent>())
			{				
				if (InventoryComponent->TryAddItem(InventoryItemObject, true))
				{
					Destroy();
				}
			}
		}					
	}	
}

FString ADroppedItem::GetInteractionText_Implementation()
{
	if(InventoryItemCommonParams)
	{
		return InventoryItemCommonParams->GetItemName();
	}
	
	return FString();
}

bool ADroppedItem::CanInteracted_Implementation() const
{
	return true;
}

UInventoryItemParam* ADroppedItem::GetInventoryItemParam()
{
	if (InventoryItemParam)
	{
		InventoryItemParam->ItemAmount = ItemAmount;
		InventoryItemParam->ModuleItemObjects = ModuleItemObjects;
	}

	return InventoryItemParam;
}

UInventoryItemCommonParams* ADroppedItem::GetInventoryItemCommonParams() const
{
	return InventoryItemCommonParams;
}

UMeshComponent* ADroppedItem::GetDroppedItemMesh() const
{
	return DroppedItemMesh;
}

void ADroppedItem::StartUpdatingTransformOnClients()
{
	if (GetLocalRole() != ROLE_Authority)
		return;

	if(!UpdateTransformHandle.IsValid())
	{
		GetWorldTimerManager().SetTimer(UpdateTransformHandle, this, &ADroppedItem::UpdateTransformOnClients, UpdatingTransformFrequency, true);

		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ADroppedItem::StopUpdatingTransformOnClients, UpdatingTransformTime);
	}
}

void ADroppedItem::StopUpdatingTransformOnClients()
{
	if(UpdateTransformHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(UpdateTransformHandle);
	}
}

void ADroppedItem::UpdateTransformOnClients()
{
	if (GetLocalRole() != ROLE_Authority)
		return;

	MulticastUpdateTransform(GetActorTransform());
}

void ADroppedItem::MulticastEnableReplicatedPhysicsSimulation_Implementation()
{
	EnableReplicatedPhysicsSimulation();
}

void ADroppedItem::ModuleItemInitialization()
{
	bModuleItem = true;
	
	MulticastDisableReplicatedPhysicsSimulation();
	MulticastDisableCollision();

	StartUpdatingTransformOnClients();
}

void ADroppedItem::OnRep_ModuleItem()
{
	DisableReplicatedPhysicsSimulation();
	DisableCollision();
}

// Called when the game starts or when spawned
void ADroppedItem::BeginPlay()
{
	Super::BeginPlay();
	
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &ADroppedItem::StartModuleItemObjectsInitialization, 0.5, true);

	if (GetLocalRole() == ROLE_Authority)
	{
		EnableReplicatedPhysicsSimulation();
	}

	if (bModuleItem)  //For Clients
	{
		DisableCollision();
	}
	
	CreateInventoryItemParam();	
}

// Called every frame
void ADroppedItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CheckOnItemStopping();
}

void ADroppedItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADroppedItem, bModuleItem);
}

void ADroppedItem::StartModuleItemObjectsInitialization()
{
	if (GetLocalRole() != ROLE_Authority || bStartModuleItemObjectsInitialized)
		return;

	if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
	{
		if (auto InventoryItemObjectsManager = CustomGameState->GetInventoryItemObjectsManager())
		{
			for (int i = 0; i < StartModuleItemObjectClasses.Num(); ++i)
			{
				if (auto ModuleItemObjectClass = StartModuleItemObjectClasses[i])
				{
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.Owner = this;
					
					if (auto ModuleItemObject = InventoryItemObjectsManager->CreateInventoryItemObject(ModuleItemObjectClass, SpawnParameters))
					{
						CreateModuleItem(ModuleItemObject);
						ModuleItemObjects.Add(ModuleItemObject);
					}
				}
			}
			
			bStartModuleItemObjectsInitialized = true;
		}
	}
}

void ADroppedItem::CreateInventoryItemParam()
{
	if (!InventoryItemParam)
	{
		InventoryItemParam = NewObject<UInventoryItemParam>();
	}
}

void ADroppedItem::CheckOnItemStopping()
{
	if (DroppedItemMesh && GetLocalRole() == ROLE_Authority)
	{
		if (bItemHitted && !bItemStopped)
		{
			if (FMath::IsNearlyZero(DroppedItemMesh->GetPhysicsAngularVelocityInDegrees().Size(), AngularToleranceDegree) &&
				FMath::IsNearlyZero(DroppedItemMesh->GetPhysicsLinearVelocity().Size(), LinearToleranceVelocity))
			{
				MulticastDisableReplicatedPhysicsSimulation();
				bItemStopped = true;
			}
		}
	}
}

void ADroppedItem::EnableReplicatedPhysicsSimulation()
{
	if (DroppedItemMesh && !bReplicatedPhysicsSimulationEnabled)
	{
		DroppedItemMesh->SetSimulatePhysics(true);
		DroppedItemMesh->bReplicatePhysicsToAutonomousProxy = true;
		DroppedItemMesh->OnComponentHit.AddDynamic(this, &ADroppedItem::OnDroppedItemHit);

		bReplicatedPhysicsSimulationEnabled = true;
	}
}

void ADroppedItem::DisableReplicatedPhysicsSimulation()
{
	if (DroppedItemMesh && bReplicatedPhysicsSimulationEnabled)
	{
		DroppedItemMesh->SetSimulatePhysics(false);
		DroppedItemMesh->bReplicatePhysicsToAutonomousProxy = false;
		DroppedItemMesh->OnComponentHit.RemoveDynamic(this, &ADroppedItem::OnDroppedItemHit);

		bReplicatedPhysicsSimulationEnabled = false;
	}
}

void ADroppedItem::DisableCollision()
{
	if (DroppedItemMesh)
	{
		DroppedItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void ADroppedItem::MulticastUpdateTransform_Implementation(FTransform Transform)
{
	SetActorTransform(Transform, true, nullptr, ETeleportType::ResetPhysics);
}

void ADroppedItem::MulticastDisableReplicatedPhysicsSimulation_Implementation()
{
	DisableReplicatedPhysicsSimulation();
}

void ADroppedItem::OnDroppedItemHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	bItemHitted = true;
}

