// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/InventoryItem.h"
#include "AdditionalEquipment.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AAdditionalEquipment : public AInventoryItem
{
	GENERATED_BODY()
	
};
