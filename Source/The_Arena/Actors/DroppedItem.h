// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "The_Arena/Interfaces/Interactable.h"
#include "The_Arena/UObjects/InventoryItemParams/InventoryItemParam.h"

#include "DroppedItem.generated.h"

class UInventoryItemCommonParams;
class AInventoryItemObject;

UCLASS()
class THE_ARENA_API ADroppedItem : public AActor, public IInteractable
{
	GENERATED_BODY()

		//Methods
public:
	// Sets default values for this actor's properties
	ADroppedItem();

	virtual void Initialization(const UInventoryItemParam* InInventoryItemParam);

	void CreateModuleItems(TArray<AInventoryItemObject*> ItemObjects);
	void CreateModuleItem(AInventoryItemObject* ItemObject);
	
	void Interact_Implementation(AActor* InteractionCauser) override;
	FString GetInteractionText_Implementation() override;
	bool CanInteracted_Implementation() const override;
	
	virtual UInventoryItemParam* GetInventoryItemParam();
	UInventoryItemCommonParams* GetInventoryItemCommonParams() const;

	UMeshComponent* GetDroppedItemMesh() const;

	void ModuleItemInitialization();
	
	UFUNCTION()
	void StartUpdatingTransformOnClients();
	UFUNCTION()
	void StopUpdatingTransformOnClients();

	UFUNCTION()
	void UpdateTransformOnClients();
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastUpdateTransform(FTransform Transform);
	
	UFUNCTION()
	void OnRep_ModuleItem();
	
	void EnableReplicatedPhysicsSimulation();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastEnableReplicatedPhysicsSimulation();
	void DisableReplicatedPhysicsSimulation();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastDisableReplicatedPhysicsSimulation();
	void DisableCollision();
	UFUNCTION(NetMulticast, Reliable)
	void MulticastDisableCollision();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	void StartModuleItemObjectsInitialization();

	virtual void CreateInventoryItemParam();
	void CheckOnItemStopping();
	
	UFUNCTION()
	void OnDroppedItemHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
	//Fields
protected:
	UPROPERTY(BlueprintReadWrite)
	UMeshComponent* DroppedItemMesh;

	UPROPERTY(EditDefaultsOnly, Category = "DeactivationPhysicsSimulation")
	float AngularToleranceDegree = 5.0f;
	UPROPERTY(EditDefaultsOnly, Category = "DeactivationPhysicsSimulation")
	float LinearToleranceVelocity = 5.0f;
	UPROPERTY(EditDefaultsOnly, Category = "UpdatingTransform")
	float UpdatingTransformTime;
	UPROPERTY(EditDefaultsOnly, Category = "UpdatingTransform")
	float UpdatingTransformFrequency;
	UPROPERTY(EditDefaultsOnly, Category = "Interaction")
	float CanAddItemDistance = 300.0f;
	UPROPERTY(EditAnywhere, Category = "InventoryItemObject")
	int ItemAmount = 1;
	UPROPERTY(EditAnywhere, Category = "InventoryItemObject")
	TArray<TSubclassOf<AInventoryItemObject>> StartModuleItemObjectClasses;
	UPROPERTY(EditDefaultsOnly)
	UInventoryItemCommonParams* InventoryItemCommonParams;
	
	UPROPERTY(ReplicatedUsing = OnRep_ModuleItem)
	bool bModuleItem = false;
	
	UPROPERTY()
	UInventoryItemParam* InventoryItemParam;
	UPROPERTY()
	TArray<AInventoryItemObject*> ModuleItemObjects;

	FTimerHandle UpdateTransformHandle;

	bool bReplicatedPhysicsSimulationEnabled = false;
	bool bItemHitted = false;
	bool bItemStopped = false;
	bool bStartModuleItemObjectsInitialized = false;
};
