// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "The_Arena/Interfaces/Interactable.h"

#include "NPC.generated.h"

UCLASS()
class THE_ARENA_API ANPC : public AActor, public IInteractable
{
	GENERATED_BODY()

	//Methods
protected:
	ANPC();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	void Interact_Implementation(AActor* InteractionCauser) override;
	FString GetInteractionText_Implementation() override;
	virtual bool CanInteracted_Implementation() const override;

	//Fields
protected:

};
