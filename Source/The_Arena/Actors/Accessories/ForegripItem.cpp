// Fill out your copyright notice in the Description page of Project Settings.


#include "ForegripItem.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"

void AForegripItem::OnRep_AttachmentReplication()
{
	Super::OnRep_AttachmentReplication();

	ForegripInitialization();
}

void AForegripItem::InitializationOnWeapon()
{
	Super::InitializationOnWeapon();

	if(GetLocalRole() == ROLE_Authority)
	{
		ForegripInitialization();
	}
}

void AForegripItem::DeinitializationFromWeapon()
{
	ForegripDeInitialization();

	Super::DeinitializationFromWeapon();
}

void AForegripItem::ForegripInitialization()
{
	if(!bTestItem)
	{
		if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
		{
			if(FiringWeapon->IsInHandsInitialized())
			{
				LinkForegripAnimLayers();
			}

			if(!FiringWeapon->GetDrawingDispatcher().IsAlreadyBound(this, &AForegripItem::LinkForegripAnimLayersCover))
			{
				FiringWeapon->GetDrawingDispatcher().AddDynamic(this, &AForegripItem::LinkForegripAnimLayersCover);
			}	
		}		
	}	
}

void AForegripItem::ForegripDeInitialization()
{
	if (!bTestItem)
	{
		if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
		{
			if(FiringWeapon->IsInHandsInitialized())
			{
				if (auto FPCharacter = Cast<AFirstPersonCharacter>(FiringWeapon->GetOwner()))
				{
					if (auto FPSkeletalMesh = FPCharacter->GetFirstPersonSkeletalMesh())
					{
						if (auto AnimInstance = FPSkeletalMesh->GetAnimInstance())
						{
							if (AnimInstance->GetLinkedAnimLayerInstanceByClass(FPForegripAnimLayerClass))
							{
								AnimInstance->UnlinkAnimClassLayers(FPForegripAnimLayerClass);
							}
						}
					}

					if (auto SkeletalMesh = FPCharacter->GetMesh())
					{
						if (auto AnimInstance = SkeletalMesh->GetAnimInstance())
						{
							if (AnimInstance->GetLinkedAnimLayerInstanceByClass(TPForegripAnimLayerClass))
							{
								AnimInstance->UnlinkAnimClassLayers(TPForegripAnimLayerClass);
							}
						}
					}
				}
			}
		}
	}
}

void AForegripItem::LinkForegripAnimLayers()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		if (auto FPCharacter = Cast<AFirstPersonCharacter>(FiringWeapon->GetOwner()))
		{
			if (auto FPSkeletalMesh = FPCharacter->GetFirstPersonSkeletalMesh())
			{
				if (auto AnimInstance = FPSkeletalMesh->GetAnimInstance())
				{
					AnimInstance->LinkAnimClassLayers(FPForegripAnimLayerClass);
				}
			}

			if (auto SkeletalMesh = FPCharacter->GetMesh())
			{
				if (auto AnimInstance = SkeletalMesh->GetAnimInstance())
				{
					AnimInstance->LinkAnimClassLayers(TPForegripAnimLayerClass);
				}
			}
		}
	}
}

void AForegripItem::LinkForegripAnimLayersCover(bool bNeedless)
{
	LinkForegripAnimLayers();
}
