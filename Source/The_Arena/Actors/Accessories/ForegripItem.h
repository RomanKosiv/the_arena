// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/Actors/InventoryItem.h"

#include "ForegripItem.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AForegripItem : public AInventoryItem
{
	GENERATED_BODY()

	//Methods	
protected:
	virtual void OnRep_AttachmentReplication() override;
	
	virtual void InitializationOnWeapon() override;
	virtual void DeinitializationFromWeapon() override;

	void ForegripInitialization();
	void ForegripDeInitialization();

	void LinkForegripAnimLayers();
	UFUNCTION()
	void LinkForegripAnimLayersCover(bool bNeedless);

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UAnimInstance> FPForegripAnimLayerClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UAnimInstance> TPForegripAnimLayerClass;
};
