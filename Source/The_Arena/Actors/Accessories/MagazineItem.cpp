// Fill out your copyright notice in the Description page of Project Settings.


#include "MagazineItem.h"

#include "The_Arena/Actors/Weapons/RifleBase.h"

void AMagazineItem::InitializationOnWeapon()
{
	Super::InitializationOnWeapon();

	MagazineInitialization();
}

void AMagazineItem::DeinitializationFromWeapon()
{
	MagazineDeInitialization();
	
	Super::DeinitializationFromWeapon();
}

int AMagazineItem::GetMaxBulletsInMagazine() const
{
	return MaxBulletsInMagazine;
}

void AMagazineItem::MagazineInitialization()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		FiringWeapon->SetMaxBulletsInMagazine(MaxBulletsInMagazine);

		if (!bTestItem)
		{
			FiringWeapon->FullFillMagazine();
		}
	}
}

void AMagazineItem::MagazineDeInitialization()
{
	if (auto FiringWeapon = Cast<ARifleBase>(GetOwner()))
	{
		if(!bTestItem)
		{
			if (GetLocalRole() == ROLE_Authority)
			{
				FiringWeapon->SetBulletsInBackPack(FiringWeapon->GetBulletsInBackPack() + FiringWeapon->GetCurrentBulletsInMagazine());
				FiringWeapon->MulticastSetCurrentBulletsInMagazine(0);
			}

			FiringWeapon->SetMaxBulletsInMagazine(0);
		}	
	}
}
