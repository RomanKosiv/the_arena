// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/InventoryItemObject.h"
#include "MedicineItemObject.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AMedicineItemObject : public AInventoryItemObject
{
	GENERATED_BODY()
	
};
