// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "The_Arena/Actors/TPInventoryItem.h"

#include "InventoryItem.generated.h"

class UInventoryItemCommonParams;
class AInventoryItemObject;

UCLASS()
class THE_ARENA_API AInventoryItem : public AActor
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this actor's properties
	AInventoryItem();

	void Initialization(bool bInTestItem);
	UFUNCTION(NetMulticast, Reliable)
	void MulticastInitialization(bool bInTestItem);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UInventoryItemCommonParams* GetInventoryItemCommonParams() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	UMeshComponent* GetItemMeshComponent() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	TSubclassOf<ATPInventoryItem> GetTPInventoryItemClass() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ATPInventoryItem* GetTPInventoryItem() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsTestItem() const;
	
	template < class T >
	T* GetInventoryItemCommonParams() const
	{
		return Cast<T>(GetInventoryItemCommonParams());
	}
	
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void ModuleInitialization();
	void ModuleDeinitialization();

	virtual void InitializationOnWeapon();
	virtual void DeinitializationFromWeapon();
	
	void CreateTPInventoryItem();

	UFUNCTION()
		void ShowItemMesh();
	UFUNCTION()
		void HideItemMesh();
	
	//Fields
protected:
	UPROPERTY(Replicated, BlueprintReadWrite)
		UMeshComponent* ItemMeshComponent;
	
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ATPInventoryItem> TPInventoryItemClass;
	UPROPERTY(Replicated)
		ATPInventoryItem* TPInventoryItem;
	
	UPROPERTY(EditDefaultsOnly)
	UInventoryItemCommonParams* InventoryItemCommonParams;

	bool bTestItem = false;
};
