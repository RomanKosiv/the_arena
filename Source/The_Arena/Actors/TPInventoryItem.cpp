// Fill out your copyright notice in the Description page of Project Settings.


#include "TPInventoryItem.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/ActorComponents/WeaponModules/ModuleItemsManager.h"


UMeshComponent* ATPInventoryItem::GetTPItemMeshComponent() const
{
	return TPItemMeshComponent;
}

TArray<AActor*> ATPInventoryItem::GetVisibleActors() const
{
	auto VisibleActorsLocal = VisibleActors;
	
	if (auto TPModuleItemsManager = FindComponentByClass<UModuleItemsManager>())
	{
		VisibleActorsLocal += TPModuleItemsManager->GetAttachedModuleItems();
	}

	return VisibleActorsLocal;
}

// Sets default values
ATPInventoryItem::ATPInventoryItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	NetUpdateFrequency = 20.0f;
}

// Called when the game starts or when spawned
void ATPInventoryItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPInventoryItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATPInventoryItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPInventoryItem, TPItemMeshComponent);
	DOREPLIFETIME(ATPInventoryItem, VisibleActors);
}

