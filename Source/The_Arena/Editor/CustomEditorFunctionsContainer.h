// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"

#include "The_Arena/ActorComponents/StaticMeshColorComponent.h"

#include "CustomEditorFunctionsContainer.generated.h"

UCLASS()
class THE_ARENA_API ACustomEditorFunctionsContainer : public AActor
{
	GENERATED_BODY()

	//Methods
public:	
	ACustomEditorFunctionsContainer();
	
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, CallInEditor)
		void AddStaticMeshColorComponent();
	
	//Fields
protected:
	
};
