// Fill out your copyright notice in the Description page of Project Settings.

#include "FirstPersonCharacter.h"
#include "Components/CapsuleComponent.h"

#include "The_Arena/PlayerStates/The_Arena_PlayerState.h"
#include "The_Arena/GameMods/The_Arena_MatchMakingGameMode.h"
#include "The_Arena/Interfaces/InHandsUsable.h"
#include "The_Arena/UObjects/Factories/CharacteristicEffectsManager.h"
#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/Actors/PersonalEquipment/Outfit.h"


bool operator<(const FMovementPriority& Pr1, const FMovementPriority& Pr2)
{
	if (Pr1.Priority > Pr2.Priority)
		return true;

	return false;
}

bool operator==(const FMovementPriority& Pr1, const FMovementPriority& Pr2)
{
	if (Pr1.Type == Pr2.Type && Pr1.Priority == Pr2.Priority && Pr1.Name == Pr2.Name)
		return true;

	return false;
}

// Sets default values
AFirstPersonCharacter::AFirstPersonCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->NavAgentProps.bCanCrouch = true;
}

// Called when the game starts or when spawned
void AFirstPersonCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (auto MovementComponent = GetCharacterMovement())
	{
		MovementComponent->MaxWalkSpeed = SpeedOfFastWalk;
		MovementComponent->MaxWalkSpeedCrouched = SpeedOfSlowWalk;
	}

	CurrentCameraShake = FastWalkCameraShake;

	AddMovementPriority(EMovementType::FastWalk, "FastForwardWalk");
	AddJumpPriority("CanJump");

	if (FPJumpingSequencer)
	{
		FPJumpingSequencer->UpdateLocationDelegate.AddUObject(this, &AFirstPersonCharacter::PauseMoveInJump);
		FPJumpingSequencer->UpdateRotationDelegate.AddUObject(this, &AFirstPersonCharacter::PauseRotateInJump);
	}

	if (FPCrouchSequencer)
	{
		FPCrouchSequencer->UpdateLocationDelegate.AddUObject(this, &AFirstPersonCharacter::PauseMoveInCrouch);
		FPCrouchSequencer->UpdateRotationDelegate.AddUObject(this, &AFirstPersonCharacter::PauseRotateInCrouch);
	}

	if (FPUncrouchSequencer)
	{
		FPUncrouchSequencer->UpdateLocationDelegate.AddUObject(this, &AFirstPersonCharacter::PauseMoveInUncrouch);
		FPUncrouchSequencer->UpdateRotationDelegate.AddUObject(this, &AFirstPersonCharacter::PauseRotateInUncrouch);
	}

	if(auto HealthComponent = FindComponentByClass<UHealthComponent>())
	{
		HealthComponent->GetChangedCurrentStatDispatcher().AddDynamic(this, &AFirstPersonCharacter::OnHealthChanged);

		FTimerHandle Handle;
		GetWorldTimerManager().SetTimer(Handle, this, &AFirstPersonCharacter::OnHealthChanged, 1.0f);
	}

	UpdateDefaultCharacterSkeletalMesh();

	if(GetLocalRole() == ROLE_Authority)
	{
		GetWorldTimerManager().SetTimer(CameraShakeHandle, this, &AFirstPersonCharacter::PlayCameraShake, 0.03, true, 0.0f);
	}
}

void AFirstPersonCharacter::OnRep_Controller()
{
	Super::OnRep_Controller();

	if (IsLocallyControlled() && !UpdateCameraPitchRotationHandle.IsValid())
	{
		GetWorldTimerManager().SetTimer(UpdateCameraPitchRotationHandle, this, &AFirstPersonCharacter::UpdateFPCameraPitchRotation, 0.03, true, 0.0f);
	}
	
	ControllerDispatcher.Broadcast();
}

bool AFirstPersonCharacter::IsCrouchWalking() const
{
	EMovementType Type = GetMinMovementPriority();

	if (Type == EMovementType::CrouchWalk)
		return true;

	return false;
}

bool AFirstPersonCharacter::IsSlowWalking() const
{
	EMovementType Type = GetMinMovementPriority();

	if (Type == EMovementType::SlowWalk)
		return true;

	return false;
}

bool AFirstPersonCharacter::IsFastWalking() const
{
	EMovementType Type = GetMinMovementPriority();

	if (Type == EMovementType::FastWalk)
		return true;

	return false;
}

bool AFirstPersonCharacter::IsRunning() const
{
	EMovementType Type = GetMinMovementPriority();

	if (Type == EMovementType::Run)
		return true;

	return false;
}

bool AFirstPersonCharacter::IsAbleToJump() const
{
	return CanJump() && GetMinJumpPriority() == EAbility::Can;
}

float AFirstPersonCharacter::GetWalkingSpeed() const
{
	return SpeedOfFastWalk;
}

float AFirstPersonCharacter::GetRunningSpeed() const
{
	return RunningSpeed;
}

EMovementCondition AFirstPersonCharacter::GetVerticalMovementCondition() const
{
	return VerticalMovementCondition;
}

EMovementCondition AFirstPersonCharacter::GetHorizontalMovementCondition() const
{
	return HorizontalMovementCondition;
}

FName AFirstPersonCharacter::GetFirstFiringWeaponSocketName() const
{
	return FirstFiringWeaponSocketName;
}

FName AFirstPersonCharacter::GetSecondFiringWeaponSocketName() const
{
	return SecondFiringWeaponSocketName;
}

bool AFirstPersonCharacter::IsAlive() const
{
	if (auto HealthComponent = FindComponentByClass<UHealthComponent>())
	{
		return HealthComponent->IsAlive();
	}

	return false;
}

AActor* AFirstPersonCharacter::GetCurrentHittedInteractableActor() const
{
	return CurrentHittedInteractableActor;
}

UInventoryComponent* AFirstPersonCharacter::GetFPInventoryComponent() const
{
	return FPInventoryComponent;
}

UEquipmentComponent* AFirstPersonCharacter::GetFPEquipmentComponent() const
{
	return FPEquipmentComponent;
}

USkeletalMesh* AFirstPersonCharacter::GetDefaultCharacterSkeletalMesh() const
{
	return DefaultCharacterSkeletalMesh;
}

// Called every frame
void AFirstPersonCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	deltaTime = DeltaTime;
	
	 //lags
	UpdateDeadBodyLocation();

	if (IsLocallyControlled())
	{
		MoveMeshHorizontally(MoveHorizontalValue);
		CheckOnInteraction();
	}
}

void AFirstPersonCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (IsLocallyControlled() && !UpdateCameraPitchRotationHandle.IsValid())
	{
		GetWorldTimerManager().SetTimer(UpdateCameraPitchRotationHandle, this, &AFirstPersonCharacter::UpdateFPCameraPitchRotation, 0.03, true, 0.0f);
	}
}

void AFirstPersonCharacter::UnPossessed()
{
	UnPossess_Controller_Dispatcher.Broadcast(GetController());
	Super::UnPossessed();
}

// Called to bind functionality to input
void AFirstPersonCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AFirstPersonCharacter::MoveVertical);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFirstPersonCharacter::MoveHorizontal);
	PlayerInputComponent->BindAxis("LookUp", this, &AFirstPersonCharacter::LookUp);
	PlayerInputComponent->BindAxis("Turn", this, &AFirstPersonCharacter::TurnRight);
	//PlayerInputComponent->BindAxis("Incline", this, &AFirstPersonCharacter::Incline);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFirstPersonCharacter::Jump);

	PlayerInputComponent->BindAction<FD_Bool>("Crouch", IE_Pressed, this, &AFirstPersonCharacter::CrouchCharacter, true);
	PlayerInputComponent->BindAction<FD_Bool>("Crouch", IE_Released, this, &AFirstPersonCharacter::CrouchCharacter, false);

	PlayerInputComponent->BindAction<FD_Bool>("SlowWalk", IE_Pressed, this, &AFirstPersonCharacter::SlowWalkAction, true);
	PlayerInputComponent->BindAction<FD_Bool>("SlowWalk", IE_Released, this, &AFirstPersonCharacter::SlowWalkAction, false);

	PlayerInputComponent->BindAction<FD_Bool>("Run", IE_Pressed, this, &AFirstPersonCharacter::RunAction, true);
	PlayerInputComponent->BindAction<FD_Bool>("Run", IE_Released, this, &AFirstPersonCharacter::RunAction, false);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFirstPersonCharacter::FireButtonPressed);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AFirstPersonCharacter::FireButtonReleased);

	PlayerInputComponent->BindAction("Aiming", IE_Pressed, this, &AFirstPersonCharacter::AimingButtonPressed);
	PlayerInputComponent->BindAction("Aiming", IE_Released, this, &AFirstPersonCharacter::AimingButtonReleased);
	
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AFirstPersonCharacter::ReloadButtonPressed);
	PlayerInputComponent->BindAction("FireMode", IE_Pressed, this, &AFirstPersonCharacter::ChangeFireModeButtonPressed);

	PlayerInputComponent->BindAction<FD_Bool>("Interact", IE_Pressed, this, &AFirstPersonCharacter::InteractionAction, true);
	PlayerInputComponent->BindAction<FD_Bool>("Interact", IE_Released, this, &AFirstPersonCharacter::InteractionAction, false);

	PlayerInputComponent->BindAction("Slot_0", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_0);
	PlayerInputComponent->BindAction("Slot_1", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_1);
	PlayerInputComponent->BindAction("Slot_2", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_2);
	PlayerInputComponent->BindAction("Slot_3", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_3);
	PlayerInputComponent->BindAction("Slot_4", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_4);
	PlayerInputComponent->BindAction("Slot_5", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_5);
	PlayerInputComponent->BindAction("Slot_6", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_6);
	PlayerInputComponent->BindAction("Slot_7", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_7);
	PlayerInputComponent->BindAction("Slot_8", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_8);
	PlayerInputComponent->BindAction("Slot_9", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);

	PlayerInputComponent->BindAction("InventorySlot_0", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("InventorySlot_1", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("InventorySlot_2", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("InventorySlot_3", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("InventorySlot_4", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("InventorySlot_5", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("InventorySlot_6", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("InventorySlot_7", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);

	PlayerInputComponent->BindAction("EquipmentSlot_0", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("EquipmentSlot_1", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("EquipmentSlot_2", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("EquipmentSlot_3", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("EquipmentSlot_4", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);
	PlayerInputComponent->BindAction("EquipmentSlot_5", IE_Pressed, this, &AFirstPersonCharacter::SlotAction_9);

	PlayerInputComponent->BindAction<FD_EquipmentType>("Melee", IE_Pressed, this, &AFirstPersonCharacter::UseEquipment, EEquipmentType::Melee);
	PlayerInputComponent->BindAction<FD_EquipmentType>("Pistol", IE_Pressed, this, &AFirstPersonCharacter::UseEquipment, EEquipmentType::Pistol);
	PlayerInputComponent->BindAction<FD_EquipmentType>("FirstFiringWeapon", IE_Pressed, this, &AFirstPersonCharacter::UseEquipment, EEquipmentType::FirstFiringWeapon);
	PlayerInputComponent->BindAction<FD_EquipmentType>("SecondFiringWeapon", IE_Pressed, this, &AFirstPersonCharacter::UseEquipment, EEquipmentType::SecondFiringWeapon);
}

float AFirstPersonCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	FHitResult OutResult;
	FVector OutImpulseDir;

	DamageEvent.GetBestHitInfo(this, DamageCauser, OutResult, OutImpulseDir);

	auto DamageType = DamageEvent.DamageTypeClass.GetDefaultObject();

	if (!IsAlive() && EventInstigator && EventInstigator->IsLocalPlayerController())
	{
		ApplyDamageImpulse(OutResult.ImpactNormal * -1 * DamageType->DamageImpulse * 0.2f, OutResult.BoneName, true);
	}

	if (DamageCauser != this && GetLocalRole() == ROLE_Authority)
	{
		if (auto FirstPersonCharacter = Cast<AFirstPersonCharacter>(DamageCauser))
		{
			if (!FirstPersonCharacter->IsAlive() || CheckOnFriendlyFireInArenaGameMode(DamageCauser))
			{
				return DamageAmount;
			}
		}

		if (auto FPController = GetController<AFirstPersonPlayerController>())
		{
			float Rand = FMath::RandRange(0.0f, 1.0f);

			if (Rand <= ChanceToPlayTakeDamageCameraShake)
			{
				FPController->ClientStartCameraShake(TakeDamageCameraShake);
			}		
		}
		
		auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState());

		if(CustomGameState)
		{
			if (auto CharacteristicEffectManager = CustomGameState->GetCharacteristicEffectsManager())
			{
				const float ChanceToCreateBleedingEffect = 0.1f;

				float Rand = FMath::RandRange(0.0f, 1.0f);

				if (Rand <= ChanceToCreateBleedingEffect)
				{
					const float AmountEffectPerSec = -1.0f;
					const float MaxAmountEffectPerSec = -2.0f;

					FCharacteristicEffectParams EffectParams;
					EffectParams.EffectName = "BleedingEffect";
					EffectParams.EffectType = ECharacteristicEffectType::Infinity;

					CharacteristicEffectManager->CreateAccuringEffect(this, MaxAmountEffectPerSec, AmountEffectPerSec, UHealthComponent::StaticClass(), EffectParams);
				}

				FCharacteristicEffectParams EffectParams;
				EffectParams.EffectName = "StopStrongHealingEffect";
				EffectParams.EffectType = ECharacteristicEffectType::Endless;
				EffectParams.ActionTime = 0.2f;

				const auto StrongHealingEffectClass = CharacteristicEffectManager->FindCharacteristicEffectClass("StrongHealingEffect");

				CharacteristicEffectManager->CreateStopCharacteristicEffect(this, StrongHealingEffectClass, EffectParams);
			}
		}
		
		if (auto HealthComponent = FindComponentByClass<UHealthComponent>())
		{
			float CurrentHealth = HealthComponent->GetCurrentStat();

			if (CurrentHealth > 0)
			{
				float Armory = 0.0f;
				
				if(FPEquipmentComponent)
				{
					auto Outfit = FPEquipmentComponent->GetEquipment(EEquipmentType::Outfit);

					if(auto OutfitItem = Cast<AOutfit>(Outfit.InventoryItem))
					{
						Armory = OutfitItem->GetPhysicalProtection();
					}
				}
				
				float FinalHealth = CurrentHealth - (DamageAmount - DamageAmount * Armory);

				if (FinalHealth < 0)
				{
					FinalHealth = 0;
				}

				HealthComponent->SetCurrentStat(FinalHealth);

				if (FMath::IsNearlyZero(FinalHealth))
				{
					MulticastApplyDamageImpulse(OutResult.ImpactNormal * -1 * DamageType->DamageImpulse, OutResult.BoneName, true);
				}
			}
		}
	}

	return DamageAmount;
}

void AFirstPersonCharacter::ApplyDamageImpulse(FVector ImpulseDirection, FName BoneName, bool bVelChange)
{
	if (auto ThirdPersonMesh = GetMesh())
	{
		ThirdPersonMesh->SetSimulatePhysics(true);
		ThirdPersonMesh->SetAnimClass(nullptr);
		DeadBodyLocaiton = ThirdPersonMesh->GetComponentLocation();

		ThirdPersonMesh->AddImpulse(ImpulseDirection, BoneName, bVelChange);
	}

	if (auto FPCapsuleComponent = GetCapsuleComponent())
	{
		//FPCapsuleComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		FPCapsuleComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
		FPCapsuleComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	}
}

void AFirstPersonCharacter::MulticastApplyDamageImpulse_Implementation(FVector ImpulseDirection, FName BoneName,
	bool bVelChange)
{
	ApplyDamageImpulse(ImpulseDirection, BoneName, bVelChange);
}

bool AFirstPersonCharacter::CheckOnFriendlyFireInArenaGameMode(AActor* DamageCauser)
{
	auto ArenaGameMode = GetWorld()->GetAuthGameMode<AThe_Arena_MatchMakingGameMode>();

	if (ArenaGameMode && !ArenaGameMode->IsFriendlyFire())
	{
		if (auto DamageCauserCharacter = Cast<AFirstPersonCharacter>(DamageCauser))
		{
			auto CurrentPlayerState = GetPlayerState<AThe_Arena_PlayerState>();
			auto DamageCauserPlayerState = DamageCauserCharacter->GetPlayerState<AThe_Arena_PlayerState>();

			if (CurrentPlayerState && DamageCauserPlayerState)
			{
				if (CurrentPlayerState->GetCurrentTeam() == DamageCauserPlayerState->GetCurrentTeam())
				{
					return true;
				}
			}
		}
	}

	return false;
}

void AFirstPersonCharacter::OnMoveVertical(float Value)
{
	if (IsLocallyControlled())
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			MulticastOnMoveVertical(Value);
		}
		else
		{
			ServerOnMoveVertical(Value);
		}
	}

	if (FMath::IsNearlyZero(Value))
	{
		VerticalMovementCondition = EMovementCondition::Zero;

		if (FastBackWalkPriorityAdded)
		{
			RemoveMovementPriority(EMovementType::FastWalk, "FastBackWalk");
			FastBackWalkPriorityAdded = false;
		}
	}
	else if (Value > 0)
	{
		VerticalMovementCondition = EMovementCondition::Positive;

		if (FastBackWalkPriorityAdded)
		{
			RemoveMovementPriority(EMovementType::FastWalk, "FastBackWalk");
			FastBackWalkPriorityAdded = false;
		}
	}
	else
	{
		VerticalMovementCondition = EMovementCondition::Negative;

		if (!FastBackWalkPriorityAdded)
		{
			AddMovementPriority(EMovementType::FastWalk, "FastBackWalk");
			FastBackWalkPriorityAdded = true;
		}
	}

	MoveVerticalDispatcher.Broadcast(Value);
	DiagonalRunning();

	UpdateMovement();
}

void AFirstPersonCharacter::ServerOnMoveVertical_Implementation(float Value)
{
	MulticastOnMoveVertical(Value);
}

void AFirstPersonCharacter::OnDeath()
{
	auto LocalController = GetController();
	
	PreDeath_Controller_Dispatcher.Broadcast(LocalController);
	
	bCanInteracted = true;

	ApplyDamageImpulse(FVector::ZeroVector, "NONE", true);

	if(FPSoundComponent)
	{
		FPSoundComponent->PlayDeathSound();
	}

	if(GetLocalRole() == ROLE_Authority)
	{
		if (FPEquipmentComponent)
		{
			FPEquipmentComponent->RemoveEquipmentWithAddedToInventory(EEquipmentType::FirstFiringWeapon);
			FPEquipmentComponent->RemoveEquipmentWithAddedToInventory(EEquipmentType::SecondFiringWeapon);
			FPEquipmentComponent->RemoveEquipmentWithAddedToInventory(EEquipmentType::Pistol);
		}
		
		if (auto FPController = Cast<AFirstPersonPlayerController>(LocalController))
		{
			FPController->StartDeathSpectating();
		}	
	}

	DeathDispatcher.Broadcast();
	Death_Controller_Dispatcher.Broadcast(LocalController);
}

void AFirstPersonCharacter::OnHealthChanged()
{
	if(auto HealthComponent = FindComponentByClass<UHealthComponent>())
	{
		auto CurrentHealth = HealthComponent->GetCurrentStat();

		if(FMath::IsNearlyZero(CurrentHealth))
		{
			TArray<UCharacteristicEffectBase*> CharacteristicEffects;
			GetComponents<UCharacteristicEffectBase>(CharacteristicEffects);

			for (int i = 0; i < CharacteristicEffects.Num(); ++i)
			{
				if (auto CharacteristicEffect = CharacteristicEffects[i])
				{
					CharacteristicEffect->RemoveEffect();
				}
			}		

			OnDeath();
		}
	}
}

void AFirstPersonCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFirstPersonCharacter, FPCameraPitchRotation);
	DOREPLIFETIME(AFirstPersonCharacter, DeadBodyLocaiton);
}

void AFirstPersonCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (UpdateCameraPitchRotationHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(UpdateCameraPitchRotationHandle);
	}
	
	if (CameraShakeHandle.IsValid())
	{
		GetWorldTimerManager().ClearTimer(CameraShakeHandle);
	}
	
	Super::EndPlay(EndPlayReason);
}

void AFirstPersonCharacter::MulticastOnMoveVertical_Implementation(float Value)
{
	if (!IsLocallyControlled())
	{
		OnMoveVertical(Value);
	}
}

void AFirstPersonCharacter::MoveVertical(float Value)
{
	if ((FMath::IsNearlyZero(Value) && VerticalMovementCondition != EMovementCondition::Zero) ||
		(Value > 0 && VerticalMovementCondition != EMovementCondition::Positive) ||
		(Value < 0 && VerticalMovementCondition != EMovementCondition::Negative))
	{
		OnMoveVertical(Value);
	}

	AddMovementInput(GetActorForwardVector(), Value);
}

void AFirstPersonCharacter::MoveHorizontal(float Value)
{
	if (!FMath::IsNearlyEqual(MoveHorizontalValue, Value))
	{
		MoveHorizontalValue = Value;
	}

	if ((FMath::IsNearlyZero(Value) && HorizontalMovementCondition != EMovementCondition::Zero) ||
		(Value > 0 && HorizontalMovementCondition != EMovementCondition::Positive) ||
		(Value < 0 && HorizontalMovementCondition != EMovementCondition::Negative))
	{
		OnMoveHorizontal(Value);
	}

	AddMovementInput(GetActorRightVector(), Value);
}

void AFirstPersonCharacter::OnMoveHorizontal(float Value)
{
	if (IsLocallyControlled())
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			MulticastOnMoveHorizontal(Value);
		}
		else
		{
			ServerOnMoveHorizontal(Value);
		}
	}

	if (FMath::IsNearlyZero(Value))
	{
		HorizontalMovementCondition = EMovementCondition::Zero;

		if (FastRightWalkPriorityAdded)
		{
			RemoveMovementPriority(EMovementType::FastWalk, "FastHorizontalWalk");
			FastRightWalkPriorityAdded = false;
		}
	}
	else if (Value > 0)
	{
		HorizontalMovementCondition = EMovementCondition::Positive;

		if (!FastRightWalkPriorityAdded)
		{
			AddMovementPriority(EMovementType::FastWalk, "FastHorizontalWalk");
			FastRightWalkPriorityAdded = true;
		}
	}
	else
	{
		HorizontalMovementCondition = EMovementCondition::Negative;

		if (!FastRightWalkPriorityAdded)
		{
			AddMovementPriority(EMovementType::FastWalk, "FastHorizontalWalk");
			FastRightWalkPriorityAdded = true;
		}
	}

	MoveHorizontalDispatcher.Broadcast(Value);
	DiagonalRunning();

	UpdateMovement();
}

void AFirstPersonCharacter::ServerOnMoveHorizontal_Implementation(float Value)
{
	MulticastOnMoveHorizontal(Value);
}

void AFirstPersonCharacter::MulticastOnMoveHorizontal_Implementation(float Value)
{
	if (!IsLocallyControlled())
	{
		OnMoveHorizontal(Value);
	}
}

void AFirstPersonCharacter::UpdateMovement()
{
	if (GetMovementComponent()->IsFalling() && !bLanded)
	{
		return;
	}

	EMovementType CurMovement = GetMinMovementPriority();

	switch (CurMovement)
	{
	case EMovementType::CantWalk:
		CantWalk();
		break;
	case EMovementType::CrouchWalk:
		CrouchWalk();
		break;
	case EMovementType::SlowWalk:
		SlowWalk();
		break;
	case EMovementType::FastWalk:
		FastWalk();
		break;
	case EMovementType::Run:
		Run();
		break;
	default:
		break;
	}
}

void AFirstPersonCharacter::LookUp(float Value)
{
	AddControllerPitchInput(Value);

	if (VerticalRotationMesh)
		VerticalRotationMesh->SetScaleOfMoving(Value / deltaTime);
}

void AFirstPersonCharacter::TurnRight(float Value)
{
	AddControllerYawInput(Value);

	if (HorizontalRotationMesh)
		HorizontalRotationMesh->SetScaleOfMoving(Value / deltaTime);
}

void AFirstPersonCharacter::Jump()
{
	if (GetMinJumpPriority() != EAbility::Can)
		return;

	if (this->bIsCrouched)
	{
		//CrouchCharacter(1.0f);
		if (FPPlayerControllerRepParams && FPPlayerControllerRepParams->IsHoldingToCrouch())
		{
			CrouchWithHoldingButton(false);
		}
		else
		{
			CrouchWithoutHoldingButton(true);
		}

		FTimerHandle TimerHandle;
		GetWorldTimerManager().SetTimer(TimerHandle, this, &ACharacter::Jump, 0.001f, false);
	}
	else
	{
		Super::Jump();
	}
}

void AFirstPersonCharacter::OnJumped_Implementation()
{
	/*if(GetLocalRole() == ROLE_Authority)
	{
		MulticastOnJumped_Implementation();     //While I turn off multicast, since it is not needed yet.
	}*/

	Super::OnJumped_Implementation();

	if (FPJumpingSequencer && IsLocallyControlled())
	{
		FPJumpingSequencer->ResetLocationSequence();
		FPJumpingSequencer->ResetRotationSequence();

		FPJumpingSequencer->PlayLocationSequence();
		FPJumpingSequencer->PlayRotationSequence();

		PlayedJumpSequence = true;
	}

	if (FPSoundComponent)
	{
		FPSoundComponent->PlayJumpSound();
	}

	JumpedDispatcher.Broadcast();
}

void AFirstPersonCharacter::MulticastOnJumped_Implementation_Implementation()
{
	if (!IsLocallyControlled() && GetLocalRole() != ROLE_Authority)
	{
		OnJumped_Implementation();
	}
}

void AFirstPersonCharacter::Falling()
{
	Super::Falling();

	if (FPJumpingSequencer && !PlayedFlySequence && !PlayedJumpSequence)
	{
		PlayedFlySequence = true;
		FPJumpingSequencer->SetCurrentLocationIndex(StartFlyLocationIndex);
		FPJumpingSequencer->SetCurrentRotationIndex(StartFlyRotationIndex);
	}

	bLanded = false;
	UpdateMovement();

	FallingDispatcher.Broadcast();
}

void AFirstPersonCharacter::CrouchCharacter(bool bButtonPressed)
{
	if (GetCharacterMovement()->IsFalling())
		return;

	if (FPPlayerControllerRepParams)
	{
		if (FPPlayerControllerRepParams->IsHoldingToCrouch())
		{
			CrouchWithHoldingButton(bButtonPressed);
		}
		else
		{
			CrouchWithoutHoldingButton(bButtonPressed);
		}
	}
}

void AFirstPersonCharacter::CrouchWithHoldingButton(bool bButtonPressed)
{
	if (bButtonPressed)
	{
		if (!bIsCrouched)
		{
			Crouch(true);
		}
	}
	else
	{
		if (bIsCrouched)
		{
			UnCrouch(true);
		}
	}
}

void AFirstPersonCharacter::CrouchWithoutHoldingButton(bool bButtonPressed)
{
	if (bButtonPressed)
	{
		if (!bDoOnceCrouch)
		{
			bDoOnceCrouch = true;

			if (!bIsCrouched)
			{
				Crouch(true);
			}
		}
		else
		{
			bDoOnceCrouch = false;

			if (bIsCrouched)
			{
				UnCrouch(true);
			}
		}
	}
}

void AFirstPersonCharacter::OnStartCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnStartCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	if (CameraCrouchMoving)
		CameraCrouchMoving->SetScaleOfMoving(1);

	AddMovementPriority(EMovementType::CrouchWalk, "Crouch");

	if (IsLocallyControlled())
	{
		if (FPUncrouchSequencer)
		{
			FPUncrouchSequencer->PauseLocationSequence();
			FPUncrouchSequencer->PauseRotationSequence();
			FPUncrouchSequencer->PlayLocationFragmentAt(EndUncrouchLocationIndex + 1);
			FPUncrouchSequencer->PlayRotationFragmentAt(EndUncrouchRotationIndex + 1);
		}

		if (FPCrouchSequencer)
		{
			FPCrouchSequencer->ResetLocationSequence();
			FPCrouchSequencer->ResetRotationSequence();

			FPCrouchSequencer->PlayLocationSequence();
			FPCrouchSequencer->PlayRotationSequence();
		}
	}

	StartCrouchDispatcher.Broadcast();
}

void AFirstPersonCharacter::OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust)
{
	Super::OnEndCrouch(HalfHeightAdjust, ScaledHalfHeightAdjust);

	if (CameraCrouchMoving)
		CameraCrouchMoving->SetScaleOfMoving(0);

	RemoveMovementPriority(EMovementType::CrouchWalk, "Crouch");

	if (IsLocallyControlled())
	{
		if (FPCrouchSequencer)
		{
			FPCrouchSequencer->PauseLocationSequence();
			FPCrouchSequencer->PauseRotationSequence();
			FPCrouchSequencer->PlayLocationFragmentAt(EndCrouchLocationIndex + 1);
			FPCrouchSequencer->PlayRotationFragmentAt(EndCrouchRotationIndex + 1);
		}

		if (FPUncrouchSequencer)
		{
			FPUncrouchSequencer->ResetLocationSequence();
			FPUncrouchSequencer->ResetRotationSequence();

			FPUncrouchSequencer->PlayLocationSequence();
			FPUncrouchSequencer->PlayRotationSequence();
		}
	}

	EndCrouchDispatcher.Broadcast();
}

void AFirstPersonCharacter::SlowWalkAction(bool bButtonPressed)
{
	if (IsLocallyControlled())
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			MulticastSlowWalkAction(bButtonPressed);
		}
		else
		{
			ServerSlowWalkAction(bButtonPressed);
		}
	}

	if (FPPlayerControllerRepParams)
	{
		if (FPPlayerControllerRepParams->IsHoldingToSlowWalk())
		{
			SlowWalkActionWithHoldingButton(bButtonPressed);
		}
		else
		{
			SlowWalkActionWithoutHoldingButton(bButtonPressed);
		}
	}
}

void AFirstPersonCharacter::SlowWalkActionWithHoldingButton(bool bButtonPressed)
{
	if (bButtonPressed)
	{
		if (!bSlowWalkPriorityAdded)
		{
			bSlowWalkPriorityAdded = true;
			AddMovementPriority(EMovementType::SlowWalk, "SlowWalk");

			OnStartSlowWalk();
		}
	}
	else
	{
		if (bSlowWalkPriorityAdded)
		{
			bSlowWalkPriorityAdded = false;
			RemoveMovementPriority(EMovementType::SlowWalk, "SlowWalk");

			OnEndSlowWalk();
		}
	}
}

void AFirstPersonCharacter::SlowWalkActionWithoutHoldingButton(bool bButtonPressed)
{
	if (bButtonPressed)
	{
		if (!bDoOnceSlowWalk)
		{
			bDoOnceSlowWalk = true;

			if (!bSlowWalkPriorityAdded)
			{
				bSlowWalkPriorityAdded = true;
				AddMovementPriority(EMovementType::SlowWalk, "SlowWalk");

				OnStartSlowWalk();
			}
		}
		else
		{
			bDoOnceSlowWalk = false;

			if (bSlowWalkPriorityAdded)
			{
				bSlowWalkPriorityAdded = false;
				RemoveMovementPriority(EMovementType::SlowWalk, "SlowWalk");

				OnEndSlowWalk();
			}
		}
	}
}

void AFirstPersonCharacter::OnStartSlowWalk()
{
	StartSlowWalkDispatcher.Broadcast();
}

void AFirstPersonCharacter::OnEndSlowWalk()
{
	EndSlowWalkDispatcher.Broadcast();
}

void AFirstPersonCharacter::MulticastSlowWalkAction_Implementation(bool bButtonPressed)
{
	if (!IsLocallyControlled())
	{
		SlowWalkAction(bButtonPressed);
	}
}

void AFirstPersonCharacter::ServerSlowWalkAction_Implementation(bool bButtonPressed)
{
	MulticastSlowWalkAction(bButtonPressed);
}

void AFirstPersonCharacter::RunActionWithHoldingButton(bool bButtonPressed)
{
	if (bButtonPressed)
	{
		if (!bRunPriorityAdded)
		{
			bRunPriorityAdded = true;
			AddMovementPriority(EMovementType::Run, "Run");

			OnStartRun();
		}
	}
	else
	{
		if (bRunPriorityAdded)
		{
			bRunPriorityAdded = false;
			RemoveMovementPriority(EMovementType::Run, "Run");

			OnEndRun();
		}
	}
}

void AFirstPersonCharacter::RunActionWithoutHoldingButton(bool bButtonPressed)
{
	if (bButtonPressed)
	{
		if (!bDoOnceRun)
		{
			bDoOnceRun = true;

			if (!bRunPriorityAdded)
			{
				bRunPriorityAdded = true;
				AddMovementPriority(EMovementType::Run, "Run");

				OnStartRun();
			}
		}
		else
		{
			bDoOnceRun = false;

			if (bRunPriorityAdded)
			{
				bRunPriorityAdded = false;
				RemoveMovementPriority(EMovementType::Run, "Run");

				OnEndRun();
			}
		}
	}
}

void AFirstPersonCharacter::OnStartRun()
{
	StartRunDispatcher.Broadcast();
	DiagonalRunning();
}

void AFirstPersonCharacter::OnEndRun()
{
	EndRunDispatcher.Broadcast();
	DiagonalRunning();
}

void AFirstPersonCharacter::DiagonalRunning()
{
	if (VerticalMovementCondition == EMovementCondition::Positive && bRunPriorityAdded)
	{
		if (!DiagonalRunningPriorityAdded)
		{
			AddMovementPriority(EMovementType::Run, "RunHorizontal");
			DiagonalRunningPriorityAdded = true;
		}
	}
	else
	{
		if (DiagonalRunningPriorityAdded)
		{
			RemoveMovementPriority(EMovementType::Run, "RunHorizontal");
			DiagonalRunningPriorityAdded = false;
		}
	}
}

void AFirstPersonCharacter::CantWalk()
{
	auto FPCharacterMovement = GetCharacterMovement();
	if (FPCharacterMovement)
	{
		FPCharacterMovement->MaxWalkSpeed = 0.0f;
		FPCharacterMovement->MaxWalkSpeedCrouched = 0.0f;
	}

	if (FPSoundComponent)
	{
		FPSoundComponent->SetCurrentWalkingMode(EMovementType::CantWalk);
	}
}

void AFirstPersonCharacter::CrouchWalk()
{
	auto FPCharacterMovement = GetCharacterMovement();
	if (FPCharacterMovement && FPCharacterMovement->MaxWalkSpeedCrouched != SpeedOfSlowWalk)
	{
		FPCharacterMovement->MaxWalkSpeedCrouched = SpeedOfSlowWalk;
	}

	if (FPSoundComponent)
	{
		FPSoundComponent->SetCurrentWalkingMode(EMovementType::CrouchWalk);
	}
}

void AFirstPersonCharacter::SlowWalk()
{
	auto FPCharacterMovement = GetCharacterMovement();
	if (FPCharacterMovement && FPCharacterMovement->MaxWalkSpeed != SpeedOfSlowWalk)
	{
		FPCharacterMovement->MaxWalkSpeed = SpeedOfSlowWalk;
	}

	if (FPSoundComponent)
	{
		FPSoundComponent->SetCurrentWalkingMode(EMovementType::SlowWalk);
	}
}

void AFirstPersonCharacter::FastWalk()
{
	auto FPCharacterMovement = GetCharacterMovement();
	if (FPCharacterMovement && FPCharacterMovement->MaxWalkSpeed != SpeedOfFastWalk)
	{
		FPCharacterMovement->MaxWalkSpeed = SpeedOfFastWalk;
	}

	if (FPSoundComponent)
	{
		FPSoundComponent->SetCurrentWalkingMode(EMovementType::FastWalk);
	}
}

void AFirstPersonCharacter::Run()
{
	auto FPCharacterMovement = GetCharacterMovement();
	if (FPCharacterMovement && FPCharacterMovement->MaxWalkSpeed != RunningSpeed)
	{
		FPCharacterMovement->MaxWalkSpeed = RunningSpeed;
	}

	if (FPSoundComponent)
	{
		FPSoundComponent->SetCurrentWalkingMode(EMovementType::Run);
	}
}

void AFirstPersonCharacter::ServerRunAction_Implementation(bool bButtonPressed)
{
	MulticastRunAction(bButtonPressed);
}

void AFirstPersonCharacter::RunAction(bool bButtonPressed)
{
	if (IsLocallyControlled())
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			MulticastRunAction(bButtonPressed);
		}
		else
		{
			ServerRunAction(bButtonPressed);
		}
	}

	if (FPPlayerControllerRepParams)
	{
		if (FPPlayerControllerRepParams->IsHoldingToRun())
		{
			RunActionWithHoldingButton(bButtonPressed);
		}
		else
		{
			RunActionWithoutHoldingButton(bButtonPressed);
		}
	}
}

void AFirstPersonCharacter::MulticastRunAction_Implementation(bool bButtonPressed)
{
	if (!IsLocallyControlled())
	{
		RunAction(bButtonPressed);
	}
}

void AFirstPersonCharacter::Incline(float Value)
{
	if (!FMath::IsNearlyZero(Value, 0.001f))
		bIsIncline = true;
	else
		bIsIncline = false;


	if (FPIncline)
		FPIncline->SetScaleOfMoving(Value);

	if (FPMeshMovingWhenIncline)
		FPMeshMovingWhenIncline->SetScaleOfMoving(Value);

	if (FPEquipmentComponent)
	{
		if (auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnInclineButton(UsingItemInHands, Value);
			}
		}
	}
}

void AFirstPersonCharacter::UpdateFPCameraPitchRotation()
{
	if (FirstPersonCamera && IsLocallyControlled())
	{
		if (GetLocalRole() == ROLE_Authority)
		{
			FPCameraPitchRotation = FirstPersonCamera->GetRelativeRotation().Pitch;
		}
		else
		{
			ServerUpdateFPCameraPitchRotation(FirstPersonCamera->GetRelativeRotation().Pitch);
		}
	}
}

void AFirstPersonCharacter::ServerUpdateFPCameraPitchRotation_Implementation(float Pitch)
{
	FPCameraPitchRotation = Pitch;
}

void AFirstPersonCharacter::AddMovementPriority(EMovementType Type, FString PriorityName)
{
	FMovementPriority MovementPriority = { Type, -1 };

	if (MovementPriorityAsset)
	{
		int Priority = MovementPriorityAsset->GetPriority(PriorityName);

		if (Priority != -1)
		{
			MovementPriority.Priority = Priority;
			MovementPriority.Name = PriorityName;
			MovementPriorities.HeapPush(MovementPriority);
		}

		UpdateMovement();
	}
}

void AFirstPersonCharacter::MulticastAddMovementPriority_Implementation(EMovementType Type, const FString& PriorityName)
{
	AddMovementPriority(Type, PriorityName);
}

bool AFirstPersonCharacter::FindMovementPriority(EMovementType Type, FString PriorityName)
{
	FMovementPriority MovementPriority = { Type, -1 };

	if (MovementPriorityAsset)
	{
		int Priority = MovementPriorityAsset->GetPriority(PriorityName);

		if (Priority != -1)
		{
			MovementPriority.Priority = Priority;
			MovementPriority.Name = PriorityName;
			int PriorityIndex = MovementPriorities.Find(MovementPriority);

			if (PriorityIndex != INDEX_NONE)
			{
				return true;
			}
		}
	}

	return false;
}

void AFirstPersonCharacter::RemoveMovementPriority(EMovementType Type, FString PriorityName)
{
	FMovementPriority MovementPriority = { Type, -1 };

	if (MovementPriorityAsset)
	{
		int Priority = MovementPriorityAsset->GetPriority(PriorityName);

		if (Priority != -1)
		{
			MovementPriority.Priority = Priority;
			MovementPriority.Name = PriorityName;
			int PriorityIndex = MovementPriorities.Find(MovementPriority);
			if (PriorityIndex != INDEX_NONE)
				MovementPriorities.HeapRemoveAt(PriorityIndex);

			UpdateMovement();
		}
	}
}

void AFirstPersonCharacter::MulticastRemoveMovementPriority_Implementation(EMovementType Type, const FString& PriorityName)
{
	RemoveMovementPriority(Type, PriorityName);
}

void AFirstPersonCharacter::AddJumpPriority(FString PriorityName)
{
	if(JumpPriorityDataAsset)
	{
		auto JumpPriority = JumpPriorityDataAsset->GetPriority(PriorityName);

		if(JumpPriority.Ability != EAbility::NONE)
		{
			JumpPriorities.HeapPush(JumpPriority);
		}		
	}
}

void AFirstPersonCharacter::RemoveJumpPriority(FString PriorityName)
{
	if (JumpPriorityDataAsset)
	{
		auto JumpPriority = JumpPriorityDataAsset->GetPriority(PriorityName);

		if (JumpPriority.Ability != EAbility::NONE)
		{
			JumpPriorities.HeapPop(JumpPriority);
		}	
	}
}

EAbility AFirstPersonCharacter::GetMinJumpPriority() const
{
	if (JumpPriorities.Num() > 0)
		return JumpPriorities.HeapTop().Ability;

	FJumpPriorityParam NONE = {"None", 0, EAbility::NONE};
	return NONE.Ability;
}

EMovementType AFirstPersonCharacter::GetMinMovementPriority() const
{
	if (MovementPriorities.Num() > 0)
		return MovementPriorities.HeapTop().Type;

	FMovementPriority NONE = { EMovementType::NONE_MovementType, 0 };
	return NONE.Type;
}

FDMD& AFirstPersonCharacter::GetJumpedDispatcher()
{
	return JumpedDispatcher;
}

FDMD& AFirstPersonCharacter::GetFallingDispatcher()
{
	return FallingDispatcher;
}

FDMD& AFirstPersonCharacter::GetLandedDispatcher()
{
	return LandedDispatcher;
}

FDMD& AFirstPersonCharacter::GetStartCrouchDispatcher()
{
	return StartCrouchDispatcher;
}

FDMD& AFirstPersonCharacter::GetEndCrouchDispatcher()
{
	return EndCrouchDispatcher;
}

FDMD& AFirstPersonCharacter::GetStartSlowWalkDispatcher()
{
	return StartSlowWalkDispatcher;
}

FDMD& AFirstPersonCharacter::GetEndSlowWalkDispatcher()
{
	return EndSlowWalkDispatcher;
}

FDMD& AFirstPersonCharacter::GetStartRunDispatcher()
{
	return StartRunDispatcher;
}

FDMD& AFirstPersonCharacter::GetEndRunDispatcher()
{
	return EndRunDispatcher;
}

FDMD_Float& AFirstPersonCharacter::GetMoveVerticalDispatcher()
{
	return MoveVerticalDispatcher;
}

FDMD_Float& AFirstPersonCharacter::GetMoveHorizontalDispatcher()
{
	return MoveHorizontalDispatcher;
}

FDMD& AFirstPersonCharacter::GetControllerDispatcher()
{
	return ControllerDispatcher;
}

FDMD& AFirstPersonCharacter::GetDeathDispatcher()
{
	return DeathDispatcher;
}

FDMD_C& AFirstPersonCharacter::Get_PreDeath_Controller_Dispatcher()
{
	return PreDeath_Controller_Dispatcher;
}

FDMD_C& AFirstPersonCharacter::Get_Death_Controller_Dispatcher()
{
	return Death_Controller_Dispatcher;
}

FDMD_C& AFirstPersonCharacter::Get_UnPossess_Controller_Dispatcher()
{
	return UnPossess_Controller_Dispatcher;
}

FDMD& AFirstPersonCharacter::GetStartHitInteractionDispatcher()
{
	return StartHitInteractionDispatcher;
}

FDMD& AFirstPersonCharacter::GetUpdateHitInteractionDispatcher()
{
	return UpdateHitInteractionDispatcher;
}

FDMD& AFirstPersonCharacter::GetEndHitInteractionDispatcher()
{
	return EndHitInteractionDispatcher;
}

FDMD_Actor& AFirstPersonCharacter::GetInteractionDispatcher()
{
	return InteractionDispatcher;
}

UPriorityWithOneEqualParamAsset* AFirstPersonCharacter::GetMovementPriorityAsset() const
{
	return MovementPriorityAsset;
}

UFPPlayerControllerRepParams* AFirstPersonCharacter::GetFPPlayerControllerRepParams() const
{
	return FPPlayerControllerRepParams;
}

UCameraComponent* AFirstPersonCharacter::GetFirstPersonCamera() const
{
	return FirstPersonCamera;
}

void AFirstPersonCharacter::SetFirstPersonCamera(UCameraComponent* val)
{
	FirstPersonCamera = val;
}

float AFirstPersonCharacter::GetFPCameraPitchRotation() const
{
	return FPCameraPitchRotation;
}

void AFirstPersonCharacter::FireButtonPressed()
{
	if(FPEquipmentComponent)
	{
		if(auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnFireButtonPressed(UsingItemInHands);
			}	
		}
	}
}

void AFirstPersonCharacter::FireButtonReleased()
{
	if (FPEquipmentComponent)
	{
		if (auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnFireButtonReleased(UsingItemInHands);
			}
		}
	}
}

void AFirstPersonCharacter::ReloadButtonPressed()
{
	if (FPEquipmentComponent)
	{
		if (auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnReloadButtonPressed(UsingItemInHands);
			}
		}
	}
}

void AFirstPersonCharacter::ChangeFireModeButtonPressed()
{
	if (FPEquipmentComponent)
	{
		if (auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnChangeFireModeButtonPressed(UsingItemInHands);
			}
		}
	}
}

void AFirstPersonCharacter::AimingButtonPressed()
{
	if (FPEquipmentComponent)
	{
		if (auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnAimingButtonPressed(UsingItemInHands);
			}
		}
	}
}

void AFirstPersonCharacter::AimingButtonReleased()
{
	if (FPEquipmentComponent)
	{
		if (auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnAimingButtonReleased(UsingItemInHands);
			}
		}
	}
}

void AFirstPersonCharacter::Landed(const FHitResult& Hit)
{
	/*if(GetLocalRole() == ROLE_Authority)
	{
		MulticastLanded(Hit);     //While I turn off multicast, since it is not needed yet.
	}*/

	Super::Landed(Hit);

	if (FPJumpingSequencer && IsLocallyControlled())
	{
		FPJumpingSequencer->SetCurrentLocationIndex(EndFlyLocationIndex + 1);
		FPJumpingSequencer->SetCurrentRotationIndex(EndFlyRotationIndex + 1);
		FPJumpingSequencer->PlayLocationSequence();
		FPJumpingSequencer->PlayRotationSequence();

		PlayedJumpSequence = false;
	}

	if (PlayedFlySequence)
		PlayedFlySequence = false;

	if (FPSoundComponent)
	{
		FPSoundComponent->PlayJumpSound();
	}

	AddJumpPriority("Landed");
	
	FTimerHandle TimerHandle;
	FTimerDelegate TimerDelegate;
	TimerDelegate.BindUObject(this, &AFirstPersonCharacter::RemoveJumpPriority, FString("Landed"));
	GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, DelayBetweenTwoJumps, false);
	
	bLanded = true;
	UpdateMovement();

	if (FPStaminaComponent)
	{
		FPStaminaComponent->RemoveBonusEffect("Jumping");
	}
	
	LandedDispatcher.Broadcast();
}

void AFirstPersonCharacter::MulticastLanded_Implementation(const FHitResult& Hit)
{
	if (!IsLocallyControlled() && GetLocalRole() != ROLE_Authority)
	{
		Landed(Hit);
	}
}

void AFirstPersonCharacter::PauseRotateInJump(int Index)
{
	if (Index == EndFlyRotationIndex && GetCharacterMovement()->IsFalling())
	{
		if (FPJumpingSequencer)
		{
			FPJumpingSequencer->PauseRotationSequence();
			FPJumpingSequencer->PlayRotationFragmentAt(Index);
		}
	}
}

void AFirstPersonCharacter::PauseMoveInJump(int Index)
{
	if (Index == EndFlyLocationIndex && GetCharacterMovement()->IsFalling())
	{
		if (FPJumpingSequencer)
		{
			FPJumpingSequencer->PauseLocationSequence();
			FPJumpingSequencer->PlayLocationFragmentAt(Index);
		}
	}
}

void AFirstPersonCharacter::PauseRotateInCrouch(int Index)
{
	if (Index == EndCrouchRotationIndex && GetCharacterMovement()->IsCrouching())
	{
		if (FPCrouchSequencer)
		{
			FPCrouchSequencer->PauseRotationSequence();
			FPCrouchSequencer->PlayRotationFragmentAt(Index);
		}
	}
}

void AFirstPersonCharacter::PauseMoveInCrouch(int Index)
{
	if (Index == EndCrouchLocationIndex && GetCharacterMovement()->IsCrouching())
	{
		if (FPCrouchSequencer)
		{
			FPCrouchSequencer->PauseLocationSequence();
			FPCrouchSequencer->PlayLocationFragmentAt(Index);
		}
	}
}

void AFirstPersonCharacter::PauseRotateInUncrouch(int Index)
{
	if (Index == EndUncrouchRotationIndex && !GetCharacterMovement()->IsCrouching())
	{
		if (FPUncrouchSequencer)
		{
			FPUncrouchSequencer->PauseRotationSequence();
			FPUncrouchSequencer->PlayRotationFragmentAt(Index);
		}
	}
}

void AFirstPersonCharacter::PauseMoveInUncrouch(int Index)
{
	if (Index == EndUncrouchLocationIndex && !GetCharacterMovement()->IsCrouching())
	{
		if (FPUncrouchSequencer)
		{
			FPUncrouchSequencer->PauseLocationSequence();
			FPUncrouchSequencer->PlayLocationFragmentAt(Index);
		}
	}
}

void AFirstPersonCharacter::UpdateDeadBodyLocation()
{
	if (IsAlive())
		return;

	if(GetLocalRole() == ROLE_Authority)
	{
		if (auto TPMesh = GetMesh())
		{		
			if (CurrentClientDeadBodyLocationUpdateTime >= ClientDeadBodyLocationUpdateFrequency)
			{
				DeadBodyLocaiton = TPMesh->GetComponentLocation();
				CurrentClientDeadBodyLocationUpdateTime = 0.0f;
			}
			else
			{
				CurrentClientDeadBodyLocationUpdateTime += deltaTime;
			}
		}
	}
	else
	{
		if (auto TPMesh = GetMesh())
		{
			if ((TPMesh->GetComponentLocation() - DeadBodyLocaiton).Size() > ToleranceBetweenServerAndClientDeadBodyLocations)
			{
				TPMesh->SetWorldLocation(FMath::VInterpTo(TPMesh->GetComponentLocation(), DeadBodyLocaiton, deltaTime, UpdateDeadBodyLocationInterpSpeed * deltaTime), true, nullptr, ETeleportType::TeleportPhysics);
			}
		}
	}
}

void AFirstPersonCharacter::MoveMeshHorizontally(float Value)
{
	if (!IsLocallyControlled())
	{
		return;
	}

	if (GetCharacterMovement()->IsFalling())
	{
		Value = 0;
	}

	if (HorizontalMovingMesh)
		HorizontalMovingMesh->SetScaleOfMoving(Value);

	if (FPEquipmentComponent)
	{
		if (auto UsingItemInHands = FPEquipmentComponent->GetUsingItemInHands())
		{
			if (auto InHandsUsableItem = Cast<IInHandsUsable>(FPEquipmentComponent->GetUsingItemInHands()))
			{
				InHandsUsableItem->Execute_OnHorizontalMovingButton(UsingItemInHands, Value);
			}
		}
	}
}

FString AFirstPersonCharacter::GetInteractionText_Implementation()
{
	return "Search body";
}

bool AFirstPersonCharacter::CanInteracted_Implementation() const
{
	return bCanInteracted;
}

void AFirstPersonCharacter::CheckOnInteraction()
{
	if (!FirstPersonCamera || !IsAlive())
		return;

	FHitResult OutHit;
	FVector Start = FirstPersonCamera->GetComponentLocation();
	FVector End = Start + FirstPersonCamera->GetForwardVector() * DistanceToInteract;

	FCollisionQueryParams CollisionParams;

	//Tracer
	//const FName TraceTag("MyTraceTag");
	//GetWorld()->DebugDrawTraceTag = TraceTag;
	//CollisionParams.TraceTag = TraceTag;

	CollisionParams.AddIgnoredActor(this);
	CollisionParams.bReturnPhysicalMaterial = true;

	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams))
	{
		if (auto HittedActor = OutHit.GetActor())
		{
			if (HittedActor->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
			{
				if (Execute_CanInteracted(HittedActor))
				{
					if (!CurrentHittedInteractableActor)
					{
						CurrentHittedInteractableActor = HittedActor;
						OnStartHitInteraction();
					}
					else if (CurrentHittedInteractableActor != HittedActor)
					{
						CurrentHittedInteractableActor = HittedActor;
						OnUpdateHitInteraction();
					}

					if (bAllowedToInteract)
					{
						InteractWithHittedActor(HittedActor);
						bAllowedToInteract = false;
					}

					return;
				}
			}
		}
	}

	if (CurrentHittedInteractableActor)
	{
		CurrentHittedInteractableActor = nullptr;
		OnEndHitInteraction();
	}
}

void AFirstPersonCharacter::InteractWithHittedActor(AActor* HittedActor)
{
	if (!HittedActor)
		return;

	if (GetLocalRole() != ROLE_Authority)
	{
		ServerInteractWithHittedActor(HittedActor);
	}

	if(IsAlive())
	{
		Execute_Interact(HittedActor, this);
		OnInteraction(HittedActor);
	}	
}

void AFirstPersonCharacter::ServerInteractWithHittedActor_Implementation(AActor* HittedActor)
{
	InteractWithHittedActor(HittedActor);
}

void AFirstPersonCharacter::InteractionAction(bool bPressed)
{
	if (bPressed)
	{
		bAllowedToInteract = true;
	}
	else
	{
		bAllowedToInteract = false;
	}
}

void AFirstPersonCharacter::OnStartHitInteraction()
{
	StartHitInteractionDispatcher.Broadcast();
}

void AFirstPersonCharacter::OnUpdateHitInteraction()
{
	UpdateHitInteractionDispatcher.Broadcast();
}

void AFirstPersonCharacter::OnEndHitInteraction()
{
	EndHitInteractionDispatcher.Broadcast();
}

void AFirstPersonCharacter::OnInteraction(AActor* HittedActor)
{
	InteractionDispatcher.Broadcast(HittedActor);
}

void AFirstPersonCharacter::SlotAction_0()
{
	ChangeActiveSlot(0);
}

void AFirstPersonCharacter::SlotAction_1()
{
	ChangeActiveSlot(1);
}

void AFirstPersonCharacter::SlotAction_2()
{
	ChangeActiveSlot(2);
}

void AFirstPersonCharacter::SlotAction_3()
{
	ChangeActiveSlot(3);
}

void AFirstPersonCharacter::SlotAction_4()
{
	ChangeActiveSlot(4);
}

void AFirstPersonCharacter::SlotAction_5()
{
	ChangeActiveSlot(5);
}

void AFirstPersonCharacter::SlotAction_6()
{
	ChangeActiveSlot(6);
}

void AFirstPersonCharacter::SlotAction_7()
{
	ChangeActiveSlot(7);
}

void AFirstPersonCharacter::SlotAction_8()
{
	ChangeActiveSlot(8);
}

void AFirstPersonCharacter::SlotAction_9()
{
	ChangeActiveSlot(9);
}

void AFirstPersonCharacter::UseEquipment(EEquipmentType EquipmentType)
{
	if(FPEquipmentComponent)
	{
		FPEquipmentComponent->ServerUseEquipment(EquipmentType);
	}
}

void AFirstPersonCharacter::ChangeActiveSlot(int Slot)
{
	//if (QuickFPInventory)
	//	QuickFPInventory->ChangeActiveSlot(Slot);
}

void AFirstPersonCharacter::PlayCameraShake()
{
	if (GetLocalRole() != ROLE_Authority)
		return;

	TSubclassOf<UMatineeCameraShake> CurCameraShake = nullptr;
	EMovementType CurMovement = GetMinMovementPriority();

	switch (CurMovement)
	{
	case EMovementType::CrouchWalk:
		if (GetCharacterMovement()->Velocity.Size() > CrouchBlockingCameraShakeSpeed)
		{
			CurCameraShake = CrouchCameraShake;
		}
		break;
	case EMovementType::SlowWalk:
		if (GetCharacterMovement()->Velocity.Size() > SlowWalkBlockingCameraShakeSpeed)
		{
			CurCameraShake = SlowWalkCameraShake;
		}
		break;
	case EMovementType::FastWalk:
		if (GetCharacterMovement()->Velocity.Size() > FastWalkBlockingCameraShakeSpeed)
		{
			CurCameraShake = FastWalkCameraShake;
		}
		break;
	case EMovementType::Run:
		if (GetCharacterMovement()->Velocity.Size() > BlockingCameraShakeRunningSpeed)
		{
			CurCameraShake = RunCameraShake;
		}
		break;
	default:
		break;
	}

	if (CurCameraShake && !GetMovementComponent()->IsFalling())
	{
		if(auto FPController = GetController<AFirstPersonPlayerController>())
		{
			FPController->ClientStartCameraShake(CurCameraShake);
		}
	}
}

void AFirstPersonCharacter::UpdateDefaultCharacterSkeletalMesh()
{
	if(auto ArenaPlayerState = GetPlayerState<AThe_Arena_PlayerState>())
	{	
		auto CurrentTeam = ArenaPlayerState->GetCurrentTeam();

		switch (CurrentTeam)
		{
		case EGameTeam::Team_1:
			DefaultCharacterSkeletalMesh = Team_1DefaultSkeletalMesh;
			break;
		case EGameTeam::Team_2:
			DefaultCharacterSkeletalMesh = Team_2DefaultSkeletalMesh;
			break;
		default:
			break;
		}

		if(CurrentTeam == EGameTeam::Team_1 || CurrentTeam == EGameTeam::Team_2)
		{
			bool bOutfitDressed = false;
			
			if(FPEquipmentComponent)
			{
				FEquipment OutfitEquipment = FPEquipmentComponent->GetEquipment(EEquipmentType::Outfit);

				if(OutfitEquipment.InventoryItem)
				{
					bOutfitDressed = true;
				}
			}

			if(!bOutfitDressed)
			{
				if (auto MeshLocal = GetMesh())
				{
					MeshLocal->SetSkeletalMesh(DefaultCharacterSkeletalMesh, false);
				}
			}	
		}

		auto& CurrentTeamDispatcher = ArenaPlayerState->GetCurrentTeamDispatcher();
		if(!CurrentTeamDispatcher.IsAlreadyBound(this, &AFirstPersonCharacter::UpdateDefaultCharacterSkeletalMesh))
		{
			ArenaPlayerState->GetCurrentTeamDispatcher().AddDynamic(this, &AFirstPersonCharacter::UpdateDefaultCharacterSkeletalMesh);
		}
		
		if (UpdateDefaultCharacterSkeletalMeshHandle.IsValid())
		{
			GetWorldTimerManager().ClearTimer(UpdateDefaultCharacterSkeletalMeshHandle);
		}
	}
	else
	{
		if(!UpdateDefaultCharacterSkeletalMeshHandle.IsValid())
		{
			GetWorldTimerManager().SetTimer(UpdateDefaultCharacterSkeletalMeshHandle, this, &AFirstPersonCharacter::UpdateDefaultCharacterSkeletalMesh, 0.1f, true);
		}	
	}
}

USkeletalMeshComponent* AFirstPersonCharacter::GetFirstPersonSkeletalMesh() const
{
	return FirstPersonSkeletonMesh;
}

void AFirstPersonCharacter::SetFirstPersonSkeletalMesh(USkeletalMeshComponent* FPMesh)
{
	FirstPersonSkeletonMesh = FPMesh;
}

UAudioComponent* AFirstPersonCharacter::GetCharacterVoiceComponent() const
{
	return CharacterVoiceComponent;
}

TSubclassOf<UAnimInstance> AFirstPersonCharacter::GetThirdPersonAnimBP() const
{
	return ThirdPersonAnimBP;
}

float AFirstPersonCharacter::GetCurrentVelocity()
{
	if(GetMesh() && GetMesh()->IsSimulatingPhysics())
	{
		return 0.0f;
	}
	
	auto CurVelocity = GetMovementComponent()->Velocity;
	CurVelocity.Z = 0;

	return CurVelocity.Size();
}
