// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "KeptMatchMakingGameModeSettings.generated.h"

class AThe_Arena_MatchMakingGameMode;

UCLASS()
class THE_ARENA_API AKeptMatchMakingGameModeSettings : public AActor
{
	GENERATED_BODY()
	
		//Methods
protected:
	// Sets default values for this actor's properties
	AKeptMatchMakingGameModeSettings();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void CopyParametersFrom(AThe_Arena_MatchMakingGameMode* GameMode);
	void CopyParametersTo(AThe_Arena_MatchMakingGameMode* GameMode);
	
	//Fields
protected:
	bool bGameStarted = false;
	bool bNewGameStarted = false;
	bool bEndMatchTimerEnabled = false;
};
