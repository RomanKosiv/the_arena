// Fill out your copyright notice in the Description page of Project Settings.


#include "KeptMatchMakingGameModeSettings.h"
#include "The_Arena/GameMods/The_Arena_MatchMakingGameMode.h"

// Sets default values
AKeptMatchMakingGameModeSettings::AKeptMatchMakingGameModeSettings()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AKeptMatchMakingGameModeSettings::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKeptMatchMakingGameModeSettings::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKeptMatchMakingGameModeSettings::CopyParametersFrom(AThe_Arena_MatchMakingGameMode* GameMode)
{
	if (!GameMode)
		return;

	if(GameMode->IsNewGameStarted())
	{
		bNewGameStarted = true;
	}
	else
	{
		bGameStarted = GameMode->IsGameStarted();
		bEndMatchTimerEnabled = GameMode->IsEndMatchWithoutPlayersTimerEnabled();
	}
	
}

void AKeptMatchMakingGameModeSettings::CopyParametersTo(AThe_Arena_MatchMakingGameMode* GameMode)
{
	if (!GameMode)
		return;

	if(bNewGameStarted)
	{
		GameMode->SetNewGameStarted(true);
		bNewGameStarted = false;
	}
	else
	{
		GameMode->SetGameStarted(bGameStarted);
		GameMode->SetEndMatchTimerEnabled(bEndMatchTimerEnabled);
	}
}

