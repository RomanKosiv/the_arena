// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"

#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"
#include "The_Arena/GameStates/KeptMatchMakingGameStateSettings.h"
#include "The_Arena/GameMods/KeptMatchMakingGameModeSettings.h"
#include "The_Arena/PlayerControllers/KeptFPControllerSettings.h"

#include "The_Arena_MatchMakingGameMode.generated.h"


namespace MatchState
{
	extern THE_ARENA_API const FName WaitingForPlayers;  //Waiting players. Game is not ready yet. We can move and shoot.
	extern THE_ARENA_API const FName BuyingState;        //Players can buy items, but can't move and play.
	extern THE_ARENA_API const FName EndRound;           //Write data in transition actor and restart the game.
}

/**
 * Please don't try to refactor this class. This class was written by several people in Epic, whose logic is different. 
 * Just solve your problems is this class and don't spend time to imagine how to improve this code.
 */
UCLASS(Config=Game)
class THE_ARENA_API AThe_Arena_MatchMakingGameMode : public AGameMode
{
	GENERATED_BODY()

		//Methods
protected:
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	virtual AActor* FindPlayerStart_Implementation(AController* Player, const FString& IncomingName) override;
	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;
	virtual void HandleSeamlessTravelPlayer(AController*& C) override;
	virtual bool ReadyToStartMatch_Implementation() override;
	virtual bool ReadyToEndMatch_Implementation() override;
	virtual bool PlayerCanRestart_Implementation(APlayerController* Player) override;
	virtual void RestartPlayer(AController* NewPlayer) override;
	virtual void StartPlay() override;
	virtual void EndMatch() override;
	virtual bool HasMatchStarted() const override;
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void InitGameState() override;
	virtual void InitSeamlessTravelPlayer(AController* NewController) override;
	virtual void InitializeHUDForPlayer_Implementation(APlayerController* NewPlayer) override;
	
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void PostSeamlessTravel() override;
	virtual void Logout(AController* Exiting) override;
	virtual void GetSeamlessTravelActorList(bool bToTransition, TArray<AActor*>& ActorList) override;
		
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "Game")
	bool IsWaitingForPlayers() const;
	UFUNCTION(BlueprintCallable, Category = "Game")
	bool IsBuyingState() const;

	UFUNCTION(BlueprintCallable, Category = "Game")
	bool ReadyToWaitForPlayers() const;
	UFUNCTION(BlueprintNativeEvent, Category = "Game")
	bool ReadyToStartBuyingState();
	UFUNCTION(BlueprintNativeEvent, Category = "Game")
	bool ReadyToEndRound();
	
	UFUNCTION()
	void HandleSeamlessTravelGameMode();
	UFUNCTION()
	void HandlePlayerInGameState(AController* Player);

	UFUNCTION()
	void SaveKeptFPControllerSettings(AController* Controller);
	UFUNCTION()
	void LoadKeptFPControllerSettings(APlayerController* PlayerController);
	UFUNCTION()
	void LoadKeptFPControllerSettingsForAllControllers();

	UFUNCTION()
	void SpawnMoneyForPlayer(APlayerController* PlayerController, int Money);
	UFUNCTION()
	void SpawnPistolForPlayer(APlayerController* PlayerController);
	
	UFUNCTION()
	void EnableRestrictionsOnBuyingState();
	UFUNCTION()
	void DisableRestrictionsOnBuyingState();
	UFUNCTION()
	void EnableRestrictionsOnWaitingPlayersState();
	UFUNCTION()
	void DisableRestrictionsOnWaitingPlayersState();

	UFUNCTION()
	void RestartPlayerOnWaitingState(AController* Controller);
	UFUNCTION()
	void DestroyPlayerOnWaitingState(AController* Controller);
	
	UFUNCTION()
	void StartEndMatchWithoutPlayersTimer();
	UFUNCTION()
	void StopEndMatchWithoutPlayersTimer();
	UFUNCTION()
	void EndMatchWithoutPlayers();
public:
	UFUNCTION(BlueprintCallable)
	void InitializationServerParameters(int InBuyingStateDuration, int InRoundDuration, int InEndRoundDuration, int InEndMatchWithoutPlayersDuration, int InRestartServerDuration,
		int InRoundsToWinCount, int InPlayersToStart, bool InbFriendlyFire, int InWaitingPlayersMoney, int InRoundMoney, int InWonRoundMoney);
	
	UFUNCTION()
	void SelectTeam(APlayerController* Controller, EGameTeam Team);

	UFUNCTION(BlueprintCallable, Category = "Game")
	void StartWaitForPlayers();
	UFUNCTION(BlueprintCallable, Category = "Game")
	void StartBuyingState();
	UFUNCTION(BlueprintCallable, Category = "Game")
	void EndRound();
	UFUNCTION(BlueprintCallable)
	void SetEndMatchTimerEnabled(bool Enabled);
	UFUNCTION()
	int GetEndMatchWithoutPlayersDuration() const;
	UFUNCTION()
	TArray<AKeptFPControllerSettings*> GetKeptFPControllerSettingsArray() const;	
	
	UFUNCTION(BlueprintCallable)
	bool IsEndMatchWithoutPlayersTimerEnabled() const;
	UFUNCTION(BlueprintCallable)
	bool IsFriendlyFire() const;
	UFUNCTION(BlueprintCallable)
	bool IsGameStarted() const;
	UFUNCTION(BlueprintCallable)
	void SetGameStarted(bool InGameStarted);
	UFUNCTION(BlueprintCallable)
	bool IsNewGameStarted() const;
	UFUNCTION(BlueprintCallable)
	void SetNewGameStarted(bool InNewGameStarted);
	UFUNCTION(BlueprintCallable)
	bool IsGameOver() const;
	UFUNCTION(BlueprintCallable)
	void SetGameOver(bool bInGameOver);
	//Fields
protected:
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int BuyingStateDuration;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int RoundDuration;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int EndRoundDuration;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int EndMatchWithoutPlayersDuration;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int RestartServerDuration;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int RoundsToWinCount;	
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int PlayersToStart;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	bool bFriendlyFire;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	float TimeForDestroyCharacterAfterDead;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int WaitingPlayersMoney;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int RoundMoney;
	UPROPERTY(GlobalConfig, BlueprintReadOnly, VisibleDefaultsOnly)
	int WonRoundMoney;

	UPROPERTY(EditDefaultsOnly, Category = "WaitingForPlayers")
	float TimeForRestartCharacterAfterDead = 0.0f;

	bool bGameOver = false;
	bool bGameStarted = false;
	bool bNewGameStarted = false;
	bool bEndMatchWithoutPlayersTimerEnabled = false;
	EGameTeam TeamOfLastLeftPlayer;
	FTimerHandle EndMatchWithoutPlayersHandle;

	TArray<TPair<APlayerController*, FTimerHandle>> SpawnMoneyTimers;

	UPROPERTY()
	AKeptMatchMakingGameStateSettings* KeptMatchMakingGameStateSettings;
	UPROPERTY()
	AKeptMatchMakingGameModeSettings* KeptMatchMakingGameModeSettings;
	UPROPERTY()
	TArray<AKeptFPControllerSettings*> KeptFPControllerSettingsArray;
};
