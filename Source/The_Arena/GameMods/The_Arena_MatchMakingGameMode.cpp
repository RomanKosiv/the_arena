// Fill out your copyright notice in the Description page of Project Settings.


#include "The_Arena_MatchMakingGameMode.h"
#include "The_Arena/PlayerStates/The_Arena_PlayerState.h"
#include "The_Arena/GameStates/The_Arena_MatchMakingGameState.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"
#include "The_Arena/ActorComponents/MoneyComponent.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"

#include "GameFramework/PlayerStart.h"
#include "EngineUtils.h"
#include "Engine/PlayerStartPIE.h"


namespace MatchState
{
	const FName WaitingForPlayers = FName(TEXT("WaitingForPlayers"));
	const FName BuyingState = FName(TEXT("BuyingState"));
	const FName EndRound = FName(TEXT("EndRound"));
}

void AThe_Arena_MatchMakingGameMode::StartWaitForPlayers()
{
	SetMatchState(MatchState::WaitingForPlayers);
}

void AThe_Arena_MatchMakingGameMode::StartBuyingState()
{
	DisableRestrictionsOnWaitingPlayersState();
	
	if(!bGameStarted)
	{
		SetGameStarted(true);
		
		if(bNewGameStarted)
		{
			SetNewGameStarted(false);
		}
		
		RestartGame();
	}
	else
	{
		SetMatchState(MatchState::BuyingState);
	}
}

void AThe_Arena_MatchMakingGameMode::EndRound()
{
	SetMatchState(MatchState::EndRound);

	if(auto ArenaGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{		
		if(ArenaGameState->GetWonRounds_Team_1() >= RoundsToWinCount ||
		   ArenaGameState->GetWonRounds_Team_2() >= RoundsToWinCount)
		{
			SetGameOver(true);
	
			if (ArenaGameState->GetWonRounds_Team_1() >= RoundsToWinCount)
			{
				ArenaGameState->SetWonMatchTeam(EGameTeam::Team_1);
			}
			else if (ArenaGameState->GetWonRounds_Team_2() >= RoundsToWinCount)
			{
				ArenaGameState->SetWonMatchTeam(EGameTeam::Team_2);
			}

			for (auto PlayerState : ArenaGameState->PlayerArray)
			{
				if (auto ArenaPlayerState = Cast<AThe_Arena_PlayerState>(PlayerState))
				{
					ArenaPlayerState->SetIsOnlyASpectator(true);
					ArenaPlayerState->SetCurrentTeam(EGameTeam::Spectators);
				}
			}
			
			return;
		}
	}
	
	FTimerHandle Handle;
	GetWorldTimerManager().SetTimer(Handle, this, &AThe_Arena_MatchMakingGameMode::RestartGame, EndRoundDuration);
}

bool AThe_Arena_MatchMakingGameMode::IsEndMatchWithoutPlayersTimerEnabled() const
{
	return bEndMatchWithoutPlayersTimerEnabled;
}

bool AThe_Arena_MatchMakingGameMode::IsFriendlyFire() const
{
	return bFriendlyFire;
}

void AThe_Arena_MatchMakingGameMode::SetEndMatchTimerEnabled(bool Enabled)
{
	bEndMatchWithoutPlayersTimerEnabled = Enabled;
}

bool AThe_Arena_MatchMakingGameMode::ReadyToEndRound_Implementation()
{
	if(IsMatchInProgress())
	{
		if (auto ArenaGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
		{
			auto AlivePlayersInTeam_1 = ArenaGameState->GetAlivePlayersInTeam_1();

			if(AlivePlayersInTeam_1.Num() == 0)
			{
				ArenaGameState->SetWonRounds_Team_2(ArenaGameState->GetWonRounds_Team_2() + 1);
				ArenaGameState->SetWonRoundTeam(EGameTeam::Team_2);
				return true;
			}

			auto AlivePlayersInTeam_2 = ArenaGameState->GetAlivePlayersInTeam_2();

			if(AlivePlayersInTeam_2.Num() == 0)
			{
				ArenaGameState->SetWonRounds_Team_1(ArenaGameState->GetWonRounds_Team_1() + 1);
				ArenaGameState->SetWonRoundTeam(EGameTeam::Team_1);
				return true;
			}

			if (ArenaGameState->ElapsedTime > RoundDuration)
			{
				//return true;   //In future fix this!
			}
		}
	}

	return false;
}

AActor* AThe_Arena_MatchMakingGameMode::ChoosePlayerStart_Implementation(AController* Player)
{	
	AThe_Arena_PlayerState* ArenaPlayerState = nullptr;

	if (Player)
	{
		ArenaPlayerState = Player->GetPlayerState<AThe_Arena_PlayerState>();
	}

	if (ArenaPlayerState)
	{
		// Choose a player start
		APlayerStart* FoundPlayerStart = nullptr;
		UClass* PawnClass = GetDefaultPawnClassForController(Player);
		APawn* PawnToFit = PawnClass ? PawnClass->GetDefaultObject<APawn>() : nullptr;
		TArray<APlayerStart*> UnOccupiedAblePoints;
		TArray<APlayerStart*> OccupiedAblePoints;
		UWorld* World = GetWorld();
		for (TActorIterator<APlayerStart> It(World); It; ++It)
		{
			APlayerStart* PlayerStart = *It;
			
			if (PlayerStart->IsA<APlayerStartPIE>())
			{
				// Always prefer the first "Play from Here" PlayerStart, if we find one while in PIE mode
				FoundPlayerStart = PlayerStart;
				break;
			}
			else
			{
				FVector ActorLocation = PlayerStart->GetActorLocation();
				const FRotator ActorRotation = PlayerStart->GetActorRotation();

				if (ArenaPlayerState->GetCurrentTeamName() == "Spectators")  //Fix #1
				{
					if (!World->EncroachingBlockingGeometry(PawnToFit, ActorLocation, ActorRotation) && PlayerStart->PlayerStartTag == ArenaPlayerState->GetCurrentTeamName())
					{
						UnOccupiedAblePoints.Add(PlayerStart);
					}
					else if (World->FindTeleportSpot(PawnToFit, ActorLocation, ActorRotation) && PlayerStart->PlayerStartTag == ArenaPlayerState->GetCurrentTeamName())
					{
						OccupiedAblePoints.Add(PlayerStart);
					}
				}
				else
				{
					if (!World->EncroachingBlockingGeometry(PawnToFit, ActorLocation, ActorRotation) && !PlayerStart->Tags.Contains("PlayerStartIsUsing") && PlayerStart->PlayerStartTag == ArenaPlayerState->GetCurrentTeamName())
					{
						UnOccupiedAblePoints.Add(PlayerStart);
					}
					else if (World->FindTeleportSpot(PawnToFit, ActorLocation, ActorRotation) && PlayerStart->PlayerStartTag == ArenaPlayerState->GetCurrentTeamName())
					{
						OccupiedAblePoints.Add(PlayerStart);
					}
				}
			}
		}
		
		if (FoundPlayerStart == nullptr)
		{
			if (UnOccupiedAblePoints.Num() > 0)
			{
				FoundPlayerStart = UnOccupiedAblePoints[FMath::RandRange(0, UnOccupiedAblePoints.Num() - 1)];

				if(FoundPlayerStart->PlayerStartTag != "Spectators") //Fix #1
				{
					FoundPlayerStart->Tags.Add("PlayerStartIsUsing"); //Fix #1
				}
				
			}
			else if (OccupiedAblePoints.Num() > 0)
			{
				FoundPlayerStart = OccupiedAblePoints[FMath::RandRange(0, OccupiedAblePoints.Num() - 1)];
				
				if (FoundPlayerStart->PlayerStartTag != "Spectators") //Fix #1
				{
					FoundPlayerStart->Tags.Add("PlayerStartIsUsing"); //Fix #1
				}
			}
		}

		return FoundPlayerStart;
	}

	return nullptr;
}

AActor* AThe_Arena_MatchMakingGameMode::FindPlayerStart_Implementation(AController* Player, const FString& IncomingName)
{	
	UWorld* World = GetWorld();

	// If incoming start is specified, then just use it
	if (!IncomingName.IsEmpty())
	{
		const FName IncomingPlayerStartTag = FName(*IncomingName);
		for (TActorIterator<APlayerStart> It(World); It; ++It)
		{
			APlayerStart* Start = *It;
			if (Start && Start->PlayerStartTag == IncomingPlayerStartTag)
			{
				return Start;
			}
		}
	}

	AActor* BestStart = ChoosePlayerStart(Player);
	if (BestStart == nullptr)
	{
		// No player start found
		UE_LOG(LogGameMode, Log, TEXT("FindPlayerStart: PATHS NOT DEFINED or NO PLAYERSTART with positive rating"));
		
		// This is a bit odd, but there was a complex chunk of code that in the end always resulted in this, so we may as well just 
		// short cut it down to this.  Basically we are saying spawn at 0,0,0 if we didn't find a proper player start
		BestStart = World->GetWorldSettings();
	}

	return BestStart;
}

bool AThe_Arena_MatchMakingGameMode::ReadyToStartMatch_Implementation()
{
	if(GetMatchState() == MatchState::BuyingState)
	{	
		if(auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
		{
			if(ArenaMatchMakingGameState->GetBuyingStateTimePassed() >= BuyingStateDuration)
			{
				return true;
			}
		}
	}

	return false;
}

bool AThe_Arena_MatchMakingGameMode::ReadyToEndMatch_Implementation()
{
	if(auto ArenaGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{
		if (GetMatchState() == MatchState::EndRound && bGameOver && ArenaGameState->GetRestartServerTimePassed() > RestartServerDuration)
		{
			return true;
		}
	}	
	
	return false;
}

void AThe_Arena_MatchMakingGameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	// If players should start as spectators, leave them in the spectator state
	if (!MustSpectate(NewPlayer))
	{
		// If match is in progress, start the player
		if (PlayerCanRestart(NewPlayer))
		{
			bool AlreadyHasPawn = false;

			if(NewPlayer && NewPlayer->GetPawn())
			{
				AlreadyHasPawn = true;
			}
			
			RestartPlayer(NewPlayer);

			if (auto World = GetWorld())
			{	
				if (!AlreadyHasPawn)
				{
					if (auto NewKeptFPControllerSettings = World->SpawnActor<AKeptFPControllerSettings>())
					{
						NewKeptFPControllerSettings->SetPlayerController(NewPlayer);
						KeptFPControllerSettingsArray.Add(NewKeptFPControllerSettings);
					}

					if (bGameStarted)
					{
						if (auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
						{
							if (auto ArenaPlayerState = NewPlayer->GetPlayerState<AThe_Arena_PlayerState>())
							{
								if (ArenaPlayerState->GetCurrentTeam() == ArenaMatchMakingGameState->GetLastWonRoundTeam())
								{
									SpawnMoneyForPlayer(NewPlayer, WonRoundMoney);
								}
								else
								{
									SpawnMoneyForPlayer(NewPlayer, RoundMoney);
								}
							}
						}
					}
				}
			}
		}
		// Check to see if we should start right away, avoids a one frame lag in single player games
		else if (GetMatchState() == MatchState::WaitingToStart)
		{
			// Check to see if we should start the match
			if (ReadyToWaitForPlayers())
			{
				StartWaitForPlayers();
			}
		}
	}
}

bool AThe_Arena_MatchMakingGameMode::PlayerCanRestart_Implementation(APlayerController* Player)
{
	if (HasMatchStarted())
	{
		return false;
	}

	return Super::Super::PlayerCanRestart_Implementation(Player);
}

void AThe_Arena_MatchMakingGameMode::StartPlay()
{
	// Don't call super, this class handles begin play/match start itself

	if (MatchState == MatchState::EnteringMap)
	{
		SetMatchState(MatchState::WaitingToStart);
	}

	// Check to see if we should immediately transfer to match start
	if (MatchState == MatchState::WaitingToStart && ReadyToWaitForPlayers())
	{
		StartWaitForPlayers();
	}
}

void AThe_Arena_MatchMakingGameMode::EndMatch()
{
	SetMatchState(MatchState::WaitingPostMatch);
	SetNewGameStarted(true);

	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		if(auto FPPlayerController = Cast<AFirstPersonPlayerController>(*It))
		{
			FPPlayerController->MulticastSetSelectTeamWidgetInitialized(false);
		}
	}

	RestartGame();
}

void AThe_Arena_MatchMakingGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	//GameState parameters initialization
	for (TActorIterator<AKeptMatchMakingGameStateSettings> KeptSettings(GetWorld()); KeptSettings; ++KeptSettings)
	{
		if(*KeptSettings)
		{
			KeptMatchMakingGameStateSettings = *KeptSettings;
			break;
		}
	}

	if(!KeptMatchMakingGameStateSettings)
	{
		KeptMatchMakingGameStateSettings = GetWorld()->SpawnActor<AKeptMatchMakingGameStateSettings>(FVector::ZeroVector, FRotator::ZeroRotator);
	}
	
	//GameMode parameters initialization
	for (TActorIterator<AKeptMatchMakingGameModeSettings> KeptSettings(GetWorld()); KeptSettings; ++KeptSettings)
	{
		if (*KeptSettings)
		{
			KeptMatchMakingGameModeSettings = *KeptSettings;
			break;
		}
	}

	if (!KeptMatchMakingGameModeSettings)
	{
		KeptMatchMakingGameModeSettings = GetWorld()->SpawnActor<AKeptMatchMakingGameModeSettings>(FVector::ZeroVector, FRotator::ZeroRotator);
	}

	HandleSeamlessTravelGameMode();

	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AThe_Arena_MatchMakingGameMode::LoadKeptFPControllerSettingsForAllControllers, 0.1f, true);
}

void AThe_Arena_MatchMakingGameMode::InitGameState()
{
	Super::InitGameState();

	if(auto ArenaGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{
		ArenaGameState->SetBuyingStateDuration(BuyingStateDuration);
		ArenaGameState->SetRoundDuration(RoundDuration);
		ArenaGameState->SetEndMatchWithoutPlayersDuration(EndMatchWithoutPlayersDuration);
		ArenaGameState->SetRestartServerDuration(RestartServerDuration);
		ArenaGameState->SetGameStarted(bGameStarted);
		ArenaGameState->SetGameOver(bGameOver);
	}
}

void AThe_Arena_MatchMakingGameMode::InitializeHUDForPlayer_Implementation(APlayerController* NewPlayer)
{
	if(auto FPPlayerController = Cast<AFirstPersonPlayerController>(NewPlayer))
	{		
		// Tell client what HUD class to use
		FPPlayerController->ClientSetHUD(HUDClass);
	}
}

void AThe_Arena_MatchMakingGameMode::RestartPlayer(AController* NewPlayer)
{
	auto FPPlayerController = Cast<AFirstPersonPlayerController>(NewPlayer);
	bool AlreadyHasPawn = false;
	
	if (FPPlayerController)
	{	
		FPPlayerController->ClientSetHUD(HUDClass);

		if(FPPlayerController->GetPawn())
		{
			AlreadyHasPawn = true;
		}
	}
	
	Super::RestartPlayer(NewPlayer);

	if(FPPlayerController)
	{
		if(!AlreadyHasPawn)
		{
			if (auto FPCharacter = FPPlayerController->GetPawn<AFirstPersonCharacter>())
			{
				if (auto ArenaGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
				{
					ArenaGameState->AddPlayerStateInAlivePlayersTeam(FPCharacter->GetPlayerState());
					
					FPCharacter->Get_Death_Controller_Dispatcher().AddDynamic(ArenaGameState, &AThe_Arena_MatchMakingGameState::RemovePlayerStateFromAlivePlayersTeam_Cover);
					FPCharacter->Get_Death_Controller_Dispatcher().AddDynamic(this, &AThe_Arena_MatchMakingGameMode::RestartPlayerOnWaitingState);
					FPCharacter->Get_PreDeath_Controller_Dispatcher().AddDynamic(this, &AThe_Arena_MatchMakingGameMode::SaveKeptFPControllerSettings);

					if(ArenaGameState->GetLastWonRoundTeam() == EGameTeam::NONE)
					{
						SpawnPistolForPlayer(FPPlayerController);
					}
				}

				if ((GetMatchState() == MatchState::WaitingForPlayers || GetMatchState() == MatchState::WaitingToStart) && !bGameStarted)
				{
					FPCharacter->Get_PreDeath_Controller_Dispatcher().AddDynamic(this, &AThe_Arena_MatchMakingGameMode::DestroyPlayerOnWaitingState);
					SpawnMoneyForPlayer(FPPlayerController, WaitingPlayersMoney);
				}
			}
		}	
	}
}

bool AThe_Arena_MatchMakingGameMode::IsWaitingForPlayers() const
{
	if (GetMatchState() == MatchState::WaitingForPlayers)
	{
		return true;
	}

	return false;
}

bool AThe_Arena_MatchMakingGameMode::IsBuyingState() const
{
	if (GetMatchState() == MatchState::BuyingState)
	{
		return true;
	}

	return false;
}

bool AThe_Arena_MatchMakingGameMode::ReadyToWaitForPlayers() const
{
	// If bDelayed Start is set, wait for a manual start waiting
	if (bDelayedStart)
	{
		return false;
	}

	// By default start when we have > 0 players
	if (GetMatchState() == MatchState::WaitingToStart)
	{
		if (NumPlayers + NumBots > 0)
		{
			return true;
		}
	}
	
	return false;
}

void AThe_Arena_MatchMakingGameMode::HandleSeamlessTravelGameMode()
{
	if (!KeptMatchMakingGameModeSettings)
		return;

	KeptMatchMakingGameModeSettings->CopyParametersTo(this);
}

void AThe_Arena_MatchMakingGameMode::HandlePlayerInGameState(AController* Player)
{
	AThe_Arena_MatchMakingGameState* ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>();
	AThe_Arena_PlayerState* ArenaMatchMakingPlayerState = nullptr;
	
	if(Player)
	{
		ArenaMatchMakingPlayerState = Player->GetPlayerState<AThe_Arena_PlayerState>();
	}

	if(ArenaMatchMakingGameState && ArenaMatchMakingPlayerState)
	{
		auto Team = ArenaMatchMakingPlayerState->GetCurrentTeam();
		ArenaMatchMakingGameState->AddPlayerStateInTeam(Team, ArenaMatchMakingPlayerState);
	}	
}

void AThe_Arena_MatchMakingGameMode::SaveKeptFPControllerSettings(AController* Controller)
{
	if (!Controller)
		return;
	
	if (auto Result = KeptFPControllerSettingsArray.FindByPredicate([&](AKeptFPControllerSettings* Settings)
		{
			return Settings->GetPlayerController() == Controller;
		}))
	{
		if (auto KeptFPControllerSettings = *Result)
		{
			KeptFPControllerSettings->SavePlayerData();
		}
	}
}

void AThe_Arena_MatchMakingGameMode::LoadKeptFPControllerSettings(APlayerController* PlayerController)
{
	if (!PlayerController)
		return;
	
	for (int i = 0; i < KeptFPControllerSettingsArray.Num(); ++i)
	{
		if (auto KeptFPControllerSettings = KeptFPControllerSettingsArray[i])
		{
			if (KeptFPControllerSettings->GetPlayerController() == PlayerController)
			{
				KeptFPControllerSettings->LoadPlayerData();

				if (KeptFPControllerSettings->IsDataLoaded())
				{
					KeptFPControllerSettingsArray.Remove(KeptFPControllerSettings);
					KeptFPControllerSettings->Destroy();

					SpawnPistolForPlayer(PlayerController);

					return;
				}
			}
		}
	}
}

void AThe_Arena_MatchMakingGameMode::LoadKeptFPControllerSettingsForAllControllers()
{
	for(auto It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		LoadKeptFPControllerSettings(It->Get());
	}
}

void AThe_Arena_MatchMakingGameMode::SpawnMoneyForPlayer(APlayerController* PlayerController, int Money)
{
	if (!PlayerController)
		return;

	if (auto FPCharacter = Cast<AFirstPersonCharacter>(PlayerController->GetPawn()))
	{
		if (auto MoneyComponent = FPCharacter->FindComponentByClass<UMoneyComponent>())
		{
			if(MoneyComponent->HasBegunPlay())
			{
				MoneyComponent->SetMoney(MoneyComponent->GetMoney() + Money);

				if (auto Result = SpawnMoneyTimers.FindByPredicate([&](TPair<APlayerController*, FTimerHandle>& Settings)
					{
						return Settings.Key == PlayerController;
					}))
				{
					auto SpawnMoneyTimer = *Result;
					GetWorldTimerManager().ClearTimer(SpawnMoneyTimer.Value);
					SpawnMoneyTimers.Remove(SpawnMoneyTimer);
				}
			}
			else
			{
				FTimerHandle TimerHandle;
				FTimerDelegate TimerDelegate;
				TimerDelegate.BindUObject(this, &AThe_Arena_MatchMakingGameMode::SpawnMoneyForPlayer, PlayerController, Money);
				GetWorldTimerManager().SetTimer(TimerHandle, TimerDelegate, 0.5f, true);
				SpawnMoneyTimers.Add(TPair<APlayerController*, FTimerHandle> {PlayerController, TimerHandle});
			}
		}
	}
}

void AThe_Arena_MatchMakingGameMode::SpawnPistolForPlayer(APlayerController* PlayerController)
{
	if (!PlayerController)
		return;

	if (auto ArenaGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{
		if (auto InventoryItemObjectsManager = ArenaGameState->GetInventoryItemObjectsManager())
		{
			if (auto Pawn = PlayerController->GetPawn())
			{
				if (auto EquipmentComponent = Pawn->FindComponentByClass<UEquipmentComponent>())
				{
					auto PistolEquipment = EquipmentComponent->GetEquipment(EEquipmentType::Pistol);

					if (!PistolEquipment.InventoryItemObject)
					{
						if (auto InventoryComponent = Pawn->FindComponentByClass<UCustomInventoryComponent>())
						{
							FActorSpawnParameters SpawnParameters;
							SpawnParameters.Owner = Pawn;

							if (auto ItemObject = InventoryItemObjectsManager->CreateInventoryItemObject("9x18 PMM", SpawnParameters))
							{
								ItemObject->SetCurrentItemAmount(60);

								if (!InventoryComponent->TryAddItem(ItemObject, true))
								{
									ItemObject->Destroy();
								}
							}
						}

						FActorSpawnParameters SpawnParameters;
						SpawnParameters.Owner = Pawn;

						if (auto ItemObject = InventoryItemObjectsManager->CreateInventoryItemObject("fort_Inst_1", SpawnParameters))
						{
							auto NewPistolEquipment = EquipmentComponent->CreatePistol(ItemObject, true);

							if (!NewPistolEquipment.InventoryItemObject)
							{
								ItemObject->Destroy();
							}
						}
					}
				}

				return;
			}
		}
	}

	FTimerHandle TimerHandle;
	FTimerDelegate Delegate;
	Delegate.BindUObject(this, &AThe_Arena_MatchMakingGameMode::SpawnPistolForPlayer, PlayerController);
	
	GetWorldTimerManager().SetTimer(TimerHandle, Delegate, 0.2f, false);
}

void AThe_Arena_MatchMakingGameMode::PostLogin(APlayerController* NewPlayer)
{
	if (NewPlayer)
	{
		auto ArenaPlayerState = NewPlayer->GetPlayerState<AThe_Arena_PlayerState>();

		if (ArenaPlayerState)
		{
			auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>();

			if (ArenaMatchMakingGameState)
			{
				ArenaMatchMakingGameState->AddPlayerStateInTeam(EGameTeam::Spectators, ArenaPlayerState);
			}

			ArenaPlayerState->SetIsOnlyASpectator(true);
			ArenaPlayerState->SetCurrentTeam(EGameTeam::Spectators);
		}
	}

	Super::PostLogin(NewPlayer);
}

void AThe_Arena_MatchMakingGameMode::PostSeamlessTravel()
{	
	if(KeptMatchMakingGameStateSettings)
	{
		KeptMatchMakingGameStateSettings->CopyParametersTo(GetGameState<AThe_Arena_MatchMakingGameState>());		
	}

	for (TActorIterator<AKeptFPControllerSettings> It(GetWorld()); It; ++It)
	{
		if(auto KeptFPControllerSettings = *It)
		{	
			KeptFPControllerSettingsArray.Add(KeptFPControllerSettings);
		}
	}
	
	Super::PostSeamlessTravel();
}

void AThe_Arena_MatchMakingGameMode::Logout(AController* Exiting)
{
	if (auto PlayerController = Cast<APlayerController>(Exiting))
	{
		auto ArenaPlayerState = PlayerController->GetPlayerState<AThe_Arena_PlayerState>();

		if (ArenaPlayerState)
		{
			TeamOfLastLeftPlayer = ArenaPlayerState->GetCurrentTeam();
			auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>();

			if (ArenaMatchMakingGameState)
			{
				ArenaMatchMakingGameState->RemovePlayerStateFromTeam(ArenaPlayerState->GetCurrentTeam(), ArenaPlayerState);
			}
		}

		while (auto Result = KeptFPControllerSettingsArray.FindByPredicate([&](AKeptFPControllerSettings* Settings)
			{
				return Settings->GetPlayerController() == PlayerController;
			}))
		{
			if (auto KeptFPControllerSettings = *Result)
			{
				KeptFPControllerSettingsArray.Remove(KeptFPControllerSettings);
				KeptFPControllerSettings->Destroy();
			}
		}
	}
	
	Super::Logout(Exiting);
}

void AThe_Arena_MatchMakingGameMode::GetSeamlessTravelActorList(bool bToTransition, TArray<AActor*>& ActorList)
{
	Super::GetSeamlessTravelActorList(bToTransition, ActorList);

	if (KeptMatchMakingGameModeSettings)
	{
		KeptMatchMakingGameModeSettings->CopyParametersFrom(this);
		ActorList.Add(KeptMatchMakingGameModeSettings);
	}

	if(!bGameOver)
	{
		if (KeptMatchMakingGameStateSettings)
		{
			KeptMatchMakingGameStateSettings->CopyParametersFrom(GetGameState<AThe_Arena_MatchMakingGameState>());
			ActorList.Add(KeptMatchMakingGameStateSettings);
		}

		if(IsGameStarted())
		{
			for(int i = 0; i < KeptFPControllerSettingsArray.Num(); ++i)
			{
				if(auto KeptFPControllerSettings = KeptFPControllerSettingsArray[i])
				{
					KeptFPControllerSettings->SavePlayerData();
				}	
			}
			
			ActorList.Append(KeptFPControllerSettingsArray);
		}
	}
}

void AThe_Arena_MatchMakingGameMode::HandleSeamlessTravelPlayer(AController*& C)
{
	UE_LOG(LogGameMode, Log, TEXT(">> GameMode::HandleSeamlessTravelPlayer: %s "), *C->GetName());

	APlayerController* PC = Cast<APlayerController>(C);

	UClass* PCClassToSpawn = GetPlayerControllerClassToSpawnForSeamlessTravel(PC);

	if (PC && PC->GetClass() != PCClassToSpawn)
	{
		if (PC->Player != nullptr)
		{
			// We need to spawn a new PlayerController to replace the old one
			APlayerController* const NewPC = SpawnPlayerControllerCommon(PC->IsLocalPlayerController() ? ROLE_SimulatedProxy : ROLE_AutonomousProxy, PC->GetFocalLocation(), PC->GetControlRotation(), PCClassToSpawn);
			if (NewPC == nullptr)
			{
				UE_LOG(LogGameMode, Warning, TEXT("Failed to spawn new PlayerController for %s (old class %s)"), *PC->GetHumanReadableName(), *PC->GetClass()->GetName());
				PC->Destroy();
				return;
			}
			else
			{
				PC->SeamlessTravelTo(NewPC);
				NewPC->SeamlessTravelFrom(PC);
				SwapPlayerControllers(PC, NewPC);
				PC = NewPC;
				C = NewPC;
			}
		}
		else
		{
			PC->Destroy();
		}
	}
	else
	{
		// clear out data that was only for the previous game
		C->PlayerState->Reset();
		// create a new PlayerState and copy over info; this is necessary because the old GameMode may have used a different PlayerState class
		APlayerState* OldPlayerState = C->PlayerState;
		C->InitPlayerState();
		OldPlayerState->SeamlessTravelTo(C->PlayerState);
		// we don"t need the old PlayerState anymore
		//@fixme: need a way to replace PlayerStates that doesn't cause incorrect "player left the game"/"player entered the game" messages
		OldPlayerState->Destroy();

		if(bNewGameStarted)
		{
			if (auto ArenaPlayerState = C->GetPlayerState<AThe_Arena_PlayerState>())
			{
				ArenaPlayerState->SetCurrentTeam(EGameTeam::Spectators);
			}
		}	
	}
	
	HandlePlayerInGameState(C);
	
	InitSeamlessTravelPlayer(C);
	
	// Initialize hud and other player details, shared with PostLogin
	GenericPlayerInitialization(C);
	
	if (auto FPPlayerController = Cast<AFirstPersonPlayerController>(C))
	{
		if(bNewGameStarted)
		{		
			FPPlayerController->Reset();
			FPPlayerController->ClientReset();
		}
		else
		{
			/*auto Pawn = FPPlayerController->GetPawn();
			
			if(Pawn != nullptr)
			{
				FPPlayerController->UnPossess();
				FPPlayerController->ClientRemoveAllWidgets();

				if (auto ArenaGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
				{
					ArenaGameState->RemovePlayerStateFromAlivePlayersTeam(FPPlayerController->PlayerState);
				}
				
				Pawn->Destroy();
			}	*/	
			
			// This may spawn the player pawn if the game is in progress
			HandleStartingNewPlayer(FPPlayerController);
		}				
	}

	UE_LOG(LogGameMode, Log, TEXT("<< GameMode::HandleSeamlessTravelPlayer: %s"), *C->GetName());
}

void AThe_Arena_MatchMakingGameMode::InitSeamlessTravelPlayer(AController* NewController)
{
	AFirstPersonPlayerController* FPPlayerController = Cast<AFirstPersonPlayerController>(NewController);

	if (FPPlayerController != nullptr)
	{		
		FPPlayerController->PostSeamlessTravel();
		
		if (MustSpectate(FPPlayerController))
		{
			FPPlayerController->StartSpectatingOnly();
			FPPlayerController->ClientGotoState(NAME_Spectating);
			
			FPPlayerController->ClientSpectatorPawnLocationAndRotationInitialization();
		}
		else
		{
			NumPlayers++;
			NumTravellingPlayers--;
		}
	}
	else
	{
		NumBots++;
	}
}

bool AThe_Arena_MatchMakingGameMode::HasMatchStarted() const
{
	if (GetMatchState() == MatchState::EnteringMap || GetMatchState() == MatchState::WaitingToStart || GetMatchState() == MatchState::WaitingForPlayers || GetMatchState() == MatchState::BuyingState)
	{
		return false;
	}

	return true;
}

void AThe_Arena_MatchMakingGameMode::Tick(float DeltaSeconds)
{
	Super::Super::Tick(DeltaSeconds);

	if (GetMatchState() == MatchState::WaitingToStart)
	{
		// Check to see if we should start the match
		if (ReadyToWaitForPlayers())
		{
			UE_LOG(LogGameMode, Log, TEXT("GameMode returned ReadyToWaitForPlayers"));
			StartWaitForPlayers();
		}
	}
	if (GetMatchState() == MatchState::WaitingForPlayers)
	{
		if (IsGameStarted())
		{
			EnableRestrictionsOnWaitingPlayersState();
		}

		// Check to see if we should start the match
		if (ReadyToStartBuyingState())
		{
			UE_LOG(LogGameMode, Log, TEXT("GameMode returned ReadyToStartBuyingState"));
			StartBuyingState();
		}
	}
	if (GetMatchState() == MatchState::BuyingState)
	{
		DisableRestrictionsOnWaitingPlayersState();
		EnableRestrictionsOnBuyingState();
		
		// Check to see if we should start the match
		if (ReadyToStartMatch())
		{
			UE_LOG(LogGameMode, Log, TEXT("GameMode returned ReadyToStartMatch"));
			StartMatch();
		}
	}
	if (GetMatchState() == MatchState::InProgress)
	{
		DisableRestrictionsOnBuyingState();
		
		// Check to see if we should start the match
		if (ReadyToEndRound())
		{
			UE_LOG(LogGameMode, Log, TEXT("GameMode returned ReadyToEndRound"));
			EndRound();
		}
	}
	if(GetMatchState() == MatchState::EndRound)
	{
		// Check to see if we should start the match
		if (ReadyToEndMatch())
		{
			UE_LOG(LogGameMode, Log, TEXT("GameMode returned ReadyToEndMatch"));
			EndMatch();
		}
	}
}

void AThe_Arena_MatchMakingGameMode::EnableRestrictionsOnBuyingState()
{
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AController* Controller = It->Get();
		AFirstPersonCharacter* Character = nullptr;
		ARifleBase* Rifle = nullptr;
		UEquipmentComponent* EquipmentComponent = nullptr;
		
		if(Controller)
		{
			Character = Controller->GetPawn<AFirstPersonCharacter>();
		}

		if(Character)
		{
			EquipmentComponent = Character->GetFPEquipmentComponent();

			/*if(!Character->FindMovementPriority(EMovementType::CantWalk, "BuyingStatePriority"))
			{
				Character->MulticastAddMovementPriority(EMovementType::CantWalk, "BuyingStatePriority");		
			}*/	
		}

		if(EquipmentComponent)
		{
			Rifle = Cast<ARifleBase>(EquipmentComponent->GetUsingItemInHands());
		}

		if(Rifle)
		{
			if(!Rifle->FindPriority(EWeaponAction::Fire, "BuyingStatePriority", EAbility::Cant))
			{
				Rifle->MulticastAddPriority(EWeaponAction::Fire, "BuyingStatePriority", EAbility::Cant);
			}		
		}
	}
}

void AThe_Arena_MatchMakingGameMode::DisableRestrictionsOnBuyingState()
{
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AController* Controller = It->Get();
		AFirstPersonCharacter* Character = nullptr;
		ARifleBase* Rifle = nullptr;
		UEquipmentComponent* EquipmentComponent = nullptr;

		if (Controller)
		{
			Character = Controller->GetPawn<AFirstPersonCharacter>();
		}

		if (Character)
		{
			EquipmentComponent = Character->GetFPEquipmentComponent();

			/*if (Character->FindMovementPriority(EMovementType::CantWalk, "BuyingStatePriority"))
			{
				Character->MulticastRemoveMovementPriority(EMovementType::CantWalk, "BuyingStatePriority");
			}*/
		}

		if (EquipmentComponent)
		{
			Rifle = Cast<ARifleBase>(EquipmentComponent->GetUsingItemInHands());
		}

		if (Rifle)
		{
			if (Rifle->FindPriority(EWeaponAction::Fire, "BuyingStatePriority", EAbility::Cant))
			{
				Rifle->MulticastRemovePriority(EWeaponAction::Fire, "BuyingStatePriority", EAbility::Cant);
			}
		}
	}
}

void AThe_Arena_MatchMakingGameMode::EnableRestrictionsOnWaitingPlayersState()
{
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AController* Controller = It->Get();
		AFirstPersonCharacter* Character = nullptr;
		ARifleBase* Rifle = nullptr;
		UEquipmentComponent* EquipmentComponent = nullptr;

		if (Controller)
		{
			Character = Controller->GetPawn<AFirstPersonCharacter>();
		}

		if (Character)
		{
			EquipmentComponent = Character->GetFPEquipmentComponent();

			/*if(!Character->FindMovementPriority(EMovementType::CantWalk, "WaitingStatePriority"))
			{
				Character->MulticastAddMovementPriority(EMovementType::CantWalk, "WaitingStatePriority");
			}*/
		}

		if (EquipmentComponent)
		{
			Rifle = Cast<ARifleBase>(EquipmentComponent->GetUsingItemInHands());
		}

		if (Rifle)
		{
			if (!Rifle->FindPriority(EWeaponAction::Fire, "WaitingStatePriority", EAbility::Cant))
			{
				Rifle->MulticastAddPriority(EWeaponAction::Fire, "WaitingStatePriority", EAbility::Cant);
			}
		}
	}
}

void AThe_Arena_MatchMakingGameMode::DisableRestrictionsOnWaitingPlayersState()
{
	for (auto It = GetWorld()->GetControllerIterator(); It; ++It)
	{
		AController* Controller = It->Get();
		AFirstPersonCharacter* Character = nullptr;
		ARifleBase* Rifle = nullptr;
		UEquipmentComponent* EquipmentComponent = nullptr;

		if (Controller)
		{
			Character = Controller->GetPawn<AFirstPersonCharacter>();
		}

		if (Character)
		{
			EquipmentComponent = Character->GetFPEquipmentComponent();

			/*if (Character->FindMovementPriority(EMovementType::CantWalk, "WaitingStatePriority"))
			{
				Character->MulticastRemoveMovementPriority(EMovementType::CantWalk, "WaitingStatePriority");
			}*/
		}

		if (EquipmentComponent)
		{
			Rifle = Cast<ARifleBase>(EquipmentComponent->GetUsingItemInHands());
		}

		if (Rifle)
		{
			if (Rifle->FindPriority(EWeaponAction::Fire, "WaitingStatePriority", EAbility::Cant))
			{
				Rifle->MulticastRemovePriority(EWeaponAction::Fire, "WaitingStatePriority", EAbility::Cant);
			}
		}
	}
}

void AThe_Arena_MatchMakingGameMode::RestartPlayerOnWaitingState(AController* Controller)
{
	FTimerHandle Handle;
	FTimerDelegate Delegate;

	if(GetMatchState() == MatchState::WaitingForPlayers)
	{
		Delegate.BindUFunction(this, "RestartPlayer", Controller);
		GetWorldTimerManager().SetTimer(Handle, Delegate, TimeForRestartCharacterAfterDead, false);
	}
}

void AThe_Arena_MatchMakingGameMode::DestroyPlayerOnWaitingState(AController* Controller)
{
	if (!Controller)
		return;

	if(auto FPCharacter = Cast<AFirstPersonCharacter>(Controller->GetPawn()))
	{
		if (!FMath::IsNearlyZero(TimeForDestroyCharacterAfterDead))
		{
			FTimerHandle Handle;
			GetWorldTimerManager().SetTimer(Handle, FPCharacter, &AFirstPersonCharacter::K2_DestroyActor, TimeForDestroyCharacterAfterDead, false);
		}
	}
}

void AThe_Arena_MatchMakingGameMode::StartEndMatchWithoutPlayersTimer()
{
	if (bEndMatchWithoutPlayersTimerEnabled)
		return;
	
	if (auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{
		bEndMatchWithoutPlayersTimerEnabled = true;
				
		float Rate = float(EndMatchWithoutPlayersDuration - ArenaMatchMakingGameState->GetEndMatchWithoutPlayersTimePassed());
		GetWorldTimerManager().SetTimer(EndMatchWithoutPlayersHandle, this, &AThe_Arena_MatchMakingGameMode::EndMatchWithoutPlayers, Rate);
	}
}

void AThe_Arena_MatchMakingGameMode::StopEndMatchWithoutPlayersTimer()
{
	if (!bEndMatchWithoutPlayersTimerEnabled)
		return;

	bEndMatchWithoutPlayersTimerEnabled = false;
	GetWorldTimerManager().ClearTimer(EndMatchWithoutPlayersHandle);

	FTimerHandle Handle;
	GetWorldTimerManager().SetTimer(Handle, this, &AThe_Arena_MatchMakingGameMode::RestartGame, 0.1f);
}

void AThe_Arena_MatchMakingGameMode::EndMatchWithoutPlayers()
{	
	if (auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{
		if (ArenaMatchMakingGameState->GetTeam_1().Num() == 0 &&
			ArenaMatchMakingGameState->GetTeam_2().Num() == 0)
		{
			if(TeamOfLastLeftPlayer == EGameTeam::Team_1)
			{
				ArenaMatchMakingGameState->SetWonMatchTeam(EGameTeam::Team_1);
			}
			else if(TeamOfLastLeftPlayer == EGameTeam::Team_2)
			{
				ArenaMatchMakingGameState->SetWonMatchTeam(EGameTeam::Team_2);
			}
		}
		else if(ArenaMatchMakingGameState->GetTeam_1().Num() == 0)
		{
			ArenaMatchMakingGameState->SetWonMatchTeam(EGameTeam::Team_2);
		}
		else if(ArenaMatchMakingGameState->GetTeam_2().Num() == 0)
		{
			ArenaMatchMakingGameState->SetWonMatchTeam(EGameTeam::Team_1);
		}
	}

	SetGameOver(true);
	SetMatchState(MatchState::EndRound);
}

void AThe_Arena_MatchMakingGameMode::InitializationServerParameters(int InBuyingStateDuration, int InRoundDuration,
	int InEndRoundDuration, int InEndMatchWithoutPlayersDuration, int InRestartServerDuration, int InRoundsToWinCount,
	int InPlayersToStart, bool InbFriendlyFire, int InWaitingPlayersMoney, int InRoundMoney, int InWonRoundMoney)
{
	BuyingStateDuration = InBuyingStateDuration;
	RoundDuration = InRoundDuration;
	EndRoundDuration = InEndRoundDuration;
	EndMatchWithoutPlayersDuration = InEndMatchWithoutPlayersDuration;
	RestartServerDuration = InRestartServerDuration;
	RoundsToWinCount = InRoundsToWinCount;
	PlayersToStart = InPlayersToStart;
	bFriendlyFire = InbFriendlyFire;
	WaitingPlayersMoney = InWaitingPlayersMoney;
	RoundMoney = InRoundMoney;
	WonRoundMoney = InWonRoundMoney;
}

int AThe_Arena_MatchMakingGameMode::GetEndMatchWithoutPlayersDuration() const
{
	return EndMatchWithoutPlayersDuration;
}

TArray<AKeptFPControllerSettings*> AThe_Arena_MatchMakingGameMode::GetKeptFPControllerSettingsArray() const
{
	return KeptFPControllerSettingsArray;
}

bool AThe_Arena_MatchMakingGameMode::IsGameStarted() const
{
	return bGameStarted;
}

bool AThe_Arena_MatchMakingGameMode::IsNewGameStarted() const
{
	return bNewGameStarted;
}

void AThe_Arena_MatchMakingGameMode::SetNewGameStarted(bool InNewGameStarted)
{
	bNewGameStarted = InNewGameStarted;
}

bool AThe_Arena_MatchMakingGameMode::IsGameOver() const
{
	return bGameOver;
}

void AThe_Arena_MatchMakingGameMode::SetGameOver(bool bInGameOver)
{
	bGameOver = bInGameOver;
	
	if (auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{
		ArenaMatchMakingGameState->SetGameOver(bInGameOver);
	}
}

void AThe_Arena_MatchMakingGameMode::SetGameStarted(bool InGameStarted)
{
	bGameStarted = InGameStarted;

	if(auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
	{
		ArenaMatchMakingGameState->SetGameStarted(InGameStarted);
	}
}

bool AThe_Arena_MatchMakingGameMode::ReadyToStartBuyingState_Implementation()
{
	if (GetMatchState() == MatchState::WaitingForPlayers)
	{
		if (auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>())
		{
			if (bGameStarted)
			{
				if (ArenaMatchMakingGameState->GetTeam_1().Num() == 0 ||
					ArenaMatchMakingGameState->GetTeam_2().Num() == 0)
				{
					if(!bEndMatchWithoutPlayersTimerEnabled && NumTravellingPlayers == 0)
					{
						StartEndMatchWithoutPlayersTimer();
					}
					
					return false;
				}

				if(bEndMatchWithoutPlayersTimerEnabled)
				{
					StopEndMatchWithoutPlayersTimer();
				}
				
				return true;
			}

			if (ArenaMatchMakingGameState->GetTeam_1().Num() >= PlayersToStart / 2 &&
				ArenaMatchMakingGameState->GetTeam_2().Num() >= PlayersToStart / 2)
			{
				return true;
			}
		}
	}

	return false;
}

void AThe_Arena_MatchMakingGameMode::SelectTeam(APlayerController* Controller, EGameTeam Team)
{
	if (auto FPPlayerController = Cast<AFirstPersonPlayerController>(Controller))
	{
		auto ArenaPlayerState = FPPlayerController->GetPlayerState<AThe_Arena_PlayerState>();

		if (ArenaPlayerState)
		{
			auto ArenaMatchMakingGameState = GetGameState<AThe_Arena_MatchMakingGameState>();

			if (ArenaMatchMakingGameState)
			{
				if (ArenaPlayerState->GetCurrentTeam() == Team)
					return;
				
				if (ArenaPlayerState->GetCurrentTeam() == EGameTeam::Spectators)
				{
					ArenaMatchMakingGameState->RemovePlayerStateFromTeam(ArenaPlayerState->GetCurrentTeam(), ArenaPlayerState);
					ArenaMatchMakingGameState->AddPlayerStateInTeam(Team, ArenaPlayerState);

					ArenaPlayerState->SetIsOnlyASpectator(false);
					ArenaPlayerState->SetCurrentTeam(Team);
					
					NumSpectators--;
					NumPlayers++;
				}
				else
				{
					TeamOfLastLeftPlayer = ArenaPlayerState->GetCurrentTeam();

					ArenaMatchMakingGameState->RemovePlayerStateFromTeam(ArenaPlayerState->GetCurrentTeam(), ArenaPlayerState);
					
					if (auto FPCharacter = Cast<AFirstPersonCharacter>(FPPlayerController->GetPawn()))
					{
						auto& Death_Controller_Dispatcher = FPCharacter->Get_Death_Controller_Dispatcher();

						if(Death_Controller_Dispatcher.IsAlreadyBound(this, &AThe_Arena_MatchMakingGameMode::RestartPlayerOnWaitingState))
						{
							Death_Controller_Dispatcher.RemoveDynamic(this, &AThe_Arena_MatchMakingGameMode::RestartPlayerOnWaitingState);
						}
						
						if(auto HealthComponent = FPCharacter->FindComponentByClass<UHealthComponent>())
						{
							HealthComponent->SetCurrentStat(0.0f);
						}
					}

					ArenaMatchMakingGameState->AddPlayerStateInTeam(Team, ArenaPlayerState);
					ArenaPlayerState->SetCurrentTeam(Team);
					
					if (Team == EGameTeam::Spectators)
					{
						if(ReadyToWaitForPlayers() || IsWaitingForPlayers())
						{
							FPPlayerController->ClientSetHUD(HUDClass);
							
							FPPlayerController->StartSpectatingOnly();
							FPPlayerController->ClientGotoState(NAME_Spectating);

							FPPlayerController->ClientSpectatorPawnLocationAndRotationInitialization();
						}
						else
						{
							ArenaPlayerState->SetIsSpectator(true);
							ArenaPlayerState->SetIsOnlyASpectator(true);
						}

						NumSpectators++;
						NumPlayers--;
					}					
				}
				
				if ((ReadyToWaitForPlayers() || IsWaitingForPlayers()) && Team != EGameTeam::Spectators)
				{
					RestartPlayer(FPPlayerController);
				}
			}
		}
	}
}
