// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponRecoilComponent.h"


// Sets default values for this component's properties
UWeaponRecoilComponent::UWeaponRecoilComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	MeshStandartRecoilTC = CreateDefaultSubobject<UTransformController>("MeshStandartRecoilTC");
	MeshAimingRecoilTC = CreateDefaultSubobject<UTransformController>("MeshAimingRecoilTC");
	CameraStandartRecoilTC = CreateDefaultSubobject<UCharacterTransformController>("CameraStandartRecoilTC");
	CameraAimingRecoilTC = CreateDefaultSubobject<UCharacterTransformController>("CameraAimingRecoilTC");
	CameraUpRotationTC = CreateDefaultSubobject<UCharacterTransformController>("CameraUpRotationTC");
}


// Called when the game starts
void UWeaponRecoilComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UWeaponRecoilComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateRandomMeshTransformRecoil();
	UpdateRandomCameraRotationRecoil();
	UpRotationStabilization();
}

void UWeaponRecoilComponent::InitializationParameters(AFirstPersonCharacter* Character, USceneComponent* MeshComponent)
{
	if(Character)
	{
		if (CameraStandartRecoilTC)
		{
			CameraStandartRecoilTC->SetFPCharacter(Character);
			CameraStandartRecoilTC->SetSceneComp(Character->GetFirstPersonCamera());
		}

		if (CameraAimingRecoilTC)
		{
			CameraAimingRecoilTC->SetFPCharacter(Character);
			CameraAimingRecoilTC->SetSceneComp(Character->GetFirstPersonCamera());
		}

		if (CameraUpRotationTC)
		{
			CameraUpRotationTC->SetFPCharacter(Character);
			CameraUpRotationTC->SetSceneComp(Character->GetFirstPersonCamera());
		}

		FP_PlayerController = Cast<AFirstPersonPlayerController>(Character->Controller);
	}

	if (MeshStandartRecoilTC)
		MeshStandartRecoilTC->SetSceneComp(MeshComponent);

	if (MeshAimingRecoilTC)
		MeshAimingRecoilTC->SetSceneComp(MeshComponent);
}

void UWeaponRecoilComponent::UpdateRecoilParameters(float ScaleOfMoving, bool Aiming)
{
	bIsAiming = Aiming;

	if (Aiming)
	{
		if (MeshAimingRecoilTC)
		{
			MeshAimingRecoilTC->SetScaleOfMoving(ScaleOfMoving);
		}

		if (MeshStandartRecoilTC)
		{
			MeshStandartRecoilTC->SetScaleOfMoving(0);
		}

		if (CameraAimingRecoilTC)
		{
			CameraAimingRecoilTC->SetScaleOfMoving(ScaleOfMoving);
		}

		if (CameraStandartRecoilTC)
		{
			CameraStandartRecoilTC->SetScaleOfMoving(0);
		}
	}
	else
	{
		if (MeshStandartRecoilTC)
		{
			MeshStandartRecoilTC->SetScaleOfMoving(ScaleOfMoving);
		}

		if (MeshAimingRecoilTC)
		{
			MeshAimingRecoilTC->SetScaleOfMoving(0);
		}

		if (CameraStandartRecoilTC)
		{
			CameraStandartRecoilTC->SetScaleOfMoving(ScaleOfMoving);
		}

		if (CameraAimingRecoilTC)
		{
			CameraAimingRecoilTC->SetScaleOfMoving(0);
		}
	}

	if (CameraUpRotationTC && CanCameraStabilize)
	{
		CameraUpRotationTC->SetScaleOfMoving(ScaleOfMoving);
	}

	if (FMath::IsNearlyZero(ScaleOfMoving))
	{
		if (bRecoilActivated)
		{
			bRecoilActivated = false;

			ResetRecoilLocation(MeshStandartRecoilTC, StandartMeshLocationRecoil);
			ResetRecoilLocation(MeshAimingRecoilTC, AimingMeshLocationRecoil);
			ResetRecoilRotation(CameraStandartRecoilTC, StandartCameraRotationRecoil);
			ResetRecoilRotation(CameraAimingRecoilTC, AimingCameraRotationRecoil);

			ResetUpRotationParameters();
		}
	}
	else
	{
		if (!bRecoilActivated)
		{
			bRecoilActivated = true;
			UpdateUpRotationParameters();
		}
	}
}

void UWeaponRecoilComponent::PlayRecoilShake()
{	
	if (bIsAiming && FP_PlayerController)
	{
		FP_PlayerController->ClientStartCameraShake(AimingCameraShakeRecoil.RecoilCameraShakeClass, 1.0f);
	}
}

float UWeaponRecoilComponent::GetVerticalRecoil() const
{
	return VerticalRecoil;
}

void UWeaponRecoilComponent::SetVerticalRecoil(float VRecoil)
{
	VerticalRecoil = VRecoil;

	UpdateVerticalRecoilParameters(VRecoil);
}

float UWeaponRecoilComponent::GetHorizontalRecoil() const
{
	return HorizontalRecoil;
}

void UWeaponRecoilComponent::SetHorizontalRecoil(float HRecoil)
{
	HorizontalRecoil = HRecoil;

	UpdateHorizontalRecoilParameters(HRecoil);
}

void UWeaponRecoilComponent::UpdateRandomMeshTransformRecoil()
{
	if (!bRecoilActivated)
		return;

	if (!bIsAiming && MeshStandartRecoilTC)
	{
		if (StandartMeshLocationRecoil.Forward.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(MeshStandartRecoilTC->Forward.Location, MeshStandartRecoilTC->GetPassLocation().X, StandartMeshLocationRecoil.Forward.CurrencyToEqualDistances))
			{
				float LocationRecoil = FMath::FRandRange(StandartMeshLocationRecoil.Forward.RandomDistance.X, StandartMeshLocationRecoil.Forward.RandomDistance.Y);
				MeshStandartRecoilTC->Forward.Location = LocationRecoil;
				MeshStandartRecoilTC->Roll.Rotation = LocationRecoil * -StandartMeshRotationCoefs.RollCoef;
			}
		}

		if (StandartMeshLocationRecoil.Right.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(MeshStandartRecoilTC->Right.Location, MeshStandartRecoilTC->GetPassLocation().Y, StandartMeshLocationRecoil.Right.CurrencyToEqualDistances))
			{
				float LocationRecoil = FMath::FRandRange(StandartMeshLocationRecoil.Right.RandomDistance.X, StandartMeshLocationRecoil.Right.RandomDistance.Y);
				MeshStandartRecoilTC->Right.Location = LocationRecoil;
				MeshStandartRecoilTC->Yaw.Rotation = LocationRecoil * -StandartMeshRotationCoefs.YawCoef;
			}
		}

		if (StandartMeshLocationRecoil.Up.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(MeshStandartRecoilTC->Up.Location, MeshStandartRecoilTC->GetPassLocation().Z, StandartMeshLocationRecoil.Up.CurrencyToEqualDistances))
			{
				float LocationRecoil = FMath::FRandRange(StandartMeshLocationRecoil.Up.RandomDistance.X, StandartMeshLocationRecoil.Up.RandomDistance.Y);
				MeshStandartRecoilTC->Up.Location = LocationRecoil;
				MeshStandartRecoilTC->Pitch.Rotation = LocationRecoil * -StandartMeshRotationCoefs.PitchCoef;
			}

		}
	}
	else if (bIsAiming && MeshAimingRecoilTC)
	{
		if (AimingMeshLocationRecoil.Forward.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(MeshAimingRecoilTC->Forward.Location, MeshAimingRecoilTC->GetPassLocation().X, AimingMeshLocationRecoil.Forward.CurrencyToEqualDistances))
				MeshAimingRecoilTC->Forward.Location = FMath::FRandRange(AimingMeshLocationRecoil.Forward.RandomDistance.X, AimingMeshLocationRecoil.Forward.RandomDistance.Y);
		}

		if (AimingMeshLocationRecoil.Right.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(MeshAimingRecoilTC->Right.Location, MeshAimingRecoilTC->GetPassLocation().Y, AimingMeshLocationRecoil.Right.CurrencyToEqualDistances))
				MeshAimingRecoilTC->Right.Location = FMath::FRandRange(AimingMeshLocationRecoil.Right.RandomDistance.X, AimingMeshLocationRecoil.Right.RandomDistance.Y);
		}

		if (AimingMeshLocationRecoil.Up.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(MeshAimingRecoilTC->Up.Location, MeshAimingRecoilTC->GetPassLocation().Z, AimingMeshLocationRecoil.Up.CurrencyToEqualDistances))
				MeshAimingRecoilTC->Up.Location = FMath::FRandRange(AimingMeshLocationRecoil.Up.RandomDistance.X, AimingMeshLocationRecoil.Up.RandomDistance.Y);
		}
	}
}

void UWeaponRecoilComponent::UpdateRandomCameraRotationRecoil()
{
	if (!bRecoilActivated)
		return;

	if (!bIsAiming && CameraStandartRecoilTC)
	{
		if (StandartCameraRotationRecoil.Roll.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(CameraStandartRecoilTC->Roll.Rotation, CameraStandartRecoilTC->GetPassRotation().Roll, StandartCameraRotationRecoil.Roll.CurrencyToEqualDistances))
				CameraStandartRecoilTC->Roll.Rotation = FMath::FRandRange(StandartCameraRotationRecoil.Roll.RandomDistance.X, StandartCameraRotationRecoil.Roll.RandomDistance.Y);
		}

		if (StandartCameraRotationRecoil.Pitch.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(CameraStandartRecoilTC->Pitch.Rotation, CameraStandartRecoilTC->GetPassRotation().Pitch, StandartCameraRotationRecoil.Pitch.CurrencyToEqualDistances))
				CameraStandartRecoilTC->Pitch.Rotation = FMath::FRandRange(StandartCameraRotationRecoil.Pitch.RandomDistance.X, StandartCameraRotationRecoil.Pitch.RandomDistance.Y);
		}

		if (StandartCameraRotationRecoil.Yaw.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(CameraStandartRecoilTC->Yaw.Rotation, CameraStandartRecoilTC->GetPassRotation().Yaw, StandartCameraRotationRecoil.Yaw.CurrencyToEqualDistances))
				CameraStandartRecoilTC->Yaw.Rotation = FMath::FRandRange(StandartCameraRotationRecoil.Yaw.RandomDistance.X, StandartCameraRotationRecoil.Yaw.RandomDistance.Y);
		}
	}
	else if (bIsAiming && CameraAimingRecoilTC)
	{
		if (AimingCameraRotationRecoil.Roll.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(CameraAimingRecoilTC->Roll.Rotation, CameraAimingRecoilTC->GetPassRotation().Roll, AimingCameraRotationRecoil.Roll.CurrencyToEqualDistances))
				CameraAimingRecoilTC->Roll.Rotation = FMath::FRandRange(AimingCameraRotationRecoil.Roll.RandomDistance.X, AimingCameraRotationRecoil.Roll.RandomDistance.Y);
		}

		if (AimingCameraRotationRecoil.Pitch.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(CameraAimingRecoilTC->Pitch.Rotation, CameraAimingRecoilTC->GetPassRotation().Pitch, AimingCameraRotationRecoil.Pitch.CurrencyToEqualDistances))
				CameraAimingRecoilTC->Pitch.Rotation = FMath::FRandRange(AimingCameraRotationRecoil.Pitch.RandomDistance.X, AimingCameraRotationRecoil.Pitch.RandomDistance.Y);
		}

		if (AimingCameraRotationRecoil.Yaw.bUseRandomDistance)
		{
			if (FMath::IsNearlyEqual(CameraAimingRecoilTC->Yaw.Rotation, CameraAimingRecoilTC->GetPassRotation().Yaw, AimingCameraRotationRecoil.Yaw.CurrencyToEqualDistances))
				CameraAimingRecoilTC->Yaw.Rotation = FMath::FRandRange(AimingCameraRotationRecoil.Yaw.RandomDistance.X, AimingCameraRotationRecoil.Yaw.RandomDistance.Y);
		}
	}
}

void UWeaponRecoilComponent::ResetRecoilLocation(UTransformController* RecoilTC, FRecoilLocationParam& Param)
{
	if (RecoilTC)
	{
		if (Param.Forward.bUseRandomDistance)
			RecoilTC->Forward.Location = 0;

		if (Param.Right.bUseRandomDistance)
			RecoilTC->Right.Location = 0;

		if (Param.Up.bUseRandomDistance)
			RecoilTC->Up.Location = 0;
	}
}

void UWeaponRecoilComponent::ResetRecoilRotation(UCharacterTransformController* RecoilTC, FRecoilRotationParam& Param)
{
	if (RecoilTC)
	{
		if (Param.Roll.bUseRandomDistance)
			RecoilTC->Roll.Rotation = 0;

		if (Param.Pitch.bUseRandomDistance)
			RecoilTC->Pitch.Rotation = 0;

		if (Param.Yaw.bUseRandomDistance)
			RecoilTC->Yaw.Rotation = 0;
	}
}

void UWeaponRecoilComponent::UpRotationStabilization()
{
	if (bRecoilActivated && CameraUpRotationTC)
	{
		if (bIsAiming)
		{
			if (FMath::IsNearlyEqual(CameraUpRotationTC->Pitch.Rotation, CameraUpRotationTC->GetPassRotation().Pitch))
			{
				if (CanCameraStabilize)
				{
					CameraUpRotationTC->SetScaleOfMoving(0);
					CameraUpRotationTC->Pitch.BackSpeedRotation = AimingCameraUpRotation.StabilizationSpeed;
					CanCameraStabilize = false;
				}
			}
		}
		else
		{
			if (FMath::IsNearlyEqual(CameraUpRotationTC->Pitch.Rotation, CameraUpRotationTC->GetPassRotation().Pitch))
			{
				if (CanCameraStabilize)
				{
					CameraUpRotationTC->SetScaleOfMoving(0);
					CameraUpRotationTC->Pitch.BackSpeedRotation = StandartCameraUpRotation.StabilizationSpeed;
					CanCameraStabilize = false;
				}
			}
		}
	}
}

void UWeaponRecoilComponent::UpdateUpRotationParameters()
{
	if (bIsAiming)
	{
		if (CanCameraUpRotate)
		{
			CameraUpRotationTC->Pitch.Rotation = AimingCameraUpRotation.Rotation;
			CameraUpRotationTC->Pitch.ForwardSpeedRotation = AimingCameraUpRotation.ForwardSpeed;
			CanCameraUpRotate = false;
		}
	}
	else
	{
		if (CanCameraUpRotate)
		{
			CameraUpRotationTC->Pitch.Rotation = StandartCameraUpRotation.Rotation;
			CameraUpRotationTC->Pitch.ForwardSpeedRotation = StandartCameraUpRotation.ForwardSpeed;
			CanCameraUpRotate = false;
		}
	}
}

void UWeaponRecoilComponent::ResetUpRotationParameters()
{
	if (CameraUpRotationTC)
	{
		CameraUpRotationTC->Pitch.Rotation = 0;

		if (bIsAiming)
		{
			CameraUpRotationTC->Pitch.BackSpeedRotation = AimingCameraUpRotation.BackSpeed;
		}
		else
		{
			CameraUpRotationTC->Pitch.BackSpeedRotation = StandartCameraUpRotation.BackSpeed;
		}

		CanCameraUpRotate = true;
		CanCameraStabilize = true;
	}
}

float UWeaponRecoilComponent::GetRecoilCoef(UCurveFloat* Curve, float Recoil) const
{
	if (Curve)
	{
		return Curve->GetFloatValue(Recoil);
	}

	return 0.0f;
}

void UWeaponRecoilComponent::UpdateLocationRecoilParameters(float Recoil, FRecoilRandomParam& RandomParam, FSceneCompMoveParam& MoveParam, FSceneCompRotParam* RotParam, float RotCoef)
{
	if (RandomParam.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(RandomParam.DistanceXCurve, Recoil);
		Distance.Y = GetRecoilCoef(RandomParam.DistanceYCurve, Recoil);
		RandomParam.RandomDistance = Distance;
	}
	else
	{
		float RecoilCoef = GetRecoilCoef(RandomParam.DistanceCurve, Recoil);
		MoveParam.Location = RecoilCoef;

		if (RotParam)
		{
			RotParam->Rotation = RecoilCoef * -RotCoef;
		}
	}

	float ForwardSpeed = GetRecoilCoef(RandomParam.ForwardSpeedCurve, Recoil);
	float BackSpeed = GetRecoilCoef(RandomParam.BackSpeedCurve, Recoil);

	MoveParam.ForwardSpeedLocation = ForwardSpeed;
	MoveParam.BackSpeedLocation = BackSpeed;

	if (RotParam)
	{
		RotParam->ForwardSpeedRotation = ForwardSpeed * RotCoef;
		RotParam->BackSpeedRotation = BackSpeed * RotCoef;
	}
}

void UWeaponRecoilComponent::UpdateRotationRecoilParameters(float Recoil, FRecoilRandomParam& RandomParam, FSceneCompRotParam& RotParam)
{
	if (RandomParam.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(RandomParam.DistanceXCurve, Recoil);
		Distance.Y = GetRecoilCoef(RandomParam.DistanceYCurve, Recoil);
		RandomParam.RandomDistance = Distance;
	}
	else
	{
		RotParam.Rotation = GetRecoilCoef(RandomParam.DistanceCurve, Recoil);
	}

	RotParam.ForwardSpeedRotation = GetRecoilCoef(RandomParam.ForwardSpeedCurve, Recoil);
	RotParam.BackSpeedRotation = GetRecoilCoef(RandomParam.BackSpeedCurve, Recoil);
}

void UWeaponRecoilComponent::UpdateHorizontalRecoilParameters(float HRecoil)
{
	if (MeshStandartRecoilTC)
	{
		UpdateLocationRecoilParameters(HRecoil, StandartMeshLocationRecoil.Right, MeshStandartRecoilTC->Right, &MeshStandartRecoilTC->Yaw, StandartMeshRotationCoefs.YawCoef);
		UpdateLocationRecoilParameters(HRecoil, StandartMeshLocationRecoil.Forward, MeshStandartRecoilTC->Forward, &MeshStandartRecoilTC->Roll, StandartMeshRotationCoefs.RollCoef);
	}

	if (MeshAimingRecoilTC)
	{
		UpdateLocationRecoilParameters(HRecoil, AimingMeshLocationRecoil.Right, MeshAimingRecoilTC->Right);
		UpdateLocationRecoilParameters(HRecoil, AimingMeshLocationRecoil.Forward, MeshAimingRecoilTC->Forward);
	}

	if (CameraStandartRecoilTC)
	{
		UpdateRotationRecoilParameters(HRecoil, StandartCameraRotationRecoil.Yaw, CameraStandartRecoilTC->Yaw);
		UpdateRotationRecoilParameters(HRecoil, StandartCameraRotationRecoil.Roll, CameraStandartRecoilTC->Roll);
	}

	if (CameraAimingRecoilTC)
	{
		UpdateRotationRecoilParameters(HRecoil, AimingCameraRotationRecoil.Yaw, CameraAimingRecoilTC->Yaw);
		UpdateRotationRecoilParameters(HRecoil, AimingCameraRotationRecoil.Roll, CameraAimingRecoilTC->Roll);
	}

	if(AimingCameraShakeRecoil.RecoilCameraShakeClass)
	{
		if (auto AimingRecoilCameraShake = Cast<UMatineeCameraShake>(AimingCameraShakeRecoil.RecoilCameraShakeClass.Get()))
		{
			AimingRecoilCameraShake->RotOscillation.Yaw.Amplitude = GetRecoilCoef(AimingCameraShakeRecoil.YawCurve, HRecoil);
			AimingRecoilCameraShake->RotOscillation.Yaw.Frequency = GetRecoilCoef(AimingCameraShakeRecoil.YawSpeedCurve, HRecoil);
			AimingRecoilCameraShake->RotOscillation.Roll.Amplitude = GetRecoilCoef(AimingCameraShakeRecoil.RollCurve, HRecoil);
			AimingRecoilCameraShake->RotOscillation.Roll.Frequency = GetRecoilCoef(AimingCameraShakeRecoil.RollSpeedCurve, HRecoil);
		}
	}
	

	/*if(StandartMeshLocationRecoil.Right.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(StandartMeshLocationRecoil.Right.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(StandartMeshLocationRecoil.Right.DistanceYCurve, HRecoil);
		StandartMeshLocationRecoil.Right.RandomDistance = Distance;
	}
	else
	{
		if(MeshStandartRecoilTC)
		{
			MeshStandartRecoilTC->Right.Location = GetRecoilCoef(StandartMeshLocationRecoil.Right.DistanceCurve, HRecoil);
		}
	}

	if(MeshStandartRecoilTC)
	{
		MeshStandartRecoilTC->Right.ForwardSpeedLocation = GetRecoilCoef(StandartMeshLocationRecoil.Right.ForwardSpeedCurve, HRecoil);
		MeshStandartRecoilTC->Right.BackSpeedLocation = GetRecoilCoef(StandartMeshLocationRecoil.Right.BackSpeedCurve, HRecoil);
	}

	if(AimingMeshLocationRecoil.Right.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(AimingMeshLocationRecoil.Right.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(AimingMeshLocationRecoil.Right.DistanceYCurve, HRecoil);
		AimingMeshLocationRecoil.Right.RandomDistance = Distance;
	}
	else
	{
		if(MeshAimingRecoilTC)
		{
			MeshAimingRecoilTC->Right.Location = GetRecoilCoef(AimingMeshLocationRecoil.Right.DistanceCurve, HRecoil);
		}
	}

	if(MeshAimingRecoilTC)
	{
		MeshAimingRecoilTC->Right.ForwardSpeedLocation = GetRecoilCoef(AimingMeshLocationRecoil.Right.ForwardSpeedCurve, HRecoil);
		MeshAimingRecoilTC->Right.BackSpeedLocation = GetRecoilCoef(AimingMeshLocationRecoil.Right.BackSpeedCurve, HRecoil);
	}


	if(StandartCameraRotationRecoil.Yaw.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(StandartCameraRotationRecoil.Yaw.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(StandartCameraRotationRecoil.Yaw.DistanceYCurve, HRecoil);
		StandartCameraRotationRecoil.Yaw.RandomDistance = Distance;
	}
	else
	{
		if(CameraStandartRecoilTC)
		{
			CameraStandartRecoilTC->Yaw.Rotation = GetRecoilCoef(StandartCameraRotationRecoil.Yaw.DistanceCurve, HRecoil);
		}
	}

	if(CameraStandartRecoilTC)
	{
		CameraStandartRecoilTC->Yaw.ForwardSpeedRotation = GetRecoilCoef(StandartCameraRotationRecoil.Yaw.ForwardSpeedCurve, HRecoil);
		CameraStandartRecoilTC->Yaw.BackSpeedRotation = GetRecoilCoef(StandartCameraRotationRecoil.Yaw.BackSpeedCurve, HRecoil);
	}

	if(AimingCameraRotationRecoil.Yaw.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(AimingCameraRotationRecoil.Yaw.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(AimingCameraRotationRecoil.Yaw.DistanceYCurve, HRecoil);
		AimingCameraRotationRecoil.Yaw.RandomDistance = Distance;
	}
	else
	{
		if(CameraAimingRecoilTC)
		{
			CameraAimingRecoilTC->Yaw.Rotation = GetRecoilCoef(AimingCameraRotationRecoil.Yaw.DistanceCurve, HRecoil);
		}
	}

	if(CameraAimingRecoilTC)
	{
		CameraAimingRecoilTC->Yaw.ForwardSpeedRotation = GetRecoilCoef(AimingCameraRotationRecoil.Yaw.ForwardSpeedCurve, HRecoil);
		CameraAimingRecoilTC->Yaw.BackSpeedRotation = GetRecoilCoef(AimingCameraRotationRecoil.Yaw.BackSpeedCurve, HRecoil);
	}

	AimingCameraShakeRecoil.RecoilCameraShake.GetDefaultObject()->RotOscillation.Yaw.Amplitude = GetRecoilCoef(AimingCameraShakeRecoil.YawCurve, HRecoil);
	AimingCameraShakeRecoil.RecoilCameraShake.GetDefaultObject()->RotOscillation.Yaw.Frequency = GetRecoilCoef(AimingCameraShakeRecoil.YawSpeedCurve, HRecoil);

	if(StandartCameraRotationRecoil.Roll.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(StandartCameraRotationRecoil.Roll.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(StandartCameraRotationRecoil.Roll.DistanceYCurve, HRecoil);
		StandartCameraRotationRecoil.Roll.RandomDistance = Distance;
	}
	else
	{
		if(CameraStandartRecoilTC)
		{
			CameraStandartRecoilTC->Roll.Rotation = GetRecoilCoef(StandartCameraRotationRecoil.Roll.DistanceCurve, HRecoil);
		}
	}

	if(CameraStandartRecoilTC)
	{
		CameraStandartRecoilTC->Roll.ForwardSpeedRotation = GetRecoilCoef(StandartCameraRotationRecoil.Roll.ForwardSpeedCurve, HRecoil);
		CameraStandartRecoilTC->Roll.BackSpeedRotation = GetRecoilCoef(StandartCameraRotationRecoil.Roll.BackSpeedCurve, HRecoil);
	}

	if(AimingCameraRotationRecoil.Roll.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(AimingCameraRotationRecoil.Roll.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(AimingCameraRotationRecoil.Roll.DistanceYCurve, HRecoil);
		AimingCameraRotationRecoil.Roll.RandomDistance = Distance;
	}
	else
	{
		if(CameraAimingRecoilTC)
		{
			CameraAimingRecoilTC->Roll.Rotation = GetRecoilCoef(AimingCameraRotationRecoil.Roll.DistanceCurve, HRecoil);
		}
	}

	if(CameraAimingRecoilTC)
	{
		CameraAimingRecoilTC->Roll.ForwardSpeedRotation = GetRecoilCoef(AimingCameraRotationRecoil.Roll.ForwardSpeedCurve, HRecoil);
		CameraAimingRecoilTC->Roll.BackSpeedRotation = GetRecoilCoef(AimingCameraRotationRecoil.Roll.BackSpeedCurve, HRecoil);
	}

	AimingCameraShakeRecoil.RecoilCameraShake.GetDefaultObject()->RotOscillation.Roll.Amplitude = GetRecoilCoef(AimingCameraShakeRecoil.RollCurve, HRecoil);
	AimingCameraShakeRecoil.RecoilCameraShake.GetDefaultObject()->RotOscillation.Roll.Frequency = GetRecoilCoef(AimingCameraShakeRecoil.RollSpeedCurve, HRecoil);


	if(StandartMeshLocationRecoil.Forward.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(StandartMeshLocationRecoil.Forward.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(StandartMeshLocationRecoil.Forward.DistanceYCurve, HRecoil);
		StandartMeshLocationRecoil.Forward.RandomDistance = Distance;
	}
	else
	{
		if(MeshStandartRecoilTC)
		{
			MeshStandartRecoilTC->Forward.Location = GetRecoilCoef(StandartMeshLocationRecoil.Forward.DistanceCurve, HRecoil);
		}
	}

	if(MeshStandartRecoilTC)
	{
		MeshStandartRecoilTC->Forward.ForwardSpeedLocation = GetRecoilCoef(StandartMeshLocationRecoil.Forward.ForwardSpeedCurve, HRecoil);
		MeshStandartRecoilTC->Forward.BackSpeedLocation = GetRecoilCoef(StandartMeshLocationRecoil.Forward.BackSpeedCurve, HRecoil);
	}

	if(AimingMeshLocationRecoil.Forward.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(AimingMeshLocationRecoil.Forward.DistanceXCurve, HRecoil);
		Distance.Y = GetRecoilCoef(AimingMeshLocationRecoil.Forward.DistanceYCurve, HRecoil);
		AimingMeshLocationRecoil.Forward.RandomDistance = Distance;
	}
	else
	{
		if(MeshAimingRecoilTC)
		{
			MeshAimingRecoilTC->Forward.Location = GetRecoilCoef(AimingMeshLocationRecoil.Forward.DistanceCurve, HRecoil);
		}
	}

	if(MeshAimingRecoilTC)
	{
		MeshAimingRecoilTC->Forward.ForwardSpeedLocation = GetRecoilCoef(AimingMeshLocationRecoil.Forward.ForwardSpeedCurve, HRecoil);
		MeshAimingRecoilTC->Forward.BackSpeedLocation = GetRecoilCoef(AimingMeshLocationRecoil.Forward.BackSpeedCurve, HRecoil);
	}
	*/
}

void UWeaponRecoilComponent::UpdateVerticalRecoilParameters(float VRecoil)
{
	if (MeshStandartRecoilTC)
	{
		UpdateLocationRecoilParameters(VRecoil, StandartMeshLocationRecoil.Up, MeshStandartRecoilTC->Up, &MeshStandartRecoilTC->Pitch, StandartMeshRotationCoefs.PitchCoef);
	}

	if (MeshAimingRecoilTC)
	{
		UpdateLocationRecoilParameters(VRecoil, AimingMeshLocationRecoil.Up, MeshAimingRecoilTC->Up);
	}

	if (CameraStandartRecoilTC)
	{
		UpdateRotationRecoilParameters(VRecoil, StandartCameraRotationRecoil.Pitch, CameraStandartRecoilTC->Pitch);
	}

	if (CameraAimingRecoilTC)
	{
		UpdateRotationRecoilParameters(VRecoil, AimingCameraRotationRecoil.Pitch, CameraAimingRecoilTC->Pitch);
	}

	if(AimingCameraShakeRecoil.RecoilCameraShakeClass)
	{
		if (auto AimingRecoilCameraShake = Cast<UMatineeCameraShake>(AimingCameraShakeRecoil.RecoilCameraShakeClass.Get()))
		{
			AimingRecoilCameraShake->RotOscillation.Pitch.Amplitude = GetRecoilCoef(AimingCameraShakeRecoil.PitchCurve, VRecoil);
			AimingRecoilCameraShake->RotOscillation.Pitch.Frequency = GetRecoilCoef(AimingCameraShakeRecoil.PitchSpeedCurve, VRecoil);
		}
	}
	
	StandartCameraUpRotation.Rotation = GetRecoilCoef(StandartCameraUpRotation.RotationCurve, VRecoil);
	StandartCameraUpRotation.ForwardSpeed = GetRecoilCoef(StandartCameraUpRotation.ForwardSpeedCurve, VRecoil);
	StandartCameraUpRotation.BackSpeed = GetRecoilCoef(StandartCameraUpRotation.BackSpeedCurve, VRecoil);
	StandartCameraUpRotation.StabilizationSpeed = GetRecoilCoef(StandartCameraUpRotation.StabilizationSpeedCurve, VRecoil);

	AimingCameraUpRotation.Rotation = GetRecoilCoef(AimingCameraUpRotation.RotationCurve, VRecoil);
	AimingCameraUpRotation.ForwardSpeed = GetRecoilCoef(AimingCameraUpRotation.ForwardSpeedCurve, VRecoil);
	AimingCameraUpRotation.BackSpeed = GetRecoilCoef(AimingCameraUpRotation.BackSpeedCurve, VRecoil);
	AimingCameraUpRotation.StabilizationSpeed = GetRecoilCoef(AimingCameraUpRotation.StabilizationSpeedCurve, VRecoil);

	/*if(StandartMeshLocationRecoil.Up.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(StandartMeshLocationRecoil.Up.DistanceXCurve, VRecoil);
		Distance.Y = GetRecoilCoef(StandartMeshLocationRecoil.Up.DistanceYCurve, VRecoil);
		StandartMeshLocationRecoil.Up.RandomDistance = Distance;
	}
	else
	{
		if(MeshStandartRecoilTC)
		{
			MeshStandartRecoilTC->Up.Location = GetRecoilCoef(StandartMeshLocationRecoil.Up.DistanceCurve, VRecoil);
		}
	}

	if(MeshStandartRecoilTC)
	{
		MeshStandartRecoilTC->Up.ForwardSpeedLocation = GetRecoilCoef(StandartMeshLocationRecoil.Up.ForwardSpeedCurve, VRecoil);
		MeshStandartRecoilTC->Up.BackSpeedLocation = GetRecoilCoef(StandartMeshLocationRecoil.Up.BackSpeedCurve, VRecoil);
	}

	if(AimingMeshLocationRecoil.Up.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(AimingMeshLocationRecoil.Up.DistanceXCurve, VRecoil);
		Distance.Y = GetRecoilCoef(AimingMeshLocationRecoil.Up.DistanceYCurve, VRecoil);
		AimingMeshLocationRecoil.Up.RandomDistance = Distance;
	}
	else
	{
		if(MeshAimingRecoilTC)
		{
			MeshAimingRecoilTC->Up.Location = GetRecoilCoef(AimingMeshLocationRecoil.Up.DistanceCurve, VRecoil);
		}
	}

	if(MeshAimingRecoilTC)
	{
		MeshAimingRecoilTC->Up.ForwardSpeedLocation = GetRecoilCoef(AimingMeshLocationRecoil.Up.ForwardSpeedCurve, VRecoil);
		MeshAimingRecoilTC->Up.BackSpeedLocation = GetRecoilCoef(AimingMeshLocationRecoil.Up.BackSpeedCurve, VRecoil);
	}

	if(StandartCameraRotationRecoil.Pitch.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(StandartCameraRotationRecoil.Pitch.DistanceXCurve, VRecoil);
		Distance.Y = GetRecoilCoef(StandartCameraRotationRecoil.Pitch.DistanceYCurve, VRecoil);
		StandartCameraRotationRecoil.Pitch.RandomDistance = Distance;
	}
	else
	{
		if(CameraStandartRecoilTC)
		{
			CameraStandartRecoilTC->Pitch.Rotation = GetRecoilCoef(StandartCameraRotationRecoil.Pitch.DistanceCurve, VRecoil);
		}
	}

	if(CameraStandartRecoilTC)
	{
		CameraStandartRecoilTC->Pitch.ForwardSpeedRotation = GetRecoilCoef(StandartCameraRotationRecoil.Pitch.ForwardSpeedCurve, VRecoil);
		CameraStandartRecoilTC->Pitch.BackSpeedRotation = GetRecoilCoef(StandartCameraRotationRecoil.Pitch.BackSpeedCurve, VRecoil);
	}

	if(AimingCameraRotationRecoil.Pitch.bUseRandomDistance)
	{
		FVector2D Distance;
		Distance.X = GetRecoilCoef(AimingCameraRotationRecoil.Pitch.DistanceXCurve, VRecoil);
		Distance.Y = GetRecoilCoef(AimingCameraRotationRecoil.Pitch.DistanceYCurve, VRecoil);
		AimingCameraRotationRecoil.Pitch.RandomDistance = Distance;
	}
	else
	{
		if(CameraAimingRecoilTC)
		{
			CameraAimingRecoilTC->Pitch.Rotation = GetRecoilCoef(AimingCameraRotationRecoil.Pitch.DistanceCurve, VRecoil);
		}
	}

	if(CameraAimingRecoilTC)
	{
		CameraAimingRecoilTC->Pitch.ForwardSpeedRotation = GetRecoilCoef(AimingCameraRotationRecoil.Pitch.ForwardSpeedCurve, VRecoil);
		CameraAimingRecoilTC->Pitch.BackSpeedRotation = GetRecoilCoef(AimingCameraRotationRecoil.Pitch.BackSpeedCurve, VRecoil);
	}


	AimingCameraShakeRecoil.RecoilCameraShake.GetDefaultObject()->RotOscillation.Pitch.Amplitude = GetRecoilCoef(AimingCameraShakeRecoil.PitchCurve, VRecoil);
	AimingCameraShakeRecoil.RecoilCameraShake.GetDefaultObject()->RotOscillation.Pitch.Frequency = GetRecoilCoef(AimingCameraShakeRecoil.PitchSpeedCurve, VRecoil);

	StandartCameraUpRotation.Rotation = GetRecoilCoef(StandartCameraUpRotation.RotationCurve, VRecoil);
	StandartCameraUpRotation.ForwardSpeed = GetRecoilCoef(StandartCameraUpRotation.ForwardSpeedCurve, VRecoil);
	StandartCameraUpRotation.BackSpeed = GetRecoilCoef(StandartCameraUpRotation.BackSpeedCurve, VRecoil);
	StandartCameraUpRotation.StabilizationSpeed = GetRecoilCoef(StandartCameraUpRotation.StabilizationSpeedCurve, VRecoil);

	AimingCameraUpRotation.Rotation = GetRecoilCoef(AimingCameraUpRotation.RotationCurve, VRecoil);
	AimingCameraUpRotation.ForwardSpeed = GetRecoilCoef(AimingCameraUpRotation.ForwardSpeedCurve, VRecoil);
	AimingCameraUpRotation.BackSpeed = GetRecoilCoef(AimingCameraUpRotation.BackSpeedCurve, VRecoil);
	AimingCameraUpRotation.StabilizationSpeed = GetRecoilCoef(AimingCameraUpRotation.StabilizationSpeedCurve, VRecoil);
	*/
}
