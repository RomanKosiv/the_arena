// Fill out your copyright notice in the Description page of Project Settings.


#include "BasketBaseComponent.h"
#include "Net/UnrealNetwork.h"

FString UBasketBaseComponent::GetReplicatedName() const
{
	return ReplicatedName;
}

int UBasketBaseComponent::GetCommonItemsCost() const
{
	return CommonItemsCost;
}

FDMD& UBasketBaseComponent::GetCommonItemsCostDispatcher()
{
	return CommonItemsCostDispatcher;
}

void UBasketBaseComponent::BeginPlay()
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		Rows = 8;
		Columns = 12;

		ReplicatedName = GetName();
	}

	Super::BeginPlay();
}

void UBasketBaseComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBasketBaseComponent, ReplicatedName);
	DOREPLIFETIME(UBasketBaseComponent, CommonItemsCost);
}

void UBasketBaseComponent::UpdateOnCurrentItemAmount()
{
	Super::UpdateOnCurrentItemAmount();

	UpdateCommonItemsCost();
}

void UBasketBaseComponent::UpdateOnAttachModule()
{
	Super::UpdateOnAttachModule();

	UpdateCommonItemsCost();
}

void UBasketBaseComponent::UpdateCommonItemsCost()
{
	if (GetOwnerRole() != ROLE_Authority)
		return;

	auto ItemsLocal = GetAllItems();

	float CommonItemsCostLocal = 0.0f;

	for (auto ItemsIterator = ItemsLocal.begin(); ItemsIterator != ItemsLocal.end(); ++ItemsIterator)
	{
		if (auto ItemObject = ItemsIterator->Key)
		{
			CommonItemsCostLocal += ItemObject->GetCurrentItemCost();
		}
	}

	CommonItemsCost = CommonItemsCostLocal;

	OnRep_CommonItemsCost();
}

void UBasketBaseComponent::OnRep_CommonItemsCost()
{
	CommonItemsCostDispatcher.Broadcast();
}
