// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterTransformController.h"
#include "The_Arena/Characters/FirstPersonCharacter.h"


UCharacterTransformController::UCharacterTransformController()
{
}

AFirstPersonCharacter* UCharacterTransformController::GetFPCharacter() const
{
	return FPCharacter;
}

void UCharacterTransformController::SetFPCharacter(AFirstPersonCharacter* Character)
{
	FPCharacter = Character;
}

void UCharacterTransformController::RotateSceneComponent(float DeltaTime, FSceneCompRotParam& Param, float& PassAngle, FRotator direction)
{
	auto ReturnToStartLambda = [&](int Koef) {

		float Offset = Koef * Param.BackSpeedRotation * DeltaTime * GetSpeedCoef(PassAngle, Param.BackSpeedRotationCurve);

		if (Koef * (PassAngle + Offset) > 0)
		{
			float DistanceToEndRotation = -PassAngle;

			if (FPCharacter)
			{
				if(direction == FRotator(1,0,0))
				{
					FPCharacter->AddControllerPitchInput(DistanceToEndRotation);					
				}
				else if (direction == FRotator(0,1,0))
				{
					FPCharacter->AddControllerYawInput(DistanceToEndRotation);			
				}
				else
				{
					FPCharacter->AddControllerRollInput(DistanceToEndRotation);
				}

				PassAngle = 0;
			}
		}
		else
		{
			if (FPCharacter)
			{
				if(direction == FRotator(1,0,0))
				{
					FPCharacter->AddControllerPitchInput(Offset);					
				}
				else if (direction == FRotator(0,1,0))
				{
					FPCharacter->AddControllerYawInput(Offset);			
				}
				else
				{
					FPCharacter->AddControllerRollInput(Offset);
				}

				PassAngle += Offset;
			}
		}
	};

	auto RotateComponentLambda = [&](int Koef, float Rotation) {
		float Offset = Koef * Param.ForwardSpeedRotation * DeltaTime * fabsf(ScaleOfMoving) * GetSpeedCoef(PassAngle, Param.ForwardSpeedRotationCurve);

		if (Koef * (PassAngle + Offset) > Koef* Rotation)
		{
			float DistanceToEndRotation = Rotation - PassAngle;

			if (FPCharacter)
			{
				if(direction == FRotator(1,0,0))
				{
					FPCharacter->AddControllerPitchInput(DistanceToEndRotation);
					
				}
				else if (direction == FRotator(0,1,0))
				{
					FPCharacter->AddControllerYawInput(DistanceToEndRotation);
				}
				else
				{
					FPCharacter->AddControllerRollInput(DistanceToEndRotation);
				}

				PassAngle = Rotation;
			}
		}
		else
		{
			if (FPCharacter)
			{
				if(direction == FRotator(1,0,0))
				{
					FPCharacter->AddControllerPitchInput(Offset);					
				}
				else if (direction == FRotator(0,1,0))
				{
					FPCharacter->AddControllerYawInput(Offset);			
				}
				else
				{
					FPCharacter->AddControllerRollInput(Offset);
				}

				PassAngle += Offset;
			}
		}
	};

	if (FMath::IsNearlyZero(ScaleOfMoving, CurrencyScaleOfMoving))
	{
		if (PassAngle < 0)
		{
			ReturnToStartLambda(1);
		}
		else if (PassAngle > 0)
		{
			ReturnToStartLambda(-1);
		}
	}
	else if (ScaleOfMoving > 0)
	{
		if (PassAngle < Param.Rotation)
		{
			RotateComponentLambda(1, Param.Rotation);
		}
		else if (PassAngle > Param.Rotation)
		{
			RotateComponentLambda(-1, Param.Rotation);
		}
	}
	else
	{
		if (Param.Mirror)
		{
			if (PassAngle < -Param.Rotation)
			{
				RotateComponentLambda(1, -Param.Rotation);
			}
			else if (PassAngle > -Param.Rotation)
			{
				RotateComponentLambda(-1, -Param.Rotation);
			}
		}
		else
		{
			if (PassAngle < Param.Rotation)
			{
				RotateComponentLambda(1, Param.Rotation);
			}
			else if (PassAngle > Param.Rotation)
			{
				RotateComponentLambda(-1, Param.Rotation);
			}
		}
	}
}
