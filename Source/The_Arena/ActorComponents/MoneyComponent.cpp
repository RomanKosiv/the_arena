// Fill out your copyright notice in the Description page of Project Settings.


#include "MoneyComponent.h"

#include "The_Arena/GameStates/CustomGameState.h"

UMoneyComponent::UMoneyComponent()
{
	Columns = 2;
	Rows = 1;
}

void UMoneyComponent::DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	auto DraggedItem = DragAndDropComponent->GetDraggedItem();
	
	if(Cast<AMoneyItemObject>(DraggedItem.InventoryItemObject))
	{
		Super::DropItem(DragAndDropComponent, bWithItemAmount);
	}
}

void UMoneyComponent::DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent,
	bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	auto DraggedItem = DragAndDropComponent->GetDraggedItem();

	if (Cast<AMoneyItemObject>(DraggedItem.InventoryItemObject))
	{
		Super::DropItemAt(TopLeftIndex, DragAndDropComponent, bWithItemAmount);
	}
}

bool UMoneyComponent::TryAddItem(AInventoryItemObject* InventoryItemObject, bool bWithItemAmount)
{
	if (Cast<AMoneyItemObject>(InventoryItemObject))
	{
		return Super::TryAddItem(InventoryItemObject, bWithItemAmount);
	}
	
	return false;
}

bool UMoneyComponent::TryAddItemAt(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount)
{
	if (Cast<AMoneyItemObject>(InventoryItemObject))
	{
		return Super::TryAddItemAt(InventoryItemObject, TopLeftIndex, bWithItemAmount);
	}

	return false;
}

bool UMoneyComponent::IsRoomAvailable(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount)
{
	if (Cast<AMoneyItemObject>(InventoryItemObject))
	{
		return Super::IsRoomAvailable(InventoryItemObject, TopLeftIndex, bWithItemAmount);
	}

	return false;
}

int UMoneyComponent::GetMoney() const
{
	AInventoryItemObject* MoneyItemObject = nullptr;

	if (Items.Num() > 0)
	{
		MoneyItemObject = Items[0];
	}
	
	if(MoneyItemObject)
	{
		return MoneyItemObject->GetCurrentItemAmount();
	}

	return 0;
}

void UMoneyComponent::SetMoney(int Money)
{
	AInventoryItemObject* MoneyItemObject = nullptr;

	if(Items.Num() > 0)
	{
		MoneyItemObject = Items[0];
	}
	
	if(Money == 0)
	{
		if(MoneyItemObject)
		{
			RemoveItem(MoneyItemObject, ERemoveItemOption::WithoutSave);
		}
		
		return;
	}
	
	if (MoneyItemObject)
	{
		MoneyItemObject->SetCurrentItemAmount(Money);
	}
	else
	{	
		if(auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
		{
			if(auto InventoryItemObjectManager = CustomGameState->GetInventoryItemObjectsManager())
			{
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.Owner = GetOwner();

				auto NewMoneyItem = InventoryItemObjectManager->CreateInventoryItemObject("Money", SpawnParameters);
				
				if (NewMoneyItem)
				{
					NewMoneyItem->SetCurrentItemAmount(Money);
					
					if (!TryAddItem(NewMoneyItem, true))
					{
						if(auto DroppedItemManager = CustomGameState->GetDroppedItemsManager())
						{
							DroppedItemManager->SpawnInventoryItemInFrontOfTargetActor(NewMoneyItem, GetOwner());
							NewMoneyItem->Destroy();
						}
					}
				}
			}	
		}
	}
}
