// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FiringWeaponAdditionalParameters.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UFiringWeaponAdditionalParameters : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:	
	

protected:
	UFiringWeaponAdditionalParameters();
	virtual void BeginPlay() override;	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void Initialization();
	void Deinitialization();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetAdditionalHorizontalRecoil() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetAdditionalVerticalRecoil() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetAdditionalHorizontalSpread() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
		float GetAdditionalVerticalSpread() const;

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
		float AdditionalHorizontalRecoil;
	UPROPERTY(EditDefaultsOnly)
		float AdditionalVerticalRecoil;
	UPROPERTY(EditDefaultsOnly)
		float AdditionalHorizontalSpread;
	UPROPERTY(EditDefaultsOnly)
		float AdditionalVerticalSpread;
		
};
