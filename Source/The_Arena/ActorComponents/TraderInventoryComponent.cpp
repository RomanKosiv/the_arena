// Fill out your copyright notice in the Description page of Project Settings.


#include "TraderInventoryComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/PlayerBasketComponent.h"
#include "The_Arena/ActorComponents/TraderBasketComponent.h"
#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/ActorComponents/HealthComponent.h"
#include "The_Arena/Actors/TraderBasketHolder.h"


void UTraderInventoryComponent::TraderInventoryOpeningInitialization(APlayerController* OpeningController)
{
	if (!OpeningController || GetOwnerRole() != ROLE_Authority)
		return;

	auto Pawn = OpeningController->GetPawn();

	if (Pawn)
	{
		auto PlayerInventoryComponent = Pawn->FindComponentByClass<UCustomInventoryComponent>();
		auto PlayerMoneyComponent = Pawn->FindComponentByClass<UMoneyComponent>();
		
		auto PlayerBasket = NewObject<UPlayerBasketComponent>(Pawn);
		
		if (PlayerBasket && PlayerInventoryComponent && PlayerMoneyComponent)
		{
			PlayerBasket->RegisterComponent();
			PlayerBasket->SetIsReplicated(true);
			
			auto EquipmentComponent = Pawn->FindComponentByClass<UEquipmentComponent>();
			
			if(PlayerInventoryComponent && EquipmentComponent)
			{
				PlayerBasket->Initialization(PlayerInventoryComponent, EquipmentComponent, PlayerMoneyComponent);
			}
			
			PlayerBasketComponents.Add(PlayerBasket);
			OnRep_PlayerBasketComponents();
		}

		if(auto OwnerLocal = GetOwner())
		{
			if(auto TraderBasketHolder = GetWorld()->SpawnActor<ATraderBasketHolder>(TraderBasketHolderClass, OwnerLocal->GetActorLocation(), OwnerLocal->GetActorRotation()))
			{
				auto TraderBasket = TraderBasketHolder->FindComponentByClass<UTraderBasketComponent>();

				if (TraderBasket && PlayerInventoryComponent && PlayerMoneyComponent)
				{
					TraderBasket->Initialization(this, PlayerInventoryComponent, PlayerMoneyComponent);

					TraderBasketComponents.Add(TraderBasket);
					OnRep_TraderBasketComponents();
				}
			}
		}
	}
}

void UTraderInventoryComponent::TraderInventoryClosingInitialization(APlayerController* ClosingController)
{
	if (!ClosingController || GetOwnerRole() != ROLE_Authority)
		return;

	auto Pawn = ClosingController->GetPawn();

	if (Pawn)
	{
		int PlayerBasketComponentNum = -1;
		if (auto PlayerBasket = Pawn->FindComponentByClass<UPlayerBasketComponent>())
		{
			PlayerBasketComponentNum = PlayerBasketComponents.Find(PlayerBasket);
			PlayerBasketComponents.Remove(PlayerBasket);
  			PlayerBasket->DestroyComponent();
		}

		if (PlayerBasketComponentNum != INDEX_NONE && PlayerBasketComponentNum < TraderBasketComponents.Num())
		{
			if (auto TraderBasket = TraderBasketComponents[PlayerBasketComponentNum])
			{
				TraderBasketComponents.Remove(TraderBasket);

				if (auto TraderBasketOwner = TraderBasket->GetOwner())
				{
					TraderBasketOwner->Destroy();
				}
				else
				{
					TraderBasket->DestroyComponent();
				}		
			}
		}	

		if(auto CustomInventoryComponent = Pawn->FindComponentByClass<UCustomInventoryComponent>())
		{
			CustomInventoryComponent->GetInventoryOpened_PC_Dispatcher().RemoveDynamic(this, &UTraderInventoryComponent::TraderInventoryOpeningInitialization);
			CustomInventoryComponent->GetInventoryClosed_PC_Dispatcher().RemoveDynamic(this, &UTraderInventoryComponent::TraderInventoryClosingInitialization);
		}
	}
}

TArray<UPlayerBasketComponent*> UTraderInventoryComponent::GetPlayerBasketComponents() const
{
	return PlayerBasketComponents;
}

TArray<UTraderBasketComponent*> UTraderInventoryComponent::GetTraderBasketComponents() const
{
	return TraderBasketComponents;
}

void UTraderInventoryComponent::DragItem(AInventoryItemObject* InventoryItemObject,
                                         UDragAndDropManagerComponent* DragAndDropComponent)
{
	if (!InventoryItemObject || !DragAndDropComponent)
		return;

	if (Contains(InventoryItemObject))
	{
		int Tile = IndexOfItem(InventoryItemObject);

		if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
		{
			if (auto InventoryItemObjectManager = CustomGameState->GetInventoryItemObjectsManager())
			{
				UInventoryItemParam* NewItemObjectParam = NewObject<UInventoryItemParam>(GetOwner());
				
				if(NewItemObjectParam)
				{
					if (auto ItemObjectParam = InventoryItemObject->GetInventoryItemParam())
					{
						NewItemObjectParam->ItemAmount = ItemObjectParam->ItemAmount;
					}
				}
				
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.Owner = DragAndDropComponent->GetControlledPawn();
				
				auto ItemObject = InventoryItemObjectManager->CreateInventoryItemObject(InventoryItemObject->GetClass(), SpawnParameters, NewItemObjectParam);

				if (ItemObject)
				{
					DragAndDropComponent->SetDraggedItem(ItemObject, this, Tile);
				}
			}
		}	
	}
}

void UTraderInventoryComponent::DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	FDraggedItem DraggedItem = DragAndDropComponent->GetDraggedItem();

	if (!Cast<UTraderBasketComponent>(DraggedItem.InventoryBaseComponent) &&
		!Cast<UTraderInventoryComponent>(DraggedItem.InventoryBaseComponent))
	{		
		return;
	}

	if (Cast<UTraderInventoryComponent>(DraggedItem.InventoryBaseComponent))
	{
		if(DraggedItem.InventoryBaseComponent != this)
		{
			return;
		}
	}

	DragAndDropComponent->DestroyDraggedItem();
}

void UTraderInventoryComponent::DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	DropItem(DragAndDropComponent, bWithItemAmount);
}

bool UTraderInventoryComponent::CanUsed(AActor* ActorUser)
{
	if (!ActorUser)
		return false;

	if (auto Owner = GetOwner())
	{
		auto HealthComponent = ActorUser->FindComponentByClass<UHealthComponent>();

		if (HealthComponent)
		{
			if (!HealthComponent->IsAlive())
			{
				return false;
			}
		}

		float CurrentDistance = 0.0f;
		if (auto MeshComponent = Owner->FindComponentByClass<UMeshComponent>())
		{
			CurrentDistance = (MeshComponent->GetComponentLocation() - ActorUser->GetActorLocation()).Size();
		}
		else
		{
			CurrentDistance = Owner->GetDistanceTo(ActorUser);
		}

		if (CurrentDistance > InteractionDistance)
		{
			return false;
		}

		return true;
	}

	return false;
}

void UTraderInventoryComponent::RemoveItem(AInventoryItemObject* InventoryItemObject,
	ERemoveItemOption RemoveItemOption)
{
	
}

FDMD& UTraderInventoryComponent::GetPlayerBasketComponentsChangedDispatcher()
{
	return PlayerBasketComponentsChangedDispatcher;
}

FDMD& UTraderInventoryComponent::GetTraderBasketComponentsChangedDispatcher()
{
	return TraderBasketComponentsChangedDispatcher;
}

void UTraderInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	if(GetOwnerRole() == ROLE_Authority)
	{
		StartTraderItemsInitialization();
	}
}

void UTraderInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTraderInventoryComponent, PlayerBasketComponents);
	DOREPLIFETIME(UTraderInventoryComponent, TraderBasketComponents);
}

void UTraderInventoryComponent::StartTraderItemsInitialization()
{
	if (StartTraderItems.Num() == 0)
		return;

	if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
	{
		if (auto InventoryItemObjectManager = CustomGameState->GetInventoryItemObjectsManager())
		{
			for (int i = 0; i < StartTraderItems.Num(); ++i)
			{
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.Owner = GetOwner();
				
				auto ItemObject = InventoryItemObjectManager->CreateInventoryItemObject(StartTraderItems[i].ItemObjectClass, SpawnParameters);

				if (ItemObject)
				{
					ItemObject->SetCurrentItemAmount(StartTraderItems[i].Amount);
					TryAddItem(ItemObject, true);
				}
			}
		}
		else
		{
			CustomGameState->GetInitializationDispatcher().AddDynamic(this, &UTraderInventoryComponent::StartTraderItemsInitialization);
		}
	}
}

void UTraderInventoryComponent::OnRep_PlayerBasketComponents()
{
	PlayerBasketComponentsChangedDispatcher.Broadcast();
}

void UTraderInventoryComponent::OnRep_TraderBasketComponents()
{
	TraderBasketComponentsChangedDispatcher.Broadcast();
}
