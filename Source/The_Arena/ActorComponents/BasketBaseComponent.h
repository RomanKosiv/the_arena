// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/ActorComponents/ResizableInventoryComponent.h"
#include "BasketBaseComponent.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API UBasketBaseComponent : public UResizableInventoryComponent
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FString GetReplicatedName() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetCommonItemsCost() const;

	FDMD& GetCommonItemsCostDispatcher();
	
protected:
	void BeginPlay() override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void UpdateOnCurrentItemAmount() override;
	void UpdateOnAttachModule() override;
	
	UFUNCTION()
	virtual void UpdateCommonItemsCost();

	UFUNCTION()
	void OnRep_CommonItemsCost();
	
	//Fields
protected:
	UPROPERTY(Replicated)
	FString ReplicatedName;
	UPROPERTY(ReplicatedUsing = OnRep_CommonItemsCost)
	int CommonItemsCost;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD CommonItemsCostDispatcher;
};
