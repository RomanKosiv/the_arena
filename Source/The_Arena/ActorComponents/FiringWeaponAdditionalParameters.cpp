// Fill out your copyright notice in the Description page of Project Settings.


#include "FiringWeaponAdditionalParameters.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"

// Sets default values for this component's properties
UFiringWeaponAdditionalParameters::UFiringWeaponAdditionalParameters()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFiringWeaponAdditionalParameters::BeginPlay()
{
	Super::BeginPlay();

	Initialization();
}


// Called every frame
void UFiringWeaponAdditionalParameters::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UFiringWeaponAdditionalParameters::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Deinitialization();
}

void UFiringWeaponAdditionalParameters::Initialization()
{
	if(auto Owner = GetOwner())
	{
		if (auto FiringWeapon = Cast<ARifleBase>(Owner->GetOwner()))
		{
			FiringWeapon->SetHorizontalRecoil(FiringWeapon->GetHorizontalRecoil() + AdditionalHorizontalRecoil);
			FiringWeapon->SetVerticalRecoil(FiringWeapon->GetVerticalRecoil() + AdditionalVerticalRecoil);
			FiringWeapon->SetVerticalSpread(FiringWeapon->GetVerticalSpread() + AdditionalVerticalSpread);
			FiringWeapon->SetHorizontalSpread(FiringWeapon->GetHorizontalSpread() + AdditionalHorizontalSpread);
		}
	}	
}

void UFiringWeaponAdditionalParameters::Deinitialization()
{
	if (auto Owner = GetOwner())
	{
		if (auto FiringWeapon = Cast<ARifleBase>(Owner->GetOwner()))
		{
			FiringWeapon->SetHorizontalRecoil(FiringWeapon->GetHorizontalRecoil() - AdditionalHorizontalRecoil);
			FiringWeapon->SetVerticalRecoil(FiringWeapon->GetVerticalRecoil() - AdditionalVerticalRecoil);
			FiringWeapon->SetVerticalSpread(FiringWeapon->GetVerticalSpread() - AdditionalVerticalSpread);
			FiringWeapon->SetHorizontalSpread(FiringWeapon->GetHorizontalSpread() - AdditionalHorizontalSpread);
		}
	}
}

float UFiringWeaponAdditionalParameters::GetAdditionalHorizontalRecoil() const
{
	return AdditionalHorizontalRecoil;
}

float UFiringWeaponAdditionalParameters::GetAdditionalVerticalRecoil() const
{
	return AdditionalVerticalRecoil;
}

float UFiringWeaponAdditionalParameters::GetAdditionalHorizontalSpread() const
{
	return AdditionalHorizontalSpread;
}

float UFiringWeaponAdditionalParameters::GetAdditionalVerticalSpread() const
{
	return AdditionalVerticalSpread;
}

