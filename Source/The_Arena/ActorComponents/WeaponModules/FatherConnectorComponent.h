// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/WeaponModules/BaseConnectorComponent.h"

#include "FatherConnectorComponent.generated.h"


class UMotherConnectorComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class THE_ARENA_API UFatherConnectorComponent : public UBaseConnectorComponent
{
	GENERATED_BODY()

	//Methods
public:
	void SetupAttachment(UMotherConnectorComponent* MotherConnector);
	void RemoveAttachment();

	AActor* GetAttachedModule() const;
	UMotherConnectorComponent* GetAttachedMotherConnector() const;
protected:
	UFatherConnectorComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//Fields
protected:
	UPROPERTY(Replicated)
	UMotherConnectorComponent* AttachedMotherConnector;
	UPROPERTY(Replicated)
	AActor* AttachedModule;
};
