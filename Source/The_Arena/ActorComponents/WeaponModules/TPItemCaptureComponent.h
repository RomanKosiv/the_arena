// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/Classes/Components/SceneCaptureComponent2D.h"

#include "TPItemCaptureComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UTPItemCaptureComponent : public UActorComponent
{
	GENERATED_BODY()

	
	//Methods
public:
	USceneCaptureComponent2D* GetItemSceneCapture() const;
	TSubclassOf<AActor> GetItemCreatingIconSceneClass() const;
	
protected:
	// Sets default values for this component's properties
	UTPItemCaptureComponent();
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Fields
protected:
	UPROPERTY(BlueprintReadWrite)
		USceneCaptureComponent2D* ItemSceneCapture;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AActor> ItemCreatingIconSceneClass;

		
};
