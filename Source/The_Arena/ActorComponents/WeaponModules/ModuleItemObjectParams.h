// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ModuleItemObjectParams.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UModuleItemObjectParams : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:
	FIntPoint GetAdditionalDimensions() const;
	FVector GetAdditionalSceneCaptureLocation() const;
protected:
	UModuleItemObjectParams();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	FIntPoint AdditionalDimensions;
	UPROPERTY(EditDefaultsOnly)
	FVector AdditionalSceneCaptureLocation;
};
