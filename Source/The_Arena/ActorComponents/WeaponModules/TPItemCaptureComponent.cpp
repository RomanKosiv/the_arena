// Fill out your copyright notice in the Description page of Project Settings.


#include "TPItemCaptureComponent.h"

USceneCaptureComponent2D* UTPItemCaptureComponent::GetItemSceneCapture() const
{
	return ItemSceneCapture;
}

TSubclassOf<AActor> UTPItemCaptureComponent::GetItemCreatingIconSceneClass() const
{
	return ItemCreatingIconSceneClass;
}

// Sets default values for this component's properties
UTPItemCaptureComponent::UTPItemCaptureComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTPItemCaptureComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTPItemCaptureComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

