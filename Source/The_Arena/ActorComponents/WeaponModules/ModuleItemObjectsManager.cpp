// Fill out your copyright notice in the Description page of Project Settings.


#include "ModuleItemObjectsManager.h"

#include "Net/UnrealNetwork.h"

#include "The_Arena/ActorComponents/EquipmentComponent.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"
#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemsManager.h"


void UModuleItemObjectsManager::CreateModuleItems(AInventoryItem* InInventoryItem, bool bTestItem)
{
	if (!InInventoryItem)
	{
		return;
	}

	for (auto ModuleItemObject : ModuleItemObjects)
	{
		TryCreateModuleItem(InInventoryItem, ModuleItemObject, bTestItem);
	}
}

bool UModuleItemObjectsManager::TryAttachModule(AInventoryItemObject* InModuleItemObject)
{
	if (!InModuleItemObject)
		return false;

	bool bAttachedSuccessful = false;
	
	if (auto ItemObject = Cast<AInventoryItemObject>(GetOwner()))
	{
		if (auto ItemObjectOwner = ItemObject->GetOwner())
		{
			bool bMarkedForDestory = false;
			AInventoryItem* ModulableItem = nullptr;
			
			if (auto EquipmentComponent = ItemObjectOwner->FindComponentByClass<UEquipmentComponent>())
			{
				auto EquipmentType = EquipmentComponent->FindEquipmentType(ItemObject);
				auto UsingItemObjectInHands = EquipmentComponent->GetUsingItemObjectInHands();

				if (EquipmentType != EEquipmentType::NONE)
				{
					auto Equipment = EquipmentComponent->GetEquipment(EquipmentType);
					ModulableItem = Equipment.InventoryItem;
				}
				else if (UsingItemObjectInHands && UsingItemObjectInHands == ItemObject)
				{
					ModulableItem = EquipmentComponent->GetUsingItemInHands();
				}
			}

			if(!ModulableItem)
			{
				auto ItemObjectCommonParams = ItemObject->GetInventoryItemCommonParams();
				auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState());

				if (ItemObjectCommonParams && CustomGameState)
				{
					if (auto InventoryItemsManager = CustomGameState->GetInventoryItemsManager())
					{
						FActorSpawnParameters SpawnParameters;
						FInventoryItemParams ItemParams;
						ItemParams.ItemObject = ItemObject;
						ItemParams.bTestItem = true;

						ModulableItem = InventoryItemsManager->CreateInventoryItem(ItemObjectCommonParams->GetClassOfInventoryItem(), SpawnParameters, ItemParams);
						bMarkedForDestory = true;
					}
				}
			}	

			if (ModulableItem)
			{
				bool bTestModuleItem = false;
				if(bMarkedForDestory)
				{
					bTestModuleItem = true;
				}
				
				if (TryCreateModuleItem(ModulableItem, InModuleItemObject, bTestModuleItem))
				{
					ModuleItemObjects.Add(InModuleItemObject);
					OnRep_ModuleItemObjects();
					bAttachedSuccessful = true;
				}
			}

			if (bMarkedForDestory)
			{
				if (ModulableItem)
				{
					ModulableItem->Destroy();
				}
			}
		}
	}

	return bAttachedSuccessful;
}

bool UModuleItemObjectsManager::TryDetachModule(AInventoryItemObject* InModuleItemObject)
{
	if (!InModuleItemObject)
		return false;

	if(ModuleItemObjects.Contains(InModuleItemObject))
	{
		if (auto ItemObject = Cast<AInventoryItemObject>(GetOwner()))
		{
			if (auto ItemObjectOwner = ItemObject->GetOwner())
			{
				if (auto EquipmentComponent = ItemObjectOwner->FindComponentByClass<UEquipmentComponent>())
				{
					AInventoryItem* ModulableItem = nullptr;

					auto EquipmentType = EquipmentComponent->FindEquipmentType(ItemObject);

					if (EquipmentType != EEquipmentType::NONE)
					{
						auto Equipment = EquipmentComponent->GetEquipment(EquipmentType);
						ModulableItem = Equipment.InventoryItem;
					}
					else
					{
						if (auto UsingItemObjectInHands = EquipmentComponent->GetUsingItemObjectInHands())
						{
							if (UsingItemObjectInHands == ItemObject)
							{
								ModulableItem = EquipmentComponent->GetUsingItemInHands();
							}
						}
					}

					if (ModulableItem)
					{
						TryRemoveModuleItem(ModulableItem, InModuleItemObject);
					}
				}
			}
		}
		
		ModuleItemObjects.Remove(InModuleItemObject);
		OnRep_ModuleItemObjects();
		
		return true;
	}

	return false;
}

bool UModuleItemObjectsManager::CanModuleAttached(AInventoryItemObject* InModuleItemObject)
{
	if (!InModuleItemObject)
		return false;

	bool bCanAttached = false;
	
	if (auto ItemObject = Cast<AInventoryItemObject>(GetOwner()))
	{
		if (auto ItemObjectCommonParams = ItemObject->GetInventoryItemCommonParams())
		{
			if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
			{
				if (auto InventoryItemsManager = CustomGameState->GetInventoryItemsManager())
				{
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.Owner = ItemObject->GetOwner();
					
					FInventoryItemParams ItemParams;
					ItemParams.ItemObject = ItemObject;
					ItemParams.bTestItem = true;
					
					auto ModulableItem = InventoryItemsManager->CreateInventoryItem(ItemObjectCommonParams->GetClassOfInventoryItem(), SpawnParameters, ItemParams);

					if(ModulableItem)
					{
						if(TryCreateModuleItem(ModulableItem, InModuleItemObject, true))
						{
							bCanAttached = true;
						}
						
						ModulableItem->Destroy();
					}
				}
			}
		}
	}
	
	return bCanAttached;
}

TArray<AInventoryItemObject*> UModuleItemObjectsManager::GetModuleItemObjects() const
{
	return ModuleItemObjects;
}

void UModuleItemObjectsManager::ClearModuleItemObjectsArray()
{
	ModuleItemObjects.Empty();
}

FDMD& UModuleItemObjectsManager::GetModuleItemObjectsDispatcher()
{
	return ModuleItemObjectsDispatcher;
}

// Sets default values for this component's properties
UModuleItemObjectsManager::UModuleItemObjectsManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UModuleItemObjectsManager::BeginPlay()
{
	Super::BeginPlay();

	StartModuleItemObjectsInitialization();
}


// Called every frame
void UModuleItemObjectsManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UModuleItemObjectsManager::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(GetOwnerRole() == ROLE_Authority)
	{
		for (auto ModuleItemObject : ModuleItemObjects)
		{
			if (ModuleItemObject)
			{
				ModuleItemObject->Destroy();
			}
		}

		ModuleItemObjects.Empty();
	}
	
	Super::EndPlay(EndPlayReason);
}

void UModuleItemObjectsManager::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UModuleItemObjectsManager, ModuleItemObjects)
}

void UModuleItemObjectsManager::StartModuleItemObjectsInitialization()
{
	if (GetOwnerRole() != ROLE_Authority)
		return;

	if(auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
	{
		if(auto InventoryItemObjectsManager = CustomGameState->GetInventoryItemObjectsManager())
		{
			for(int i = 0; i < StartModuleItemObjectClasses.Num(); ++i)
			{
				if(auto ModuleItemObjectClass = StartModuleItemObjectClasses[i])
				{		
					FActorSpawnParameters SpawnParameters;

					if(auto ItemObject = GetOwner())
					{
						if(auto ItemObjectOwner = ItemObject->GetOwner())
						{
							SpawnParameters.Owner = ItemObjectOwner;
						}
					}
						
					if(auto ModuleItemObject = InventoryItemObjectsManager->CreateInventoryItemObject(ModuleItemObjectClass, SpawnParameters))
					{
						TryAttachModule(ModuleItemObject);
					}
				}
			}
		}
	}
}

bool UModuleItemObjectsManager::TryCreateModuleItem(AInventoryItem* InInventoryItem, AInventoryItemObject* ModuleItemObject, bool bTestItem)
{
	if (!InInventoryItem || !ModuleItemObject)
		return false;

	auto ModuleItemsManager = InInventoryItem->FindComponentByClass<UModuleItemsManager>();
	auto CommonParams = ModuleItemObject->GetInventoryItemCommonParams();
	auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState());

	if (ModuleItemsManager && CommonParams && CustomGameState)
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = InInventoryItem;

		FInventoryItemParams InventoryItemParams;
		InventoryItemParams.ItemObject = ModuleItemObject;
		InventoryItemParams.bTestItem = bTestItem;

		if (auto InventoryItemsManager = CustomGameState->GetInventoryItemsManager())
		{
			auto ModuleItem = InventoryItemsManager->CreateInventoryItem(CommonParams->GetClassOfInventoryItem(), SpawnParameters, InventoryItemParams);

			if (ModuleItem)
			{
				if (ModuleItemsManager->TryAttachModule(ModuleItem))
				{
					if (auto TPInventoryItem = InInventoryItem->GetTPInventoryItem())
					{
						if (auto TPModuleItemsManager = TPInventoryItem->FindComponentByClass<UModuleItemsManager>())
						{
							if (auto TPModuleItem = ModuleItem->GetTPInventoryItem())
							{
								TPModuleItemsManager->TryAttachModule(TPModuleItem);
							}
						}
					}

					return true;
				}

				ModuleItem->Destroy();
			}
		}
	}

	return false;
}

bool UModuleItemObjectsManager::TryRemoveModuleItem(AInventoryItem* InInventoryItem,
	AInventoryItemObject* ModuleItemObject)
{
	if (!InInventoryItem || !ModuleItemObject)
		return false;

	auto ModuleItemsManager = InInventoryItem->FindComponentByClass<UModuleItemsManager>();
	auto ModuleCommonParams = ModuleItemObject->GetInventoryItemCommonParams();
	auto TPInventoryItem = InInventoryItem->GetTPInventoryItem();

	if (ModuleItemsManager && ModuleCommonParams && TPInventoryItem)
	{
		auto AttachedModules = ModuleItemsManager->GetAttachedModuleItems();
		auto ModuleItemClass = ModuleCommonParams->GetClassOfInventoryItem();
		auto TPModuleItemsManager = TPInventoryItem->FindComponentByClass<UModuleItemsManager>();

		if (TPModuleItemsManager && ModuleItemClass)
		{
			for (auto AttachedModule : AttachedModules)
			{
				if (auto ModuleItem = Cast<AInventoryItem>(AttachedModule))
				{
					if (ModuleItem->GetClass() == ModuleItemClass)
					{
						if (ModuleItemsManager->TryDetachModule(ModuleItem))
						{
							TPModuleItemsManager->TryDetachModule(ModuleItem->GetTPInventoryItem());
							ModuleItem->Destroy();

							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

void UModuleItemObjectsManager::OnRep_ModuleItemObjects()
{
	ModuleItemObjectsDispatcher.Broadcast();
}

