// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "The_Arena/UObjects/DefaultDelegates.h"

#include "ModuleItemObjectsManager.generated.h"


class AInventoryItemObject;
class AInventoryItem;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UModuleItemObjectsManager : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable)
	void CreateModuleItems(AInventoryItem* InInventoryItem, bool bTestItem = false);
	
	UFUNCTION(BlueprintCallable)
	bool TryAttachModule(AInventoryItemObject* InModuleItemObject);
	UFUNCTION(BlueprintCallable)
	bool TryDetachModule(AInventoryItemObject* InModuleItemObject);
	UFUNCTION(BlueprintCallable)
	bool CanModuleAttached(AInventoryItemObject* InModuleItemObject);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<AInventoryItemObject*> GetModuleItemObjects() const;

	void ClearModuleItemObjectsArray();

	FDMD& GetModuleItemObjectsDispatcher();

protected:
	UModuleItemObjectsManager();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void StartModuleItemObjectsInitialization();

	bool TryCreateModuleItem(AInventoryItem* InInventoryItem, AInventoryItemObject* ModuleItemObject, bool bTestItem = false);
	bool TryRemoveModuleItem(AInventoryItem* InInventoryItem, AInventoryItemObject* ModuleItemObject);

	UFUNCTION()
	void OnRep_ModuleItemObjects();
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<AInventoryItemObject>> StartModuleItemObjectClasses;
	UPROPERTY(ReplicatedUsing = OnRep_ModuleItemObjects)
	TArray<AInventoryItemObject*> ModuleItemObjects;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD ModuleItemObjectsDispatcher;
};
