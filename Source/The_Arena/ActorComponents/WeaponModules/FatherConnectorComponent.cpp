// Fill out your copyright notice in the Description page of Project Settings.


#include "FatherConnectorComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/ActorComponents/WeaponModules/MotherConnectorComponent.h"


void UFatherConnectorComponent::SetupAttachment(UMotherConnectorComponent* MotherConnector)
{
	if (!MotherConnector)
		return;

	auto Owner = MotherConnector->GetOwner();

	if(auto ModuleItem = Cast<AActor>(Owner))
	{
		AttachedMotherConnector = MotherConnector;
		AttachedModule = ModuleItem;
	}	
}

void UFatherConnectorComponent::RemoveAttachment()
{
	AttachedMotherConnector = nullptr;
	AttachedModule = nullptr;
}

AActor* UFatherConnectorComponent::GetAttachedModule() const
{
	return AttachedModule;
}

UMotherConnectorComponent* UFatherConnectorComponent::GetAttachedMotherConnector() const
{
	return AttachedMotherConnector;
}

// Sets default values for this component's properties
UFatherConnectorComponent::UFatherConnectorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UFatherConnectorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UFatherConnectorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UFatherConnectorComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UFatherConnectorComponent, AttachedModule);
	DOREPLIFETIME(UFatherConnectorComponent, AttachedMotherConnector);
}

