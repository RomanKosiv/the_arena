// Fill out your copyright notice in the Description page of Project Settings.


#include "DragAndDropManagerComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/PlayerBasketComponent.h"
#include "The_Arena/ActorComponents/TraderBasketComponent.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectsManager.h"
#include "The_Arena/UObjects/FunctionLibraries/DefaultFunctionLibrary.h"
#include "The_Arena/Interfaces/InventoryUsable.h"


// Sets default values for this component's properties
UDragAndDropManagerComponent::UDragAndDropManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

FDraggedItem UDragAndDropManagerComponent::GetDraggedItem() const
{
	return DraggedItem;
}

void UDragAndDropManagerComponent::SetDraggedItem(FDraggedItem InDraggedItem)
{
	DraggedItem = InDraggedItem;
}

void UDragAndDropManagerComponent::SetDraggedItem(AInventoryItemObject* InventoryItemObject,
	UInventoryBaseComponent* InventoryBaseComponent, int TileIndex)
{
	DraggedItem = { InventoryItemObject, InventoryBaseComponent, TileIndex };
}

void UDragAndDropManagerComponent::DestroyDraggedItem()
{
	if (auto DraggedItemObject = DraggedItem.InventoryItemObject)
	{
		DraggedItemObject->Destroy();
	}

	SetDraggedItem(nullptr, nullptr);
}

void UDragAndDropManagerComponent::ServerSpawnItemsWithSplitting_Implementation(int ItemAmount)
{
	if (auto DraggedItemObject = DraggedItem.InventoryItemObject)
	{
		if (DraggedItemObject->GetCurrentItemAmount() <= ItemAmount)
		{
			ServerDestroyDraggedItemWithSpawn();
		}
		else
		{
			auto InventoryBaseComponent = DraggedItem.InventoryBaseComponent;
			auto TileIndex = DraggedItem.TileIndex;

			auto SplittedItemObject = SplitDraggedItem(ItemAmount);
			ServerDropItemBack();

			if (SplittedItemObject)
			{
				SetDraggedItem(SplittedItemObject, InventoryBaseComponent, TileIndex);
				ServerDestroyDraggedItemWithSpawn();
			}
		}
	}
}

void UDragAndDropManagerComponent::ServerRotateDraggedItem_Implementation()
{
	if (auto DraggedItemObject = DraggedItem.InventoryItemObject)
	{
		DraggedItemObject->ServerRotate();
	}
}

void UDragAndDropManagerComponent::ServerDropItemBack_Implementation(bool bTryAddItemToPlayerInventory)
{
	if (auto InventoryBaseComponent = DraggedItem.InventoryBaseComponent)
	{
		DropItemAt(DraggedItem.TileIndex, InventoryBaseComponent, false);

		if (DraggedItem.InventoryItemObject != nullptr)
		{
			DropItem(InventoryBaseComponent, false);
		}
	}

	if (bTryAddItemToPlayerInventory)
	{
		if (DraggedItem.InventoryItemObject != nullptr)
		{
			DropItem(GetControlledPawnInventoryComponent(), false);
		}
	}

	if (DraggedItem.InventoryItemObject != nullptr)
	{
		ServerDestroyDraggedItemWithSpawn();
	}
}

void UDragAndDropManagerComponent::ServerDropItemsWithSplitting_Implementation(int SplitNumber, int TileIndex,
	UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount)
{
	if (!InventoryBaseComponent)
	{
		ServerDropItemBack();
		return;
	}

	auto DraggedInventoryBaseComponent = DraggedItem.InventoryBaseComponent;
	auto DraggedTileIndex = DraggedItem.TileIndex;

	auto SplittedItemObject = SplitDraggedItem(SplitNumber);

	ServerDropItemBack();

	if (SplittedItemObject)
	{
		SetDraggedItem(SplittedItemObject, DraggedInventoryBaseComponent, DraggedTileIndex);
		ServerDropItemAt(TileIndex, InventoryBaseComponent, bWithItemAmount);
	}
}

void UDragAndDropManagerComponent::ServerDestroyDraggedItemWithSpawn_Implementation()
{
	auto CustomGameState = GetWorld()->GetGameState<ACustomGameState>();
	APawn* TargetActor = nullptr;

	auto Controller = Cast<APlayerController>(GetOwner());

	if (Controller)
	{
		TargetActor = Controller->GetPawn();
	}

	if (auto InventoryBaseComponent = DraggedItem.InventoryBaseComponent)
	{
		if (!Cast<UEquipmentComponent>(InventoryBaseComponent) &&
			!Cast<UCustomInventoryComponent>(InventoryBaseComponent) &&
			!Cast<UPlayerBasketComponent>(InventoryBaseComponent) &&
			!Cast<UMoneyComponent>(InventoryBaseComponent))
		{
			DestroyDraggedItem();
			return;
		}
	}

	if (CustomGameState && TargetActor)
	{
		if (auto DroppedItemsManager = CustomGameState->GetDroppedItemsManager())
		{
			DroppedItemsManager->SpawnInventoryItemInFrontOfTargetActor(DraggedItem.InventoryItemObject, TargetActor);
		}
	}

	DestroyDraggedItem();
}

void UDragAndDropManagerComponent::ServerDragItem_Implementation(AInventoryItemObject* InventoryItemObject, UInventoryBaseComponent* InventoryBaseComponent)
{
	if (!InventoryItemObject || !InventoryBaseComponent)
		return;

	if (IsUsable(InventoryBaseComponent))
	{
		InventoryBaseComponent->DragItem(InventoryItemObject, this);
	}
}

void UDragAndDropManagerComponent::ServerDropItem_Implementation(UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount)
{
	if (!InventoryBaseComponent)
	{
		ServerDropItemBack();
		return;
	}

	DropItem(InventoryBaseComponent, bWithItemAmount);

	if (DraggedItem.InventoryItemObject != nullptr)
	{
		ServerDropItemBack();
	}
}

void UDragAndDropManagerComponent::ServerDropItemAt_Implementation(int TileIndex,
	UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount)
{
	if (!InventoryBaseComponent)
	{
		ServerDropItemBack();
		return;
	}

	DropItemAt(TileIndex, InventoryBaseComponent, bWithItemAmount);

	if (DraggedItem.InventoryItemObject != nullptr)
	{
		ServerDropItemBack();
	}
}

// Called when the game starts
void UDragAndDropManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


// Called every frame
void UDragAndDropManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UDragAndDropManagerComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void UDragAndDropManagerComponent::DropItem(UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount)
{
	if (!InventoryBaseComponent)
		return;

	if (IsUsable(InventoryBaseComponent))
	{
		if(auto ItemObject = DraggedItem.InventoryItemObject)
		{
			ItemObject->SetOwner(InventoryBaseComponent->GetOwner());
		}
		
		InventoryBaseComponent->DropItem(this, bWithItemAmount);
	}
}

void UDragAndDropManagerComponent::DropItemAt(int TileIndex, UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount)
{
	if (!InventoryBaseComponent)
		return;

	if (IsUsable(InventoryBaseComponent))
	{
		if (auto ItemObject = DraggedItem.InventoryItemObject)
		{
			ItemObject->SetOwner(InventoryBaseComponent->GetOwner());
		}
		
		InventoryBaseComponent->DropItemAt(TileIndex, this, bWithItemAmount);
	}
}

AInventoryItemObject* UDragAndDropManagerComponent::SplitDraggedItem(int SplitNumber)
{
	if (auto ItemObject = DraggedItem.InventoryItemObject)
	{
		if (ItemObject->TryRemoveItems(SplitNumber))
		{
			auto InventoryItemParam = NewObject<UInventoryItemParam>();
			auto DraggedItemInventoryParam = ItemObject->GetInventoryItemParam();

			if (InventoryItemParam && DraggedItemInventoryParam)
			{
				DraggedItemInventoryParam->CopyTo(InventoryItemParam);
				InventoryItemParam->ItemAmount = SplitNumber;
			}

			if (auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
			{
				if (auto InventoryItemObjectManager = CustomGameState->GetInventoryItemObjectsManager())
				{
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.Owner = GetControlledPawn();
					
					return InventoryItemObjectManager->CreateInventoryItemObject(ItemObject->GetClass(), SpawnParameters, InventoryItemParam);
				}
			}
		}
	}

	return nullptr;
}

void UDragAndDropManagerComponent::ServerAttachDraggedItem_Implementation(AInventoryItemObject* ModulableItemObject, UInventoryBaseComponent* InventoryBaseComponent)
{
	auto AttachDraggedItemLambda = [&](AInventoryItemObject* InModuleItemObject)
	{
		if (auto ModuleItemsObjectsManager = ModulableItemObject->FindComponentByClass<UModuleItemObjectsManager>())
		{
			if (ModuleItemsObjectsManager->TryAttachModule(InModuleItemObject))
			{
				if(Cast<UCustomInventoryComponent>(InventoryBaseComponent) || Cast<UBasketBaseComponent>(InventoryBaseComponent))
				{
					ServerDragItem(ModulableItemObject, InventoryBaseComponent);
					ServerDropItemBack();
				}
			}
			else
			{
				ServerDropItemBack();
			}
		}
	};
	
	if (!ModulableItemObject || !InventoryBaseComponent)
		return;
	
	if (auto ModuleItemObject = DraggedItem.InventoryItemObject)
	{
		if (UDefaultFunctionLibrary::CanModuleAttached(GetControlledPawn(), ModulableItemObject, InventoryBaseComponent, ModuleItemObject, DraggedItem.InventoryBaseComponent))
		{
			auto DraggedItemInventoryBaseComponent = DraggedItem.InventoryBaseComponent;
			auto DraggedItemTileIndex = DraggedItem.TileIndex;

			if (ModuleItemObject->GetCurrentItemAmount() > 1)
			{
				auto SplittedModuleItemObject = SplitDraggedItem(ModuleItemObject->GetCurrentItemAmount() - 1);

				AttachDraggedItemLambda(ModuleItemObject);

				SetDraggedItem(SplittedModuleItemObject, DraggedItemInventoryBaseComponent, DraggedItemTileIndex);
				ServerDropItemBack();
			}
			else
			{
				AttachDraggedItemLambda(ModuleItemObject);
			}

			return;
		}
	}	

	ServerDropItemBack();
}

void UDragAndDropManagerComponent::ServerDetachWithDropInPlayerInventory_Implementation(AInventoryItemObject* ModulableItemObject, UInventoryBaseComponent* InventoryBaseComponent, AInventoryItemObject* ModuleItemObject)
{
	if (!ModulableItemObject || !InventoryBaseComponent || !ModuleItemObject)
		return;

	if (UDefaultFunctionLibrary::CanModuleDetached(GetControlledPawn(), ModulableItemObject))
	{
		if (auto ModuleItemsObjectsManager = ModulableItemObject->FindComponentByClass<UModuleItemObjectsManager>())
		{
			if (ModuleItemsObjectsManager->TryDetachModule(ModuleItemObject))
			{
				if(Cast<UCustomInventoryComponent>(InventoryBaseComponent) || Cast<UBasketBaseComponent>(InventoryBaseComponent))
				{
					ServerDragItem(ModulableItemObject, InventoryBaseComponent);
					ServerDropItemBack();
				}

				DraggedItem.InventoryItemObject = ModuleItemObject;
				DraggedItem.InventoryBaseComponent = InventoryBaseComponent;
				
				if(Cast<UEquipmentComponent>(InventoryBaseComponent))
				{
					ServerDropItem(GetControlledPawnInventoryComponent(), true);
				}
				else
				{	
					ServerDropItem(InventoryBaseComponent, true);
				}	
			}
		}
	}
}

void UDragAndDropManagerComponent::ServerUse_Implementation(AInventoryItemObject* InventoryItemObject,
	AActor* UsedActor)
{
	if (!InventoryItemObject || !UsedActor)
		return;

	if(InventoryItemObject->GetClass()->ImplementsInterface(UInventoryUsable::StaticClass()))
	{
		IInventoryUsable::Execute_Use(InventoryItemObject, UsedActor);
	}
}

void UDragAndDropManagerComponent::ServerBuy_Implementation(UTraderBasketComponent* BasketComponent)
{
	if (!BasketComponent)
		return;

	if(BasketComponent->CanUsed(GetControlledPawn()))
	{
		BasketComponent->Buy();
	}
}

void UDragAndDropManagerComponent::ServerSell_Implementation(UPlayerBasketComponent* BasketComponent)
{
	if (!BasketComponent)
		return;

	if (BasketComponent->CanUsed(GetControlledPawn()))
	{
		BasketComponent->Sell();
	}
}

bool UDragAndDropManagerComponent::IsUsable(UInventoryBaseComponent* InventoryBaseComponent)
{
	if (!InventoryBaseComponent)
		return false;

	if (auto PlayerController = Cast<APlayerController>(GetOwner()))
	{
		if (InventoryBaseComponent->CanUsed(PlayerController->GetPawn()))
		{
			return true;
		}
	}

	return false;
}

APawn* UDragAndDropManagerComponent::GetControlledPawn() const
{
	if (auto PlayerController = Cast<APlayerController>(GetOwner()))
	{
		if (auto ControlledPawn = PlayerController->GetPawn())
		{
			return ControlledPawn;
		}
	}

	return nullptr;
}

UCustomInventoryComponent* UDragAndDropManagerComponent::GetControlledPawnInventoryComponent() const
{
	if (auto PlayerPawn = GetControlledPawn())
	{
		if (auto ControlledPawnInventoryComponent = PlayerPawn->FindComponentByClass<UCustomInventoryComponent>())
		{
			return ControlledPawnInventoryComponent;
		}
	}

	return nullptr;
}
