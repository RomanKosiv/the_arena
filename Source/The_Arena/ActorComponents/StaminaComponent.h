// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/StatBaseComponent.h"

#include "StaminaComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UStaminaComponent : public UStatBaseComponent
{
	GENERATED_BODY()
	
	//Methods
protected:
	UStaminaComponent();
	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OnPreChangeCurrentStat() override;
	void OnChangedCurrentStat() override;
	
	UFUNCTION()
	void RunInitOnPreChanging();
	UFUNCTION()
	void RunInitOnChanged();
	UFUNCTION()
	void SoundInitOnChanged();

	UFUNCTION()
	void StaminaInitOnJumped();
	UFUNCTION()
	void StaminaInitOnLanded();
	UFUNCTION()
	void JumpInitOnChanged();
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Category = "RunningStaminaParameters")
		float SpeedOfDecreasingStaminaOnRunning = 1.0f;
	UPROPERTY(EditDefaultsOnly, Category = "RunningStaminaParameters")
		float RunningSpeedForBlockingStaminaDecrease = 0;
	UPROPERTY(EditDefaultsOnly, Category = "RunningStaminaParameters")
		float StaminaForEnableRunning = 5.0f;
	
	UPROPERTY(EditDefaultsOnly, Category = "JumpingStaminaParameters")
		float SpeedOfDecreasingStaminaOnJumping = 1.0f;
	UPROPERTY(EditDefaultsOnly, Category = "JumpingStaminaParameters")
		float StaminaForEnableJumping = 0;
	
	bool bRunBonusAdded = false;
	bool bRunningSpeedBlockedByStamina = false;
	bool bJumpRestrictionAdded = false;
};