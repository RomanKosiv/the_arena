// Fill out your copyright notice in the Description page of Project Settings.


#include "StatBaseComponent.h"
#include "Net/UnrealNetwork.h"


bool operator==(const FStatBonusEffect& Pr1, const FStatBonusEffect& Pr2)
{
	if (Pr1.Value == Pr2.Value && Pr1.Name == Pr2.Name)
		return true;

	return false;
}

// Sets default values for this component's properties
UStatBaseComponent::UStatBaseComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UStatBaseComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwnerRole() == ROLE_Authority)
	{
		SetCurrentStat(MaxStat);
		GetWorld()->GetTimerManager().SetTimer(ChangeStatHandle, this, &UStatBaseComponent::ChangeStat, Rate, true);
	}	
}


// Called every frame
void UStatBaseComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UStatBaseComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UStatBaseComponent, CurrentStat);
	DOREPLIFETIME(UStatBaseComponent, MaxStat);
}

void UStatBaseComponent::ChangeStat()
{
	OnPreChangeCurrentStat();

	float SumBonusEffects = StatIncreaseSpeed;

	for (auto Effect : BonusEffects)
	{
		if (!FMath::IsNearlyZero(Effect.Value))
		{
			if (Effect.Value > 0)
			{
				if (RestrictionsOnUsePositiveEffects.Num() > 0)
				{
					continue;
				}
			}
			else
			{
				if (RestrictionsOnUseNegativeEffects.Num() > 0)
				{
					continue;
				}
			}
		}

		SumBonusEffects += Effect.Value;
	}

	float FinalStat = CurrentStat + SumBonusEffects * Rate;

	if (FinalStat > MaxStat)
	{
		FinalStat = MaxStat;
	}

	if (FinalStat < 0.0f)
	{
		FinalStat = 0.0f;
	}

	if(!FMath::IsNearlyEqual(FinalStat, CurrentStat))
	{
		SetCurrentStat(FinalStat);
	}	
}

void UStatBaseComponent::OnPreChangeCurrentStat()
{
	PreChangeCurrentStatDispatcher.Broadcast();
}

void UStatBaseComponent::OnChangedCurrentStat()
{
	ChangedCurrentStatDispatcher.Broadcast();
}

float UStatBaseComponent::GetMaxStat() const
{
	return MaxStat;
}

void UStatBaseComponent::SetMaxStat(float InMaxStat)
{
	MaxStat = InMaxStat;
}

float UStatBaseComponent::GetCurrentStat() const
{
	return CurrentStat;
}

void UStatBaseComponent::SetCurrentStat(float InCurrentStat)
{
	CurrentStat = InCurrentStat;

	if (GetOwnerRole() == ROLE_Authority)
	{
		OnChangedCurrentStat();
	}
}

FDMD& UStatBaseComponent::GetChangedCurrentStatDispatcher()
{
	return ChangedCurrentStatDispatcher;
}

FDMD& UStatBaseComponent::GetPreChangeCurrentStatDispatcher()
{
	return PreChangeCurrentStatDispatcher;
}

void UStatBaseComponent::AddBonusEffect(FName Name, float Value)
{
	FStatBonusEffect BonusEffect;

	BonusEffect.Name = Name;
	BonusEffect.Value = Value;

	BonusEffects.Add(BonusEffect);
}

FStatBonusEffect* UStatBaseComponent::FindBonusEffect(FName Name)
{
	FStatBonusEffect* FoundEffect = BonusEffects.FindByPredicate([Name](const FStatBonusEffect& Effect)
		{
			return Effect.Name == Name;
		});

	if(FoundEffect)
	{
		return FoundEffect;
	}

	return nullptr;
}

void UStatBaseComponent::RemoveBonusEffect(FName Name)
{
	FStatBonusEffect* FoundEffect = BonusEffects.FindByPredicate([Name](const FStatBonusEffect& Effect)
		{
			return Effect.Name == Name;
		});

	if (FoundEffect)
	{
		FStatBonusEffect Effect = *FoundEffect;
		BonusEffects.Remove(Effect);
	}
}

void UStatBaseComponent::AddRestrictionOnUsePositiveEffects(FName Name)
{
	RestrictionsOnUsePositiveEffects.Add(Name);
}

bool UStatBaseComponent::FindRestrictionOnUsePositiveEffects(FName Name)
{
	int RestrictionIndex = RestrictionsOnUsePositiveEffects.Find(Name);

	if (RestrictionIndex != INDEX_NONE)
		return true;

	return false;
}

void UStatBaseComponent::RemoveRestrictionOnUsePositiveEffects(FName Name)
{
	RestrictionsOnUsePositiveEffects.Remove(Name);
}

void UStatBaseComponent::AddRestrictionOnUseNegativeEffects(FName Name)
{
	RestrictionsOnUseNegativeEffects.Add(Name);
}

bool UStatBaseComponent::FindRestrictionOnUseNegativeEffects(FName Name)
{
	int RestrictionIndex = RestrictionsOnUseNegativeEffects.Find(Name);

	if (RestrictionIndex != INDEX_NONE)
		return true;

	return false;
}

void UStatBaseComponent::RemoveRestrictionOnUseNegativeEffects(FName Name)
{
	RestrictionsOnUseNegativeEffects.Remove(Name);
}

