// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/ActorComponents/InventoryComponent.h"
#include "ResizableInventoryComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UResizableInventoryComponent : public UInventoryComponent
{
	GENERATED_BODY()
	
		//Methods
public:
	virtual bool TryAddItem(AInventoryItemObject* InventoryItemObject, bool bWithItemAmount) override;
	virtual void RemoveItem(AInventoryItemObject* InventoryItemObject, ERemoveItemOption RemoveItemOption) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	void AddRow();
	void RemoveRow();
	//Fields
protected:
	int DefaultColumns;
	int DefaultRows;
};
