// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"

#include "The_Arena/ActorComponents/TransformController.h"

#include "WeaponClippingComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FClippingSignature);

class AFirstPersonCharacter;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UWeaponClippingComponent : public UActorComponent
{
	GENERATED_BODY()
	//Methods
public:
	// Sets default values for this component's properties
	UWeaponClippingComponent();

	UFUNCTION(BlueprintNativeEvent)
	void OnStartClipping();
	UFUNCTION(BlueprintNativeEvent)
	void OnPutDownWeapon();
	
	UFUNCTION(BlueprintCallable)
	UTransformController* GetClippingTransformController() const;
	UFUNCTION(BlueprintCallable)
	void SetClippingTransformController(UTransformController* Controller);
	UFUNCTION(BlueprintCallable)
	USceneComponent* GetEndOfWeaponComp() const;
	UFUNCTION(BlueprintCallable)
	USceneComponent* GetEndOfWeaponInFPAreaComp() const;
	UFUNCTION(BlueprintCallable)
	USceneComponent* GetFPHandsComponent() const;
	UFUNCTION(BlueprintCallable)
	void SetFPHandsComponent(USceneComponent* Component);
	UFUNCTION(BlueprintCallable)
	bool IsClipping() const;

	void OnComponentDestroyed(bool bDestroyingHierarchy) override;
	void InitializationParameters(AFirstPersonCharacter* FPOwner, USceneComponent* EndOfWeapon);

	UFUNCTION()
	bool GetDoOnceAfterPutDownTheWeapon() const;
	
	static float GetTimeOfAbilityToFireAfterClipping();
	static void SetTimeOfAbilityToFireAfterClipping(float Time);
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void CheckOnClipping();
	void UpdateEndOfWeaponInFPArea();  //Remember, it works in any case. Better results comes out with big location values and small scale values. For Example: CurrencyToWeaponLocation = 6, ChangingWeaponLocationScale = 0.8
	void UpdateAbilityToFire(float DeltaTime);
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Category = "Clipping Parameters")
		float DistanceToClip;
	UPROPERTY(EditDefaultsOnly, Category = "Clipping Parameters")
		float CurrencyToWeaponLocation;
	UPROPERTY(EditDefaultsOnly, Category = "Clipping Parameters")
		float ChangingWeaponLocationScale;
	
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Offset")
		float ForwardLocationOffset;
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Offset")
		float PitchRotationOffset;
	UPROPERTY(EditDefaultsOnly, Category = "Weapon Offset")
		float UpLocationOffset;

	UPROPERTY()
	UTransformController* ClippingTransformController;
	UPROPERTY()
	USceneComponent* EndOfWeaponComp;
	UPROPERTY()
	USceneComponent* EndOfWeaponInFPAreaComp;
	UPROPERTY()
	USceneComponent* FPHandsComp;
	
	FCollisionQueryParams ClippingLineTraceParams;
	float DistanceToClipToForwardLocationOffsetRatio;
	float PitchRotationRatio;
	float UpLocationRatio;
	bool Clipping = false;
	FVector LastEndOfWeaponInFPAreaLocation;
	FRotator LastEndOfWeaponInFPAreaRotation;
	float LastDistance;
	UCameraComponent* FPCamera;
	static float TimeOfAbilityToFireAfterClipping;
	float CurTimeOfAbilityToFireAfterClipping;
	bool DoOnceAfterPutDownTheWeapon = false;
	//Events
public:
	FClippingSignature StartClippingDispatcher;
	FClippingSignature PutDownWeaponDispatcher;
};
