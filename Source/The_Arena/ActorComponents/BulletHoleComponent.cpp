// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletHoleComponent.h"


TArray<UDecalComponent*> UBulletHoleComponent::Decals;
bool UBulletHoleComponent::AttachedToChangeLevelDelegate = false;
bool UBulletHoleComponent::AttachedToWorldCleanupDelegate = false;

// Sets default values for this component's properties
UBulletHoleComponent::UBulletHoleComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UBulletHoleComponent::CreateBulletHole(FHitResult& OutHit)
{
	if (!BulletHoleDataAsset)
		return;
	
	if(auto Owner = GetOwner())
	{
		if (auto Pawn = Cast<APawn>(Owner->GetOwner()))
		{
			if (Pawn->IsLocallyControlled())
			{
				if (Pawn->GetLocalRole() == ROLE_Authority)
				{
					MulticastCreateBulletHole(OutHit);
				}
				else
				{
					ServerCreateBulletHole(OutHit);
				}
			}
		}
	}

	auto SurfaceType = UGameplayStatics::GetSurfaceType(OutHit);

	for (int i = 0; i < BulletHoleDataAsset->BulletHoles.Num(); i++)
	{
		if (BulletHoleDataAsset->BulletHoles[i].Surface == SurfaceType)
		{
			CreateDecal(OutHit, BulletHoleDataAsset->BulletHoles[i]);
			PlayBulletHoleSound(OutHit, BulletHoleDataAsset->BulletHoles[i]);
			CreateParticle(OutHit, BulletHoleDataAsset->BulletHoles[i]);
			break;
		}
	}
}

void UBulletHoleComponent::ServerCreateBulletHole_Implementation(FHitResult OutHit)
{
	MulticastCreateBulletHole(OutHit);
}

void UBulletHoleComponent::MulticastCreateBulletHole_Implementation(FHitResult OutHit)
{
	if (auto Owner = GetOwner())
	{
		if (auto Pawn = Cast<APawn>(Owner->GetOwner()))
		{
			if (!Pawn->IsLocallyControlled())
			{
				CreateBulletHole(OutHit);
			}
		}
	}
}

// Called when the game starts
void UBulletHoleComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!AttachedToChangeLevelDelegate)
	{
		FWorldDelegates::LevelRemovedFromWorld.AddStatic(&UBulletHoleComponent::AttachFunctionToChangeLevelDelegate);
		AttachedToChangeLevelDelegate = true;
	}

	if (!AttachedToWorldCleanupDelegate)
	{
		FWorldDelegates::OnWorldCleanup.AddStatic(&UBulletHoleComponent::AttachFunctionToWorldCleanupDelegate);
		AttachedToWorldCleanupDelegate = true;
	}
}


// Called every frame
void UBulletHoleComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UBulletHoleComponent::CreateDecal(FHitResult& OutHit, FBulletHoleParam& Param)
{
	if (!BulletHoleDataAsset || Param.Decals.Num() == 0)
	{
		return;
	}

	int NumberOfDecal = FMath::RandRange(0, Param.Decals.Num() - 1);
	FVector DecalSize = Param.Decals[NumberOfDecal].UseDefaultSize ? BulletHoleDataAsset->DefaultDecalSize : Param.Decals[NumberOfDecal].DecalSize;
	auto BulletDecal = UGameplayStatics::SpawnDecalAtLocation(this, Param.Decals[NumberOfDecal].Material, DecalSize, OutHit.Location, OutHit.Normal.Rotation());


	if (BulletDecal)
	{
		auto DynamicMaterial = BulletDecal->CreateDynamicMaterialInstance();

		auto Mesh = Cast<UStaticMeshComponent>(OutHit.GetComponent());
		if (Mesh)
		{
			FColor CurColor = GetStaticMeshColor(Mesh);
			auto LinColor = FLinearColor::FromSRGBColor(CurColor);
			DynamicMaterial->SetVectorParameterValue("Color", LinColor);
		}
		else
		{
			auto Landscape = Cast<ALandscape>(OutHit.GetActor());
			if(Landscape)
			{
				FColor CurColor = GetLandscapeColor(Landscape);
				auto LinColor = FLinearColor::FromSRGBColor(CurColor);
				DynamicMaterial->SetVectorParameterValue("Color", LinColor);
			}			
		}
	
		BulletDecal->SetFadeScreenSize(0.0f);
		float RandomRotation = FMath::RandRange(0, 360);
		BulletDecal->AddRelativeRotation(FRotator(0, 0, RandomRotation));

		auto CurBulletScale = BulletDecal->GetComponentScale();
		CurBulletScale.X = BulletHoleDataAsset->BulletHoleScale_X;
		BulletDecal->SetWorldScale3D(CurBulletScale);

		EnqueueDecal(BulletDecal);
	}

}

void UBulletHoleComponent::CreateParticle(FHitResult& OutHit, FBulletHoleParam& Param)
{
	if (!BulletHoleDataAsset || Param.Particles.Num() == 0)
	{
		return;
	}

	int NumberOfDecal = FMath::RandRange(0, Param.Particles.Num() - 1);
	auto BulletParticle = UGameplayStatics::SpawnEmitterAtLocation(this, Param.Particles[NumberOfDecal], OutHit.Location, OutHit.Normal.Rotation());
}

void UBulletHoleComponent::PlayBulletHoleSound(FHitResult& OutHit, FBulletHoleParam& Param)
{
	if (Param.Sounds.Num() == 0)
	{
		return;
	}

	int NumberOfSound = FMath::RandRange(0, Param.Sounds.Num() - 1);
	UGameplayStatics::PlaySoundAtLocation(this, Param.Sounds[NumberOfSound], OutHit.Location);
}

FColor UBulletHoleComponent::GetStaticMeshColor(UStaticMeshComponent* MeshComp)
{
	if (!MeshComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("MeshComp is nullptr!") );
		return FColor::White;
	}
		

	TArray<USceneComponent*> StaticMeshChildComponents;
	auto Owner = MeshComp->GetOwner();
	
	if (Owner)
	{
		auto StaticMeshColorComp = Owner->FindComponentByClass<UStaticMeshColorComponent>();
		
		if(StaticMeshColorComp)
		{
			return StaticMeshColorComp->GetStaticMeshColor();
		}
	}
	
	UE_LOG(LogTemp, Log, TEXT("The mesh is missing UStaticMeshColorComponent as a child component!") );
	return FColor::White;
}

FColor UBulletHoleComponent::GetLandscapeColor(AActor* Landscape)
{
	if (!Landscape)
	{
		UE_LOG(LogTemp, Warning, TEXT("Landscape is nullptr!"));
		return FColor::White;
	}
		
	auto LandscapeName = Landscape->GetName();
	auto LandscapeColorName = LandscapeName + "_Color";

	TArray<AActor*> LandscapeColorActors;
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), "LandscapeColor", LandscapeColorActors);

	if(LandscapeColorActors.Num() > 0)
	{
		for(auto LandscapeColorActor : LandscapeColorActors)
		{
			if(LandscapeColorActor->GetName() == LandscapeColorName)
			{	
				auto StaticMeshColorComp = LandscapeColorActor->FindComponentByClass<UStaticMeshColorComponent>();
				
				if (StaticMeshColorComp)
				{
					return StaticMeshColorComp->GetStaticMeshColor();
				}		
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("The LandscapeColorActor for current landscape is missing!"));
	}

	if(LandscapeColorActors.Num() > 0)
		UE_LOG(LogTemp, Warning, TEXT("The LandscapeColorActor is missing UStaticMeshColorComponent as a child component!"));
	
	return FColor::White;
}

void UBulletHoleComponent::AttachFunctionToChangeLevelDelegate(ULevel* Level, UWorld* World)
{
	ClearBulletHoleArrayStatic();
	AttachedToChangeLevelDelegate = false;
}

void UBulletHoleComponent::AttachFunctionToWorldCleanupDelegate(UWorld* World, bool SessionEnded, bool CleanupResources)
{
	ClearBulletHoleArrayStatic();
	AttachedToWorldCleanupDelegate = false;
}

void UBulletHoleComponent::EnqueueDecal(UDecalComponent* Decal)
{
	if (!Decal || !BulletHoleDataAsset)
		return;

	if (Decals.Num() >= BulletHoleDataAsset->MaxBulletHolesAmmount && Decals.Num() > 0)
	{
		PopBulletHole();
	}

	Decals.Push(Decal);
}

void UBulletHoleComponent::PopBulletHole()
{
	PopBulletHoleStatic();
}

void UBulletHoleComponent::ClearBulletHoleArray()
{
	ClearBulletHoleArrayStatic();
}

void UBulletHoleComponent::PopBulletHoleStatic()
{
	if (Decals[0])
	{
		Decals[0]->DestroyComponent();
	}

	Decals.RemoveAt(0);
}

void UBulletHoleComponent::ClearBulletHoleArrayStatic()
{
	while (Decals.Num() != 0)
	{
		PopBulletHoleStatic();
	}
}
