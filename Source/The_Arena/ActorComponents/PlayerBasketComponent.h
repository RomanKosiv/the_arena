// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/BasketBaseComponent.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/MoneyComponent.h"

#include "PlayerBasketComponent.generated.h"


/**
 * 
 */
UCLASS()
class THE_ARENA_API UPlayerBasketComponent : public UBasketBaseComponent
{
	GENERATED_BODY()

	//Methods
public:
	void Initialization(UCustomInventoryComponent* InCustomInventoryComponent, UEquipmentComponent* InEquipmentComponent, UMoneyComponent* InMoneyComponent);
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;
	void DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;

	void Sell();

protected:
	void UpdateCommonItemsCost() override;

	//Fields
protected:
	UPROPERTY()
	UCustomInventoryComponent* CustomInventoryComponent;
	UPROPERTY()
	UEquipmentComponent* EquipmentComponent;
	UPROPERTY()
	UMoneyComponent* PlayerMoneyComponent;
};
