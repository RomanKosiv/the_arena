// Fill out your copyright notice in the Description page of Project Settings.


#include "ResizableInventoryComponent.h"

#include "WeaponModules/ModuleItemObjectsManager.h"

bool UResizableInventoryComponent::TryAddItem(AInventoryItemObject* InventoryItemObject, bool bWithItemAmount)
{
	if (!InventoryItemObject || GetOwnerRole() != ROLE_Authority)
		return false;

	if(Super::TryAddItem(InventoryItemObject, bWithItemAmount))
	{
		return true;
	}

	AddRow();

	return TryAddItem(InventoryItemObject, bWithItemAmount);
}

void UResizableInventoryComponent::RemoveItem(AInventoryItemObject* InventoryItemObject, ERemoveItemOption RemoveItemOption)
{
	RemoveItemInternal(InventoryItemObject, RemoveItemOption);

	while (IsRoomAvailableInternal(Columns, 1, TileToIndex({ 0, Rows - 1 }), &UResizableInventoryComponent::CheckOnConsideringItemLambda) && Rows > DefaultRows)
	{
		RemoveRow();
	}

	OnRep_Items();
}

void UResizableInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	DefaultColumns = Columns;
	DefaultRows = Rows;
}

void UResizableInventoryComponent::AddRow()
{
	Items.SetNum(Items.Num() + Columns);
	++Rows;
}

void UResizableInventoryComponent::RemoveRow()
{
	Items.SetNum(Items.Num() - Columns);
	--Rows;
}