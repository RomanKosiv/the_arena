// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

#include "The_Arena/UObjects/DataAssets/FPSoundDataAsset.h"

#include "FPSoundComponent.generated.h"


class AFirstPersonCharacter;
enum EMovementType;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UFPSoundComponent : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this component's properties
	UFPSoundComponent();
	
	UFUNCTION(BlueprintCallable)
	void PlayJumpSound();

	UFUNCTION(Client, Unreliable, BlueprintCallable)
	void ClientPlayDamageSound();

	UFUNCTION(BlueprintCallable)
	void PlayBreathLoopSound();
	UFUNCTION(BlueprintCallable)
	void StopBreathLoopSound();
	UFUNCTION(BlueprintCallable)
	void PlayBreathPantSound();
	UFUNCTION(BlueprintCallable)
	void PlayCoughSound();
	UFUNCTION(BlueprintCallable)
	void StopCoughSound();
	UFUNCTION(BlueprintCallable)
	void PlayDeathSound();
	
	void SetCurrentWalkingMode(EMovementType Mode);	
protected:
	UFUNCTION(NetMulticast, Unreliable)
	void MulticastSpawnSound(USoundBase* Sound);
	void MulticastSpawnSound_Implementation(USoundBase* Sound);
	void SpawnSound(USoundBase* Sound);
	
	bool IsGroundUnderCharacterExist(FHitResult& OutResult);
	void CalculateRandomNumberOfSound(int SoundArraySize, int& CurNumberOfSound);
	void PlayStepSound();
	void UpdateStepSound(float DeltaTime);
	void PlaySound(TArray<FFootstepSoundParam>& Sounds, int& CurNumberOfSound);

	void BeginPlay() override;
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Category = "CharacterVoice")
	USoundBase* BreathLoopSound;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterVoice")
	USoundBase* BreathPantSound;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterVoice")
	USoundBase* CoughSound;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterVoice")
	USoundBase* DeathSound;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterVoice")
	USoundAttenuation* Character2DAttenuation;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterVoice")
	USoundAttenuation* LoudCharacter3DAttenuation;
	UPROPERTY(EditDefaultsOnly, Category = "CharacterVoice")
	USoundAttenuation* QuietCharacter3DAttenuation;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
	USoundBase* DamageSound;
	
	UPROPERTY(EditDefaultsOnly)
	UFPSoundDataAsset* SoundAsset;
	UPROPERTY(EditDefaultsOnly)
	float BlockingStepSoundSpeed = 0.0f;
	
	UPROPERTY()
	AFirstPersonCharacter* FPOwner;

	bool bCoughPlayed = false;
	float CurrentStepSoundFrequency = 0.0f;
	int CurStepNumberOfSound = 0;
	int CurJumpNumberOfSound = 0;
	EMovementType CurrentWalkingMode;
};
