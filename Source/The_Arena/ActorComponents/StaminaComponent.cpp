// Fill out your copyright notice in the Description page of Project Settings.


#include "StaminaComponent.h"

#include "The_Arena/Characters/FirstPersonCharacter.h"
#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"



// Sets default values for this component's properties
UStaminaComponent::UStaminaComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UStaminaComponent::BeginPlay()
{
	Super::BeginPlay();

	if(GetOwnerRole() == ROLE_Authority)
	{
		if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
		{
			FPCharacter->GetJumpedDispatcher().AddDynamic(this, &UStaminaComponent::StaminaInitOnJumped);
			FPCharacter->GetLandedDispatcher().AddDynamic(this, &UStaminaComponent::StaminaInitOnLanded);
		}
	}
}

// Called every frame
void UStaminaComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UStaminaComponent::OnPreChangeCurrentStat()
{
	Super::OnPreChangeCurrentStat();
	RunInitOnPreChanging();
}

void UStaminaComponent::OnChangedCurrentStat()
{
	Super::OnChangedCurrentStat();
	RunInitOnChanged();
	JumpInitOnChanged();
	SoundInitOnChanged();
}

void UStaminaComponent::RunInitOnPreChanging()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if(FPCharacter->IsRunning() && FPCharacter->GetCurrentVelocity() >= RunningSpeedForBlockingStaminaDecrease)
		{
			if(!bRunBonusAdded)
			{
				AddBonusEffect("Run", SpeedOfDecreasingStaminaOnRunning);
				bRunBonusAdded = true;
			}	
		}
		else
		{
			if(bRunBonusAdded)
			{
				RemoveBonusEffect("Run");
				bRunBonusAdded = false;
			}	
		}
	}
}

void UStaminaComponent::RunInitOnChanged()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (CurrentStat >= StaminaForEnableRunning && bRunningSpeedBlockedByStamina)
		{
			bRunningSpeedBlockedByStamina = false;
			FPCharacter->RemoveMovementPriority(EMovementType::SlowWalk, "Stamina");

			if (FPCharacter->IsRunning() && FPCharacter->GetCurrentVelocity() >= RunningSpeedForBlockingStaminaDecrease)
			{
				AddBonusEffect("Run", SpeedOfDecreasingStaminaOnRunning);
			}
		}

		if (FMath::IsNearlyZero(CurrentStat) && !bRunningSpeedBlockedByStamina)
		{
			bRunningSpeedBlockedByStamina = true;
			FPCharacter->AddMovementPriority(EMovementType::SlowWalk, "Stamina");
			RemoveBonusEffect("Run");	
		}
	}
}

void UStaminaComponent::SoundInitOnChanged()
{
	if (CurrentStat < StaminaForEnableRunning)
	{
		if(auto FPSoundComponent = GetOwner()->FindComponentByClass<UFPSoundComponent>())
		{
			FPSoundComponent->PlayBreathLoopSound();
		}
	}
	else
	{
		if (auto FPSoundComponent = GetOwner()->FindComponentByClass<UFPSoundComponent>())
		{
			FPSoundComponent->StopBreathLoopSound();
		}
	}
}

void UStaminaComponent::StaminaInitOnJumped()
{
	float FinalStamina = CurrentStat - SpeedOfDecreasingStaminaOnJumping;

	if (FinalStamina > MaxStat)
	{
		FinalStamina = MaxStat;
	}

	if (FinalStamina < 0.0f)
	{
		FinalStamina = 0.0f;
	}

	SetCurrentStat(FinalStamina);
	//AddRestrictionOnUsePositiveEffects("Jump");
}

void UStaminaComponent::StaminaInitOnLanded()
{
	//RemoveRestrictionOnUsePositiveEffects("Jump");
}

void UStaminaComponent::JumpInitOnChanged()
{
	if (auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner()))
	{
		if (CurrentStat >= StaminaForEnableJumping && bJumpRestrictionAdded)
		{
			FPCharacter->RemoveJumpPriority("EmptyStamina");
			bJumpRestrictionAdded = false;
		}
		
		if (FMath::IsNearlyZero(CurrentStat) && !bJumpRestrictionAdded)
		{
			FPCharacter->AddJumpPriority("EmptyStamina");
			bJumpRestrictionAdded = true;
		}
	}
}

