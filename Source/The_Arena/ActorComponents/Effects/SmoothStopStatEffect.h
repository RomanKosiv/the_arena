// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/StatBaseComponent.h"
#include "The_Arena/ActorComponents/Effects/StopStatEffectBaseComponent.h"
#include "The_Arena/ActorComponents/Effects/StatEffectBaseComponent.h"

#include "SmoothStopStatEffect.generated.h"


UCLASS(Blueprintable)
class THE_ARENA_API USmoothStopStatEffect : public UStopStatEffectBaseComponent
{
	GENERATED_BODY()

	//Methods
public:
	void SmoothStopStatEffectInitialization(TSubclassOf<UStatEffectBaseComponent> InStatEffectComponentClass, float InAmountEffectPerSec, FCharacteristicEffectParams InEffectParams);
protected:
	void OnRate() override;
	void SmoothRemoveStatEffect();

	//Fields
protected:
	UPROPERTY()
	TSubclassOf<UStatEffectBaseComponent> StatEffectComponentClass;
	UPROPERTY()
	float AmountEffectPerSec;
};
