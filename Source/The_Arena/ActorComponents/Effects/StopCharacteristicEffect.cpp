// Fill out your copyright notice in the Description page of Project Settings.


#include "StopCharacteristicEffect.h"

#include "The_Arena/ActorComponents/StatBaseComponent.h"


void UStopCharacteristicEffect::StopCharacteristicEffectInitialization(TSubclassOf<UCharacteristicEffectBase> InCharacteristicEffectClass, FCharacteristicEffectParams InEffectParams)
{
	CharacteristicEffectClass = InCharacteristicEffectClass;
	CharacteristicEffectBaseInitialization(InEffectParams);
}

void UStopCharacteristicEffect::OnRate()
{
	Super::OnRate();

	RemoveCharacteristicEffect();
}

void UStopCharacteristicEffect::RemoveCharacteristicEffect()
{
	if (auto Owner = GetOwner())
	{
		auto CharacteristicEffectComponent = Cast<UCharacteristicEffectBase>(Owner->GetComponentByClass(CharacteristicEffectClass));

		if(CharacteristicEffectComponent)
		{
			CharacteristicEffectComponent->RemoveEffect();
		}
	}
}

