// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/Effects/CharacteristicEffectBase.h"

#include "StopCharacteristicEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class THE_ARENA_API UStopCharacteristicEffect : public UCharacteristicEffectBase
{
	GENERATED_BODY()

	//Methods
public:
	void StopCharacteristicEffectInitialization(TSubclassOf<UCharacteristicEffectBase> InCharacteristicEffectClass, FCharacteristicEffectParams InEffectParams);
	
protected:
	void OnRate() override;
	void RemoveCharacteristicEffect();

	//Fields
protected:
	UPROPERTY()
	TSubclassOf<UCharacteristicEffectBase> CharacteristicEffectClass;
};
