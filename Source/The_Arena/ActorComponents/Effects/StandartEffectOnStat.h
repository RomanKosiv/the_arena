// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/Effects/StatEffectBaseComponent.h"

#include "StandartEffectOnStat.generated.h"


UCLASS(Blueprintable)
class THE_ARENA_API UStandartEffectOnStat : public UStatEffectBaseComponent
{
	GENERATED_BODY()

		//Methods
public:
	void StandartEffectBaseOnStatInitialization(float InAmountEffectPerSec, TSubclassOf<UStatBaseComponent> InStatComponentClass, FCharacteristicEffectParams InEffectParams);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetAmountEffectPerSec() const;

protected:
	void CreateEffectInternal() override;
	void RemoveEffectInternal() override;

	virtual void CreateStandartEffect();

	//Fields
protected:
	UPROPERTY()
	float AmountEffectPerSec;
};
