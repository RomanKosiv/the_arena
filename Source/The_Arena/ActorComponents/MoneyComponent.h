// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/InventoryComponent.h"
#include "The_Arena/Actors/MoneyItemObject.h"

#include "MoneyComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UMoneyComponent : public UInventoryComponent
{
	GENERATED_BODY()

	//Methods
public:
	UMoneyComponent();
	
	void DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;
	void DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;

	bool TryAddItem(AInventoryItemObject* InventoryItemObject, bool bWithItemAmount) override;
	bool TryAddItemAt(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount) override;

	bool IsRoomAvailable(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount) override;

	int GetMoney() const;
	void SetMoney(int Money);

	//Fields
protected:
};
