// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/ResizableInventoryComponent.h"

#include "TraderInventoryComponent.generated.h"

class UTraderBasketComponent;
class UPlayerBasketComponent;
class ATraderBasketHolder;

USTRUCT()
struct FTradeItem
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	TSubclassOf<AInventoryItemObject> ItemObjectClass;
	UPROPERTY(EditAnywhere)
	int Amount = 1;
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UTraderInventoryComponent : public UResizableInventoryComponent
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION()
	void TraderInventoryOpeningInitialization(APlayerController* OpeningController);
	UFUNCTION()
	void TraderInventoryClosingInitialization(APlayerController* ClosingController);

	UFUNCTION(BlueprintCallable)
	TArray<UPlayerBasketComponent*> GetPlayerBasketComponents() const;
	UFUNCTION(BlueprintCallable)
	TArray<UTraderBasketComponent*> GetTraderBasketComponents() const;

	void DragItem(AInventoryItemObject* InventoryItemObject, UDragAndDropManagerComponent* DragAndDropComponent) override;
	void DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;
	void DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount) override;

	bool CanUsed(AActor* ActorUser) override;

	void RemoveItem(AInventoryItemObject* InventoryItemObject, ERemoveItemOption RemoveItemOption) override;

	FDMD& GetPlayerBasketComponentsChangedDispatcher();
	FDMD& GetTraderBasketComponentsChangedDispatcher();
	
protected:
	void BeginPlay() override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION()
	void StartTraderItemsInitialization();

	UFUNCTION()
	void OnRep_PlayerBasketComponents();
	UFUNCTION()
	void OnRep_TraderBasketComponents();
	
	//Fields
protected:
	UPROPERTY(EditAnywhere, Category= "Start items")
	TArray<FTradeItem> StartTraderItems;

	UPROPERTY(EditDefaultsOnly, Category= "Basket holder")
	TSubclassOf<ATraderBasketHolder> TraderBasketHolderClass;
	
	UPROPERTY(ReplicatedUsing = OnRep_PlayerBasketComponents)
	TArray<UPlayerBasketComponent*> PlayerBasketComponents;
	UPROPERTY(ReplicatedUsing = OnRep_TraderBasketComponents)
	TArray<UTraderBasketComponent*> TraderBasketComponents;

	UPROPERTY(BlueprintAssignable)
	FDMD PlayerBasketComponentsChangedDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD TraderBasketComponentsChangedDispatcher;
};
