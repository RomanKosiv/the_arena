// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/Characters/FirstPersonCharacter.h"
#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectsManager.h"


// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UInventoryComponent::ServerOpenInventory_Implementation()
{
	MulticastOnInventoryOpened();
}

void UInventoryComponent::ServerCloseInventory_Implementation()
{
	MulticastOnInventoryClosed();
}

void UInventoryComponent::RemoveItemFromBaseInventory(AInventoryItemObject* InventoryItemObject)
{
	if (!InventoryItemObject)
		return;

	auto CustomGameState = GetWorld()->GetGameState<ACustomGameState>();
	auto FPCharacter = Cast<AFirstPersonCharacter>(GetOwner());

	if (CustomGameState && FPCharacter)
	{
		if (Contains(InventoryItemObject))
		{
			if(auto DroppedItemsManager = CustomGameState->GetDroppedItemsManager())
			{
				DroppedItemsManager->SpawnInventoryItemInFrontOfTargetActor(InventoryItemObject, FPCharacter);
				RemoveItem(InventoryItemObject, ERemoveItemOption::WithoutSave);		
			}	
		}
	}
}

void UInventoryComponent::DragItem(AInventoryItemObject* InventoryItemObject,
	UDragAndDropManagerComponent* DragAndDropComponent)
{
	if (!InventoryItemObject || !DragAndDropComponent)
		return;

	if (Contains(InventoryItemObject))
	{
		int Tile = IndexOfItem(InventoryItemObject);
		DragAndDropComponent->SetDraggedItem(InventoryItemObject, this, Tile);
		RemoveItem(InventoryItemObject, ERemoveItemOption::WithSave);
	}
}

void UInventoryComponent::DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	if (auto DraggedInventoryItemObject = DragAndDropComponent->GetDraggedItem().InventoryItemObject)
	{
		if (TryAddItem(DraggedInventoryItemObject, bWithItemAmount))
		{
			DragAndDropComponent->SetDraggedItem(nullptr, nullptr);
		}
	}
}

void UInventoryComponent::DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	if (auto DraggedInventoryItemObject = DragAndDropComponent->GetDraggedItem().InventoryItemObject)
	{	
		if(TryAddItemAt(DraggedInventoryItemObject, TopLeftIndex, bWithItemAmount))
		{
			DragAndDropComponent->SetDraggedItem(nullptr, nullptr);
		}	
		else
		{
			DropItem(DragAndDropComponent, false);
		}
	}
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	if(GetOwnerRole() == ROLE_Authority)
	{
		Items.Init(nullptr, Columns * Rows);
	}
}

// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UInventoryComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(GetOwnerRole() == ROLE_Authority)
	{
		auto AllItems = GetAllItems();

		for (auto Iter = AllItems.begin(); Iter != AllItems.end(); ++Iter)
		{
			if (auto ItemObject = Iter->Key)
			{
				ItemObject->Destroy();
			}
		}

		Items.Empty();
	}

	Super::EndPlay(EndPlayReason);
}

void UInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventoryComponent, Items);
	DOREPLIFETIME(UInventoryComponent, Columns);
	DOREPLIFETIME(UInventoryComponent, Rows);
	DOREPLIFETIME(UInventoryComponent, CommonItemsWeight);
}

void UInventoryComponent::AddItemAt(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount)
{
	if (!InventoryItemObject || GetOwnerRole() != ROLE_Authority)
		return;
	
	auto TileOfTopLeftIndex = IndexToTile(TopLeftIndex);
	auto DimensionsOfInventoryItemObject = InventoryItemObject->GetDimensions();

	int EndOfItemObject_X = TileOfTopLeftIndex.X + DimensionsOfInventoryItemObject.X;
	int EndOfItemObject_Y = TileOfTopLeftIndex.Y + DimensionsOfInventoryItemObject.Y;

	if(bWithItemAmount)
	{
		for (int i = TileOfTopLeftIndex.X; i < EndOfItemObject_X; ++i)
		{
			for (int j = TileOfTopLeftIndex.Y; j < EndOfItemObject_Y; ++j)
			{
				FTile CurrentTile = { i, j };
				int CurrentIndex = TileToIndex(CurrentTile);

				if(auto Item = Items[CurrentIndex])
				{
					if (Item->TryIncreaseItemAmount(InventoryItemObject))
					{
						return;
					}
				}
			}
		}
	}
	
	for (int i = TileOfTopLeftIndex.X; i < EndOfItemObject_X; ++i)
	{
		for (int j = TileOfTopLeftIndex.Y; j < EndOfItemObject_Y; ++j)
		{
			FTile CurrentTile = { i, j };
			int CurrentIndex = TileToIndex(CurrentTile);

			Items[CurrentIndex] = InventoryItemObject;		
		}
	}

	auto& CurrentItemAmountDispatcher = InventoryItemObject->GetCurrentItemAmountDispatcher();

	if(!CurrentItemAmountDispatcher.IsAlreadyBound(this, &UInventoryComponent::UpdateOnCurrentItemAmount))
	{
		CurrentItemAmountDispatcher.AddDynamic(this, &UInventoryComponent::UpdateOnCurrentItemAmount);
	}

	if(auto ModuleItemObjectsManager = InventoryItemObject->FindComponentByClass<UModuleItemObjectsManager>())
	{
		auto& ModuleItemObjectsDispatcher = ModuleItemObjectsManager->GetModuleItemObjectsDispatcher();
		
		if(!ModuleItemObjectsDispatcher.IsAlreadyBound(this, &UInventoryComponent::UpdateOnAttachModule))
		{
			ModuleItemObjectsDispatcher.AddDynamic(this, &UInventoryComponent::UpdateOnAttachModule);
		}	
	}

	OnRep_Items();
}

void UInventoryComponent::RemoveItemInternal(AInventoryItemObject* InventoryItemObject,
	ERemoveItemOption RemoveItemOption)
{
	if (!InventoryItemObject || GetOwnerRole() != ROLE_Authority)
		return;

	if (Contains(InventoryItemObject))
	{
		for (int i = 0; i < Items.Num(); ++i)
		{
			if (Items[i] == InventoryItemObject)
			{
				Items[i] = nullptr;
			}
		}

		auto& CurrentItemAmountDispatcher = InventoryItemObject->GetCurrentItemAmountDispatcher();

		if (CurrentItemAmountDispatcher.IsAlreadyBound(this, &UInventoryComponent::UpdateOnCurrentItemAmount))
		{
			CurrentItemAmountDispatcher.RemoveDynamic(this, &UInventoryComponent::UpdateOnCurrentItemAmount);
		}

		if (auto ModuleItemObjectsManager = InventoryItemObject->FindComponentByClass<UModuleItemObjectsManager>())
		{
			auto& ModuleItemObjectsDispatcher = ModuleItemObjectsManager->GetModuleItemObjectsDispatcher();

			if (ModuleItemObjectsDispatcher.IsAlreadyBound(this, &UInventoryComponent::UpdateOnAttachModule))
			{
				ModuleItemObjectsDispatcher.RemoveDynamic(this, &UInventoryComponent::UpdateOnAttachModule);
			}
		}

		if (RemoveItemOption == ERemoveItemOption::WithoutSave)
		{
			InventoryItemObject->Destroy();
		}
	}
}

void UInventoryComponent::OnInventoryChanged()
{
	UpdateOnCurrentItemAmount();
	InventoryChangedDispatcher.Broadcast();
}

void UInventoryComponent::OnRep_CommonItemsWeight()
{
	CommonItemsWeightDispatcher.Broadcast();
}

void UInventoryComponent::MulticastOnInventoryClosed_Implementation()
{
	InventoryClosedDispathcer.Broadcast();

	if (auto Pawn = Cast<APawn>(GetOwner()))
	{
		if (auto PlayerController = Pawn->GetController<APlayerController>())
		{
			InventoryClosed_PC_Dispatcher.Broadcast(PlayerController);
		}
	}
}

void UInventoryComponent::MulticastOnInventoryOpened_Implementation()
{
	InventoryOpenedDispatcher.Broadcast();

	if (auto Pawn = Cast<APawn>(GetOwner()))
	{
		if (auto PlayerController = Pawn->GetController<APlayerController>())
		{
			InventoryOpened_PC_Dispatcher.Broadcast(PlayerController);
		}
	}
}

void UInventoryComponent::OnRep_Items()
{
	OnInventoryChanged();
}

int UInventoryComponent::GetColumns() const
{
	return Columns;
}

void UInventoryComponent::SetColumns(int InColumns)
{
	Columns = InColumns;
}

int UInventoryComponent::GetRows() const
{
	return Rows;
}

void UInventoryComponent::SetRows(int InRows)
{
	Rows = InRows;
}

bool UInventoryComponent::TryAddItem(AInventoryItemObject* InventoryItemObject, bool bWithItemAmount)
{
	if (!InventoryItemObject || GetOwnerRole() != ROLE_Authority)
		return false;
	
	for (int i = 0; i < Items.Num(); ++i)
	{
		if (IsRoomAvailable(InventoryItemObject, i, bWithItemAmount))
		{
			AddItemAt(InventoryItemObject, i, bWithItemAmount);
			return true;
		}
	}

	InventoryItemObject->ServerRotate();

	for (int i = 0; i < Items.Num(); ++i)
	{
		if (IsRoomAvailable(InventoryItemObject, i, bWithItemAmount))
		{
			AddItemAt(InventoryItemObject, i, bWithItemAmount);
			return true;
		}
	}

	InventoryItemObject->ServerRotate();
	
	return false;
}

bool UInventoryComponent::TryAddItemAt(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount)
{
	if (!InventoryItemObject || GetOwnerRole() != ROLE_Authority)
		return false;
	
	if (IsRoomAvailable(InventoryItemObject, TopLeftIndex, bWithItemAmount))
	{
		AddItemAt(InventoryItemObject, TopLeftIndex, bWithItemAmount);
		return true;
	}

	/*InventoryItemObject->ServerRotate();

	if (IsRoomAvailable(InventoryItemObject, TopLeftIndex))
	{
		AddItemAt(InventoryItemObject, TopLeftIndex);
		return true;
	}

	InventoryItemObject->ServerRotate();*/
	
	return false;
}

void UInventoryComponent::RemoveItem(AInventoryItemObject* InventoryItemObject, ERemoveItemOption RemoveItemOption)
{
	RemoveItemInternal(InventoryItemObject, RemoveItemOption);
	OnRep_Items();
}

bool UInventoryComponent::IsRoomAvailable(AInventoryItemObject* InventoryItemObject, int TopLeftIndex, bool bWithItemAmount)
{	
	if (!InventoryItemObject)
		return false;

	auto AdditionalCondition = [&](int Index)->bool
	{
		bool bRoomAvailable;
		
		if(bWithItemAmount)
		{
			bRoomAvailable = CheckOnConsideringAndEqualityLambda(Index, InventoryItemObject);
		}
		else
		{
			bRoomAvailable = CheckOnConsideringItemLambda(Index);
		}

		return bRoomAvailable;
	};
	
	auto DimensionsOfInventoryItemObject = InventoryItemObject->GetDimensions();
	
	return IsRoomAvailableInternal(DimensionsOfInventoryItemObject.X, DimensionsOfInventoryItemObject.Y, TopLeftIndex, AdditionalCondition);
}

bool UInventoryComponent::IsRoomAvailableInternal(int SizeX, int SizeY, int TopLeftIndex, std::function<bool(int)> AdditionalCondition)
{
	auto TileOfTopLeftIndex = IndexToTile(TopLeftIndex);

	int EndOfRoom_X = TileOfTopLeftIndex.X + SizeX;
	int EndOfRoom_Y = TileOfTopLeftIndex.Y + SizeY;

	for (int i = TileOfTopLeftIndex.X; i < EndOfRoom_X; ++i)
	{
		for (int j = TileOfTopLeftIndex.Y; j < EndOfRoom_Y; ++j)
		{
			FTile CurrentTile = { i, j };

			if (CurrentTile.X < 0 || CurrentTile.X >= Columns || CurrentTile.Y < 0 || CurrentTile.Y >= Rows)
			{
				return false;
			}

			int CurrentIndex = TileToIndex(CurrentTile);

			if (!Items.IsValidIndex(CurrentIndex))
			{
				return false;
			}

			if(AdditionalCondition)
			{
				if(!AdditionalCondition(CurrentIndex))
				{
					return false;
				}
			}
		}
	}

	return true;
}

bool UInventoryComponent::IsRoomAvailableInternal(int SizeX, int SizeY, int TopLeftIndex,
	std::function<bool(UInventoryComponent*, int)> AdditionalCondition)
{
	auto Lambda = [&](int Index)->bool
	{
		if (AdditionalCondition)
		{
			if (!AdditionalCondition(this, Index))
			{
				return false;
			}
		}

		return true;
	};
	
	return IsRoomAvailableInternal(SizeX, SizeY, TopLeftIndex, Lambda);
}

bool UInventoryComponent::CheckOnConsideringItemLambda(int Index)
{
	if (Items[Index])
	{
		return false;
	}

	return true;
}

bool UInventoryComponent::CheckOnConsideringAndEqualityLambda(int Index, AInventoryItemObject* InventoryItemObject)
{	
	if (auto Item = Items[Index])
	{
		if (InventoryItemObject)
		{
			if (Item->GetClass() == InventoryItemObject->GetClass())
			{
				int MaxAmount = Item->GetMaxItemAmount();
				int CurrentAmount = Item->GetCurrentItemAmount();
				int AnotherItemCurrentAmount = InventoryItemObject->GetCurrentItemAmount();

				if (CurrentAmount + AnotherItemCurrentAmount <= MaxAmount)
				{
					return true;
				}
			}
		}
		
		return false;
	}

	return true;
}

bool UInventoryComponent::IsAttachmentAvailable(int Index, AInventoryItemObject* InventoryItemObject)
{
	if (!InventoryItemObject)
		return false;

	if (Items.IsValidIndex(Index))
	{
		if (auto Item = Items[Index])
		{
			if (auto ModuleItemObjectsManager = Item->FindComponentByClass<UModuleItemObjectsManager>())
			{
				if (ModuleItemObjectsManager->CanModuleAttached(InventoryItemObject))
				{
					return true;
				}
			}
		}
	}

	return false;
}

void UInventoryComponent::UpdateOnCurrentItemAmount()
{
	UpdateCommonItemsWeight();
}

void UInventoryComponent::UpdateOnAttachModule()
{
	UpdateCommonItemsWeight();
}

void UInventoryComponent::UpdateCommonItemsWeight()
{
	if (GetOwnerRole() != ROLE_Authority)
		return;
	
	auto ItemsLocal = GetAllItems();

	float CommonItemsWeightLocal = 0.0f;
	
	for(auto ItemsIterator = ItemsLocal.begin(); ItemsIterator != ItemsLocal.end(); ++ItemsIterator)
	{
		if(auto ItemObject = ItemsIterator->Key)
		{
			CommonItemsWeightLocal += ItemObject->GetCurrentItemWeight();
		}
	}

	CommonItemsWeight = CommonItemsWeightLocal;

	OnRep_CommonItemsWeight();
}

FTile UInventoryComponent::IndexToTile(int Index)
{
	FTile Tile;

	Tile.X = Index % Columns;
	Tile.Y = Index / Columns;
	
	return Tile;
}

int UInventoryComponent::TileToIndex(FTile Tile)
{
	return Tile.X + Tile.Y * Columns;
}

TMap<AInventoryItemObject*, FTile> UInventoryComponent::GetAllItems()
{
	TMap<AInventoryItemObject*, FTile> ItemsMap;

	for(int i = 0; i < Items.Num(); ++i)
	{
		if(Items[i])
		{
			if(!ItemsMap.Contains(Items[i]))
			{
				ItemsMap.Add(Items[i], IndexToTile(i));
			}
		}
	}

	return ItemsMap;
}

bool UInventoryComponent::Contains(AInventoryItemObject* InventoryItemObject)
{
	if(!InventoryItemObject)
		return false;

	for (int i = 0; i < Items.Num(); ++i)
	{
		if(Items[i] == InventoryItemObject)
		{
			return true;
		}
	}

	return false;
}

int UInventoryComponent::IndexOfItem(AInventoryItemObject* InventoryItemObject)
{
	if (!InventoryItemObject)
		return -1;

	for (int i = 0; i < Items.Num(); ++i)
	{
		if (Items[i] == InventoryItemObject)
		{
			return i;
		}
	}

	return -1;
}

float UInventoryComponent::GetCommonItemsWeight() const
{
	return CommonItemsWeight;
}

FDMD& UInventoryComponent::GetInventoryChangedDispatcher()
{
	return InventoryChangedDispatcher;
}

FDMD& UInventoryComponent::GetCommonItemsWeightDispatcher()
{
	return CommonItemsWeightDispatcher;
}

FDMD& UInventoryComponent::GetInventoryOpenedDispatcher()
{
	return InventoryOpenedDispatcher;
}

FDMD_PC& UInventoryComponent::GetInventoryOpened_PC_Dispatcher()
{
	return InventoryOpened_PC_Dispatcher;
}

FDMD& UInventoryComponent::GetInventoryClosedDispathcer()
{
	return InventoryClosedDispathcer;
}

FDMD_PC& UInventoryComponent::GetInventoryClosed_PC_Dispatcher()
{
	return InventoryClosed_PC_Dispatcher;
}

