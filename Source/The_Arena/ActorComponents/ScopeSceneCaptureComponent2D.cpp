// Fill out your copyright notice in the Description page of Project Settings.


#include "ScopeSceneCaptureComponent2D.h"

const AActor* UScopeSceneCaptureComponent2D::GetViewOwner() const
{
	for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		APlayerController* PlayerController = Iterator->Get();
		if (PlayerController && PlayerController->IsLocalPlayerController())
		{
			return PlayerController;
		}
	}

	return nullptr;
}
