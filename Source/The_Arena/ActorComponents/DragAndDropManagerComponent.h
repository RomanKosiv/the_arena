// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "The_Arena/Actors/InventoryItemObject.h"

#include "DragAndDropManagerComponent.generated.h"

class UInventoryBaseComponent;
class UInventoryComponent;
class UCustomInventoryComponent;
class UEquipmentComponent;
class UTraderBasketComponent;
class UPlayerBasketComponent;

USTRUCT()
struct FDraggedItem
{
	GENERATED_BODY()

	UPROPERTY()
	AInventoryItemObject* InventoryItemObject;
	UPROPERTY()
	UInventoryBaseComponent* InventoryBaseComponent;
	UPROPERTY()
	int TileIndex;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UDragAndDropManagerComponent : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:	
	// Sets default values for this component's properties
	UDragAndDropManagerComponent();

	FDraggedItem GetDraggedItem() const;
	void SetDraggedItem(FDraggedItem InDraggedItem);
	void SetDraggedItem(AInventoryItemObject* InventoryItemObject, UInventoryBaseComponent* InventoryBaseComponent, int TileIndex = 0);

	void DestroyDraggedItem();
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerDestroyDraggedItemWithSpawn();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerSpawnItemsWithSplitting(int ItemAmount);
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerRotateDraggedItem();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerDragItem(AInventoryItemObject* InventoryItemObject, UInventoryBaseComponent* InventoryBaseComponent);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerDropItem(UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerDropItemAt(int TileIndex, UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerDropItemBack(bool bTryAddItemToPlayerInventory = true);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerDropItemsWithSplitting(int SplitNumber, int TileIndex, UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerAttachDraggedItem(AInventoryItemObject* ModulableItemObject, UInventoryBaseComponent* InventoryBaseComponent);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerDetachWithDropInPlayerInventory(AInventoryItemObject* ModulableItemObject, UInventoryBaseComponent* InventoryBaseComponent, AInventoryItemObject* ModuleItemObject);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerUse(AInventoryItemObject* InventoryItemObject, AActor* UsedActor);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerBuy(UTraderBasketComponent* BasketComponent);
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerSell(UPlayerBasketComponent* BasketComponent);
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsUsable(UInventoryBaseComponent* InventoryBaseComponent);

	UFUNCTION(BlueprintCallable)
	APawn* GetControlledPawn() const;
	UFUNCTION(BlueprintCallable)
	UCustomInventoryComponent* GetControlledPawnInventoryComponent() const;
	
protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	void DropItem(UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount);
	void DropItemAt(int TopLeftIndex, UInventoryBaseComponent* InventoryBaseComponent, bool bWithItemAmount);

	AInventoryItemObject* SplitDraggedItem(int SplitNumber);
	
	//Fields
protected:
	UPROPERTY()
	FDraggedItem DraggedItem;		
};