// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/TransformController.h"

#include "CharacterTransformController.generated.h"


class AFirstPersonCharacter;

UCLASS(ClassGroup = (Custom), Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UCharacterTransformController : public UTransformController
{
	GENERATED_BODY()
	//Methods
public:
	UCharacterTransformController();

	UFUNCTION(BlueprintCallable)
		AFirstPersonCharacter* GetFPCharacter() const;
	UFUNCTION(BlueprintCallable)
		void SetFPCharacter(AFirstPersonCharacter* Character);

	void RotateSceneComponent(float DeltaTime, FSceneCompRotParam& Param, float& PassAngle, FRotator direction) override;
	//Fields
protected:
	AFirstPersonCharacter* FPCharacter = nullptr;
};
