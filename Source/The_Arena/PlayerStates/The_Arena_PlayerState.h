// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"
#include "The_Arena/UObjects/DefaultDelegates.h"

#include "The_Arena_PlayerState.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AThe_Arena_PlayerState : public APlayerState
{
	GENERATED_BODY()

	//Methods
protected:
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	void CopyProperties(APlayerState* PlayerState) override;

	UFUNCTION()
	void OnRep_CurrentTeam();
	
public:
	UFUNCTION(BlueprintCallable)
	void SetCurrentTeam(EGameTeam Team);
	UFUNCTION(BlueprintCallable)
	EGameTeam GetCurrentTeam() const;
	UFUNCTION(BlueprintCallable)
	FName GetCurrentTeamName() const;

	FDMD& GetCurrentTeamDispatcher();
	
	//Fields
protected:
	UPROPERTY(ReplicatedUsing = OnRep_CurrentTeam, BlueprintReadWrite)
	EGameTeam CurrentTeam = EGameTeam::Spectators;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD CurrentTeamDispatcher;
};
